<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|    example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|    https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|    $route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|    $route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|    $route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:    my-controller/index    -> my_controller/index
|        my-controller/my-method    -> my_controller/my_method
 */

$route['tabor-jelentkezes/(:num)/(:num)'] = 'main/campjoin/$2/$3';
$route['tabor-ertekeles']                 = 'main/camprating/';
$route['tabor/(:any)/(:any)']             = 'main/campdetails/$2/$3';
$route['tabor/(:num)']                    = 'main/campdetails/$2';
$route['taborok/(:any).html']             = 'main/campdetails/old/$2';
$route['taborok/(:any)']                  = 'main/camps_search';
$route['taborok']                         = 'main/camps';
$route['terkepes-taborkereses']           = 'main/camps_map';

$route['taborszervezoknek']       = 'main/createcamp_welcome';
$route['taborszerzodes/elonezet'] = 'camp/camp_contract_preview';

$route['kilepes']                            = 'camp/logout';
$route['belepes/(:num)/(:any)']              = 'camp/login';
$route['belepes']                            = 'camp/login';
$route['taborszerzodes']                     = 'camp/createcamp_details';
$route['taborhelyek']                        = 'camp/camp_locations';
$route['taborhely/(:num)']                   = 'camp/camp_location_edit';
$route['taborkepek/(:num)']                  = 'camp/camp_pictures';
$route['taborturnusok/(:num)/(:any)/(:num)'] = 'camp/camp_turns';
$route['taborturnusok/(:num)']               = 'camp/camp_turns';

//newsletter
$route["hirlevel/autosend/(:any)"] = "backend/newsletter/letters/autosend";
$route["hirlevel/tracking"]        = "newsletter/tracking";
$route["hirlevel/megerosites"]     = "newsletter/confirm";
$route["hirlevel/leiratkozas"]     = "newsletter/unsubscribe";
$route["hirlevel/feliratkozas"]    = "newsletter/subscribe";
$route["hirlevel/online/(:any)"]   = "newsletter/preview/";

$route['impresszum']  = 'main/impresszum';
$route['rolunk']      = 'main/about';
$route['kapcsolat']   = 'main/contact';
$route['adatkezeles'] = 'main/gdpr';

$route['leirasok/(:num)/(:any)']       = 'main/news/rejtett/$2';
$route['leiras/(:num)/(:any)']         = 'main/news/rejtett/$2';
$route['hireink/(:any)/(:num)/(:any)'] = 'main/news/$1/$2';
$route['hireink/(:any)/(:any)']        = 'main/news/$1/$2';
$route['hireink/(:any)']               = 'main/news/$1';
$route['hireink']                      = 'main/news/';

$route['backend/login']  = 'backend/main/login';
$route['backend/logout'] = 'backend/main/logout';
$route['backend/']       = 'backend/main/';

$route['default_controller']   = 'main';
$route['404_override']         = 'page404';
$route['translate_uri_dashes'] = false;
