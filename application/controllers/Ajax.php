<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Ajax extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $CI = &get_instance();
        if (!$CI->input->is_ajax_request()) {
            die("Access denied.");
        }
    }

    public function statelist()
    {
        $code = (int) $this->input->post_get("q", true);

        $tmp = $this->db->select("id, name")->from("tf_camp_states")->where("countryID", $code)->order_by("name", "ASC")->get()->result_array();
        if ($tmp) {
            echo json_encode(array("status" => "ok", "data" => $tmp));
        } else {
            echo json_encode(array("status" => "error"));
        }
    }

    public function locationList()
    {
        $code = (int) $this->input->post_get("q", true);

        $tmp = $this->db->select("id, name")->from("tf_camp_locations")->where("stateID", $code)->order_by("name", "ASC")->get()->result_array();
        if ($tmp) {
            echo json_encode(array("status" => "ok", "data" => $tmp));
        } else {
            echo json_encode(array("status" => "error"));
        }
    }

    public function zip2city()
    {
        $code = (int) $this->input->post_get("q", true);

        $tmp = $this->db->from("cities")->where("zip", $code)->get()->row();
        if ($tmp) {
            echo json_encode(array("status" => "ok", "data" => $tmp));
        } else {
            echo json_encode(array("status" => "error"));
        }
    }

    public function camp_imagelist()
    {
        $contractID  = (int) $this->session->userdata("user_contractID");
        $contractID2 = (int) $this->input->post("contractID");
        $contract    = $this->db->from("tf_camp_contracts")->where("id", $contractID, "campEditDone is NULL")->get()->row();

        if (!$contract || ($contractID !== $contractID2)) {
            echo json_encode(array("status" => "error", "message" => "Hibás szerződés adat."));
        } else {
            $locationID = (int) $this->input->post("campID");
            $camp       = $this->db->from("tf_camps")->where("id", (int) $locationID)->where("contractID", (int) $contractID)->where("closed", 0)->get()->row();
            if (!$camp && !$this->session->userdata("user_isAdmin")) {
                echo json_encode(array("status" => "error", "message" => "A táborhelyszín nem létezik, vagy le van zárva!"));
            } else {
                $photos = [];
                $result = $this->db->from("tf_camp_images")->where("campID", (int) $locationID)->order_by("ordering, id")->get()->result();
                foreach ($result as $id => $photo) {
                    $photo->dimension = getimagesize(FCPATH . $photo->path);
                    $photos[]         = $photo;
                }
                echo json_encode(array("status" => "ok", "data" => $photos));
            }
        }
    }

    public function camp_imagelist_modify()
    {
        $contractID  = (int) $this->session->userdata("user_contractID");
        $contractID2 = (int) $this->input->post("contractID");
        $contract    = $this->db->from("tf_camp_contracts")->where("id", $contractID, "campEditDone is NULL")->get()->row();

        if (!$contract || ($contractID !== $contractID2)) {
            echo json_encode(array("status" => "error", "message" => "Hibás szerződés adat."));
        } else {
            $locationID = (int) $this->input->post("campID");
            $camp       = $this->db->from("tf_camps")->where("id", (int) $locationID)->where("contractID", (int) $contractID)->where("closed", 0)->get()->row();
            if (!$camp && !$this->session->userdata("user_isAdmin")) {
                echo json_encode(array("status" => "error", "message" => "A táborhelyszín nem létezik, vagy le van zárva!"));
            } else {
                $ids    = $this->input->post("id");
                $names  = $this->input->post("name");
                $orders = $this->input->post("ordering");
                $pubs   = $this->input->post("published");
                foreach ($ids as $id => $tmp) {
                    $data = array(
                        "name"      => $names[$id],
                        "ordering"  => $orders[$id],
                        "published" => ($pubs[$id] == "on") ? 1 : 0,
                    );
                    $this->db->update("tf_camp_images", $data, array("id" => $id));
                }

                //reorder images
                $this->db->update("tf_camp_images", array("ordering" => "999"), array("id" => $data->id, "ordering" => 0));
                $photos = $this->db->from("tf_camp_images")->where("campID", (int) $locationID)->order_by("ordering, id")->get()->result();
                $poz    = 1;
                foreach ($photos as $data) {
                    $this->db->update("tf_camp_images", array("ordering" => $poz++), array("id" => $data->id));
                }
                echo json_encode(array("status" => "ok", "data" => $photos));
            }
        }
    }

    public function camp_image_rotate()
    {
        $contractID  = (int) $this->session->userdata("user_contractID");
        $contractID2 = (int) $this->input->post("contractID");
        $contract    = $this->db->from("tf_camp_contracts")->where("id", $contractID, "campEditDone is NULL")->get()->row();
        $dirLeft     = (strtoupper($this->input->post("dir")) == "LEFT");

        if (!$contract || ($contractID !== $contractID2)) {
            echo json_encode(array("status" => "error", "message" => "Hibás szerződés adat."));
        } else {
            $locationID = (int) $this->input->post("campID");
            $camp       = $this->db->from("tf_camps")->where("id", (int) $locationID)->where("contractID", (int) $contractID)->where("closed", 0)->get()->row();

            $picID = (int) $this->input->post("picID");

            $pic = $this->db->from("tf_camp_images")->where("campID", (int) $locationID)->where("id", $picID)->get()->row();           
            if ($pic) {
                $fn = [FCPATH. $pic->path, FCPATH. str_replace("properties/images", "properties/thumbs", $pic->path)];
                foreach ($fn as $file) {
                    if ($pic->type == "image/jpeg") {
                        $image = imagecreatefromjpeg($file);
                    } else {
                        $image = imagecreatefrompng($file);
                    }
                    if ($image) {
                        $rotated = imagerotate($image, ($dirLeft)?90:-90, 0);
                        if ($pic->type == "image/jpeg") {
                            $retr = imagejpeg($rotated, $file, 95); 
                        } else {
                            $retr =imagepng($rotated, $file, 95);  
                        }
                    } else {
                        var_dump($file);
                    }              
                }

                echo json_encode(array("status" => "ok"));
            } else {
                echo json_encode(array("status" => "error", "message" => "Nincs ilyen kép!"));
            }
        }
    }

    public function camp_image_delete()
    {
        $contractID  = (int) $this->session->userdata("user_contractID");
        $contractID2 = (int) $this->input->post("contractID");
        $contract    = $this->db->from("tf_camp_contracts")->where("id", $contractID, "campEditDone is NULL")->get()->row();

        if (!$contract || ($contractID !== $contractID2)) {
            echo json_encode(array("status" => "error", "message" => "Hibás szerződés adat."));
        } else {
            $locationID = (int) $this->input->post("campID");
            $camp       = $this->db->from("tf_camps")->where("id", (int) $locationID)->where("contractID", (int) $contractID)->where("closed", 0)->get()->row();
            if (!$camp && !$this->session->userdata("user_isAdmin")) {
                echo json_encode(array("status" => "error", "message" => "A táborhelyszín nem létezik, vagy le van zárva!"));
            } else {
                $picID = (int) $this->input->post("picID");

                $pic = $this->db->from("tf_camp_images")->where("campID", (int) $locationID)->where("id", $picID)->get()->row();
                if ($pic) {
                    @unlink(FCPATH . $pic->path);
                    @unlink(FCPATH . str_replace("ties/images", "ties/thumbs", $pic->path));

                    $this->db->delete("tf_camp_images", array("id" => $picID));
                    echo json_encode(array("status" => "ok"));
                } else {
                    echo json_encode(array("status" => "error", "message" => "Nincs ilyen kép!"));
                }
            }
        }
    }

    public function camp_upload_image()
    {
        $contractID  = (int) $this->session->userdata("user_contractID");
        $contractID2 = (int) $this->input->post("contractID");
        $contract    = $this->db->from("tf_camp_contracts")->where("id", $contractID, "campEditDone is NULL")->get()->row();

        if (!$contract || ($contractID !== $contractID2)) {
            echo json_encode(array("status" => "error", "message" => "Hibás szerződés adat."));
        } else {
            $locationID = (int) $this->input->post("campID");
            $camp       = $this->db->from("tf_camps")->where("id", (int) $locationID)->where("contractID", (int) $contractID)->where("closed", 0)->get()->row();
            if (!$camp && !$this->session->userdata("user_isAdmin")) {
                echo json_encode(array("status" => "error", "message" => "A táborhelyszín nem létezik, vagy le van zárva!"));
            } else {
                if (is_array($_FILES)) {
                    if (in_array($_FILES['userImage']['type'], array('image/jpeg', 'image/png'))) {
                        if (is_uploaded_file($_FILES['userImage']['tmp_name'])) {
                            $sourcePath = $_FILES['userImage']['tmp_name'];
                            $info       = getimagesize($sourcePath);
                            $extension  = image_type_to_extension($info[2]);

                            $data = array(
                                "name"       => $_FILES['userImage']['name'],
                                "published"  => 1,
                                "ordering"   => 99,
                                "type"       => image_type_to_mime_type($info[2]),
                                "modifyDate" => date("Y-m-d H:i:s"),
                                "campID"     => $locationID,
                            );
                            $this->db->insert("tf_camp_images", $data);
                            $picID = $this->db->insert_id();

                            $targetPath = "images/properties/images/" . $locationID . "/" . $picID . "_" . $locationID . $extension;
                            $thumbPath  = "images/properties/thumbs/" . $locationID . "/" . $picID . "_" . $locationID . $extension;

                            $this->db->update("tf_camp_images", array("path" => $targetPath), array("id" => $picID));

                            $photos = $this->db->from("tf_camp_images")->where("campID", (int) $locationID)->order_by("ordering, id")->get()->result();
                            $poz    = 1;
                            foreach ($photos as $data) {
                                $this->db->update("tf_camp_images", array("ordering" => $poz++), array("id" => $data->id));
                            }

                            mkdir(FCPATH . "images/properties/images/" . $locationID);
                            mkdir(FCPATH . "images/properties/thumbs/" . $locationID);

                            if (move_uploaded_file($sourcePath, FCPATH . $targetPath)) {
                                $this->tools->ResizePic(FCPATH . $targetPath, FCPATH . $targetPath, 1200, 900);
                                $this->tools->ResizePic(FCPATH . $targetPath, FCPATH . $thumbPath);
                                echo json_encode(array("status" => "ok", "message" => "A képeket feltöltöttük! " . $_FILES['userImage']['name']));
                            } else {
                                echo json_encode(array("status" => "error", "message" => "Hiba a kép mozgatásakor! " . $_FILES['userImage']['name']));
                                $this->db->delete("tf_camp_images", array("id" => $picID));
                            }
                        } else {
                            echo json_encode(array("status" => "error", "message" => "Hiba a kép feltöltésekor! " . $_FILES['userImage']['name']));
                        }
                    } else {
                        echo json_encode(array("status" => "error", "message" => "Nem megengedett képformátum!" . $_FILES['userImage']['type']));
                    }
                    unlink($_FILES['userImage']['tmp_name']);
                }

            }
        }
    }

    public function getCampMap()
    {
        $this->db->select("c.id, c.name, c.alias, c.description, c.lat, c.lng, t.name as typeTitle, l.name as locationTitle, s.name as stateTitle, co.name as countryTitle,
        (SELECT path FROM tf_camp_images WHERE campID=c.id AND published=1 ORDER BY ordering LIMIT 1) as imagePath");
        $this->db->from("tf_camps as c");
        $this->db->join("tf_camp_types as t", "t.id=c.typeID", "LEFT");
        $this->db->join("tf_camp_locations as l", "l.id=c.locationID", "LEFT");
        $this->db->join("tf_camp_states as s", "s.id=c.stateID", "LEFT");
        $this->db->join("tf_camp_countries as co", "co.id=s.countryID", "LEFT");
        $this->db->where("c.published", 1);
        $this->db->where("c.lat <>", 0);
        $this->db->where("c.lng <>", 0);

        $result = $this->db->get()->result();

        echo json_encode(array("data" => $result));
    }

    public function sendMessage()
    {
        if ($this->input->is_ajax_request()) {
            $email = filter_input(INPUT_POST, "email", FILTER_VALIDATE_EMAIL);
            if (!$email) {
                echo json_encode(array("status" => "error", "message" => "Kérem ellenőrize le az e-mail címét!"));
                return;
            }

            $name = $this->input->post("name", true);
            if (!$name) {
                echo json_encode(array("status" => "error", "message" => "Kérem adja meg a nevét!"));
                return;
            }

            $phone = $this->input->post("phone", true);
            if (!$phone) {
                echo json_encode(array("status" => "error", "message" => "Kérem adja meg a telefonszámát!"));
                return;
            }

            $message = strip_tags($this->input->post("message", true));
            if (!$message || strlen($message) < 20) {
                echo json_encode(array("status" => "error", "message" => "Az üzenete túl rövid, kérjük, fejtse ki valamivel bővebben! Köszönjük!"));
                return;
            }

            $toEmail  = $this->tools->GetSetting("contact-email");
            $campname = strip_tags($this->input->post("campname", true));
            $subject  = "Weboldali üzenet érkezett";

            $body = "<h3>Adatok</h3>\n"
            . "<label>Feladó:</label> " . $name . " <br>\n"
            . "<label>E-mail:</label> " . $email . " <br>\n"
            . "<label>Telefon:</label> " . $phone . " <br>\n"
            . "<label>Tábor neve:</label> " . $campname . " <br>\n"
            . "<label>Üzenet:</label><br>" . $message . " <br>\n"
            . "<br>\n"
            . "Érkezett: " . date("Y-m-d H:i:s") . " - IP: " . $this->tools->getIP() . "<br>\n";

            if ($this->tools->SendHtmlMail($toEmail, $subject, $body, array("reply_to" => $email))) {
                echo json_encode(array("status" => "ok"));
            } else {
                echo json_encode(array("status" => "error", "message" => "Hiba az üzenet küldésekor"));
            }
        }
    }

    public function checkMatch()
    {
        $taxID  = $this->input->post("taxID", true);
        $compID = $this->input->post("compID", true);

        if ((strlen($taxID . $compID) > 10)) {

            $this->db->select("c.id, concat(c.name, ' (~', YEAR(c.lastModify),')') as name");
            $this->db->from("tf_camps as c");
            $this->db->join("tf_camp_contracts as co", "co.id=c.contractID");
            $this->db->where("LEFT(taxID,8)", substr($taxID, 0, 8));
            $this->db->or_where("compID", $compID);
            $this->db->order_by("LEFT(c.lastModify,4) DESC, name ASC");
            $result = $this->db->get()->result();

            echo json_encode(array("status" => "OK", "data" => $result));
        } else {
            echo json_encode(array("status" => "error"));
        }
    }

}
