<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Camp extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->uri->segment(1) !== "belepes" && !$this->session->userdata("user_contractID")) {
            redirect("belepes");
        }
    }

    public function camp_locations()
    {
        $contractID = (int) $this->session->userdata("user_contractID");
        $camps      = $this->db->select("id")->from("tf_camps")->where("contractID", (int) $contractID)->get()->result();
        if (count($camps) == 0) {
            redirect("taborszerzodes");
        }

        $this->load->view("frontpage/header");

        $contract = $this->db->from("tf_camp_contracts")->where("id", $contractID, "contractSaved is NOT NULL")->get()->row();
        if (!$contract) {
            message_o("Hiba", "Az ön álatal megnyitott oldal cím hibás.", "warning");
        } else {
            if ((is_Null($contract->campEditDone)) && ($this->input->post("saveLocations") == "ok")) {
                $this->db->update("tf_camp_contracts", array("campEditDone" => date("Y-m-d H:i:s")), array("id" => $contractID));
                $this->db->update("tf_camps", array("published" => 1, "closed" => 1), array("contractID" => $contractID));
                message_o("", "A táborhelyszín adatok sikeresen zárolva lettek.<br><br>Jóváhagyást követően megjelenik a tábor a listákban, melyről e-mailes értesítést küldünk!", "success");

                $arr = array(
                    "CONTRACT"    => $contract,
                    "NAME"        => $contract->contactName,
                    "PARTNERNAME" => $contract->partnerName,
                );

                $body = "<table>";
                foreach ($contract as $key => $data) {
                    $body .= "<tr><td>" . $key . "</td><td>" . $data . "</td></tr>\n";
                }
                $body = "</table>";
                $this->tools->SendHtmlMail($this->tools->GetSetting("email-from-address"), "Táborhely adatok lezárva", $body);
            } else {
                $this->load->view("frontpage/camp-locations");
            }
        }
        $this->load->view("frontpage/footer");
    }

    public function camp_location_edit()
    {
        $contractID = (int) $this->session->userdata("user_contractID");
        $camps      = $this->db->select("id")->from("tf_camps")->where("contractID", (int) $contractID)->get()->result();
        if (count($camps) == 0) {
            redirect("taborszerzodes");
        }
        $this->load->view("frontpage/header");

        $id = $this->session->userdata("user_contractID");
        //$contract = $this->db->from("tf_camp_contracts")->where("id", (int) $id)->get()->row();
        $camp = $this->db->from("tf_camps")->where("contractID", (int) $id)->get()->row();
        if (!$camp) {
            message_o("Hiba", "Az ön álatal megnyitott oldal cím hibás.", "warning");
        } else {
            if ($this->input->post("saveLocation")) {
                $re = '/(|(http|https):\/\/)([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:\/~+#-]*[\w@?^=%&\/~+#-])?/m';

                $description = preg_replace($re, "", $this->input->post("description"));
                $text        = preg_replace($re, "", $this->input->post("text"));
                $program     = preg_replace($re, "", $this->input->post("program"));

                $data = array(
                    "name"            => $this->input->post("name"),
                    "minositoID"      => $this->input->post("minositoID"),
                    "categoryID"      => (int) $this->input->post("categoryID"),
                    "typeID"          => (int) $this->input->post("typeID"),
                    "locationID"      => (int) $this->input->post("locationID"),
                    "location_str"    => $this->input->post("location", true),
                    "stateID"         => (int) $this->input->post("stateID"),
                    "state_str"       => $this->input->post("state", true),
                    "countryID"       => (int) $this->input->post("countryID"),
                    "country_str"     => $this->input->post("country", true),
                    "zip"             => (int) $this->input->post("zip"),
                    "city"            => $this->input->post("city", true),
                    "address"         => $this->input->post("address", true),
                    "description"     => $description,
                    "text"            => $text,
                    "program"         => $program,
                    "price"           => (int) $this->input->post("price"),
                    "szep_MKB"        => (int) $this->input->post("szep_MKB"),
                    "szep_OTP"        => (int) $this->input->post("szep_OTP"),
                    "szep_KH"         => (int) $this->input->post("szep_KH"),
                    "age_min"         => (int) $this->input->post("age_min"),
                    "age_max"         => (int) $this->input->post("age_max"),
                    "contactName"     => filter_var($this->input->post("contactName"), FILTER_SANITIZE_STRING),
                    "contactPhone"    => filter_var($this->input->post("contactPhone"), FILTER_SANITIZE_STRING),
                    "contactEmail"    => filter_var($this->input->post("contactEmail"), FILTER_SANITIZE_EMAIL),
                    "contactWebsite"  => filter_var($this->input->post("contactWebsite"), FILTER_SANITIZE_URL),
                    "contactFacebook" => filter_var($this->input->post("contactFacebook"), FILTER_SANITIZE_URL),
                    "lastModify"      => date("Y-m-d H:i:s"),
                );

                if ($data["countryID"] <= 0) {
                    $tmp = $this->db->from("tf_camp_countries")->like("name", $data["country_str"])->get()->row();
                    if ($tmp) {
                        $data["countryID"] = $tmp->id;
                    }
                }

                if ($data["stateID"] <= 0) {
                    if ($data["countryID"] > 0) {
                        $tmp = $this->db->from("tf_camp_states")->like("name", $data["state_str"])->where("countryID", $data["countryID"])->get()->row();
                    } else {
                        $tmp = $this->db->from("tf_camp_states")->like("name", $data["state_str"])->get()->row();
                    }
                    if ($tmp) {
                        $data["stateID"] = $tmp->id;
                    }
                }
                if ($data["locationID"] <= 0) {
                    if ($data["stateID"] > 0) {
                        $tmp = $this->db->from("tf_camp_locations")->like("name", $data["location_str"])->where("stateID", $data["stateID"])->get()->row();
                    } else {
                        $tmp = $this->db->from("tf_camp_locations")->like("name", $data["location_str"])->get()->row();
                    }
                    if ($tmp) {
                        $data["locationID"] = $tmp->id;
                    }
                }

                if (empty($camp->alias)) {
                    $data["alias"] = $this->tools->toSlug($this->input->post("name"));
                }
                $this->db->update("tf_camps", $data, array("id" => $this->input->post("locID"), "closed" => 0));

                $this->db->delete("tf_camp_categories", array("campID" => $this->input->post("locID")));
                $cats = $this->input->post("categories");
                foreach ($cats as $tmp => $cat) {
                    $this->db->insert("tf_camp_categories", array("categoryID" => $cat, "campID" => $this->input->post("locID")));
                }
                message_o("", "A módosításokat mentettük<br><br><a href='" . site_url("taborhelyek") . "' class='btn btn-default'>Vissza a táborhelyekhez</a>", "success");
            } else {
                $this->load->view("frontpage/camp-location-edit");
            }
        }
        $this->load->view("frontpage/footer");
    }

    public function createcamp_details()
    {
        $this->load->view("frontpage/header");
        $this->load->model("m_fcamp");

        $contractID = (int) $this->session->userdata("user_contractID");
        $contract   = $this->db->from("tf_camp_contracts")->where("id", (int) $contractID)->get()->row();
        if (!$contract) {
            message_o("Hiba", "Az ön álatal megnyitott oldal cím hibás.", "warning");
            $this->load->view("frontpage/footer");
            return;
        }

        if (!is_null($contract->contractSent)) {
            $this->load->view("frontpage/camp-contract");
        } else {

            if ($this->input->post("startReg")) {
                $loc_price   = $this->tools->GetSetting("camp-baseprice");
                $theme_price = $this->tools->GetSetting("camp-themeprice");

                $contractID = $contract->id;

                $locations  = $this->input->post("loc_type");
                $loc_themes = $this->input->post("loc_themes");
                $themes     = 0;
                foreach ($loc_themes as $loc) {
                    $themes = $themes + (int) $loc;
                }

                $total = round(count($locations) * $loc_price);
                $total = $total + round(($themes - count($locations)) * $theme_price);

                $data = array(
                    "campLocations"    => (int) count($locations),
                    "campThemes"       => (int) $themes,
                    "campPrice"        => (int) $total,

                    "contactName"      => $this->input->post("contactName"),
                    "contactEmail"     => $this->input->post("contactEmail"),
                    "contactPhone"     => $this->input->post("contactPhone"),

                    "representName"    => $this->input->post("representName"),
                    "partnerName"      => $this->input->post("partnerName"),
                    "zip"              => (int) $this->input->post("zip"),
                    "city"             => $this->input->post("city"),
                    "address"          => $this->input->post("address"),
                    "taxID"            => $this->input->post("taxID"),
                    "compID"           => $this->input->post("compID"),

                    "lastModify"       => date("Y-m-d H:i:s"),
                    "contractSaved"    => date("Y-m-d H:i:s"),

                    //update reset
                    "contractSent"     => null,
                    "paymentEmailSent" => null,
                    "contractPaid"     => null,
                    "campEditSent"     => null,
                    "campEditDone"     => null,
                    "publicationTime"  => null,
                    "visible"          => 0,
                    "closed"           => 0
                );

                if ($this->db->update("tf_camp_contracts", $data, array("id" => $contractID, "contractSaved" => null))) {
                    $contract = $data;

                    foreach ($locations as $id => $title) {
                        if ($title == "new") {
                            $name = "Új táborhely " . $id;
                            $data = array(
                                "name"       => $name,
                                "contractID" => $contractID,
                                "themes"     => $loc_themes[$id],
                                "closed"     => 0,
                                "published"  => 0,
                            );
                            $this->db->insert("tf_camps", $data);
                        } else {
                            $tmp = $this->db->from("tf_camps")->where("id", (int) $title)->get()->row();
                            $this->db->insert("tf_camps_history", array("campID" => $tmp->id, "contractID" => $contractID, "datetime" => date("Y-m-d H:i:s")));
                            $this->db->update("tf_camps", array("contractID" => $contractID, "closed" => 0, "published" => 0, "themes" => $loc_themes[$id]), array("id" => $tmp->id));
                            //$this->db->update("tf_camps", array("contractID" => $contractID, "closed" => 0, "themes" => $loc_themes[$id]), array("id" => $tmp->id));
                        }
                    }

                    $pdf = $this->m_fcamp->generateContract($contract);

                    $html  = $this->tools->GetHtml("camp-create-2", true);
                    $body  = str_replace("%NAME%", $contract["contactName"], $html->body);
                    $body  = str_replace("%EMAIL%", $contract["contactEmail"], $body);
                    $title = str_replace("%partnerName%", $contract["partnerName"], $html->title);

                    if (!$this->tools->SendHtmlMail($contract["contactEmail"], $title, $body, false, array($pdf))) {
                        message_o("Hiba", "Hiba a levél kiküldésekor!", "error");
                    } else {
                        message_o("Sikeres regisztráció", "A megadott e-mail címre (" . $contract["contactEmail"] . ") elküldtük a folytatáshoz szükséges információkat.", "success");
                        $this->db->update("tf_camp_contracts", array("contractSent" => date("Y-m-d H:i:s")), array("id" => $contractID, "contractSent" => null));

                        $arr = array(
                            "CONTRACT"    => $contract,
                            "NAME"        => $contract->contactName,
                            "PARTNERNAME" => $contract->partnerName,
                        );
                        $html = $this->tools->GetHtml("email-new-contract", true);
                        $body = $this->tools->ReplaceVariableData($arr, $html->body);
                        $this->tools->SendHtmlMail($this->tools->GetSetting("email-from-address"), $html->title, $body);
                    }
                    @unlink($pdf);
                } else {
                    message_o("Hiba", "Ennek a szerződésenk már le vannak zárva az adatai. Utólag már nem módosítható!", "warning");
                }
            } else {
                $this->load->view("frontpage/new-camp-contract");
            }
        }
        $this->load->view("frontpage/footer");
    }

    public function logout()
    {
        $this->session->unset_userdata("user_contractID");
        redirect("/");
    }

    public function login()
    {
        if ($this->uri->segment(2)) {
            $id   = (int) $this->uri->segment(2);
            $hash = filter_var($this->uri->segment(3), FILTER_SANITIZE_STRIPPED);
            $data = $this->db->from("tf_camp_contracts")->where("id", $id)->get()->row();
            if ($data && $data->uniqID == $hash) {
                $this->session->set_userdata("user_contractID", $data->id);
                $this->camp_list();
            }
        } elseif ($this->input->post("login")) {
            $hash = filter_var($this->input->post("hash", true), FILTER_SANITIZE_STRIPPED);
            $data = $this->db->from("tf_camp_contracts")->where("MD5(concat(id, uniqID))=", $hash)->get()->row();
            if ($data) {
                $this->session->set_userdata("user_contractID", $data->id);
                $this->camp_list();
            }
        }
        $this->load->view("frontpage/header");
        $this->load->view("frontpage/login");
        $this->load->view("frontpage/footer");
    }

    public function camp_list()
    {
        $contractID = (int) $this->session->userdata("user_contractID");
        $camps      = $this->db->select("id")->from("tf_camps")->where("contractID", (int) $contractID)->get()->result();
        if (count($camps) == 0) {
            redirect("taborszerzodes");
        } else {
            redirect("taborhelyek");
        }
    }

    public function camp_turns()
    {

        if ($this->uri->segment(3) == "resztvevok") {
            $this->load->view("frontpage/camp-turns-members");
        } else {
            $this->load->view("frontpage/header");
            $this->load->view("frontpage/camp-turns");
            $this->load->view("frontpage/footer");
        }
    }

    public function camp_pictures()
    {
        $this->load->view("frontpage/header");
        $this->load->view("frontpage/camp-photos");
        $this->load->view("frontpage/footer");
    }

    public function camp_contract_preview()
    {
        $this->load->model("m_fcamp");

        if ($this->input->get("contractID")) {
            $contractID = (int) $this->input->get("contractID");
        } elseif (is_numeric($this->uri->segment(3))) {
            $contractID = (int) $this->uri->segment(3);
        }
        if (($contractID == $this->session->userdata("user_contractID")) || ($this->session->userdata("user_isAdmin"))) {
            $contract = (array) $this->db->from("tf_camp_contracts")->where("id", $contractID)->get()->row();

            $pdf = $this->m_fcamp->generateContract($contract);

            if ($pdf !== false) {
                header("Content-type: application/pdf");
                header("Content-Disposition: inline; filename=taborminosito.pdf");

                readfile($pdf);
                unlink($pdf);
            }
        }
    }

}
