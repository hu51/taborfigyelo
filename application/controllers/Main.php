<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Main extends CI_Controller
{

    public function impresszum()
    {
        $this->load->view("frontpage/header");
        $this->output->append_output($this->tools->HtmlArticle("html-impresszum"));
        $this->load->view("frontpage/footer");
    }

    public function about()
    {
        $this->load->view("frontpage/header");
        $this->output->append_output($this->tools->HtmlArticle("html-about"));
        $this->load->view("frontpage/footer");
    }

    public function gdpr()
    {
        $this->load->view("frontpage/header");
        $this->output->append_output($this->tools->HtmlArticle("html-gdpr"));
        $this->load->view("frontpage/footer");
    }

    public function news()
    {
        $this->load->helper("text");

        if ($this->uri->segment(1) == "leirasok") {
            $newsID = (int) $this->uri->segment(2);
            $news   = $this->db->from('tf_documents')->where('originalID', $newsID)->where("currentVersion", 1)->get()->row();
        } else {
            $slug = filter_var($this->uri->segment(2), FILTER_SANITIZE_STRING);
            $cat  = $this->db->from('tf_doc_categories')->where("id <>", 2)->where('slug', $slug)->get()->row();
            if ($cat) {
                if ($this->uri->segment(3)) {
                    $newsID = (int) $this->uri->segment(3);
                    $news   = $this->db->from('tf_documents')->where('originalID', $newsID)->where("currentVersion", 1)->get()->row();
                }
            }
        }
        $opengraph = false;
        if ($news) {
            $title = url_title(convert_accented_characters($news->title));
            if (file_exists(FCPATH . "images/news/pic_" . $newsID . ".jpg")) {
                $pic = site_url("images/news/pic_" . $newsID . ".jpg");
            } else {
                $pic = site_url("img/logo.jpg");
            }
            $opengraph = array(
                "title" => $news->title,
                "type"  => "article",
                "url"   => current_url(),
                "image" => $pic,
            );
        }
        $this->load->view("frontpage/header", array("opengraph" => $opengraph));
        $this->load->view("frontpage/news");
        $this->load->view("frontpage/footer");
    }

    public function camps_map()
    {
        $this->load->view("frontpage/header");
        $this->load->view("frontpage/camps-map");
        $this->load->view("frontpage/footer");
    }

    public function camps()
    {
        $this->load->model("m_fcamp");

        $this->load->view("frontpage/header");
        $this->load->view("frontpage/camps");
        $this->load->view("frontpage/footer");
    }

    public function camps_search()
    {
        $this->load->model("m_fcamp");

        $this->load->view("frontpage/header");
        $retr = $this->m_fcamp->campTemplates();
        $this->load->view("frontpage/camps", array("ids" => $retr["ids"], "title" => $retr["title"], "filters" => $retr["filters"]));
        $this->load->view("frontpage/footer");
    }

    public function campdetails()
    {
        $this->load->model("m_fcamp");
        $this->load->model("m_letters");

        $campID   = (int) $this->uri->segment(2);
        $campData = $this->db->from("tf_camps")->where("id", $campID)->get()->row();

        $opengraph = false;
        if ($campData) {
            $title = url_title(convert_accented_characters($campData->name));
            $this->db->from("tf_camp_images");
            $this->db->where("campID", $campID);
            $this->db->where("published", 1);
            $this->db->where("missing", 0);
            $this->db->order_by("ordering");
            $this->db->limit("1");
            $img       = $this->db->get()->row();
            $opengraph = array(
                "title" => $campData->name,
                "type"  => "article",
                "url"   => site_url("tabor/" . $campID . "/" . $title),
                "image" => site_url($img->path),
            );
        }
        $this->load->view("frontpage/header", array("opengraph" => $opengraph));

        if ($this->input->post("sendContact")) {
            $campData = $this->db->from("tf_camps")->where("id", $campID)->get()->row();

            if (filter_input(INPUT_POST, "contact-email", FILTER_VALIDATE_EMAIL)) {
                $cc = $campData->contactEmail . ", info@taborfigyelo.hu";
                $to = $this->input->post("contact-email");

                $letter = $this->tools->GetHtml("camp-question-email", true);

                $data = "<br>
                <h5>Szülő adatai</h5>
                <label>Név: </label>" . $this->input->post("contact-name") . "<br>\n
                <label>Email: </label>" . $this->input->post("contact-email") . "<br>\n
                <label>Telefon: </label>" . $this->input->post("contact-phone") . "<br>\n
                <label>Gyermek neve: </label>" . $this->input->post("contact-child") . "<br>\n
                <label>Kérdés: </label><br>" . $this->input->post("contact-comment") . "<br>\n

                <h5>Tábor adatai</h5>
                <label>Név: </label>" . $campData->name . "<br>\n
                <label>Url: </label>" . site_url("tabor/" . $campData->id . "/" . $campData->alias) . "<br>\n

                <h5>Táborszervező adatai</h5>
                <label>Név: </label>" . $campData->contactName . "<br>\n
                <label>Email: </label>" . $campData->contactEmail . "<br>\n
                <label>Telefon: </label>" . $campData->contactPhone . "<br>\n
                <label>Weboldal címe: </label>" . $campData->contactWebsite . "<br>\n
                <label>Facebook oldal címe: </label>" . $campData->contactFacebook . "<br>\n
                ";

                $body = str_replace("%NAME%", $this->input->post("contact-name"), $letter->body);
                $body = str_replace("%DATA%", $data, $body);

                if ($this->tools->SendHtmlMail($to, $letter->title, $body, array("cc" => $cc))) {
                    message_o("", "Kérdését elküldtük a táborszervezőnek.");
                }

                if ($this->input->post("addNewsletter") == "on") {
                    $newID  = $this->m_letters->createUser($this->input->post("contact-email", true), $this->input->post("contact-name", true));
                    $listID = $this->tools->GetSetting("newsletter-collect-id");

                    if ($newID == 0) {
                        message_o("Hírlevél feliratkozás", "Az Ön által megadott e-mail cím már szerepel az adatbázisunkban. ", "info");
                        $data = $this->db->from("tf_newsletter_users")->where("email", $this->input->post("contact-email", true))->get()->row();
                        if ($data) {
                            if (!$data->confirmed && is_null($data->unsubscribed)) {
                                $this->m_letters->sendConfirmLetter($data->id);
                            }
                            $this->m_letters->addUserToList($data->id, $listID);
                        }
                    } else {
                        $this->m_letters->sendConfirmLetter($newID);
                        $this->m_letters->addUserToList($newID, $listID);
                    }
                }
            } else {
                message_o("Hiba", "Kérem ellenőrizze le a megadott email címet.", "warning");
            }
        }

        if ($this->input->post("sendRating")) {
            $campData = $this->db->from("tf_camps")->where("id", $campID)->get()->row();

            if (filter_input(INPUT_POST, "rating-email", FILTER_VALIDATE_EMAIL)) {

                $to = "info@taborfigyelo.hu";

                $body = "<br>
                <h5>Táborminősítés érkezett</h5>
                <h5>Tábor adatai</h5>
                <label>Név: </label>" . $campData->name . "<br>\n
                <label>Url: </label>" . site_url("tabor/" . $campData->id . "/" . $campData->alias) . "<br>\n

                <h5>Minősítő adatai</h5>
                <label>Név: </label>" . $this->input->post("rating-name") . "<br>\n
                <label>Email: </label>" . $this->input->post("rating-email") . "<br>\n
                <label>Minősítés értéke: </label>" . $this->input->post("rating-value") . "<br>\n
                <label>Rövid leírás: </label>" . $this->input->post("rating-title") . "<br>\n
                <label>Hosszú szöveg: </label><br>" . $this->input->post("rating-text") . "<br>\n

                <br>";

                $data = array(
                    "campID"          => (int) $this->input->post("rating-camp"),
                    "turnID"          => (int) $this->input->post("rating-turn"),
                    "rating"          => (int) $this->input->post("rating-value"),
                    "email"           => $this->input->post("rating-email", true),
                    "name"            => $this->input->post("rating-name", true),
                    "title"           => $this->input->post("rating-title", true),
                    "comment"         => $this->input->post("rating-text", true),
                    "ip"              => $this->tools->getIP(),
                    "createdDatetime" => date("Y-m-d H:i:s"),
                    "status"          => 0,
                    "uniqid"          => uniqid(),
                );

                $this->db->insert("tf_camp_comments", $data);

                $id = $this->db->insert_id();
                if ($id) {                    
                    $body .= site_url("figyelo/camps/ratings/").$id;
                    $this->tools->SendHtmlMail($to, $letter->title, $body);

                    $to = $this->input->post("rating-email");
                    $letter = $this->tools->GetHtml("camp-rating-email", true);

                    $url = site_url("tabor-ertekeles/?id=".$id."&code=".md5($id.$data["uniqid"]));
                    $body = str_replace("%URL%", $url, $letter->body);
                    
                    if ($this->tools->SendHtmlMail($to, $letter->title, $body)) {
                        message_o("Tábor értékelés", "E-mailben elküldük az ön számára ({$to}), az értékeléssel kapcsolatos további teendőket.", "info");
                    }
                }
            } else {
                message_o("Hiba", "Kérem ellenőrizze le a megadott email címet.", "warning");
            }
        }
        $this->load->view("frontpage/camp-details");
        $this->load->view("frontpage/footer");
    }

    public function camprating()
    {
        $this->load->view("frontpage/header");

        
        $id = (int)$this->input->get("id");
        $code = $this->input->get("code");
        
        $data = $this->db->from("tf_camp_comments")->where("id", $id)->where("emailConfirmed", 0)->get()->row();    
        if (($data) && (md5($data->id.$data->uniqID) == $code) && ($data->status == 0)) {
            $html = $this->tools->GetHtml("camp-rating-final", true);
            $this->db->update("tf_camp_comments", array("emailConfirmed" => 1), array("id" => $id));
            message_o($html->title, $html->body, "info");            
        } else {
            message_o("Tábor értékelés", "Hibásak az adatok, vagy ez az értékelés már sikeresen meg lett erősítve.", "warning");            
        }
        $this->load->view("frontpage/footer");
    }

    public function createcamp_welcome()
    {
        $this->load->view("frontpage/header");
        if ($this->input->post("reg_name") && filter_input(INPUT_POST, "reg_email", FILTER_VALIDATE_EMAIL)) {
            $hash = uniqid();

            $data = array(
                "contactName"   => filter_input(INPUT_POST, "reg_name", FILTER_SANITIZE_STRING),
                "contactEmail"  => filter_input(INPUT_POST, "reg_email", FILTER_SANITIZE_EMAIL),
                "contactPhone"  => filter_input(INPUT_POST, "reg_phone", FILTER_SANITIZE_STRING),
                "uniqID"        => $hash,
                "campLocations" => 0,
                "campThemes"    => 0,
                "createdTime"   => date("Y-m-d H:i:s"),
            );
            if ($this->db->insert("tf_camp_contracts", $data)) {
                $contractID = $this->db->insert_id();

                $html = $this->tools->GetHtml("camp-create-1", true);
                $body = str_replace("%NAME%", $data["contactName"], $html->body);
                $body = str_replace("%URL%", site_url("belepes/" . $contractID . "/" . $hash), $body);

                $body = str_replace("%LOGINURL%", site_url("belepes"), $body);
                $body = str_replace("%HASH%", md5($contractID . $hash), $body);

                if ($this->tools->SendHtmlMail($data["contactEmail"], $html->title, $body)) {
                    message("", "Elküldtük a megadott e-mail címre a további teendőket!", "success");

                    $this->db->update("tf_camp_contracts", array("regEmailSent" => date("Y-m-d H:i:s")), array("id" => $contractID, "regEmailSent" => null));
                } else {
                    message("E-mail hiba", "Nem sikerült Ön számára a regisztrációs üzenetet elküldeni.<br>Kérem vegye fel velünk a kapcsolatot info@taborfigyelo.hu", "warning");
                }
            }
        } else {
            $this->load->view("frontpage/new-camp-welcome");
        }
        $this->load->view("frontpage/footer");
    }

    public function campjoin()
    {
        $this->load->model("m_fcamp");
        $this->load->model("m_letters");

        $this->load->view("frontpage/header");

        if ($this->input->post("sendData")) {
            $childs         = $this->input->post("child");
            $insurance      = $this->input->post("insurance");
            $birth          = $this->input->post("birth");
            $passport       = $this->input->post("passport");
            $passport_valid = $this->input->post("passport_valid");

            $camp     = $this->db->from("tf_camps")->where("published", "1")->where("id", (int) $this->uri->segment(2))->get()->row();
            $contract = $this->db->from("tf_camp_contracts")->where("id", $camp->contractID)->get()->row();
            $turn     = $this->db->from("tf_camp_turns")->where("published", "1")->where("validto >", date("Y-m-d"))->where("id", (int) $this->uri->segment(3))->get()->row();

            if (!$camp || !$turn) {
                message("Tábor jelentkezés", "Hibás tábor, vagy turnus információ!", "warning");
            } else {
                $html       = $this->tools->GetHtml("camp-email-join", true);
                $firstChild = true;
                foreach ($childs as $id => $temp) {
                    if (!empty($childs[$id])) {
                        $data = array(
                            "campID"         => (int) $this->uri->segment(2),
                            "turnID"         => (int) $this->uri->segment(3),
                            "camp_name"      => $camp->name,
                            "validfrom"      => $turn->validfrom,
                            "validto"        => $turn->validto,
                            "price"          => $turn->price,
                            "status"         => 0,

                            "parent_name_1"  => trim($this->input->post("name", true)),
                            "parent_email_1" => trim($this->input->post("email", true)),
                            "parent_phone_1" => trim($this->input->post("phone", true)),
                            "parent_name_2"  => "",
                            "parent_email_2" => "",
                            "parent_phone_2" => "",
                            "address"        => trim($this->input->post("address", true)),
                            "comment"        => trim($this->input->post("comment", true)),

                            "name"           => trim($childs[$id]),
                            "id_insurance"   => trim($insurance[$id]),
                            "birth"          => trim($birth[$id]),
                        );

                        if ($this->input->post("name2")) {
                            $data["parent_name_2"]  = trim($this->input->post("name2", true));
                            $data["parent_email_2"] = trim($this->input->post("email2", true));
                            $data["parent_phone_2"] = trim($this->input->post("phone2", true));
                        }

                        if ($camp->countryID != 1) {
                            $data["id_passport"]       = trim($passport[$id]);
                            $data["id_passport_valid"] = trim($passport_valid[$id]);
                        }

                        if ($this->db->insert("tf_camp_members", $data)) {
                            message("", $data["name"] . " jelentkezési adatait a rendszer mentette.", "success");

                            $contact = "
                            <table border='1'>
                                <tr>
                                    <td>Tábor neve:</td>
                                    <td>" . $camp->name . "</td>
                                </tr>
                                <tr>
                                    <td>Táborfigyelő URL:</td>
                                    <td>" . site_url("tabor/" . $camp->id . "/" . $camp->alias) . "</td>
                                </tr>
                                <tr>
                                    <td>Turnus neve:</td>
                                    <td>" . $turn->title . "</td>
                                </tr>
                                <tr>
                                    <td>Turnus időpontja:</td>
                                    <td>" . $this->tools->huDate($turn->validfrom, false, false) . " - " . $this->tools->huDate($turn->validto, false, true) . "</td>
                                </tr>
                                <tr>
                                    <td>Turnus költsége:</td>
                                    <td>" . number_format($turn->price, 0, ",", ".") . " Ft/turnus</td>
                                </tr>
                                <tr>
                                    <td>Táborszervező neve:</td>
                                    <td>" . $camp->contactName . "</td>
                                </tr>
                                <tr>
                                    <td>Táborszervező telefonszáma:</td>
                                    <td>" . $camp->contactPhone . "</td>
                                </tr>
                                <tr>
                                    <td>Táborszervező email címe:</td>
                                    <td>" . $camp->contactEmail . "</td>
                                </tr>
                                <tr>
                                    <td>Weboldal címe:</td>
                                    <td>" . $camp->contactWebsite . "</td>
                                </tr>
                                <tr>
                                    <td>Facebook oldal címe:</td>
                                    <td>" . $camp->contactFacebook . "</td>
                                </tr>
                                </table>";

                            $parentName = $data["parent_name_1"];
                            $to         = $data["parent_email_1"];
                            $member     = "
                                <table border='1'>
                                <tr>
                                    <td colspan='2'>Kapcsolattartói adatok:</td>
                                </tr>
                                <tr>
                                    <td>Szülő 1 neve:</td>
                                    <td>" . $data["parent_name_1"] . "</td>
                                </tr>
                                <tr>
                                    <td>Szülő 1 e-mail címe:</td>
                                    <td>" . $data["parent_email_1"] . "</td>
                                </tr>
                                <tr>
                                    <td>Szülő 1 telefonszáma:</td>
                                    <td>" . $data["parent_phone_1"] . "</td>
                                </tr>
                                <tr>
                                    <td>Pontos levelezési cím:</td>
                                    <td>" . $data["address"] . "</td>
                                </tr>
                            ";
                            if ($this->input->post("name2")) {
                                $to .= "," . $data["parent_email_2"];
                                $parentName .= " és " . $data["parent_name_2"];
                                $member .= "
                                <tr>
                                    <td>Szülő 2 neve:</td>
                                    <td>" . $data["parent_name_2"] . "</td>
                                </tr>
                                <tr>
                                    <td>Szülő 2 e-mail címe:</td>
                                    <td>" . $data["parent_email_2"] . "</td>
                                </tr>
                                <tr>
                                    <td>Szülő 2 telefonszáma:</td>
                                    <td>" . $data["parent_phone_2"] . "</td>
                                </tr>
                                ";
                            }

                            $member .= "
                                <tr>
                                    <td colspan='2'>Jelentkező adatai:</td>
                                </tr>
                                <tr>
                                    <td>Jelentkező neve:</td>
                                    <td>" . $data["name"] . "</td>
                                </tr>
                                <tr>
                                    <td>Jelentkező TAJ száma:</td>
                                    <td>" . $data["id_insurance"] . "</td>
                                </tr>
                                <tr>
                                <td>Jelentkező születési dátuma:</td>
                                <td>" . $data["birth"] . "</td>
                                </tr>
                            ";
                            if ($camp->countryID != 1) {
                                $member .= "
                                    <tr>
                                        <td colspan='2'>Külföldi utazás miatt:</td>
                                    </tr>
                                    <tr>
                                        <td>Útlevél száma:</td>
                                        <td>" . $data["id_passport"] . "</td>
                                    </tr>
                                    <tr>
                                        <td>Útlevél érvényessége:</td>
                                        <td>" . $data["id_passport_valid"] . "</td>
                                    </tr>
                                    ";
                            }
                            $member .= "
                                <tr>
                                    <td>Megjegyzés:</td>
                                    <td>" . $data["comment"] . "</td>
                                </tr>
                            </table>
                            ";
                            $body = str_ireplace("%DATA%", $member, $html->body);
                            $body = str_ireplace("%CONTACT%", $contact, $body);
                            $body = str_ireplace("%NAME%", $parentName, $body);

                            $cc   = array(empty($camp->contactEmail) ? $contract->contactEmail : $camp->contactEmail);
                            $cc[] = "info@taborfigyelo.hu";
                            if ($this->tools->SendHtmlMail($to, $html->title, $body, array("cc" => $cc))) {
                                message("", "`" . $data["name"] . "` jelentkezési adatai tovább lettek küldve a táborszervező számára.", "info");
                            }

                            if ($this->input->post("newsletter") == "on") {
                                $newID  = $this->m_letters->createUser($data["parent_email_1"], $data["parent_name_1"]);
                                $listID = $this->tools->GetSetting("newsletter-collect-id");
                                if ($newID == false) {
                                    //message_o("Hírlevél feliratkozás", "Az Ön által megadott e-mail cím (".$data["parent_email_1"].") már szerepel az adatbázisunkban. ", "info");
                                    $tmp = $this->db->from("tf_newsletter_users")->where("email", $data["parent_email_1"])->get()->row();
                                    if ($tmp) {
                                        if ($tmp->confirmed == "0" && is_null($tmp->unsubscribed)) {
                                            $this->m_letters->sendConfirmLetter($tmp->id, false);
                                        }
                                        $this->m_letters->addUserToList($tmp->id, $listID);
                                    }
                                } else {
                                    $this->m_letters->sendConfirmLetter($newID, false);
                                    $this->m_letters->addUserToList($newID, $listID);
                                }
                            }

                            if (($this->input->post("name2")) && $this->input->post("newsletter2")) {
                                $newID  = $this->m_letters->createUser($data["parent_email_2"], $data["parent_name_2"]);
                                $listID = $this->tools->GetSetting("newsletter-collect-id");
                                if ($newID == false) {
                                    //message_o("Hírlevél feliratkozás", "Az Ön által megadott e-mail cím (".$data["parent_email_2"].")  már szerepel az adatbázisunkban. ", "info");
                                    $tmp = $this->db->from("tf_newsletter_users")->where("email", $data["parent_email_2"])->get()->row();
                                    if ($tmp) {
                                        if ($tmp->confirmed == "0" && is_null($tmp->unsubscribed)) {
                                            $this->m_letters->sendConfirmLetter($tmp->id, false);
                                        }
                                        $this->m_letters->addUserToList($tmp->id, $listID);
                                    }
                                } else {
                                    $this->m_letters->sendConfirmLetter($newID, false);
                                    $this->m_letters->addUserToList($newID, $listID);
                                }
                            }
                        }
                    }
                    $firstChild = false;
                }
            }
        } else {
            $this->load->view("frontpage/camp-join");
        }
        $this->load->view("frontpage/footer");
    }

    public function index()
    {
        $this->load->view("frontpage/frontpage");
    }

    public function dailycheck()
    {
        echo "Camp featured:<br>\n";
        $this->db->query("UPDATE tf_camps SET featuredStart = NULL WHERE LEFT(featuredStart,1)='0'; ");
        $this->db->query("UPDATE tf_camps SET featuredEnd = NULL WHERE LEFT(featuredEnd,1)='0'; ");
        
        // turn off featured
        $this->db->query("UPDATE tf_camps SET featured=0 WHERE (featured=1) AND (featuredStart IS NOT NULL OR featuredEnd IS NOT NULL) AND ('".date("Y-m-d")."' NOT BETWEEN featuredStart AND featuredEnd); ");
        
        echo "OFF: ".$this->db->affected_rows()."<br>\n";
        
        // turn on featured
        $this->db->query("UPDATE tf_camps SET featured=1 WHERE (featured=0) AND (featuredStart IS NOT NULL OR featuredEnd IS NOT NULL) AND ('".date("Y-m-d")."' BETWEEN featuredStart AND featuredEnd); ");
        echo "ON: ".$this->db->affected_rows()."<br>\n";
        
        //----------------------------------
        echo "<br>Camp first turn active:<br>\n";
        $to = "info@taborfigyelo.hu";
        $dt = date("Y-m-d");
        $result = $this->db->query("SELECT DISTINCT max(`validfrom`) as ld, c.name, c.contactName, c.id FROM `tf_camp_turns` as t
        JOIN `tf_camps` as c ON c.id=t.campID WHERE t.published=1 AND c.published=1
        GROUP BY t.campID HAVING ld='".$dt."'; ")->result();

        foreach ($result as $tmp) {
            $body = $tmp->ld."<br>\n"
            .$tmp->name."<br>\n"
            .$tmp->contactName."<br><br>\n"
            .anchor(site_url("tabor/".$tmp->id))."<hr>\n";
            $this->tools->SendHtmlMail($to, "Tábor utolsó turnusa megkezdődött", $body);
        }
    }

}
