<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Newsletter extends CI_Controller
{

    public function unsubscribe()
    {
        $this->load->view('frontpage/header');
        $this->load->view('frontpage/newsletter-unsubscribe');
        $this->load->view('frontpage/footer');
    }

    public function subscribe()
    {
        $this->load->view('frontpage/header');
        $this->load->view('frontpage/newsletter-subscribe');
        $this->load->view('frontpage/footer');
    }

    public function confirm()
    {
        $this->load->view('frontpage/header');
        $this->load->view('frontpage/newsletter-confirm');
        $this->load->view('frontpage/footer');
    }


    public function preview()
    {
        $letterID = $this->uri->segment(3);

        $this->db->from("tf_newsletter_letters");
        if (strlen($letterID) <= 13) {
            $this->db->where("uniqid", $letterID);
        } else {
            $this->db->where("MD5(CONCAT(id,uniqid))", $letterID);
        }
        $letter = $this->db->get()->row();
        if ($letter) {
            $this->load->model("m_letters");
            $out = $this->m_letters->GenerateHtmlLetter($letter->id);
            $out = preg_replace("/(%[^%].*%)/mi", "", $out);
            echo $out;
        } else {
            echo "Nope...";
        }
    }

    public function tracking()
    {
        $letterID = $this->input->get("l", true);
        $userID   = $this->input->get("u", true);

        $this->db->from("tf_newsletter_letters");
        $this->db->where("MD5(CONCAT(id,uniqid))", $letterID);
        $letter = $this->db->get()->row();

        $this->db->from("tf_newsletter_users");
        $this->db->where("MD5(CONCAT(id,uniqid))", $userID);
        $user = $this->db->get()->row();

        if (($letter) && ($user)) {
            $this->db->where("letterID", $letter->id);
            $this->db->where("userID", $user->id);
            $this->db->where("received", "0");
            $this->db->update("tf_newsletter_letters_users", array("received" => "1", "receivedDatetime" => date("Y-m-d H:i:s")));
        }

        $name = FCPATH . "img/logo_small.png";
        $fp   = fopen($name, 'rb');
        header("Content-Type: image/png");
        header("Content-Length: " . filesize($name));
        fpassthru($fp);
    }

    public function index()
    {
        redirect("/");
    }

}
