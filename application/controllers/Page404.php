<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Page404 extends CI_Controller
{

    public function index()
    {
        $url = str_replace(site_url(), "", current_url());        
        $data = $this->db->from("tf_redirect")->where("oldUrl", $url)->where("enabled", 1)->get()->row(); 
        if ($data) {
            $this->db->update("tf_redirect", array("hits" => $data->hits+1), array("id" => $data->id));        
            redirect(site_url($data->newURL));
        } else{
            $news = $this->db->select("id")->from("tf_documents")->like("slug", filter_var(str_replace(".html", "", $url), FILTER_SANITIZE_URL))->where("currentVersion", 1)->where("visible", 1)->get()->row();               
            if ($news) {
                $this->load->helper("text");
                $this->load->view("frontpage/header");     
                $this->load->view("frontpage/news", array("newsID" => $news->id));
                $this->load->view("frontpage/footer");
            } else {                
                $data = $this->db->from("tf_redirect")->where("oldUrl LIKE '%*'")->where("enabled", 1)->get()->result(); 
                foreach ($data as $str) {
                    if (!is_null(($str)) {
                        if (stristr("#".$url, "#".rtrim($str, "\*"))) {
                            $this->db->update("tf_redirect", array("hits" => $data->hits+1), array("id" => $str->id));        
                            redirect(site_url($data->newURL));
                            return;
                        }
                    }
                }            
                $this->output->set_status_header('404');

                $data = $this->db->from("tf_404_pages")->where("url", $url)->get()->row();
                if (!$data) {
                    $this->db->insert("tf_404_pages", array("url" => $url, "lastTime" => date("Y-m-d H:i:s")));
                } else {
                    $this->db->update("tf_404_pages", array("hits" => $data->hits+1, "lastTime" => date("Y-m-d H:i:s")), array("id" => $data->id));
                }
                $this->load->view("frontpage/header");
                message_o("Hiba", "Az ön által keresett oldal nem található:<br>".site_url($url), "warning");
                $this->load->view("frontpage/footer");
            }
        }
    }
}
