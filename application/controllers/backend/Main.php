<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Main extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        if ($this->input->post("loginEmail")) {
            $ip = $this->tools->getUserIP();

            $passOK = true;
            if (($this->input->post("email")) && ($this->session->userdata("user_id") == false)) {
                $this->db->from('users');
                $this->db->where('email', $this->input->post("email", true));
                $this->db->where('blocked', '0');
                $userData = $this->db->get()->row();
            } else {
                $this->db->from('users');
                $this->db->where('id', $this->session->userdata("user_id"));
                $this->db->where('blocked', '0');
                $userData = $this->db->get()->row();
            }

            if (($userData) && ($this->input->post("email"))) {
                $passOK = ($userData->password == md5($this->input->post("password", true)));
            }

            if ((!$userData) || (!$passOK)) {
                $attempt = $this->db->from("login_attempts")->where("ip", $ip)->get()->row();
                //save attempt
                $this->db->set('ip', $ip);
                $this->db->set('attempts', 'attempts+1', false);
                $this->db->set('lastLogin', date("Y-m-d H:i:s"));
                if ($attempt) {
                    $this->db->where('ip', $ip);
                    $this->db->update('login_attempts');
                } else {
                    $this->db->insert('login_attempts');
                }

                redirect('backend/login');
                return;
            } else {
                //reset attempt

                $attempt = $this->db->from("login_attempts")->where("ip", $ip)->get()->row();
                $this->db->set('ip', $ip);
                $this->db->set('attempts', '0');
                $this->db->set('lastLogin', date("Y-m-d H:i:s"));
                if ($attempt) {
                    $this->db->where('ip', $ip);
                    $this->db->update('login_attempts');
                } else {
                    $this->db->insert('login_attempts');
                }

                foreach ($userData as $key => $val) {
                    if ($key != "password") {
                        $this->session->set_userdata('user_' . $key, $val);
                    }
                }

                $this->db->where('id', $userData->id);
                $this->db->update('users', array('lastLogin' => date("Y-m-d H:i:s")));
                redirect('backend/');
            }
        }
        if ($this->uri->segment(2) !== "login" && !$this->session->userdata("user_id")) {
            redirect("backend/login");
        } else {
            $this->load->model("m_camp");
        }
    }

    public function login()
    {
        $this->load->view("backend/login");
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect("backend/");
    }

    public function index()
    {
        $this->load->view("backend/header");
        $this->load->view("backend/footer");
    }

    public function settings()
    {
        $this->load->helper("form");
        $prefix = $this->uri->segment(4);

        $this->load->view("backend/header");

        if ($this->input->post("update")) {
            $key    = key($this->input->post("update"));
            $values = $this->input->post("value");
            $this->db->update($prefix . "_settings", array("value" => $values[$key]), array("slug" => $key));
            message_o("", "Értékek módosítva");
        }

        $this->load->view("backend/settings", array("" => "tm_"));
        $this->load->view("backend/footer");
    }

    public function banners()
    {
        $this->load->view("backend/header");
        $this->load->view("backend/figyelo/banners");
        $this->load->view("backend/footer");
    }

    public function campbanners()
    {
        $this->load->view("backend/header");
        $this->load->view("backend/figyelo/banners-camp");
        $this->load->view("backend/footer");
    }

}
