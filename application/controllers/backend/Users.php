<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Users extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        if ($this->uri->segment(2) !== "login" && !$this->session->userdata("user_id")) {
            redirect("backend/login");
        }
    }
    
    public function index()
    {
        $this->load->view('backend/header', array("title" => "Felhasználók"));
        $this->load->view('backend/users/index');
        $this->load->view('backend/footer');
    }

    public function edituser($contactID = false)
    {
        $this->load->view('backend/header', array("title" => "Felhasználók szerkesztése"));

        if ($contactID == false) {
            $contactID = $this->uri->segment(4);
        }

        if ($this->input->post("modify")) {

            if ($this->input->post("modify") == "main") {

                $data = array(
                    "lastName" => $this->input->post("lastName", true),
                    "firstName"  => $this->input->post("firstName", true),
                    "email"      => $this->input->post("email", true),
                    "phone"      => $this->input->post("phone", true),
                );

                if ($this->session->userdata("user_isAdmin")) {
                    $data["isAdmin"] = (int) $this->input->post("isAdmin");
                    $data["blocked"] = (int) $this->input->post("blocked");
                }

                if ($this->input->post("new_password") && (strlen($this->input->post("new_password"))>6) && ($this->input->post("new_password") == $this->input->post("new_password"))) {
                    $data["password"] = md5($this->input->post("new_password"));
                }

                if ($this->db->update("users", $data, array("id" => $contactID))) {
                    $this->tools->message("Felhasználó", "Módosítások mentve.", "success");
                }
            }
        }
        $this->load->view('backend/users/edit-user', array("contactID" => $contactID));
        $this->load->view('backend/footer');
    }

    public function adduser()
    {
        $this->load->view('backend/header', array("title" => "Új felhasználó"));

        if ($this->input->post("adduser")) {
            $this->db->from("users");
            if ($this->input->post("email")) {
                $emailExist = $this->db->from("users")->where("email", filter_var($this->input->post("email", true), FILTER_SANITIZE_EMAIL))->get()->row();
            } else {
                $emailExist = false;
            }
            if ($emailExist) {
                $this->tools->message("Hiba", "Ez az e-mail cím már létezik!", "danger");
                $this->load->view('backend/users/add-user');
            } else {
                $data = array(
                    "lastName" => $this->input->post("lastName", true),
                    "firstName"  => $this->input->post("firstName", true),
                    "email"      => $this->input->post("new_email", true),
                    "password"   => md5($this->input->post("new_password", true)),
                    "phone"      => $this->input->post("phone", true),
                );
                if ($this->session->userdata("user_isAdmin")) {
                    $data["isAdmin"] = (int) $this->input->post("isAdmin");
                    $data["blocked"] = (int) $this->input->post("blocked");
                }
                if ($this->db->insert("users", $data)) {
                    $this->tools->message("Új kapcsolat", "Az adatok mentése sikeres.", "success");
                }
            }
        } else {
            $this->load->view('backend/users/add-user');
        }
        $this->load->view('backend/footer');
    }

    public function profile()
    {
        $this->edituser($this->session->userdata("user_id"));
    }

}
