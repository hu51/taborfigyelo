<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Users
 *
 * @author szabog
 */
class Groups extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('user_id')) {
            redirect("backend/login");
        }
    }


    public function members()
    {
        $this->load->view('backend/header');
        $this->load->view('backend/figyelo/newsletter/groups/members');
        $this->load->view('backend/footer');
    }


    public function create()
    {
        redirect("backend/newsletter/groups/edit/create");
    }


    public function edit()
    {
        $this->load->view('backend/header');

        $listID = (int) $this->uri->segment(5);
        if ($this->input->post("deleteList")) {
            redirect("backend/newsletter/groups/delete/".$listID);
        }
        if ($this->input->post("saveList")) {

            if ($listID == "create") {
                $this->db->insert("tf_newsletter_lists", array("uniqid" => uniqid()));
                $listID = $this->db->insert_id();
            }

            $data = array(
                "name"        => $this->input->post("name", true),
                "description" => $this->input->post("description", true),
                "modified"    => date("Y-m-d H:i:s"),
                "active"      => (int) $this->input->post("active"),
                "public"      => (int) $this->input->post("public")
            );           
            if (!$this->db->update("tf_newsletter_lists", $data, array("id" => $listID))) {
                $this->output->append_output("<div class='alert alert-warning'>Nem tudom felülírni az adatokat.. why?</div>\n");
            }  
            if ($this->uri->segment(5) == "create") {
                redirect("backend/newsletter/groups/edit/".$listID);
            }
        }
        $this->load->view('backend/figyelo/newsletter/groups/edit');
        $this->load->view('backend/footer');
    }


    public function delete()
    {
        $this->load->view('backend/header');

        $listID = (int) $this->uri->segment(5);

        $this->db->from("tf_newsletter_lists");
        $this->db->where("id", $listID);
        $list = $this->db->get()->row();

        if ($this->input->post("deleteListFinal") && $list->uniqid == $this->input->post("confirm")) {
            $del_todo = $this->input->post("del_todo");

            if ($del_todo == "move") {
                $targetList = (int) $this->input->post("target_list");

                if ($targetList == "-1") {
                    $this->output->append_output("<div class='alert alert-warning'>Nincs csoport kiválasztva!</div>\n");
                    $this->load->view('backend/figyelo/newsletter/groups/delete');
                } else {
                    $this->db->from("tf_newsletter_lists_users");
                    $this->db->where("listID", $list->id);
                    $query = $this->db->get();
                    foreach ($query->result() as $data) {
                        $found = $this->db->from("lists_users")->where("userID", $data->userID)->where("listID", $targetList)->get()->row();
                        if (!$found) {
                            $this->db->update("lists_users", array("listID" => $targetList), array("userID" => $data->userID, "listID" => $listID));
                        } else {
                            $this->db->delete("lists_users", array("userID" => $data->userID, "listID" => $listID)); 
                        }
                    }
                    $this->db->update("tf_newsletter_letters_users", array("listID" => $targetList), array("listID" => $listID));
                    $this->db->delete("tf_newsletter_lists", array("id" => $listID));

                    $this->output->append_output("<div class='alert alert-info'>Tagok és levelek átmozgatva. Csoport törölve.</div>\n");
                }
            } elseif ($del_todo == "remove") {
                $this->db->delete("tf_newsletter_lists_users", array("listID" => $listID));
                $this->db->delete("tf_newsletter_letters_users", array("listID" => $listID));
                $this->db->delete("tf_newsletter_lists", array("id" => $listID));

                $this->output->append_output("<div class='alert alert-info'>Csoport és levelek törölve.</div>\n");
            } 
        } else {
            $this->load->view('backend/figyelo/newsletter/groups/delete');
        }
        $this->load->view('backend/footer');
    }

    public function index()
    {
        $this->load->view('backend/header');
        $this->load->view('backend/figyelo/newsletter/groups/index');
        $this->load->view('backend/footer');
    }

}
