<?php

/**
 * Description of Letters
 *
 * @author szabog
 */
class Letters extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('user_id') && ($this->uri->segment(2) !== "autosend")) {
            redirect("backend/login");
            exit;
        }
    }

    public function autosend()
    {
        // .../autosend/jkM8mBy1RuyY/
        if ($this->uri->segment(3) == "jkM8mBy1RuyY") {
            $this->load->model("m_letters");            
            $this->m_letters->BuildSend();
        }
    }    

    public function ajaxSave()
    {
        if (!$this->input->is_ajax_request()) {
            die("No ajax!");
        } else {
            $letterID = (int) $this->input->post("id");
            $data     = array("body" => $this->input->post("body"), "modified" => date("Y-m-d H:i:s"));
            if (is_numeric($letterID)) {
                $this->db->where("id", $letterID);
                $this->db->update("tf_newsletter_letters", $data);

                echo json_encode(array("saved" => true));
            } else {
                echo json_encode(array("saved" => false));
            }
        }
    }

    public function send()
    {
        $letterID = (int) $this->uri->segment(5);
        $this->load->view('backend/header');

        if ($this->input->post("confirm_send")) {
            $this->db->from("tf_newsletter_letters");
            $this->db->where("id", $letterID);
            $letter = $this->db->get()->row();

            $this->output->append_output("<h3>Hírlevél kiküldés</h3>");
            $this->output->append_output("<p>{$letter->title}</p>");

            $this->db->where("id", $letterID);
            $this->db->where("sent is NULL", "", false);
            $this->db->update("tf_newsletter_letters", array("autoSend" => "1", "sent" => date("Y-m-d H:i:s")));

            $this->output->append_output("<div class='alert alert-success'>Automata küldés bekapcsolva. A rendszer adott időközönként automatán ki fogja küldeni a leveleket.</div>");
        } else {
            $this->load->view('backend/figyelo/newsletter/letters/send');
        }
        $this->load->view('backend/footer');
    }

    public function create()
    {
        redirect("backend/newsletter/letters/edit/create");
    }

    public function edit()
    {
        $this->load->view('backend/header');
        if ($this->input->post("saveLetter")) {
            $letterID = $this->uri->segment(5);

            $data = array(
                "title"         => $this->input->post("title", true),
                "from_address"  => $this->input->post("from_address", true),
                "from_name"     => $this->input->post("from_name", true),
                "reply_address" => $this->input->post("reply_address", true),
                "templateID"    => $this->input->post("template", true),
                "body"          => $this->input->post("body"),
                "sent"          => null,
                "autoSend"      => 0,
            );

            if (is_numeric($letterID)) {
                $this->db->where("id", $letterID);
                $this->db->update("tf_newsletter_letters", $data);
            } else {
                $data["uniqid"] = uniqid();
                $this->db->insert("tf_newsletter_letters", $data);
                $letterID = $this->db->insert_id();

                redirect("backend/newsletter/letters/edit/".$letterID);
                exit;
            }

            if ($this->input->post("clear") == "status") {
                $this->db->where("letterID", $letterID);
                $this->db->update("tf_newsletter_letters_users", array("sent" => 0, "received" => 0, "sentDatetime" => null, "receivedDatetime" => null));
            }

            if ($this->input->post("clear") == "members") {
                $this->db->where("letterID", $letterID);
                $this->db->where("sent", "0");
                $this->db->delete("tf_newsletter_letters_users");
            }

            if ($this->input->post("clear") == "all") {
                $this->db->where("letterID", $letterID);
                $this->db->delete("tf_newsletter_letters_users");
            }

            if ($this->input->post("add_members")) {
                $add_members = $this->input->post("add_members");             
                foreach ($add_members as $groupID => $tmp) {

                    $this->db->from("tf_newsletter_lists_users");
                    $this->db->where("listID", $groupID);
                    $this->db->where("unsubscribed is NULL");
                    $query = $this->db->get();
                    foreach ($query->result() as $data) {

                        $this->db->from("tf_newsletter_letters_users");
                        $this->db->where("letterID", $letterID);
//                        $this->db->where("sent", "0");
                        $this->db->where("userID", $data->userID);
                        $found = $this->db->get()->num_rows();
                        if (!$found) {
                            $tmp = array(
                                "letterID" => $letterID,
                                "listID"   => $groupID,
                                "userID"   => $data->userID,
                                "sent"     => "0",
                            );
                            $this->db->insert("tf_newsletter_letters_users", $tmp);
                        }
                    }
                }
            }

            if ($this->input->post("preview_send") == "on") {
                $this->load->model("m_letters");

                $toEmail = $this->input->post("preview_to");
                if (filter_var($toEmail, FILTER_VALIDATE_EMAIL)) {
                    $letter      = $this->db->from("tf_newsletter_letters")->where("id", $letterID)->get()->row();
                    $body        = $this->m_letters->GenerateHtmlLetter($letterID);
                    $letter_body = str_ireplace("%TRACKING%", "-- tracking url --", $body);

                    $options = array(
                        "reply-to" => $letter->reply_address,
                        "from-name" => $letter->from_name,
                        "from-address" => $letter->from_address,
                    );           
                    if ($this->tools->SendHtmlMail($toEmail, $letter->title, $letter_body, $options, false, false)) {
                        $this->output->append_output("<div class='alert alert-info'>Előnézet elküldve: {$toEmail}</div>\n");
                    } else {
                        $this->output->append_output("<div class='alert alert-danger'>Hiba az előnézet elküldésekor!</div>\n");
                    } 
                } else {
                    $this->output->append_output("<div class='alert alert-danger'>Hibás a teszt e-mail cím: {$toEmail}</div>\n");
                }
            }
        }

        $this->load->view('backend/figyelo/newsletter/letters/edit');
        $this->load->view('backend/footer');
    }

    public function stat()
    {
        $this->load->view('backend/header');
        $this->load->view('backend/figyelo/newsletter/letters/stat');
        $this->load->view('backend/footer');
    }

    public function members()
    {
        $this->load->view('backend/header');
        $this->load->view('backend/figyelo/newsletter/letters/members');
        $this->load->view('backend/footer');
    }

    public function index()
    {
        $this->load->view('backend/header');
        $this->load->view('backend/figyelo/newsletter/letters/index');
        $this->load->view('backend/footer');
    }

}
