<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Media extends CI_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata("user_id")) {
            redirect("backend/login");
        }
    }

    public function index()
    {

        $this->load->library('image_lib');
        GLOBAL $img_sizes;

        $this->load->view('backend/header');

        $photoPath                = realpath(FCPATH) . '/upload/';
        $config['image_library']  = 'gd2';
        $config['upload_path']    = $photoPath;
        $config['allowed_types']  = 'jpg|png';
        $config['max_size']       = '3000';
        $config['max_width']      = '3000';
        $config['max_height']     = '2000';
        $config['maintain_ratio'] = TRUE;
        $config['remove_spaces']  = TRUE;

        $this->load->library('upload', $config);
        if (($this->input->post("del_photo")) && ($this->input->post("del_photo") == $this->input->post("del_photo_conf"))) {
            $file = FCPATH . "upload/" . $this->input->post("del_photo");
            unlink($file);

            foreach ($img_sizes as $pix) {
                @unlink(str_replace(".jpg", "_" . $pix . ".jpg", "th_" . $file));
            }
            $this->tools->message("", $file . " fájlok törölve");
        }

        if (!empty($_FILES["photo"]["name"])) {
            $overwrite           = ($this->input->post("photo_overwrite") == "on");
            $config['overwrite'] = $overwrite;

            if ($this->upload->do_upload("photo")) {
                $img_data = $this->upload->data();
                if ($img_data["is_image"]) {

                    if ($this->input->post("photo_rename")) {
                        $name = $this->input->post("photo_rename");
                    } else {
                        $name = $img_data["raw_name"];
                    }
                    $name = strtolower(convert_accented_characters(str_replace(" ", "_", $this->security->sanitize_filename($name))));

                    $config['image_library']  = 'gd2';
                    $config['source_image']   = $img_data["full_path"];
                    $config['new_image']      = $photoPath . "{$name}.jpg";
                    $config['create_thumb']   = FALSE;
                    $config['maintain_ratio'] = TRUE;
                    $config['quality']        = '95%';

                    if (($img_data["image_width"] > 1600) || ($img_data["image_height"] > 1000)) {
                        $config['width']  = 1600;
                        $config['height'] = 1000;
                    }
                    $this->image_lib->clear();
                    $this->image_lib->initialize($config);
                    if (!$this->image_lib->resize()) {
                        $this->tools->message("Kép méretezési hiba:", $name . "<br />" . $this->image_lib->display_errors(), "alert");
                    }

                    foreach ($img_sizes as $pix) {
                        $maxWidth = $pix;
                        $maxHeight = ($pix/3*2);
                        if (($img_data["image_width"] > $maxWidth) || ($img_data["image_height"] > $maxHeight)) {

                            $config['image_library']  = 'gd2';
                            $config['source_image']   = $img_data["full_path"];
                            $config['new_image']      = $photoPath . "th_{$name}_{$pix}.jpg";
                            $config['create_thumb']   = FALSE;
                            $config['maintain_ratio'] = TRUE;
                            $config['quality']        = '90%';
                            $config['width']          = $maxWidth;
                            $config['height']         = $maxHeight;                            

                            $this->image_lib->clear();
                            $this->image_lib->initialize($config);
                            if (!$this->image_lib->resize()) {
                                $this->tools->message("Kép méretezési hiba ({$pix}px):", $name . "<br />" . $this->image_lib->display_errors(), "alert");
                            }
                        }
                    }
                    $this->image_lib->clear();
                    $this->tools->message("", "A kép feltöltése sikerült.");

                    if (($this->input->post("photo_rename")) || ($img_data["image_type"] !== "jpeg")) {
                        unlink($img_data["full_path"]);
                    }
                } else {
                    unlink($img_data["full_path"]);
                }
            } else {
                $this->tools->message("Feltöltési hiba:", $img_data["raw_name"] . "<br />" . $this->upload->display_errors(), "alert");
            }
            unlink($_FILES["photo"]["tmp_name"]);
        }

        $this->load->view('backend/figyelo/newsletter/media/index');
        $this->load->view('backend/footer');
    }

}
