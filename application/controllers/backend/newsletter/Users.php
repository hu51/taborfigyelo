<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Users
 *
 * @author szabog
 */
class Users extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('user_id')) {
            redirect("backend/login");
            exit;
        }
    }

    public function index()
    {
        
        if ($this->input->get("export") == "xls") {
            $this->load->view('backend/figyelo/newsletter/users/export');
        } else {
            $this->load->view('backend/header');
            $this->load->view('backend/figyelo/newsletter/users/index');
            $this->load->view('backend/footer');
        }
    }

    public function import()
    {
        $this->load->view('backend/header');

        if ($this->input->post("import_step_3")) {
            $this->load->view('backend/figyelo/newsletter/users/import_3');
        } elseif ($this->input->post("import_step_2")) {

            if (!empty($_FILES["import_src"]["name"])) {
                $config['upload_path']   = sys_get_temp_dir();
                $config['allowed_types'] = 'csv|txt';
                $this->load->library('upload', $config);

                if ($this->upload->do_upload('import_src')) {
                    $data = array('upload_data' => $this->upload->data());
                    $this->load->view('backend/figyelo/newsletter/users/import_2', $data);
                } else {
                    $this->output->append_output(var_export(array('error' => $this->upload->display_errors()), true));
                    $this->load->view('backend/figyelo/newsletter/users/import_1');
                }
            } else {
                $this->load->view('backend/figyelo/newsletter/users/import_2');
            }
        } else {
            $this->load->view('backend/figyelo/newsletter/users/import_1');
        }
        $this->load->view('backend/footer');
    }

    public function create()
    {
        redirect("/users/edit/create");
    }

    public function edit()
    {
        $this->load->view('backend/header');

        if ($this->input->post("saveUser")) {
            $userID = (int) $this->uri->segment(5);

            $email = $this->input->post("email", true);
            if ($userID == "create") {
                if (!$this->db->from("tf_newsletter_users")->where("email", $email)->get()->row()) {
                    $this->db->insert("tf_newsletter_users", array("email" => $email, "created" => date("Y-m-d H:i:s"), "uniqid" => uniqid()));
                    $userID = $this->db->insert_id();
                } else {
                    $this->output->append_output("<div class='list-group-item alert alert-warning'>Már létezik ez az e-mail cím: {$email}</div>\n");
                    return;
                }
            }

            $data = array(
                "email"        => $email,
                "blacklisted"  => $this->input->post("blacklisted", true),
                "unsubscribed" => $this->input->post("unsubscribed", true),
            );
            if (!$this->db->update("tf_newsletter_users", $data, array("id" => $userID))) {
                $this->output->append_output("<div class='list-group-item alert alert-warning'>Nem tudom felülírni az adatokat.. why?</div>\n");
            }

            $this->db->delete("tf_newsletter_lists_users", array("unsubscribed is NULL", "userID" => $userID));
            $list_member = $this->input->post("list_member");
            if (is_array($list_member)) {
                foreach ($list_member as $listID => $value) {
                    $this->db->from("tf_newsletter_lists_users");
                    $this->db->where("userID", $userID);
                    $this->db->where("listID", $listID);
                    $found = $this->db->get()->row();
                    if (!$found) {
                        $data = array(
                            "userID"   => $userID,
                            "listID"   => $listID,
                            "modified" => date("Y-m-d H:i:s"),
                        );
                        $this->db->insert("tf_newsletter_lists_users", $data);
                    }
                }
            }
        }

        $this->load->view('backend/figyelo/newsletter/users/edit');
        $this->load->view('backend/footer');
    }

}
