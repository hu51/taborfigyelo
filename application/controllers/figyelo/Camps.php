<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Camps extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata("user_id")) {
            redirect("backend/login");
        }
    }


    public function types()
    {
        $this->load->view("backend/header");
        $this->load->view("backend/figyelo/camps/types");
        $this->load->view("backend/footer");
    }

    public function categories()
    {
        $this->load->view("backend/header");
        $this->load->view("backend/figyelo/camps/categories");
        $this->load->view("backend/footer");
    }

    public function countries()
    {
        $this->load->view("backend/header");
        $this->load->view("backend/figyelo/camps/countries");
        $this->load->view("backend/footer");
    }

    public function states()
    {
        $this->load->view("backend/header");
        $this->load->view("backend/figyelo/camps/states");
        $this->load->view("backend/footer");
    }

    public function editlocation()
    {     
        $this->load->view("backend/header");
        if ($this->input->post("saveLocation")) {
            $data = array(
                "name"            => $this->input->post("name"),
                "minositoID"      => $this->input->post("minositoID"),
                "contractID"      => (int) $this->input->post("contractID"),
                "alias"           => $this->tools->toSlug($this->input->post("name")),
                "minositoID"      => $this->input->post("minositoID"),
                "typeID"          => (int) $this->input->post("typeID"),
                "locationID"      => (int) $this->input->post("locationID"),
                "location_str"    => $this->input->post("location", true),
                "stateID"         => (int) $this->input->post("stateID"),
                "state_str"       => $this->input->post("state", true),
                "countryID"       => (int) $this->input->post("countryID"),
                "country_str"     => $this->input->post("country", true),
                "zip"             => (int) $this->input->post("zip"),
                "city"            => $this->input->post("city", true),
                "address"         => $this->input->post("address", true),
                "description"     => $this->input->post("description"),
                "text"            => $this->input->post("text"),
                "program"         => $this->input->post("program"),
                "price"           => (int) $this->input->post("price"),
                "szep_MKB"        => (int) $this->input->post("szep_MKB"),
                "szep_OTP"        => (int) $this->input->post("szep_OTP"),
                "szep_KH"         => (int) $this->input->post("szep_KH"),
                "age_min"         => (int) $this->input->post("age_min"),
                "age_max"         => (int) $this->input->post("age_max"),
                "published"       => (int) $this->input->post("published"),
                "featured"        => (int) $this->input->post("featured"),
                "featuredStart"   => $this->input->post("featuredStart"),
                "featuredEnd"     => $this->input->post("featuredEnd"),
                "closed"          => (int) $this->input->post("closed"),
                "contactName"     => filter_var($this->input->post("contactName"), FILTER_SANITIZE_STRING),
                "contactPhone"    => filter_var($this->input->post("contactPhone"), FILTER_SANITIZE_STRING),
                "contactEmail"    => filter_var($this->input->post("contactEmail"), FILTER_SANITIZE_EMAIL),
                "contactWebsite"  => filter_var($this->input->post("contactWebsite"), FILTER_SANITIZE_URL),
                "contactFacebook" => filter_var($this->input->post("contactFacebook"), FILTER_SANITIZE_URL),
                "lastModify"      => date("Y-m-d H:i:s"),
                "lat"             => (float) $this->input->post("lat"),
                "lng"             => (float) $this->input->post("lng"),
            );

            if ($data["countryID"] <= 0) {
                $tmp = $this->db->from("tf_camp_countries")->like("name", $data["country_str"])->get()->row();
                if ($tmp) {
                    $data["countryID"] = $tmp->id;
                }
            }
            
            if ($data["stateID"] <= 0) {
                if ($data["countryID"] > 0) {
                    $tmp = $this->db->from("tf_camp_states")->like("name", $data["state_str"])->where("countryID", $data["countryID"])->get()->row();
                } else {
                    $tmp = $this->db->from("tf_camp_states")->like("name", $data["state_str"])->get()->row();                        
                }               
                if ($tmp) {
                    $data["stateID"] = $tmp->id;
                }
            }
            if ($data["locationID"] <= 0) {              
                if ($data["stateID"] > 0) {
                    $tmp = $this->db->from("tf_camp_locations")->like("name", $data["location_str"])->where("stateID", $data["stateID"])->get()->row();
                } else {
                    $tmp = $this->db->from("tf_camp_locations")->like("name", $data["location_str"])->get()->row();
                }
                if ($tmp) {
                    $data["locationID"] = $tmp->id;
                }
            }
              
            if ($this->input->post("chgseo") == "on") {
                $data["alias"] = $this->tools->toSlug($this->input->post("alias", true));
            } elseif (empty($camp->alias)) {
                $data["alias"] = $this->tools->toSlug($this->input->post("name"));
            }
            $this->db->update("tf_camps", $data, array("id" => $this->input->post("locID")));

            $this->db->delete("tf_camp_categories", array("campID" => $this->input->post("locID")));
            $cats  = $this->input->post("categories");
            $first = true;
            foreach ($cats as $tmp => $cat) {
                $this->db->insert("tf_camp_categories", array("categoryID" => $cat, "campID" => $this->input->post("locID")));
                if ($first) {
                    $this->db->update("tf_camps", array("categoryID" => $cat), array("id" => $this->input->post("locID")));
                }
                $first = false;
            }
            message_o("", "A módosítások mentve</a>", "success");
        }
        $this->load->view("backend/figyelo/camps/edit-location");
        $this->load->view("backend/footer");
    }
    
    public function editcontract()
    {
        $this->load->view("backend/header");
        $this->load->view("backend/figyelo/camps/edit-contract");
        $this->load->view("backend/footer");
    }

    public function ratings()
    {
        $this->load->view("backend/header");
        $this->load->view("backend/figyelo/camps/ratings");
        $this->load->view("backend/footer");
    }
    
    public function editturns()
    {
        $this->load->view("backend/header");
        $this->load->view("backend/figyelo/camps/edit-turns");
        $this->load->view("backend/footer");
    }

    public function contracts()
    {
        if ($this->input->get("export") == "xls") {
            $this->load->view("backend/figyelo/camps/contract-list-excel");
        } else {
            $this->load->view("backend/header");
            $this->load->view("backend/figyelo/camps/contract-list");
            $this->load->view("backend/footer");
        }
    }

    public function todos()
    {
        $this->load->view("backend/header");

        if (($this->input->post("sendPayment")) && ($this->input->post("sendPayment") == $this->input->post("confSend"))) {
            $contractID = $this->input->post("sendPayment");
            $contract   = $this->db->from("tf_camp_contracts")->where("id", (int) $contractID)->get()->row();

            $html = $this->tools->GetHtml("camp-email-payment", true);
            $body = $this->tools->ReplaceVariableData(array("contract" => $contract), $html->body);
            $body = str_replace("%CONTRACTURL%", site_url("szerkeszt/" . md5($contract->id . "#" . $contract->uniqID) . "/"), $body);

            $files = [];
            if (file_exists($_FILES["attachFile"]["tmp_name"])) {
                $fn = FCPATH . "tmp/" . $_FILES["attachFile"]["name"];
                if (move_uploaded_file($_FILES["attachFile"]["tmp_name"], $fn)) {
                    $files[] = $fn;
                }
            }

            if (!$this->tools->SendHtmlMail($contract->contactEmail, $html->title, $body, false, $files)) {
                message_o("Hiba", "Hiba a fizetési információs levél kiküldésekor!", "error");
            } else {
                message_o("E-mail", "A megadott e-mail címre elküldtük a fizetési információkat...", "success");
                $this->db->update("tf_camp_contracts", array("paymentEmailSent" => date("Y-m-d H:i:s")), array("id" => $contract->id));
            }
            foreach ($files as $file) {
                @unlink($file);
            }
        }

        if (($this->input->post("confirmPayment")) && ($this->input->post("confirmPayment") == $this->input->post("confSend"))) {
            $contractID = $this->input->post("confirmPayment");
            $contract   = $this->db->from("tf_camp_contracts")->where("id", (int) $contractID)->get()->row();

            $html = $this->tools->GetHtml("camp-email-locationedit", true);
            $body = str_replace("%NAME%", $contract->contactName, $html->body);

            $body = str_replace("%URL%", site_url("belepes/" . $contractID . "/" . $contract->uniqID), $body);
            $body = str_replace("%LOGINURL%", site_url("belepes"), $body);
            $body = str_replace("%HASH%", md5($contractID . $contract->uniqID), $body);

            $this->db->update("tf_camp_contracts", array("contractPaid" => date("Y-m-d H:i:s")), array("id" => $contract->id));
            if (!$this->tools->SendHtmlMail($contract->contactEmail, $html->title, $body, false, $files)) {
                message_o("Hiba", "Hiba a táborhely szerkesztési információs levél kiküldésekor!", "error");
            } else {
                message_o("E-mail", "A megadott e-mail címre elküldtük a táborhely szerkesztési információkat...", "success");
                $this->db->update("tf_camp_contracts", array("campEditSent" => date("Y-m-d H:i:s")), array("id" => $contract->id));
            }
            foreach ($files as $file) {
                @unlink($file);
            }
        }

        if (($this->input->post("confirmFinalize")) && ($this->input->post("confirmFinalize") == $this->input->post("confFinalize"))) {
            $contractID = $this->input->post("confirmFinalize");
            $contract   = $this->db->from("tf_camp_contracts")->where("id", (int) $contractID)->get()->row();
            
            $this->db->update("tf_camp_contracts", array("closed" => 1, "publicationTime" => date("Y-m-d H:i:s")), array("id" => $contract->id));
            $this->db->update("tf_camps", array("closed" => 1, "published" => 1), array("contractID" => $contract->id));

            $html = $this->tools->GetHtml("camp-email-finalize", true);
            $body = str_replace("%NAME%", $contract->contactName, $html->body);

            $url       = "";
            $locations = $this->db->from("tf_camps")->where("contractID", $contractID)->order_by("name")->get()->result();
            foreach ($locations as $loc) {
                $url .= site_url("tabor/" . $loc->id . "/" . $loc->alias) . "<br>\n";
            }
            $body = str_replace("%URL%", $url, $body);

            if (!$this->tools->SendHtmlMail($contract->contactEmail, $html->title, $body, false, $files)) {
                message_o("Hiba", "Hiba a táborhely publikálási információs levél kiküldésekor!", "error");
            } else {
                message_o("E-mail", "A megadott e-mail címre elküldtük a táborhely publikálási információkat...", "success");
            }
        }

        $this->load->view("backend/figyelo/camps/sendpaymentemail");
        $this->load->view("backend/figyelo/camps/confirmpayment");
        $this->load->view("backend/figyelo/camps/finalcheck");

        $this->load->view("backend/footer");
    }

    public function locations()
    {
        $this->load->view("backend/header");
				
        if ($this->input->post("new_name")) {
            $data = array(
                "name"            => $this->input->post("new_name", true),
                "alias"           => $this->tools->toSlug($this->input->post("new_name", true)),
                "stateID"         => (int) $this->input->post("new_stateID"),
                "published"       => (int) $this->input->post("new_published")
            );
			$this->db->insert("tf_camp_locations", $data);
			
            message_o("", "Új bejegyzés felrögzítve</a>", "success");
		}
		
        $this->load->view("backend/figyelo/camps/locations");
        $this->load->view("backend/footer");
    }

    public function locationedit()
    {
        $this->load->view("backend/header");
		
        if ($this->input->post("name")) {
			$locID    = (int) filter_var($this->uri->segment(4));
            $data = array(
                "name"            => $this->input->post("name", true),
                "alias"           => $this->tools->toSlug($this->input->post("alias", true)),
                "stateID"         => (int) $this->input->post("stateID"),
                "published"       => (int) $this->input->post("published")
            );
			$this->db->update("tf_camp_locations", $data, array("id" => $locID));
			
            message_o("", "Bejegyzés módosítva</a>", "success");
		}
		
        $this->load->view("backend/figyelo/camps/location-edit");
        $this->load->view("backend/footer");
    }

    public function members()
    {
        $this->load->view("backend/figyelo/camps/camp-members");
    }

    public function excel()
    {
        $this->load->model("m_fcamp");
        $this->load->view("backend/figyelo/camps/camps-list-excel");
    }

    public function index()
    {
        $this->load->view("backend/header");
        $this->load->view("backend/figyelo/camps/listall");
        $this->load->view("backend/footer");
    }

    public function contractpdf()
    {
        $this->load->model("m_fcamp");
        $contractID = $this->uri->segment(4);

        $contract = $this->db->from("tf_camp_contracts")->where("id", $contractID)->get()->row();

        $fn = $this->m_fcamp->generateContract($contract);

        header("Content-type: application/pdf");
        header("Content-Disposition: inline; filename=szerzodes.pdf");
        @readfile($fn);

        unlink($fn);
    }



}
