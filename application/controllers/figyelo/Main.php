<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Main extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata("user_id")) {
            redirect("backend/login");
        }
    }

    public function index()
    {
        $this->load->view("backend/header");
        $this->load->view("backend/footer");
    }

    public function htmledit()
    {
        $this->load->view("backend/header");

        if ($this->uri->segment(4) && (is_numeric($this->uri->segment(4)))) {
            $this->load->view("backend/figyelo/htmledit_data");
        } else {
            $this->load->view("backend/figyelo/htmledit");
        }
        $this->load->view("backend/footer");
    }

    public function bannercupons()
    {
        $this->load->view("backend/header");
        $this->load->view("backend/figyelo/cuponlist");
        $this->load->view("backend/footer");
    }
    public function banners()
    {
        $this->load->view("backend/header");
        $this->load->view("backend/figyelo/banners");
        $this->load->view("backend/footer");
    }

    public function redirect()
    {
        $this->load->view("backend/header");
        $this->load->view("backend/figyelo/redirect");
        $this->load->view("backend/footer");
    }

    public function missing()
    {
        $this->load->view("backend/header");
        $this->load->view("backend/figyelo/missing");
        $this->load->view("backend/footer");
    }

    public function forms()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->get("job") == "save") {
                if ($this->input->get("id") == "new") {
                    $res = $this->db->insert("tf_forms", array(
                        "title"        => filter_input(INPUT_POST, "title", FILTER_SANITIZE_STRING),
                        "fillRedirect" => filter_input(INPUT_POST, "url", FILTER_SANITIZE_URL),
                        "sendEmail"    => filter_input(INPUT_POST, "email", FILTER_SANITIZE_EMAIL),
                    ));
                    $formID = $this->db->insert_id();
                } else {
                    $formID = (int) $this->input->get("id");
                    $res    = $this->db->update("tf_forms", array(
                        "title"        => filter_input(INPUT_POST, "title", FILTER_SANITIZE_STRING),
                        "fillRedirect" => filter_input(INPUT_POST, "url", FILTER_SANITIZE_URL),
                        "sendEmail"    => filter_input(INPUT_POST, "email", FILTER_SANITIZE_EMAIL),
                    ), array("id" => $formID));
                }

                $data     = array();
                $ids      = $this->input->post("id");
                $inputs   = $this->input->post("input");
                $types    = $this->input->post("type");
                $orders   = $this->input->post("order");
                $values   = $this->input->post("values");
                $class1   = $this->input->post("class1");
                $required = $this->input->post("required");
                foreach ($inputs as $id => $title) {
                    $data = array(
                        "id"        => $ids[$id],
                        "formID"    => $formID,
                        "ordering"  => $orders[$id],
                        "title"     => filter_var($title, FILTER_SANITIZE_STRING),
                        "type"      => filter_var($types[$id], FILTER_SANITIZE_STRIPPED),
                        "class1"    => filter_var($class1[$id], FILTER_SANITIZE_STRIPPED),
                        "defValues" => filter_var($values[$id], FILTER_SANITIZE_STRING),
                        "required"  => ($required[$id] == "on"),
                        "logging"   => ($types[$id] !== "info"),
                    );
                    if ($data["id"] == "new") {
                        $this->db->insert("tf_forms_fields", $data);
                    } else {
                        $this->db->update("tf_forms_fields", $data, array("id" => $data["id"]));
                    }
                }
                if ($res) {
                    echo "OK";
                } else {
                    echo "error";
                }
            } elseif ($this->input->get("job") == "remove") {
                $formID    = (int) $this->input->get("id");
                $elementID = (int) $this->input->get("eid");

                $data = $this->db->from("tf_forms_fields")->where("formID", $formID)->where("id", $elementID)->get()->row();
                if ($data) {
                    $this->db->delete("tf_forms_fields", array("formID" => $formID, "id" => $elementID));
                }
            } elseif ($this->input->get("job") == "load") {
                $formID = (int) $this->input->get("id");

                $data = $this->db->from("tf_forms")->where("id", $formID)->get()->row();
                if ($data) {
                    $tmp = array(
                        "id"    => $data->id,
                        "title" => $data->title,
                        "url"   => $data->fillRedirect,
                        "email" => $data->sendEmail,
                    );
                    $tmp["data"] = $this->db->from("tf_forms_fields")->where("formID", $formID)->order_by("ordering", "ASC")->get()->result();
                    echo json_encode($tmp);
                }
            }
        } else {
            if ($this->uri->segment(4)) {
                if ($this->input->get("export") == "xls") {
                    $this->load->view("backend/figyelo/forms-export");
                } else {
                    $this->load->view(
                        "backend/header",
                        array(
                            "css" => array(site_url("css/form_builder.css")),
                            "js"  => array(site_url("js/form_builder.min.js")),
                        )
                    );
                }
                $this->load->view("backend/figyelo/forms-edit");
            } else {
                $this->load->view("backend/header");
                $this->load->view("backend/figyelo/forms");
            }
            $this->load->view("backend/footer");
        }
    }

}
