<?php

defined('BASEPATH') or exit('No direct script access allowed');

class News extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata("user_id")) {
            redirect("backend/login");
        } else {
            $this->load->model("m_camp");
        }
    }

    public function categories()
    {
        $this->load->view("backend/header");
        $this->load->view("backend/figyelo/news/categories");
        $this->load->view("backend/footer");
    }

    public function edit()
    {
        $this->load->view("backend/header");
        $this->load->view("backend/figyelo/news/edit");
        $this->load->view("backend/footer");
    }
    
    function listall() {
        $this->load->view("backend/header");
        $this->load->view("backend/figyelo/news/list");
        $this->load->view("backend/footer");
    }

    function checkslug() {
        $this->load->view("backend/header");
        $this->load->view("backend/figyelo/news/checkslug");
        $this->load->view("backend/footer");
    }

    public function index()
    {
        redirect("backend/");
    }
}
