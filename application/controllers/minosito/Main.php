<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Main extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata("user_id")) {
            redirect("backend/login");
        }
    }

    public function htmledit()
    {
        $this->load->view("backend/header");
        $this->load->view("backend/minosito/htmledit");
        $this->load->view("backend/footer");
    }

    public function contractlist()
    {
        $this->load->view("backend/header");
        $this->load->view("backend/minosito/contract-list");
        $this->load->view("backend/footer");
    }

    public function questions()
    {
        $this->load->view("backend/header");
        $this->load->view("backend/minosito/questions");
        $this->load->view("backend/footer");
    }

    public function exportcontracts()
    {
        $this->load->view("backend/minosito/exportcontracts");
    }

    public function finalize()
    {
        $this->load->view("backend/header");

        if (($this->input->post("finalizeContract")) && ($this->input->post("confFinalize"))) {
            $contractID = $this->input->post("finalizeContract");
            $contract = $this->db->from("tm_contracts")->where("id", (int) $contractID)->get()->row();
            $contractUrl = site_url("szerkeszt/" . md5($contract->id . "#" . $contract->uniqID));

            $locations = $this->db->from("tm_contracts_locations")->where("contractID", $contract->id)->where("closed", 1)->get()->result();
            foreach ($locations as $loc) {
                $files = [];
                $html = $this->tools->GetHtml("email-contract-closed", true);

                $locURLID = $this->tools->ID2Text(array($contract->id, $loc->id));

                $body = str_replace("%NAME%", $contract->contactName, $html->body);
                $body = str_replace("%LINK%", site_url("taborok/" . $locURLID), $body);
                $body = str_replace("%LINK2%", $contractUrl . "/helyszinek", $body);
                $body = str_replace("%LEVEL%", $loc->level, $body);

                $files[] = $this->m_camp->generateLogo($loc->id);
                $files[] = $this->m_camp->generateBanner($loc->id);
                $files[] = $this->m_camp->GenerateDiplome($loc->id);
                $files[] = $this->m_camp->generateContractPDF($contract->id);

                if (!$this->tools->SendHtmlMail($contract->contactEmail, $html->title, $body, false, $files)) {
                    message_o("Hiba", "Hiba a diploma és a média anyagok kiküldésekor!", "error");
                }
                foreach ($files as $file) {
                    @unlink($file);
                }
            }
            message_o("Minősítési folyamat", "A megadott e-mail címre ({$contract->contactEmail}) elküldtük a minősítő oklevelet és a médiaanyagokat csatoltan. A folyamat ezzel lezárult.", "success");
            $this->db->update("tm_contracts", array("publicationTime" => date("Y-m-d H:i:s"), "closed" => 1, "visible" => 1), array("id" => $contract->id));
        }
        $this->load->view("backend/minosito/finalize");
        $this->load->view("backend/footer");
    }

    public function oldcontracts()
    {
        $this->load->view("backend/header");
        if (($this->input->post("deleteContract")) && ($this->input->post("confDelete"))) {
            $contractID = $this->input->post("confDelete");
            $contract = $this->db->from("tm_contracts")->where("id", (int) $contractID)->get()->row();

            $this->db->delete("tm_contracts_locations", array("contractID" => $contract->id));
            $this->db->delete("tm_contracts", array("id" => $contract->id));

            message_o("", "A szerződést és a hozzá tartozó adatokat a rendszer törölte!", "warning");
        }
        $this->load->view("backend/minosito/oldcontracts");
        $this->load->view("backend/footer");
    }

    public function sendpaymentemail()
    {
        $this->load->view("backend/header");
        if (($this->input->post("sendPayment")) && ($this->input->post("sendPayment") == $this->input->post("confSend"))) {
            $contractID = $this->input->post("sendPayment");
            $contract = $this->db->from("tm_contracts")->where("id", (int) $contractID)->get()->row();

            $html = $this->tools->GetHtml("email-payment-letter", true);
            $body = $this->tools->ReplaceVariableData(array("contract" => $contract), $html->body);
            $body = str_replace("%CONTRACTURL%", site_url("szerkeszt/" . md5($contract->id . "#" . $contract->uniqID) . "/"), $body);

            $files = [];
            if (file_exists($_FILES["attachFile"]["tmp_name"])) {
                $fn = FCPATH . "tmp/" . $_FILES["attachFile"]["name"];
                if (move_uploaded_file($_FILES["attachFile"]["tmp_name"], $fn)) {
                    $files[] = $fn;
                }
            }

            if (!$this->tools->SendHtmlMail($contract->contactEmail, $html->title, $body, false, $files)) {
                message_o("Hiba", "Hiba a fizetési információs levél kiküldésekor!", "error");
            } else {
                message_o("E-mail", "A megadott e-mail címre elküldtük a fizetési információkat...", "success");
                $this->db->update("tm_contracts", array("paymentEmailSent" => date("Y-m-d H:i:s")), array("id" => $contract->id));
            }
            foreach ($files as $file) {
                @unlink($file);
            }
        }
        $this->load->view("backend/minosito/sendpaymentemail");
        $this->load->view("backend/footer");
    }

    public function checkpayment()
    {
        $this->load->view("backend/header");
        if (($this->input->post("checkPayment")) && ($this->input->post("checkPayment") == $this->input->post("confPayment"))) {
            $contract = $this->db->from("tm_contracts")->where("id", (int) $this->input->post("confPayment"))->get()->row();

            $this->db->update("tm_contracts", array("contractPaid" => date("Y-m-d H:i:s")), array("id" => $contract->id));

            $html = $this->tools->GetHtml("email-location-edit", true);
            $body = $this->tools->ReplaceVariableData(array("contract" => $contract), $html->body);
            $body = str_replace("%CONTRACTURL%", site_url("szerkeszt/" . md5($contract->id . "#" . $contract->uniqID) . "/"), $body);

            $files = [];
            if (file_exists($_FILES["attachFile"]["tmp_name"])) {
                $fn = FCPATH . "tmp/" . $_FILES["attachFile"]["name"];
                if (move_uploaded_file($_FILES["attachFile"]["tmp_name"], $fn)) {
                    $files[] = $fn;
                }
            }

            if (!$this->tools->SendHtmlMail($contract->contactEmail, $html->title, $body, false, $files)) {
                message_o("Hiba", "Hiba a levél kiküldésekor!", "error");
            } else {
                message_o("E-mail", "A megadott e-mail címre ({$contract->contactEmail}) elküldtük a táborhely szerkesztési hivatkozást.", "info");
                $this->db->update("tm_contracts", array("campEditSent" => date("Y-m-d H:i:s")), array("id" => $contract->id));
            }
            foreach ($files as $file) {
                @unlink($file);
            }
        }
        $this->load->view("backend/minosito/checkpayment");
        $this->load->view("backend/footer");
    }

    public function editQuestionGroup()
    {
        $this->load->view("backend/header");
        $this->load->view("backend/minosito/edit-question-group");
        $this->load->view("backend/footer");
    }

    public function editQuestion()
    {
        $this->load->view("backend/header");
        $this->load->view("backend/minosito/edit-question");
        $this->load->view("backend/footer");
    }

    public function editcontract()
    {
        $this->load->view("backend/header");
        $this->load->view("backend/minosito/edit-contract");
        $this->load->view("backend/footer");
    }

    public function editlocation()
    {
        $this->load->view("backend/header");
        $this->load->view("backend/minosito/edit-location");
        $this->load->view("backend/footer");
    }

    public function locationTest()
    {
        $this->load->view("frontpage/header");
        $this->load->view("backend/minosito/question-test");
        $this->load->view("frontpage/footer");
    }

    public function login()
    {
        $this->load->view("backend/minosito/login");
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect("backend/minosito/");
    }

    public function cuponlist()
    {
        $this->load->view("backend/header");
        $this->load->view("backend/minosito/cuponlist");
        $this->load->view("backend/footer");
    }

    public function index()
    {
        $this->load->view("backend/header");
        $this->load->view("backend/footer");
    }

/*
public function gendiplome()
{
$locationID = (int)$this->uri->segment(4);

$location = $this->db->from("tm_contracts_locations")->where("id", $locationID)->get()->row();
if ($location) {
$fn = $this->m_camp->GenerateDiplome($locationID);

header("Content-type: application/pdf");
header("Content-Disposition: inline; filename=taborminosito_diploma.pdf");
@readfile($fn);

unlink($fn);
} else {
echo "mifak ". $locationID;
}

}
 */

    public function test_calc()
    {
        echo "<pre>\n";

        $locID = (int) $this->uri->segment(4);

        $total = $this->m_camp->calcCampPoints($locID, $this->input->get("update"));
        print_r($total);
        echo "done";
    }

    public function banners()
    {
        $this->load->view("backend/header");
        $this->load->view("backend/minosito/banners");
        $this->load->view("backend/footer");
    }    
}
