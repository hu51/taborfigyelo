<?php

class Tools
{

    public function getIP()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip_address = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip_address = $_SERVER['REMOTE_ADDR'];
        }
        return $ip_address;
    }

    public function ResizePic($srcName, $trgName, $width = 200, $height = 200, $strech = false)
    {
        $CI = &get_instance();

        if (file_exists($srcName)) {
            $CI->load->library('image_lib');

            $config['image_library']  = 'gd2';
            $config['source_image']   = $srcName;
            $config['new_image']      = $trgName;
            $config['create_thumb']   = false;
            $config['maintain_ratio'] = (!$strech);
            //            $config['quality'] = '100%;
            $config['width']  = $width;
            $config['height'] = $height;

            $CI->image_lib->clear();
            $CI->image_lib->initialize($config);
            if (!$CI->image_lib->resize()) {
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    public function message($title, $text, $type = "info")
    {
        if ($type == "error") {
            $type = "danger";
        }
        $data["title"] = $title;
        $data["text"]  = $text;
        $data["type"]  = $type;

        $ci = &get_instance();
        $ci->load->view('message', $data);
    }

    public function SendHtmlMail($to, $subject, $message, $options = false, $attachments = false, $template = true)
    {
        $ci = &get_instance();
        $ci->load->library('email');
        
        $ci->email->set_mailtype("html");
        if (isset($options["from-address"])) {
            $ci->email->from($options["from-address"], $options["from-name"]);
        } else {
            $ci->email->from($this->GetSetting("email-from-address"), $this->GetSetting("email-from-title"));
        }
        $ci->email->subject($subject);

        //TEST MODE
        if ($this->GetSetting("email-testmode")) {
            $message = "*** TEST MODE ***<br>"
            ."Orig TO: ".  print_r($to, true)."<br>"     
            ."Orig CC: ".  print_r($options["cc"], true)."<br>"     
            ."Orig Reply: ".  print_r($options["reply_to"], true)."<br>"     
            ."<hr>"
            .$message;

            $ci->email->to($this->GetSetting("email-testmode-addres"));
        } else {
            $ci->email->to(rtrim($to, ", "));

            if (isset($options["reply_to"])) {
                $ci->email->reply_to($options["reply_to"]);
            }
            if (isset($options["reply-to"])) {
                $ci->email->reply_to($options["reply-to"]);
            }
            if (isset($options["cc"])) {
                $ci->email->cc($options["cc"]);
            }
            if (isset($options["bcc"])) {
                $ci->email->bcc($options["bcc"]);
            }
        }
        if (strip_tags($message) == $message) {
            $message = nl2br(trim($message));
        }

        if (is_array($attachments)) {
            foreach ($attachments as $file) {
                if (file_exists($file)) {
                    $ci->email->attach($file);
                }
            }
        }

        if ($template) {
            $html = file_get_contents(APPPATH . "/views/mail_template.html");
            $html = trim(str_replace("{BODY}", $message, $html));
        } else {
            $html = $message;
        }
        $html = str_replace("{BASE_URL}", BASE_URL(), $html);
        $html = str_replace("{SUBJECT}", $subject, $html);
        $html = str_replace("{DATE}", date("Y-m-d"), $html);
        $html = str_replace("{TIME}", date("H:i"), $html);
        //-----------------------

//        $html = auto_link($html, 'url', true);
        $html = str_replace("{SITEURL}", site_url(), $html);
        $ci->email->message($html);

        $ret = $ci->email->send();
        $ci->email->clear(true);
        return $ret;
    }

    public function ShortCode($len = 10)
    {
        return substr(base_convert(microtime(), 10, 32), (0 - $len));
    }

    public function InsertHtmlForm($html)
    {
        $html = preg_replace("/\%@([^\%]+)\%/m", "<input type='text' class='form-control w300 required' name='$1' value='%$1%'/>\n", $html);
        $html = preg_replace("/\%#([^\%]+)\%/m", "<input type='text' class='form-control w300' name='$1' value='%$1%'/>\n", $html);
        return $html;
    }

    public function GetHtml($slug, $full = false, $pre = "tf")
    {
        $ci = &get_instance();
        if ($full) {
            return $ci->db->from($pre . "_htmls")->where("id", (int) $slug)->or_where("slug", $slug)->get()->row();
        } else {
            return $ci->db->from($pre . "_htmls")->where("id", (int) $slug)->or_where("slug", $slug)->get()->row()->body;
        }
    }

    public function GetSetting($slug, $full = false, $pre = "tf")
    {
        $ci = &get_instance();
        if ($full) {
            return $ci->db->from($pre . "_settings")->where("slug", $slug)->get()->row();
        } else {
            return $ci->db->from($pre . "_settings")->where("slug", $slug)->get()->row()->value;
        }
    }

    public function GeneratePDF($html, $filename = false, $title = "Dokumentum")
    {
        require_once APPPATH . 'third_party/tcpdf/tcpdf.php';

        if (!$filename) {
            $filename = FCPATH . "tmp/temp.pdf";
        }

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'URF-8', false);
        $pdf->SetCreator("TáborMinősítő.HU");
        $pdf->SetAuthor("TáborMinősítő.HU");
        $pdf->SetTitle($title);
        //$pdf->SetSubject("VÁLLALKOZÁSI SZERZŐDÉS");

        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->SetFont('freesans', '', 11);

        $pdf->AddPage();
        $pdf->setPageMark();
        $html = str_replace("%BASE_URL%", base_url(), $html);

        $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->Output($filename, 'F');
    }

    public function HtmlArticle($slug)
    {
        ob_start();
        $html = $this->GetHtml($slug, true);
        ?>
	        <article>
	            <div class="title"><?=$html->title;?></div>
	            <div class="body">
	                <?=$html->body;?>
	            </div>
	        </article>

        <?php
        return ob_get_clean();
    }

    public function ReplaceVariableData($params, $html)
    {
        foreach ($params as $keyg => $data) {
            if (is_array($data) || is_object($data)) {
                foreach ($data as $key => $val) {
                    if (!is_array($val)) {
                        $html = preg_replace("/\%(?:[@#]|)" . $keyg . "([\._])" . $key . "\%/mi", $val, $html);
                        //$html = str_ireplace("%".$keyg.".".$key."%", $val, $html);
                    }
                }
            } else {
                $html = preg_replace("/\%(?:[@#]|)" . $keyg . "\%/mi", $data, $html);
                //$html = str_ireplace("%".$keyg."%", $data, $html);
            }
        }
        return $html;
    }

    public function ID2Text($var)
    {
        $out = "";
        foreach ((array) $var as $val) {
            $out .= "-" . strrev(strtoupper(str_pad(base_convert($val, 10, 34), 4, "0", STR_PAD_LEFT)));
        }
        return ltrim($out, "-");
    }

    public function Text2ID($var)
    {
        $out = [];
        foreach (explode("-", $var) as $val) {
            $out[] = (int) base_convert(ltrim(strtolower(strrev($val)), "0"), 34, 10);
        }
        return $out;
    }

    public function bs_icon_checkbox($name, $defValue = 1, $values = false, $class = "")
    {
        if ($values == false) {
            $values = array(
                "1" => "<span class='glyphicon glyphicon-ok-circle'></span>",
                "0" => "<span class='glyphicon glyphicon-ban-circle'></span>",
            );
        }
        return $this->bs_checkbox($name, $defValue, $values, $class);
    }

    public function bs_checkbox($name, $defValue = 1, $values = false, $class = "")
    {
        if ($values == false) {
            $values = array(
                "1" => "Igen",
                "0" => "Nem",
            );
        }

        ob_start();

        ?>
	        <div class="btn-group" data-toggle="buttons">
	            <?php foreach ($values as $id => $val): ?>
	                <label class="btn btn-sm btn-default <?=($defValue == $id) ? " active" : "";?>">
	                    <input type="radio" name="<?=$name;?>" value="<?=$id;?>" <?=($defValue == $id) ? " checked" : "";?> <?php if (!empty($class)): ?>class="<?=$class;?> <?=$class;?>_<?=$id;?>"<?php endif;?> autocomplete="off"> <?=$val;?>
                </label>
            <?php endforeach;?>
        </div>
        <?php

        return ob_get_clean();
    }

    public function bs_select($name, $defValue = 1, $values = false, $class = "", $skipID = null)
    {
        ob_start();

        ?>
        <select name="<?=$name;?>" id="<?=$name;?>" class="<?=$class;?>">
            <?php

        foreach ($values as $id => $title) {
            if ($skipID !== $id) {
                if ($id == $defValue) {
                    echo "<option value='{$id}' selected='selected'>{$title}</option>\n";
                } else {
                    echo "<option value='{$id}'>{$title}</option>\n";
                }
            }
        }

        ?>
        </select>
        <?php

        return ob_get_clean();
    }

    public function bs_listbox($name, $defValue = 1, $values = false, $class = "", $skipID = null)
    {
        ob_start();

        ?>
        <select name="<?=$name;?>" id="<?=$name;?>" class="<?=$class;?>" size="10">
            <?php

        foreach ($values as $id => $title) {
            if ($skipID !== $id) {
                if ($id == $defValue) {
                    echo "<option value='{$id}' selected='selected'>{$title}</option>\n";
                } else {
                    echo "<option value='{$id}'>{$title}</option>\n";
                }
            }
        }

        ?>
        </select>
        <?php

        return ob_get_clean();
    }

    public function bs_multiselect($name, $defValue = 1, $values = false, $class = "")
    {
        $height = count($values) * 25;

        ob_start();

        ?>
        <div class="form-control" style="min-height: 50px; max-height: 300px; height: <?=$height;?>px; padding: 5px 10px; overflow-y: scroll;">
            <?php foreach ($values as $id => $title) {
                if ((is_array($defValue)) && (in_array($id, $defValue))) {
                    $chk = true;                
                } else {
                    $chk = false; 
                }
                echo form_checkbox($name, $id, $chk, $class);
                echo $title . "<br>\n";
            } ?>
        </div>
        <?php

        return ob_get_clean();
    }

    public function pagination($base_url, $all_rows, $page_rows = 20, $page_current = 1)
    {
        $ci = &get_instance();

        $page_max = ceil($all_rows / $page_rows);
        if ($page_current > $page_max) {
            $page_current = $page_max;
        }
        if ($page_current < 1) {
            $page_current = 1;
        }

        ob_start();

        $base_url = $base_url . "?";
        foreach ($ci->input->get() as $param => $value) {
            if ($param !== "oldal") {
                $base_url = $base_url . $param . "=" . $value . "&amp;";
            }
        }
        $base_url = $base_url . "oldal=";

        ?>
        <nav>
            <ul class="pagination">
                <?php if ($page_current > 1): ?>
                    <li class="page-item"><a class="page-link" href="<?=base_url($base_url . ($page_current - 1));?>">Előző</a></li>
                <?php endif;

        if ($page_max > 10) {
            $start = min($page_max - 10, max(1, $page_current - 5));

            if ($start > 1): ?>
                        <li class="page-item"><a class="page-link" href="<?=base_url($base_url . 1);?>">#1</a></li>
                    <?php endif;
            for ($p = $start; $p <= $start + 10; $p++): ?>
                        <?php if ($page_current == $p): ?>
                            <li class="page-item active">
                        <?php else: ?>
                            <li class="page-item">
                        <?php endif;?>
                        <a class="page-link" href="<?=base_url($base_url . $p);?>"><?=$p;?></a></li>
                    <?php endfor;?>

                    <?php if ($start < $page_max - 10): ?>
                        <li class="page-item"><a class="page-link" href="<?=base_url($base_url . $page_max);?>">#<?=$page_max;?></a></li>
                    <?php endif;
        } else {
            for ($p = 1; $p <= $page_max; $p++): ?>
                        <?php if ($page_current == $p): ?>
                            <li class="page-item active">
                        <?php else: ?>
                            <li class="page-item">
                        <?php endif;?>
                        <a class="page-link" href="<?=base_url($base_url . $p);?>"><?=$p;?></a></li>
                    <?php endfor;
        }

        if ($page_current < $page_max): ?>
                    <li class="page-item"><a class="page-link" href="<?=base_url($base_url . ($page_current + 1));?>">Következő</a></li>
                <?php endif;?>
            </ul>
        </nav>
        <?php

        $html = ob_get_clean();

        if ($page_max == 1) {
            $html = "<br>";
        }

        return array(
            "current" => $page_current,
            "from"    => ($page_current - 1) * $page_rows,
            "rows"    => $page_rows,
            "max"     => $page_max,
            "html"    => $html,
        );
    }

    public function passgen($length = 9, $specChars = true, $add_dashes = false)
    {
        $sets   = array();
        $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        $sets[] = '23456789';

        if ($specChars) {
            $sets[] = '!@#$%&*?';
        }
        $all      = '';
        $password = '';
        foreach ($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        $all = str_split($all);
        for ($i = 0; $i < $length - count($sets); $i++) {
            $password .= $all[array_rand($all)];
        }
        $password = str_shuffle($password);
        if (!$add_dashes) {
            return $password;
        }
        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while (strlen($password) > $dash_len) {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;
        return $dash_str;
    }

    public function getUserIP()
    {
        // Get real visitor IP behind CloudFlare network
        if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
            $_SERVER['REMOTE_ADDR']    = $_SERVER["HTTP_CF_CONNECTING_IP"];
            $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
        }
        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = $_SERVER['REMOTE_ADDR'];

        if (filter_var($client, FILTER_VALIDATE_IP)) {
            $ip = $client;
        } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
            $ip = $forward;
        } else {
            $ip = $remote;
        }
        return $ip;
    }

    public function toSlug($str)
    {
        return url_title(strtolower(convert_accented_characters($str)));
    }

    public function HuDate($date, $short = false, $stripYear = false)
    {
        $dt = strtotime($date);

        if ($short) {
            $m = array('dec.', 'jan.', 'feb.', 'márc.', 'ápr.', 'máj.', 'jún.', 'júl.', 'aug.', 'szept.', 'okt.', 'nov.', 'dec.');
        } else {
            $m = array('december', 'január', 'február', 'március', 'április', 'május', 'június', 'július', 'augusztus', 'szeptember', 'október', 'november', 'december');
        }
     
        if ($stripYear) {
            return sprintf("%s %d.", $m[(int) date("m", $dt)], date("d", $dt));
        } else {
            return sprintf("%d. %s %d.", date("Y", $dt), $m[(int) date("m", $dt)], date("d", $dt));
        }
    }

    public function set_value($field = '', $default = '')
    {
        $CI = &get_instance();

        $value = (isset($CI->form_validation) && is_object($CI->form_validation) && $CI->form_validation->has_rule($field))
        ? $CI->form_validation->set_value($field, $default)
        : $CI->input->post($field, false);

        isset($value) or $value = ($CI->input->get($field) === null ? $default : $CI->input->get($field));
        return (function_exists("html_escape")) ? html_escape($value) : $value;
    }

    public function showBanners($typeID, $num = 1, $prefix = "tf_", $wrapStart = "<div class='banner'>", $wrapEnd = "</div>")
    {
        $ci = &get_instance();

        $num = min(6, max(1, (int) $num));

        $btype = $ci->db->from($prefix . "banner_types")->where("id", $typeID)->get()->row();
        $out = "";

        $ci->db->from($prefix . "banners");
        $ci->db->where("(fromDate is NULL or fromDate <= NOW())");
        $ci->db->where("(endDate is NULL or endDate >= NOW())");
        $ci->db->where("(maxHits is NULL or maxHits = 0 or maxHits > hits)");
        $ci->db->where("enabled", 1);
        $ci->db->where("typeID", $typeID);
        $ci->db->order_by("rand()");
        $ci->db->limit($num);
        $res = $ci->db->get()->result();
       
        foreach ($res as $data) {
            $ci->db->set('hits', 'hits+1', false);
            $ci->db->where("id", $data->id);
            $ci->db->update($prefix . "banners");

            $out .= $wrapStart . "\n";
            $out .= "<a href='" . $data->url . "' target='_blank'><img src='" . base_url("images/banners/" . $data->image) . "' ";
            if (!is_null($btype->height)) {
                $out .= " height='" . $btype->height . "' ";
            }
            if (!is_null($btype->width)) {
                $out .= " width='" . $btype->width . "' ";
            }
            $out .= "></a>\n";
            $out .= $wrapEnd . "\n";

        }
        return $out;
//        return "<!-- ".$out." -->";
    }

    public function buildInlineForm($body)
    {
        $ci = &get_instance();

        if (preg_match("/\[form id=(\d*)\]/", $body, $match)) {
            $form_data = $ci->db->from("tf_forms")->where("id", (int) $match[1])->get()->row();
            if ($form_data) {
                if ($ci->input->post("send-inline-form")) {
                    $formID = (int) $ci->input->post("formID");

                    $fields = json_decode($form_data->data);
                    $data   = array(
                        "formID"   => $formID,
                        "datetime" => date("Y-m-d H:i:s"),
                        "ip"       => $this->getIP(),
                    );
                    if ($ci->db->insert("tf_forms_records", $data)) {
                        $html = $this->GetHtml("inline-form-email");

                        $html = str_ireplace("#FORMTITLE#", $form_data->title, $html);
                        $html = str_ireplace("#URL#", current_url(), $html);
                        
                        $mailBody = "<table border='1'>\n";
                        
                        $recordID = $ci->db->insert_id();
                        $input    = $ci->input->post("formfield");
                        $emailValue = false;
                        foreach ($input as $id => $val) {
                            $field = $ci->db->from("tf_forms_fields")->where("id", $id)->get()->row();

                            if (is_array($val)) {
                                $val = filter_var(implode(";", $val), FILTER_SANITIZE_STRING);
                            } else {
                                $val = filter_var($val, FILTER_SANITIZE_STRING);
                            }
                            $data = array(
                                "formID"   => $formID,
                                "recordID" => $recordID,
                                "fieldID"  => $id,
                                "title"    => $field->title,
                                "value"    => $val,
                            );
                            $ci->db->insert("tf_forms_records_data", $data);
                            
                            $mailBody .= "<tr><td>" . $data["title"] . "</td><td>" . implode("<br>", explode(";",$data["value"])) . "</td></tr>\n";

                            if (filter_var($val, FILTER_VALIDATE_EMAIL) && ($emailValue == false)) {
                                $emailValue = $val;
                            }
                        }
                        $html = str_ireplace("#DATA#", $mailBody, $html);
                        message_o("", "Köszönjük az űrlap kitöltését, hamarosan jelentkezünk!<br>Addig is <a href='".site_url("taborok")."'>böngésszen</a> táboraink között, és foglalja le gyerekének a tábort mielőbb!", "success");

                        if (!empty($form_data->sendEmail)) {
                            $mailBody .= "</table>\n";
                            if ($emailValue !== false) {
                                $this->SendHtmlMail($form_data->sendEmail, "Ürlap adatok kitöltve :: ".$form_data->title, $html, array("reply-to" => $emailValue));
                            } else {
                                $this->SendHtmlMail($form_data->sendEmail, "Ürlap adatok kitöltve :: ".$form_data->title, $html);
                            }
                        }

                        if (!empty($form_data->fillRedirect)) {
                            echo anchor($form_data->fillRedirect, "Tovább &raquo", " class='btn btn-primary'");
                        }
                    } else {
                        message_o("HIBA", "Hiba az adatok metnésekor!", "warning");
                    }
                } else {

                    $html = "";
                    if ($ci->session->userdata("user_isAdmin")) {
                        $html .= anchor(site_url("figyelo/main/forms/".$form_data->id), "<button class='btn btn-sm btn-primary'>Ürlap szerekesztése</button>", " target='_blank' ");
                    }
                    $ci->session->userdata("formID", $form_data->id);
                    $formID = $form_data->id;
                    $html   .= "<br style='clear: both;'><div class='inline-page-form'>"
                    . "<form method='post' action='' class='form form-horizontal'>\n"
                    . "<input type='hidden' name='formID' value='" . $form_data->id . "' />\n";

                    $fields = $ci->db->from("tf_forms_fields")->where("formID", $formID)->order_by("ordering")->get()->result();
                    foreach ($fields as $id => $data) {
                        if (empty($data->defValues)) {
                            $values = false;
                        } else {
                            $value_list = explode(PHP_EOL, trim($data->defValues));
                            if ($value_list > 1) {
                                $firsts_row = each($value_list);
                                $values     = [];
                                if (!empty($firsts_row)) {
                                    foreach ($value_list as $opt_row) {
                                        $opt = explode(";", $opt_row);
                                        if (count($opt) == 1) {
                                            $values[] = trim(reset($opt));
                                        } else {
                                            $values[trim($opt[1])] = trim($opt[2]);
                                        }
                                    }
                                }
                            } else {
                                $values = array(reset($value_list));
                            }
                        }
                        if ($data->logging == "0") {
                            $html .= "<div class='col-md-12 m5 text-info " . $data->class1 . "'>" . nl2br($data->title) . "</div>\n";
                        } else {
                            $html .= "<div class='col-md-12 m5'>\n"
                            . "<label class='col-md-4'>" . rtrim($data->title, ": ") . ":</label>\n"
                                . "<div class='col-md-8'>\n";

                            if ($data->required) {
                                $required = " required='required' ";
                                $data->class1 .= " required";
                            } else {
                                $required = "";
                            }

                            switch ($data->type) {
                                case "radio":{
                                        foreach ($values as $id => $val) {
                                            if (count($values) > 4) {
                                                $html .= "<div class='col-md-6'>";
                                            }
                                            $html .= "<input type='radio' name='formfield[" . $data->id . "]' ".$required." value='" . trim($val) . "'>" . trim($val) . "<br>\n";
                                            if (count($values) > 4) {
                                                $html .= "</div>";
                                            }
                                        }
                                    }break;
                                case "checkbox":{
                                        foreach ($values as $id => $val) {
                                            if (count($values) > 4) {
                                                $html .= "<div class='col-md-6'>";
                                            }
                                            $html .= "<input type='checkbox' name='formfield[" . $data->id . "][]' ".$required." value='" . trim($val) . "'>" . trim($val) . "<br>\n";
                                            if (count($values) > 4) {
                                                $html .= "</div>";
                                            }
                                        }
                                    }break;
                                case "select":{
                                        $html .= "<select name='formfield[" . $data->id . "]' ".$required." class='form-control " . $data->class1 . "'>";
                                        foreach ($values as $id => $val) {
                                            $html .= "<option>" . trim($val) . "</option>\n";
                                        }
                                        $html .= "</select>\n";
                                    }break;
                                case "textarea":{
                                        $html .= "<textarea name='formfield[" . $data->id . "]' ".$required." class='form-control " . $data->class1 . "'></textarea>\n";
                                    }break;

                                case "info":{
                                    }
                                    break;

                                default:{
                                        $html .= "<input type='text' name='formfield[" . $data->id . "]' ".$required." class='form-control " . $data->class1 . "'>\n";
                                    }
                                    break;
                            }
                            $html .= "</div>"
                                . "</div>";
                        }
                    }

                    $html .= "<button type='submit' name='send-inline-form' value='" . $form_data->id . "' class='btn btn-sm btn-primary'>Adatok elküldése</button>"
                        . "</form>\n"
                        . "</div><br style='clear: both;'>\n";
                    return str_ireplace($match[0], $html, $body);
                }
            } else {
                return str_ireplace($match[0], "", $body);
            }
        } else {
            return $body;
        }
    }

    public function changeSlug($table, $value, $id, $poz = false)
    {
        $value = $this->toSlug($value);

        $ci = &get_instance();
        if ($poz === false) {
            $v   = $value;
            $poz = 1;
        } else {
            $v = $value . "-" . $poz;
        }
        $data = $ci->db->from($table)->where("slug", $v)->where("id <>", $id)->order_by("id")->get()->row();
        if ($data) {
            return $this->changeSlug($table, $value, $id, $poz++);
        } else {
            $ci->db->update($table, array("slug" => $v), array("id" => $id));
            return $v;
        }
    }
}

date_default_timezone_set("Europe/Budapest");

function message($title, $text, $type = "info")
{
    $ci = &get_instance();
    return $ci->tools->message($title, $text, $type);
}

function message_o($title, $text, $type = "info")
{
    $ci = &get_instance();
    $ci->output->append_output($ci->tools->message($title, $text, $type));
}
