<?php

class M_Camp extends CI_Model 
{

    private $tree = [];
    private $themeBlocksSkip = [];
    
    public function generateLogo($locationID)
    {
        $location = $this->db->from("tm_contracts_locations")->where("id", $locationID)->get()->row();
        //$location = $this->db->from("tm_contracts")->where("id", $location->contractID)->get()->row();

        $locURLID = $this->tools->ID2Text(array($location->contractID, $location->id));
        $pic_text = "#" . $locURLID;

        $font_path = FCPATH . 'fonts/OpenSans-Bold.ttf';

        $fn = FCPATH . "tmp/rating_" . $location->contractID . "_" . $location->id . ".png";
        if (is_null($location->level) || $location->level == 0) {
            $ratePic = FCPATH . "images/tm_logo.png";
        } else {
            $ratePic = FCPATH . "images/tm_rate_" . (int) $location->level . ".png";
        }
        copy($ratePic, $fn);

        $image = imagecreatefrompng($fn);
        imagesavealpha($image, true);

        $white = imagecolorallocate($image, 255, 255, 255);
        $black = imagecolorallocate($image, 0, 0, 0);

        $this->imagettfstroketext($image, 16, 300, 270, 315, $white, $black, $font_path, $pic_text, 1);
        imagepng($image, $fn);
        imagedestroy($image);
        return $fn;
    }

    public function generateBanner($locationID)
    {
        $location = $this->db->from("tm_contracts_locations")->where("id", $locationID)->get()->row();
        //$location = $this->db->from("tm_contracts")->where("id", $location->contractID)->get()->row();

        $locURLID = $this->tools->ID2Text(array($location->contractID, $location->id));
        $pic_text = "#" . $locURLID;

        $font_path = FCPATH . 'fonts/OpenSans-Bold.ttf';

        if (is_null($location->level) || $location->level == 0) {
            $ratePic = FCPATH . "images/tm_logo.png";
        } else {
            $ratePic = FCPATH . "images/tm_rate_" . (int) $location->level . ".png";
        }
        
        //2
        $fn = FCPATH . "tmp/banner_" . $location->contractID . "_" . $location->id . ".png";
        $image = imagecreatefrompng(FCPATH . "images/tm_banner_empty.png");
        $white = imagecolorallocate($image, 255, 255, 255);
        $green = imagecolorallocate($image, 52, 124, 76);
        
        $logo = imagecreatefrompng($ratePic);
        $logo_width = imagesx($logo);
        $logo_height = imagesy($logo);
        imagecopy($image, $logo, 155, 60, 0, 0, $logo_width, $logo_height);

        $this->imagettfstroketext($image, 36, 0, 750, 500, $green, $white, $font_path, $pic_text, 2);
        imagepng($image, $fn);
        imagedestroy($image);
        return $fn;
    }

    public function generateContractPDF($contractID)
    {
        $contract = $this->db->from("tm_contracts")->where("id", $contractID)->get()->row();
        if ($contract) {
        	$arr = array(
                "CONTRACT" => $contract,
                "BASEPRICE" => $this->tools->GetSetting("camp-base-price"),
                "NAME" => $contract->contactName,
                "DATE" => date("Y. m. d.")
            );            
            $html = $this->tools->GetHtml("camp-contract", true);
            $body = $this->tools->ReplaceVariableData($arr, $html->body);

            $fn = FCPATH."tmp/contract_".$contractID.".pdf";

            $this->tools->GeneratePDF($body, $fn, "VÁLLALKOZÁSI SZERZŐDÉS");  
            return $fn;
        } else {
            return false;
        }        
    }

    public function watermarkImage()
    {
        if (file_exists($_FILES["newPic"]["tmp_name"])) {
            $locID = (int) $this->input->post("locID");
            $picPos = (int) $this->input->post("picPos");
            $picRate = (int) $this->input->post("picRate");

            $file = FCPATH . "tmp/" . uniqid() . ".png";

            $location = $this->db->from("tm_contracts_locations")->where("id", $locID)->where("closed", 1)->get()->row();
            if ($location) {
                $fn = $this->generateLogo($location->id);

                if ($_FILES["newPic"]["type"] == "image/jpeg") {
                    $image = imagecreatefromjpeg($_FILES["newPic"]["tmp_name"]);
                } else {
                    $image = imagecreatefrompng($_FILES["newPic"]["tmp_name"]);
                }
                $logo = imagecreatefrompng($fn);

                $image_width = imagesx($image);
                $image_height = imagesy($image);

                $logo_width = imagesx($logo);
                $logo_height = imagesy($logo);

                switch ($picRate) {
                    case "2": {
                            $percent = 0.4;
                            break;
                        }
                    case "3": {
                            $percent = 0.75;
                            break;
                        }
                    case "4": {
                            $percent = 0.9;
                            break;
                        }
                    default: {
                            $percent = 0.5;
                        }
                }

                $size = $image_width / $logo_width * $percent;
                if ($logo_height * $size > $image_height * $percent) {
                    $size = $image_height / $logo_height * $percent;
                }

                switch ($picPos) {
                    case "2": {
                            $x = $image_width -  (int) ($logo_width * $size);
                            $y = 1;
                            break;
                        }
                    case "3": {
                            $x = 1;
                            $y = $image_height - (int) ($logo_height * $size);
                            break;
                        }
                    case "4": {
                            $x = $image_width - (int) ($logo_width * $size);
                            $y = $image_height - (int) ($logo_height * $size);
                            break;
                        }

                    case "5": {
                            $x = 1;
                            $y = (int)(($image_height - ($logo_height * $size)) /2);
                            break;
                        }
                        case "6": {
                            $x = (int)($image_width - ($logo_width * $size));
                            $y = (int)(($image_height - ($logo_height * $size)) /2);
                            break;
                        }
                    default: {
                            $x = 1;
                            $y = 1;
                        }
                }

                imagecopyresampled($image, $logo, $x, $y, 0, 0, (int) $logo_width * $size, (int) $logo_height * $size, $logo_width, $logo_height);

                imagepng($image, $file);
                imagedestroy($image);

                unlink($_FILES["newPic"]["tmp_name"]);
                unlink($fn);

                header("Content-type: image/png");
                readfile($file);

                header('Expires: 0');
                header('Content-Length: ' . filesize($file));
        
                unlink($file);
                exit;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    private function imagettfstroketext(&$image, $size, $angle, $x, $y, &$textcolor, &$strokecolor, $fontfile, $text, $px)
    {
        for ($c1 = ($x - abs($px)); $c1 <= ($x + abs($px)); $c1++) {
            for ($c2 = ($y - abs($px)); $c2 <= ($y + abs($px)); $c2++) {
                imagettftext($image, $size, $angle, $c1, $c2, $strokecolor, $fontfile, $text);
            }
        }
        return imagettftext($image, $size, $angle, $x, $y, $textcolor, $fontfile, $text);
    }


    private function questionsDrawBlock($question, $location, $debug, $force = false)
    {
        if (!$force && in_array($question->id, $this->themeBlocksSkip)) {
            return;
        }

        $cls = "";
        $q_answered = $this->db->from("tm_con_loc_answers")->where("locationID", $location->id)->where("questionID", $question->id)->get()->result();                
        if ($q_answered) {
            $cls .= " answered ";
        }
        if (!isset($this->tree[$question->id])) {
            $this->tree[$question->id] = "";
        }

        if ($question->id == 91):?>
            <p class="text-info"><i class='glyphicon glyphicon-tent'></i> a sátor jellel megjelölt elemek számítanak be a tábortémák mennyiségébe.</p>
        <?php endif;
        echo "\n<div data-blockid='{$question->id}' data-reqID='" . $question->reqOptID. "' class='question_block question_block_{$question->id} grpbox_{$question->groupID} ".$cls;

        if ((!is_Null($question->reqOptID)) && (!empty($question->reqOptID))) {
            echo " reqbox_def reqbox_" . $this->tree[$question->id]. " '";
            if ($q_answered) {
                echo " style='display: block;' ";
            } else {
                echo " style='display: none;' ";
            }
        } else {
            echo "' style='display: block;' ";                    
        }
        echo ">\n";
        
        echo "<input type='hidden' class='questionVis' id='questionVis_".$question->id."'  name='questionVis[".$question->id."]' value='0' />\n";
                        
        if (file_exists(FCPATH."images/question".$question->id.".png")) {
            echo "<br style='clear: both;' /><a href='".base_url()."images/question".$question->id.".png' target='_blank'><img src='".base_url()."images/question".$question->id.".png' class='img-responsive' /></a><br style='clear: both;' />\n";
        }                

        echo "<label>" . $question->title;
        if ($debug) {
            echo " (@" . $question->id . ")";
        }
        echo "</label>\n";                

        if ((!is_Null($question->reqOptID)) && (!empty($question->reqOptID))) {
            echo "<div>";
            foreach (explode(",", $question->reqOptID) as $id) {
                $temp = $this->db->query("
            SELECT o.title as optionTitle, q.title as questionTitle
            FROM `tm_question_options` as o
            LEFT JOIN `tm_question` as q ON q.id = o.questionID
            WHERE o.id='" . $id . "'
            ; ")->row();

                echo "<span class='label label-info p5'>" . $temp->questionTitle . " -&raquo; " . $temp->optionTitle . "</span> ";
            }
            echo "</div><br>\n";
        }

        echo "<ul class='options'>\n";

        if ($question->multiple) {
            $options = $this->db->from("tm_question_options")->where("questionID", $question->id)->order_by("title, orderID")->get();
        } else {
            $options = $this->db->from("tm_question_options")->where("questionID", $question->id)->order_by("orderID, title")->get();
        }
        foreach ($options->result() as $option) {
            $o_answered = $this->db->from("tm_con_loc_answers")->where("locationID", $location->id)->where("answerID", $option->id)->get()->row();                
            echo "<li>";
            if ($debug) {
                echo "&nbsp;<span class='badge'>{$option->point} pt</span>";
            }
            
            $matches = $this->db->select("id")->from("tm_question")->like("CONCAT(',', reqOptID,',')", ",".$option->id.",")->get()->result();
            foreach ($matches as $next) {
                if (!isset($this->tree[$next->id])) {
                    if (!empty($this->tree[$question->id])) {
                        $this->tree[$next->id] = $this->tree[$question->id];
                    } else {
                        $this->tree[$next->id] = "";
                    }
                }
                $this->tree[$next->id] .= " reqbox_".$option->id." ";
            }

            $sel = "";
            if ($o_answered) {
                $sel = " checked='checked' ";                        
            }
            $class = "";
            if ($question->themeCounter == "1" || $option->themeCounter) {
                $class = " themeCounter";
            }
            if ($question->multiple) {
                echo "<input type='checkbox' {$sel} class='opt_set {$class}' id='opt_" . $option->id . "' data-optid='" . $option->id . "' name='opt[{$question->id}][]' value='{$option->id}'>";
            } else {
                echo "<input type='radio' {$sel} class='opt_set {$class}' id='opt_" . $option->id . "' data-optid='" . $option->id . "' name='opt[{$question->id}]' value='{$option->id}'>";
            }
            echo "&nbsp;{$option->title}";

            if ($question->themeCounter == "1" || $option->themeCounter) {
                echo "&nbsp;<i class='glyphicon glyphicon-tent text-info'></i>";
            }            

            if ($debug) {
                echo " (#{$option->id})";
            }

            if ($option->questionID == 91) {
                $tmp = $this->db->from("tm_question")->like("CONCAT(',', reqOptID,',')", ",".$option->id.",")->where("themeCounter", 1)->get()->row();
                if ($tmp) {
                    $this->themeBlocksSkip[] = $tmp->id;
                    $this->questionsDrawBlock($tmp, $location, $debug, true);
                }
            }
            echo "</li>\n";
        }
        echo "</ul><br>\n";
        echo "</div>\n";
    }


    public function buildQuestionList($location, $debug = false )
    {
        $this->tree = array();
        $this->themeBlocksSkip = []; 
        $readOnly = $location->closed;

        ?>
	    <h2>Kérdések</h2>
    	<div id="questions">
        <?php        

        $res0 = $this->db->query("SELECT * FROM `tm_question_group` ORDER BY `orderID`, `title`; ");
        foreach ($res0->result() as $group) {
            echo "<div class='questionGroup' id='grp{$group->id}'>";
            echo "<h4 class='alert alert-warning'>" . $group->title;
            if ($debug) {
                echo "&nbsp;(max: {$group->maxPoint})";
            }
            echo "</h4><br>\n";
                    
            $res = $this->db->query("SELECT * FROM `tm_question` WHERE `groupID`='{$group->id}' ORDER BY `orderID`, `id`, `title`; ");
            foreach ($res->result() as $question) {
                $this->questionsDrawBlock($question, $location, $debug);
            }
            echo "</div>\n";
        }
        ?>
        </div>

        <?php if($readOnly): ?>
            <div id="debug"></div>
        <?php endif; ?>
        <script>

            function SetStatus(txt) {
                $.notify(
                    txt,
                    {
                        type: "success",
                        placement: {
                            from: "top",
                            align: "left"
                        },
                        offset: 20,
                        delay: 1000
                    }
                );
            }

            const readOnly = <?= ($readOnly)?"true":"false"; ?>
            dataChanged = false;

            $("#zip").blur( function() {
                $.ajax({
                    url: "<?= base_url(); ?>ajax/decodeZip",
                    dataType: "json",
                    type: "POST",
                    data: { 'zip': $(this).val() },
                    success: function (data, textStatus) {
                        if (data.status == "ok") {
                            $("#city").val(data.data.city);
                            $("#county").val(data.data.county);
                        } else {
                            $("#city").val("-");
                            $("#county").val("-");
                        }
                    }
                });
                dataChanged = false;
            });

            function SendData()
            {            
                if (readOnly || dataChanged) {
                    data = $("#location_form").serialize();

                    $.ajax({
                        url: "<?= base_url(); ?>ajax/savePoints",
                        dataType: "json",
                        type: "POST",
                        data: data,
                        success: function (data, textStatus) {
                            if (data.status == "ok") {
                                if (!readOnly) {
                                    SetStatus("Adatok mentve");
                                }
                                if ($("#debug")) {
                                    $("#debug").html(data.info.replace(/\n/g, "<br />"));
                                }						
                            } else {
                                SetStatus(data.info);
                            }
                        },
                        error: function (jqXhr, textStatus, errorThrown) {
                            SetStatus("ERROR:", errorThrown);
                        }
                    });
                    dataChanged = false;
                }
                
                if (!readOnly) {
                    setTimeout(function(){ SendData(); }, 15000);	
                }
            }


            function OptChanged()
            {
                $(document).find(".question_block").each(function () {
                    var show = false; 
                    block = $(this);                     
                    if ($(this).data("reqid") !== "") {
                        opts = $(this).data("reqid")+'';
                        opts.split(",").forEach(function (item, index) {
                            chk = $("#opt_" +item).prop("checked");
                            if (chk) {
                                show = true;
                            }
                        });
                        if (!show) {
                            block.find(".opt_set").each(function () {
                                $(this).prop("checked", false);       
                            });      
                            block.hide();                                                  
                        }                    
                    }
                }); 

                if (readOnly) {
                    SendData();
                }
            };


            $(".opt_set").change(function (e) {              
                dataChanged = true;                

                chkBox = $(this);
                block = $(this).closest(".question_block");
                blockID = block.data("blockid");
                block.addClass("answered");                
                
                $(".questionVis").val("0");                
                $(document).find(".question_block:visible").each( function() {
                    $("#questionVis_" + $(this).data("blockid")).val("1");
                });            

                maxThemes = <?= $location->themesReal; ?>
                selThemes = $(document).find(".themeCounter:checkbox:checked").length
                
                if ((chkBox.prop("checked")) && (maxThemes < selThemes)) {
                    chkBox.prop("checked", false);
                    $.notify(
                        "Csak a megadott mennyiségű tábortéma választható ki! ("+maxThemes+" db)",
                        {
                            type: "danger",
                            placement: {
                                from: "top",
                                align: "center"
                            },
                            offset: 50,
                            delay: 2000
                        });
                    dataChanged = false;
                }  

                if (dataChanged) {
                    block.find(".opt_set:checked").each(function () {
                        other = ".reqbox_" +$(this).data("optid");  
                        $(other).show();                            
                    });

                    $(document).find(".question_block").each(function () {
                        var show = false; 
                        block = $(this);                     
                        if ($(this).data("reqid") !== "") {
                            opts = $(this).data("reqid")+'';
                            opts.split(",").forEach(function (item, index) {
                                chk = $("#opt_" +item).prop("checked");
                                if (chk) {
                                    show = true;
                                }
                            });
                            if (!show) {
                                block.find(".opt_set").each(function () {
                                    $(this).prop("checked", false);       
                                });      
                                block.hide();                                                  
                            }                    
                        }
                    });                  
                }

                if (readOnly) {
                    SendData();
                }
            });

            if (!readOnly) {
                $(document).on("click", "#setFinish", function () {
                    var allChecked = true;
                    $(document).find(".question_block:visible").each(function (index) {
                        db = $($(this)).find(".opt_set:checked");

                        if (db.length == 0) {
                            $(this).removeClass("answered");
                            allChecked = false;
                        }            
                        
                    });

                    dataChanged = true;
                    SendData();
                    if (allChecked) {
                        $.notify(
                        "Az adatok mentése után, bezárhatja ezt az ablakot. A minősítési folyamat megkezdéséhez, a helyszínek szerkesztését le kell zárni!",
                        {
                            type: "info",
                            placement: {
                                from: "top",
                                align: "left"
                            },
                            offset: 20,
                            delay: 5000
                        });
                    } else {
                        $.notify(
                        "Figyelem: Nincs minden kérdés kitöltve!",
                        {
                            type: "warning",
                            placement: {
                                from: "bottom",
                                align: "center"
                            },
                            offset: 50,
                            delay: 2000
                        });
                    }
                });
            }
            
            SendData();
            OptChanged();

        </script>        
        <?php
    }


    private function certificate_print_text($pdf, $x, $y, $align, $font='freeserif', $style, $size = 10, $text, $width = "") {
        $pdf->setFont($font, $style, $size);
        $pdf->SetXY($x, $y);
        $pdf->writeHTMLCell($width, 0, '', '', $text, 0, 0, 0, true, $align);
    }

    public function GenerateDiplome($locationID)
    {
        $location = $this->db->from("tm_contracts_locations")->where("id", $locationID)->get()->row();
        $contract = $this->db->from("tm_contracts")->where("id", $location->contractID)->get()->row();

        $locURLID = $this->tools->ID2Text(array($location->contractID, $location->id));
        $pic_text = "#" . $locURLID;

        if (is_null($location->level) || $location->level == 0) {
            $ratePic = FCPATH . "images/tm_logo.png";
        } else {
            $ratePic = FCPATH . "images/tm_rate_" . (int) $location->level . ".png";
        }    

        require_once APPPATH . 'third_party/tcpdf/tcpdf.php';

        $pdf = new TCPDF("L", PDF_UNIT, PDF_PAGE_FORMAT, true, 'URF-8', false);
        $pdf->SetCreator("TáborMinősítő.hu");
        $pdf->SetAuthor("TáborMinősítő.hu");
        $pdf->SetTitle("TáborMinősítő.hu Diploma");


        $pdf->SetProtection(array('modify'));
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->SetAutoPageBreak(false, 0);
        $pdf->AddPage();

        $font = TCPDF_FONTS::addTTFfont(FCPATH . 'fonts/OpenSans.ttf', "TrueTypeUnicode", "", 32);
        $fontbold = TCPDF_FONTS::addTTFfont(FCPATH . 'fonts/OpenSans-Bold.ttf', "TrueTypeUnicode", "", 32);
        // border

        $pdf->SetLineStyle(array('width' => 1.5, 'color' => array(52, 124, 76)));
        $pdf->Rect(10, 10, 277, 190);
        // create middle line border
        $pdf->SetLineStyle(array('width' => 0.2, 'color' => array(52, 124, 76)));
        $pdf->Rect(13, 13, 271, 184);
        // create inner line border
        $pdf->SetLineStyle(array('width' => 1.0, 'color' => array(52, 124, 76)));
        $pdf->Rect(16, 16, 265, 178);
    
        $seal = realpath($ratePic);        
        if (file_exists($seal)) {
            $pdf->Image($seal, 25, 125, 60, 60);
            
            $pdf->SetTextColor(52, 124, 76);             
            $this->certificate_print_text($pdf, 35, 180, 'L', $fontbold, '', 14, $pic_text);
        }
            
        
        $sig = realpath("./images/tbsign_trans.png");        
        if (file_exists($sig)) {
            $pdf->Image($sig, 180, 140, 84, 39);
            
            $pdf->SetLineStyle(array('width' => 0.3, 'color' => array(0,0,0)));
            $pdf->Line(180, 177, 260, 177);            
        
            $pdf->SetTextColor(0, 0, 0);      
            $this->certificate_print_text($pdf, 150, 180, 'C', $font, '', 12,  "Tóth Béla");
            $this->certificate_print_text($pdf, 150, 185, 'C', $font, '', 12,  "ügyvezető igazgató");
        }

        $x = 10;
        $y = 30;
        $pdf->SetTextColor(52, 124, 76);
        $this->certificate_print_text($pdf, $x, $y, 'C', $fontbold, '', 30, "Táborminősítő Oklevél");
        
        $pdf->SetTextColor(40, 40, 40);  
        $address = $location->zip.". ".$location->city.", ".$location->address;
        $this->certificate_print_text($pdf, $x+10, $y + 20, 'C', $font, '', 18, "Ezúton tanúsítjuk, hogy a(z) ".$contract->partnerName." által a ".$address." címen szervezett táborokat a Táborminősítő alapos vizsgálata alapján a", 250);              
        $this->certificate_print_text($pdf, $x+10, $y + 50, 'C', $fontbold, '', 18, "kiváló ".$location->level."  sátras vakációs lehetőségek közé soroltuk!", 250);   
        $this->certificate_print_text($pdf, $x+10, $y + 70, 'C', $font, '', 18, "A táborokat a Táborminősítő értékelte, és a szakmai követelményeknek megfelelőnek, jogilag rendezetten működőnek, és a szülők számára kifejezetten ajánlott tábornak ítélte.", 250);
        
        $this->certificate_print_text($pdf, 180, 130, 'L', $fontbold, '', 10,  "A minősítés érvényességi határa: ".date("Y. m. d.", strtotime("+1 YEAR")));
        
        $fn = FCPATH."tmp/diplome_".$locationID.".pdf";
        $pdf->Output($fn, 'F');
        return $fn;        
    }


    function calcCampPoints($locID, $updatePoints = false)
    {
        $groups = [];
        $levels = [];
        $res    = $this->db->query("SELECT * FROM `tm_settings` WHERE `slug` LIKE 'raiting-level-%' ORDER BY `slug`; ");
        foreach ($res->result() as $tmp) {
            $levels[substr($tmp->slug, -1)] = (int) $tmp->value;
        }

        $res = $this->db->query("SELECT * FROM `tm_question_group` ORDER BY `orderID`, `title`; ");
        foreach ($res->result_array() as $group) {
            $groups[$group["id"]]                = $group;
            $groups[$group["id"]]["questionNum"] = $this->db->from("tm_question")->where("groupID", $group["id"])->get()->num_rows();
        }

        $total         = array();
        $total["info"] = "\n";
        $tp            = 0;
        foreach ($groups as $grp) {
            $this->db->select("a.*, (SELECT MAX(POINT) FROM tm_question_options AS o WHERE o.questionID = q.id) as mx ");
            $this->db->from("tm_con_loc_answers as a");
            $this->db->join("tm_question as q", "q.id=a.questionID", "LEFT");
            $this->db->where("a.locationID", $locID);
            $this->db->where("q.groupID", $grp["id"]);
            $ans_all                       = $this->db->get();
            $groups[$grp["id"]]["ans_all"] = $ans_all->num_rows();

            $p = 0;
            foreach ($ans_all->result() as $tmp) {
                $p = $p + $tmp->mx;
            }
            $groups[$grp["id"]]["ans_points"] = $p;

            $this->db->select("a.*, o.point");
            $this->db->from("tm_con_loc_answers as a");
            $this->db->join("tm_question as q", "q.id=a.questionID", "LEFT");
            $this->db->join("tm_question_options as o", "o.id=a.answerID", "LEFT");
            $this->db->where("a.locationID", $locID);
            $this->db->where("q.groupID", $grp["id"]);
            $this->db->where("a.answerID <>0");
            $ans_real                       = $this->db->get();
            $groups[$grp["id"]]["ans_real"] = $ans_real->num_rows();

            $p = 0;
            foreach ($ans_real->result() as $tmp) {
                $p = $p + $tmp->point;
            }
            $groups[$grp["id"]]["ans_real_points"] = $p;

            if ($groups[$grp["id"]]["ans_points"] > 0) {
                $groups[$grp["id"]]["points"] = ($groups[$grp["id"]]["ans_real_points"] / $groups[$grp["id"]]["ans_points"] * 100);
            } else {
                $groups[$grp["id"]]["points"] = 0;
            }
            $total["info"] .= ucfirst(strtolower($grp["title"]))."\n"
                ."K: ".$groups[$grp["id"]]["ans_real"]." / ".$groups[$grp["id"]]["ans_all"].", "
                ."P: ".$groups[$grp["id"]]["ans_real_points"]." / ".$groups[$grp["id"]]["ans_points"]." "
                . " = ".numbeR_format($groups[$grp["id"]]["points"],2)."% \n";    

            $tp = $tp + $groups[$grp["id"]]["points"];

    /*                         
                if ((int) $grp["maxPoint"] > 0) {
                    $grp["points"] = min((int) $grp["maxPoint"], (int) $grp["points"]);
                }

                if ($grp["rate"] == 0) {
                    if ($grp["questionNum"] > 0) {
                        $p = (int) ($grp["answeredNum"] / $grp["questionNum"] * 100);
                    } else {
                        $p = 0;
                    }
                    $total["info"] .= ucfirst(strtolower($grp["title"])) . " (max:" . $grp["maxPoint"] . ") " . (int) $grp["answeredNum"] . " / " . $grp["questionNum"] . " (válaszolt) * 100 = " . $p . " \n";
                } else {
                    $p = (int) ($grp["points"] / (int) $grp["maxPoint"] * 100);
                    $total["info"] .= ucfirst(strtolower($grp["title"])) . " (max:" . $grp["maxPoint"] . ") " . (int) $grp["points"] . " / " . $grp["maxPoint"] . " * 100 = " . $p . " \n";
                }
                $tp = $tp + $p;
    */            
        }

        $tp                  = number_format($tp / count($groups), 2);
        $total["grandtotal"] = $tp;
        $level               = 0;
        foreach ($levels as $lvl => $point) {
            if (($tp >= $point) && ($level < $lvl)) {
                $level = $lvl;
            }
        }
        $total["info"] .= "\n-----------------------\nÖsszpont: " . $tp . " \nMinősítés: " . (int) $level . " \n";
        if ($updatePoints) {
            $this->db->update("tm_contracts_locations", array("point" => $tp, "pointSummary" => $total["info"], "level" => $level), array("id" => $locID));
        }
        return $total;     
    }
}
