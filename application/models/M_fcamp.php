<?php

class M_Fcamp extends CI_Model
{
    

    public function getTurns($campID, $joinButton = false)
    {
        setlocale(LC_ALL,'hungarian');
        setlocale(LC_ALL, 'hu_HU.UTF8');
        date_default_timezone_set("Europe/Budapest");

        $camp = $this->db->from("tf_camps")->where("id", (int) $campID)->get()->row();
        if (!$camp) {
            return false;
        }

        if ($this->input->get("key")) {
            $key = "?key=".$this->input->get("key");
        } else {
            $key = "";
        }

        ob_start();

        //$turns = $this->db->from("tf_camp_turns")->where("campID", $camp->id)->where("published", 1)->order_by("validfrom", "ASC")->get();
        $turns = $this->db->from("tf_camp_turns")->where("campID", $camp->id)->where("published", 1)->like("validfrom", date("Y"), "after")->order_by("validfrom", "ASC")->get()->result();
        if (count($turns) < 1): ?>
            <div class="row m10">
                <div class='alert alert-info'>Az adott tábor idei turnusait még szervezik, hamarosan lehet jelentezni a táborra!</div>
            </div>
        <?php else: 
            foreach ($turns as $data) {               
                ?>
                <div class="row m10">
                    <div class="m5" style="border-bottom: 1px dotted gray;">
                        <div class="col-md-9 col-sm-12 p5"><?=$data->title;?></div>
                        <div class="col-md-3 col-sm-12 badge"><?=number_format($data->price, 0, ",", ".");?> Ft/turnus</div>

                        <div class="col-md-6 col-sm-12"><?=$this->tools->huDate($data->validfrom, true, false) . " - " . $this->tools->huDate($data->validto, true, true);?></div>

                        <div class="col-md-6 col-sm-12 ar">
                            <?php if($joinButton && ($data->validfrom >= date("Y-m-d") && $data->validto > date("Y-m-d"))): ?>
                                <a href="<?= site_url("tabor-jelentkezes/".$campID."/".$data->id.$key); ?>"><button type="button" class="btn btn-sm btn-primary">Jelentkezés <i class="glyphicon glyphicon-check"></i></button></a>
                            <?php endif; ?>
                        </div>
                    </div>                    
                </div>
                <?php
            }
        endif;


        $out = ob_get_contents();
        ob_get_clean();

        return $out;
    }

    public function getCampIDS($filters = false, $orderBy = 'c.id') 
    {      

        //$this->db->select("c.id, (SELECT count(1) FROM tf_camp_members as m WHERE m.campID=c.id) as members");
        //$this->db->select("c.id");
        $this->db->select("c.id, (SELECT max(YEAR(validfrom)) FROM tf_camp_turns as ct WHERE ct.campID=c.id AND ct.published=1) as turnYear ");
        $this->db->from("tf_camps as c");
		$this->db->join("tf_camp_contracts as co", "co.id=c.contractID");

        if ((is_int($filters["type"])) && ((int)$filters["type"] > -1)) {
            if ($filters["type"] == "7") {
                $this->db->where("c.typeID <> 8");

            } elseif ($filters["type"] == "8") {
                $this->db->where("c.typeID <> 7");
            } else {
                $this->db->where("c.typeID", 9);
            }
        }
        if ((is_int($filters["country"])) && ((int)$filters["country"] > -1)) {
            $this->db->where("c.countryID", (int) $filters["country"]);
        }
        if ((is_int($filters["location"])) && ((int)$filters["location"] > -1)) {
            $this->db->where("c.locationID", (int) $filters["location"]);
        }
        if (is_array($filters["location"])) {
            $this->db->where_in("c.locationID", $filters["location"]);
        }
        if ((is_int($filters["state"])) && ((int)$filters["state"] > -1)) {
            $this->db->where("c.stateID", (int) $filters["state"]);
        }
                
        if ((is_int($filters["category"])) && ((int)$filters["category"] > -1)) {
            //$this->db->where("c.categoryID", (int) isset($filters["cat"));
            $this->db->join("tf_camp_categories as cc", "cc.campID=c.id" , "LEFT");
            $this->db->where("cc.categoryID", (int) $filters["category"]);                
        }
        if (is_array($filters["category"])) {
            //$this->db->where("c.categoryID", (int) isset($filters["cat"));
            $this->db->join("tf_camp_categories as cc", "cc.campID=c.id" , "LEFT");
            $this->db->where_in("cc.categoryID", $filters["category"]);                
        }
        
        if (is_int($filters["age_min"])) {
            $this->db->where("((c.age_min IS NULL) || (c.age_min<=". (int) $filters["age_min"]."))");
        }
        if (is_int($filters["age_max"])) {
            $this->db->where("((c.age_max IS NULL) || (c.age_max>=". (int) $filters["age_max"]."))");
        }

        if (!is_null($filters["featured"])) {
            $this->db->where("c.featured", (int)$filters["featured"]);
        }

        if ($filters["szep1"] == "1") {
            $this->db->where("c.szep_MKB", 1);
        }
        if ($filters["szep2"] == "1") {
            $this->db->where("c.szep_OTP", 1);
        }
        if ($filters["szep3"] == "1") {
            $this->db->where("c.szep_KH", 1);
        }    
        
        if ($filters["date_from"]) {
            $this->db->join("tf_camp_turns as tu", "tu.campID=c.id" , "LEFT");
            $this->db->where("tu.published", 1);
            $this->db->where("tu.validfrom >=", date("Y-m-d", strtotime($filters["date_from"])));
            $this->db->where("tu.validto <=", date("Y-m-d", strtotime($filters["date_to"])));
            
            if (is_int($filters["price_min"])) {
                $this->db->where("tu.price >=", (int) $filters["price_min"]);
            }             
            if (is_int($filters["price_max"])) {
                $this->db->where("tu.price <=", (int) $filters["price_max"]);
            }             
        } else {
            if (is_int($filters["price_min"])) {
                $this->db->where("c.price >=", (int) $filters["price_min"]);
            }             
            if (is_int($filters["price_max"])) {
                $this->db->where("c.price <=", (int) $filters["price_max"]);
            }  
        }             
        
        $this->db->where("c.published", 1);
        // $this->db->where("c.closed", 1);
        // $this->db->where("co.visible", 1);
        $this->db->order_by($orderBy);
        $result = $this->db->get();
      
        $ids = [];
        if ($result->num_rows() > 0) {
            foreach($result->result() as $row) {              
                $ids[] = $row->id;
            }       
        }
        return $ids;
    }


    public function getCampsList($ids, $orderBy = "c.featured DESC, turnYear DESC, rand()")
    {
        //$orderBy = "c.featured DESC, rand()" 

        if(($ids !== "ALL") && !is_array($ids) || count($ids) <= 0) {
            return false;
        } else {
            $this->db->select("c.*, t.name as typeTitle, cl.name as categoryTitle, l.name as locationTitle, s.name as stateTitle, co.name as countryTitle,
            (SELECT path FROM tf_camp_images WHERE campID=c.id AND published=1 ORDER BY ordering LIMIT 1) as imagePath,
            (SELECT max(YEAR(validfrom)) FROM tf_camp_turns as ct WHERE ct.campID=c.id AND ct.published=1) as turnYear ");
            $this->db->from("tf_camps as c");
            $this->db->join("tf_camp_types as t", "t.id=c.typeID", "LEFT");
            $this->db->join("tf_camp_category_list as cl", "cl.id=c.categoryID", "LEFT");
            $this->db->join("tf_camp_locations as l", "l.id=c.locationID", "LEFT");
            $this->db->join("tf_camp_states as s", "s.id=c.stateID", "LEFT");
            $this->db->join("tf_camp_countries as co", "co.id=s.countryID", "LEFT");
			$this->db->join("tf_camp_contracts as cc", "cc.id=c.contractID");
			
            if ($ids !== "ALL") {
                $this->db->where("c.published", 1);
                // $this->db->where("cc.visible", 1);
            
                $this->db->where_in("c.id", $ids);
                $this->db->order_by($orderBy);
            } else {
                $this->db->order_by("c.id DESC");

            }
            

            return $this->db->get()->result();      
        }  
    }


    public function getCampData($campID, $force = false)
    {
        $this->db->select("c.*, t.name as typeTitle, cl.name as categoryTitle, l.name as locationTitle, s.name as stateTitle, co.name as countryTitle,
        (SELECT path FROM tf_camp_images WHERE campID=c.id AND published=1 ORDER BY ordering LIMIT 1) as imagePath");
        $this->db->from("tf_camps as c");
        $this->db->join("tf_camp_types as t", "t.id=c.typeID", "LEFT");
        $this->db->join("tf_camp_category_list as cl", "cl.id=c.categoryID", "LEFT");
        $this->db->join("tf_camp_locations as l", "l.id=c.locationID", "LEFT");
        $this->db->join("tf_camp_states as s", "s.id=c.stateID", "LEFT");
        $this->db->join("tf_camp_countries as co", "co.id=s.countryID", "LEFT");
		$this->db->join("tf_camp_contracts as cc", "cc.id=c.contractID");

        if (!$force) {
            $this->db->where("c.published", 1);
			// $this->db->where("cc.visible", 1);
        }
        $this->db->where("c.id", $campID);
        
        return $this->db->get()->row();        
    }

    public function campTemplates()
    {

        $slug = $this->uri->segment(2);        
        $ids = array();
        switch ($slug) {
            case "budapesti-napkozis-taborok":{
                    $filters = array(
                        "state" => 13,
                    );
                    $title = "Budapesti napközis táborok";
                    $ids   = $this->m_fcamp->getCampIDS($filters);
                }break;
        
            case "balatoni-taborok":{
                    $query   = $this->db->select("id")->from("tf_camp_locations")->like("tag", "balaton", "BOTH")->get();
                    $arr = [];
                    foreach ($query->result() as $tmp) {
                        $arr[] = $tmp->id;
                    }
                    $filters = array (
                        "location" => $arr,
                    );
                    $title = "Balatoni táborok";
                    $ids   = $this->m_fcamp->getCampIDS($filters);
                }break;
        
            case "taborok-kisebbeknek":{
                    $filters = array(
                        "age_max" => 12,
                    );
                    $title = "Táborok kisebbeknek";
                    $ids   = $this->m_fcamp->getCampIDS($filters);
                }break;
        
            case "taborok-nagyobbaknak":{
                    $filters = array(
                        "age_max" => 13,
                    );
                    $title = "Táborok nagyobbaknak";
                    $ids   = $this->m_fcamp->getCampIDS($filters);
                }break;
        
            case "izgalmas-kalandtaborok":{
                    $filters = array(
                        "category" => array(22),
                    );
                    $title = "Izgalmas kalandtáborok";
                    $ids   = $this->m_fcamp->getCampIDS($filters);
                }break;
        
            case "mozgalmas-es-vidam-sporttaborok":{
                    $filters = array(
                        "category" => array(17),
                    );
                    $title = "Mozgalmas és vidám sporttáborok";
                    $ids   = $this->m_fcamp->getCampIDS($filters);
                }break;
        
            default:{
                    $title = "Táborok listája";
                    $ids = $this->m_fcamp->getCampIDS();
                }break;
        }

        return array("title" => $title, "ids" => $ids, "filters" => $filters);
         
    }

    public function generateContract($contract) 
    {
        $contract = (array)$contract;
        $arr = (array)$contract;
        if ($this->input->post_get("contactName")) {
            foreach ($contract as $field => $val) { 
                if ($this->input->post_get($field)) {               
                    $arr[$field] = $this->input->post_get($field);        
                }
            }
        } 

        $arr = array(
            "CONTRACT" => (array)$arr,
            "NAME" => $contract["contactName"],
            "DATE" => date("Y. m. d.")
        );

        $html = $this->tools->GetHtml("camp-contract-pdf", true);
        $body = $this->tools->ReplaceVariableData($arr, $html->body);

        $locPrice = $this->tools->GetSetting("camp-baseprice");
        $themePrice = $this->tools->GetSetting("camp-themeprice");
        $total = 0;
        $calcHtml = "<table border='1' cellpadding='2' cellspacing='2'>\n"
        ."<tr>\n"
        ."<td nobr='true'>Táborhelyszín</td>\n"
        ."<td >Alap ár</td>\n"
        ."<td style='width: 50px;'>Tábortémák</td>\n"
        ."<td >Téma ár</td>\n"
        ."<td >Összesen</td>\n"
        ."</tr>\n";	
        
        if ($this->input->post_get("loc_type")) {   
            $names = $this->input->post_get("loc_type");
            $themes = $this->input->post_get("loc_themes");           
            foreach ($names as $id => $title) {
                if ($title == "new") {
                    $name = "Új táborhely ".$id;
                } else {
                    $tmp = $this->db->from("tf_camps")->where("id", (int)$title)->get()->row();
                    $name = $tmp->name;
                }
                $themeTotal = (($themes[$id]-1) * $themePrice);
                $rowTotal = ($locPrice+(($themes[$id]-1) * $themePrice));
                $total = $total + $rowTotal;
                $calcHtml .= "<tr>\n"
                    ."<td nobr='true'>".$name."</td>\n"
                    ."<td>".$locPrice." Ft</td>\n"
                    ."<td>".$themes[$id]." db</td>\n"
                    ."<td>".$themeTotal." Ft</td>\n"
                    ."<td>".$rowTotal." Ft</td>\n"
                    ."</tr>\n";
            }
        } else {
            $locations = $this->db->from("tf_camps")->where("contractID", $contract["id"])->get()->result();
            foreach ($locations as $data) {
                
                $themeTotal = (($data->themes-1) * $themePrice);
                $rowTotal = ($locPrice+(($data->themes-1) * $themePrice));
                $total = $total + $rowTotal;
                $calcHtml .= "<tr>\n"
                    ."<td nobr='true'>".$data->name."</td>\n"
                    ."<td>".$locPrice." Ft</td>\n"
                    ."<td>".$data->themes." db</td>\n"
                    ."<td>".$themeTotal." Ft</td>\n"
                    ."<td>".$rowTotal." Ft</td>\n"
                    ."</tr>\n";
            }
        }
        $calcHtml .= "<tr>\n"
            ."<td ></td>\n"
            ."<td ></td>\n"
            ."<td ></td>\n"
            ."<td >Összesen:</td>\n"
            ."<td>".$total." Ft</td>\n"
            ."</tr>\n"
            ."</table>";

        $body = str_replace("%KALKULATOR%", $calcHtml , $body);	   
    
        $fn = FCPATH."tmp/contract_tf.pdf";

        $this->tools->GeneratePDF($body, $fn, "VÁLLALKOZÁSI SZERZŐDÉS");  
        return $fn;
    }
}
