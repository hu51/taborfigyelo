<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of letters
 *
 * @author szabog
 */
class M_letters extends CI_Model
{

    public function GenerateHtmlLetter($letterID)
    {
        $this->load->model("m_fcamp");

        $this->db->select("l.*, t.body as templateBody, t.css");
        $this->db->from("tf_newsletter_letters as l");
        $this->db->join("tf_newsletter_templates as t", "t.id = templateID");
        $this->db->where("l.id", $letterID);
        $letter = $this->db->get()->row();

        $body = $letter->templateBody;
        $body = str_ireplace("%CSS%", $letter->css, $body);
//        $body = str_ireplace("%PREHEADER%", $letter->title, $body);
        $body = str_ireplace("%PREHEADER%", "", $body);

        $body = str_ireplace("%SUBJECT%", $letter->title, $body);
        $body = str_ireplace("%CONTENT%", $letter->body, $body);
        $body = str_ireplace("%DATE%", date("Y. M. d."), $body);
        $body = str_ireplace("%TIME%", date("H:i"), $body);

        //kiemelt
        if (stristr($body, "#kiemelt#")) {
            $this->db->from("tf_camp_banners");
            $this->db->where("enabled", "1");
            $this->db->where("(maxHits is NULL or maxHits =0 or maxHits >= hits)");
            $this->db->where("(fromDate is NULL or fromDate <= NOW())");
            $this->db->where("(endDate is NULL or endDate >= NOW())");

            $this->db->order_by("rand()");
            $banners = $this->db->get()->result();

            $campHighlight = "";            
            foreach ($banners as $data) {
                if (!is_null($data->campID)){            
                    $camp = $this->m_fcamp->getCampData((int) $data->campID);

                    $box = $this->load->view("frontpage/campdata_letter", array("data" => $camp), true);
                    $box = str_replace("col-md-6", "col-md-12", $box);
                    $box = str_replace('campdata-box', 'campdata-box campdata-box2x', $box);
                    $box = str_replace('campdata-box', 'campdata-box campdata-box2x', $box);
                    $campHighlight .= "<div style='max-width: 900px; clear: both;'>".str_replace("properties/thumbs/", "properties/images/", $box)."</div>\n";
                } else {
                    $campHighlight .= "<div style='max-width: 900px; clear: both;'><a href=".$data->url." target='_blank'><img src='".site_url("images/banners/news/" . $data->image)."' style='width: 100%; height: auto;'></a></div>\n";
                }
            }                  
            $body = str_ireplace("#kiemelt#", $campHighlight, $body);
        }

        if (stristr($body, "#eloresorolt#")) {
            $campFeatured = "";
            $ids = $this->m_fcamp->getCampIDS(array("featured" => 1));
            foreach ($this->m_fcamp->getCampsList($ids) as $data) {
                $campFeatured .= $this->load->view("frontpage/campdata_letter", array("data" => $data), true);
            }
            $body = str_ireplace("#eloresorolt#", $campFeatured, $body);
        }

        $doc = new DOMDocument();
        $doc->loadHTML($body);

        $htmlbody = $doc->getElementsByTagName('body')->item(0);

        $txt = $doc->createTextNode("%TRACKING%");
        $htmlbody->appendChild($txt);

        $br = $doc->createElement('br');
        $htmlbody->appendChild($br);
    
        return $doc->saveHTML();
    }

    public function BuildSend($letterID = false)
    {
        $sentDatetime = date("Y-m-d H:i:s");      
        
        $this->db->select("lu.*");
        $this->db->from("tf_newsletter_letters_users as lu");
        $this->db->join("tf_newsletter_letters as l", "lu.letterID = l.id");
        $this->db->where("l.autoSend", "1");
        $this->db->where("lu.sent", "0");
        $this->db->where("lu.sendingTry<", "5");
        if ($letterID !== false) {
            $this->db->where("lu.letterID", $letterID);
        } else {
            $this->db->where("l.autoSend", "1");
        }
        $this->db->limit(200);        
        $query = $this->db->get();  

        if ($this->input->get("test")) {            
            print_r("TEST: " .$sentDatetime." L:".$letterID." - DB:".$query->num_rows()."\n");
        } else {
            $fn = APPPATH."logs/cron_".date("Y_m_d").".log";
            $msg = "Autosend: ".$sentDatetime." Letter ID:".$letterID." - Num:".$query->num_rows();
            file_put_contents($fn, $msg."\n", FILE_APPEND);

            if ($query->num_rows() == 0) {
                $this->output->append_output("Nincs kiküldendő levél.");
            } else {
                foreach ($query->result() as $data) {
                    $user   = $this->db->from("tf_newsletter_users")->where("id", $data->userID)->where("unsubscribed is NULL")->get()->row();
                    $list   = $this->db->from("tf_newsletter_lists")->where("id", $data->listID)->get()->row();
                    $letter = $this->db->from("tf_newsletter_letters")->where("id", $data->letterID)->get()->row();

                    if ($user && $list && $letter) {
                        $letter_body = $this->GenerateHtmlLetter($data->letterID);

                        $title = str_ireplace("#name#", $user->name, $letter->title);

                        $letter_body = str_ireplace("#name#", $user->name, $letter_body);
                        $letter_body = str_ireplace("#email#", $user->email, $letter_body);
                        $letter_body = str_ireplace("#id#", $user->id, $letter_body);
                        $letter_body = str_ireplace("#time#", date("H:i"), $letter_body);
                        $letter_body = str_ireplace("#date#", date("Y-m-d"), $letter_body);

                        $user_code   = md5($user->id . $user->uniqid);
                        $list_code   = md5($list->id . $list->uniqid);
                        $letter_code = md5($letter->id . $letter->uniqid);

                        //EMAIL TRACKING
                        $url         = base_url("hirlevel/tracking/" . "?l=" . $letter_code . "&amp;u=" . $user_code);
                        $trackingIMG = "<img src='{$url}' width='1' height='1' alt='logo' />";
                        $letter_body = str_ireplace("%TRACKING%", $trackingIMG, $letter_body);

                        //FOOTER
                        $footer = anchor(base_url("hirlevel/online/" . $letter_code), "A levél online verziója", " target='_blank' ");
                        $footer .= "<br>\n";
                        $footer .= "<b>Leíratkozás:</b><br>\n";
                        if (!is_null($list->uniqid)) {
                            $footer .= anchor("hirlevel/leiratkozas/?user=" . $user_code . "&amp;group=" . $list_code, "Leiratkozás a(z) `{$list->name}` csoport hírleveleiről", " target='_blank' ") . "<br>\n";
                        }
                        $footer .= anchor("hirlevel/leiratkozas/?user=" . $user_code, "Leiratkozás az oldal összes hírleveléről", " target='_blank' ") . "<br>\n";
                        $footer .= "<br>\n";
                        $footer .= "<small>" . date("Y. M. d, D, H:i") . "</small><br>\n";

                        $letter_body = str_ireplace("%FOOTER%", $footer, $letter_body);

                        $options = array(
                            "reply-to"     => $letter->reply_address,
                            "from-name"    => $letter->from_name,
                            "from-address" => $letter->from_address,
                        );
                    
                        if ($this->tools->SendHtmlMail($user->email, $title, $letter_body, $options, false, false)) {
                            $this->db->where("id", $data->id);
                            $this->db->where("sent", "0");
                            $this->db->update("tf_newsletter_letters_users", array("sent" => 1, "sentDatetime" => $sentDatetime));

                            $this->output->append_output("<div class='list-group-item alert alert-success'>Levél elküldve: {$user->email}</div>\n");
                        } else {
                            $this->db->where("id", $data->id);
                            $this->db->where("sent", "0");
                            $this->db->update("tf_newsletter_letters_users", array("sendingTry" => $data->sendingTry + 1));

                            $this->output->append_output("<div class='list-group-item alert alert-danger'>Hiba a küldéskor: {$user->email}</div>\n");
                        }
                    } else {
                        $this->db->where("id", $data->id);
                        $this->db->where("sent", "0");
                        $this->db->update("tf_newsletter_letters_users", array("sendingTry" => $data->sendingTry + 1));                    
                    }
                }
            }
        }
    }

    public function sendConfirmLetter($userID, $showMessage = true)
    {       
        $user = $this->db->from("tf_newsletter_users")->where("id", $userID)->get()->row();
        if ($user && is_null($data->unsubscribed) && is_null($data->blacklisted)) {
            $html      = $this->tools->GetHtml("newsletter-confirm", true);
            $user_code = md5($user->id . $user->uniqid);

            $body = str_ireplace("%NAME%", $user->name, $html->body);
            $body = str_ireplace("%URL%", site_url("hirlevel/megerosites?u=" . $user_code), $body);

            if ($this->tools->SendHtmlMail($user->email, $html->title, $body)) {
                if ($showMessage) {
                    $this->output->append_output("<div class='alert alert-info'>Ahhoz, hogy hírlevelet tudjunk önnek küldeni, levélben elküldtük a teendőket (".$user->email.").</div>\n");
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function createUser($email, $name, $source = "website")
    {
        $data = $this->db->from("tf_newsletter_users")->like("email", filter_var($email, FILTER_SANITIZE_EMAIL))->get()->row();
        if ($data) {
            return false;
        } else {
            $data = array(
                "email"    => filter_var($email, FILTER_SANITIZE_EMAIL),
                "name"     => filter_var($name, FILTER_SANITIZE_STRING),
                "uniqid"   => uniqid(),
                "created"  => date("Y-m-d H:i:s"),
                "modified" => date("Y-m-d H:i:s"),
                "lastIP"   => $this->tools->getIP(),
                "source"   => $source,
            );
            $this->db->insert("tf_newsletter_users", $data);
            return $this->db->insert_id();
        }
    }


    public function addUserToList($userID, $listID) 
    {
        $data = $this->db->from("tf_newsletter_lists_users")->where("listID", $listID)->where("userID", $userID)->get()->row();
        if ($data) {
            return false;
        } else {        
            $this->db->insert("tf_newsletter_lists_users", array("userID" => $userID, "modified" => date("Y-m-d H:i:s"), "listID" => $listID));
            return $this->db->insert_id();
        }
    }
}
