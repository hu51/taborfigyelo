<?php

$height = 300;
$width  = 900;

if ($this->input->post("addnew")) {
    if (empty($_FILES["new_image"]["name"]) && empty($this->input->post("new_campID"))) {
        message("", "Vagy képet kell feltölteni vagy tábor számot megadni", "warning");
    } else {
        $campID  = $this->input->post("new_campID", true);
        $title   = $this->input->post("new_title", true);
        $url     = $this->input->post("new_url", true);
        $from    = $this->input->post("new_from", true);
        $end     = $this->input->post("new_end", true);
        $enabled = $this->input->post("new_enabled", true);
        $amount  = $this->input->post("new_amount", true);
        $maxHits  = $this->input->post("new_maxhits", true);

        $data = array(
            "title"    => filter_var($title, FILTER_SANITIZE_STRING),
            "url"      => null,
            "campID"   => null,
            "fromDate" => (substr($from, 0, 1) == "0") ? null : $from,
            "endDate"  => (substr($end, 0, 1) == "0") ? null : $end,
            "enabled"  => (int) $enabled,
            "maxHits"  => ($amount == "0") ? null : (int) $amount,
        );
        $this->db->insert("tf_camp_banners", $data);
        $newID = $this->db->insert_id();
        if (!$newID) {
            message("HIBA", implode(", ", $this->db->error()));
        } else {
            if (!empty($_FILES["new_image"]["name"])) {
                $img = getimagesize($_FILES["new_image"]["tmp_name"]);

                if ($img["mime"] !== "image/png" && $img["mime"] !== "image/jpeg") {
                    $this->db->delete("tf_camp_banners", array("id" => $newID));
                    message("", "JPG vagy PNG képet lehet feltölteni! " . $img["mime"], "warning");
                    unlink($_FILES["new_image"]["tmp_name"]);
                } elseif (!is_null($height) && (int) $height !== $img[1]) {
                    $this->db->delete("tf_camp_banners", array("id" => $newID));
                    message("", "A képnek pont {$height} képpont magasnak kell lennie! " . $img[1], "warning");
                    unlink($_FILES["new_image"]["tmp_name"]);
                } elseif (!is_null($width) && (int) $width !== $img[0]) {
                    $this->db->delete("tf_camp_banners", array("id" => $newID));
                    message("", "A képnek pont {$width} képpont szélesnek kell lennie! " . $img[0], "warning");
                    unlink($_FILES["new_image"]["tmp_name"]);
                } else {
                    $newFile = "bnr_" . sprintf("%04d", $newID) . "." . str_replace("image/", "", $img["mime"]);
                    if (!move_uploaded_file($_FILES["new_image"]["tmp_name"], FCPATH . "images/banners/news/" . $newFile)) {
                        $this->db->delete("tf_camp_banners", array("id" => $newID));
                        message("", "Nem sikerült a fájlt feltölteni.", "warning");
                        unlink($_FILES["new_image"]["tmp_name"]);
                    } else {
                        $this->db->update("tf_camp_banners", array("image" => $newFile, "url" => filter_var($url, FILTER_SANITIZE_URL)), array("id" => $newID));
                        message("", "A képes banner felrögzítve", "success");
                    }
                }
            } else {
                $this->db->update("tf_camp_banners", array("campID" => (int) $campID), array("id" => $newID));
                message("", "A tábori banner felrögzítve", "success");
            }
        }
    }
}

if ($this->input->post("update")) {
    $id = $this->input->post("update");

    $title   = $this->input->post("title", true);
    $url     = $this->input->post("url", true);
    $campID  = $this->input->post("campID", true);
    $from    = $this->input->post("from", true);
    $end     = $this->input->post("end", true);
    $enabled = $this->input->post("enabled", true);
    $amount  = $this->input->post("amount", true);
    $data    = array(
        "title"    => filter_var($title[$id], FILTER_SANITIZE_STRING),
        "campID"   => (!isset($campID[$id])) ? null : (int) $campID[$id],
        "url"      => filter_var($url[$id], FILTER_SANITIZE_URL),
        "fromDate" => (substr($from[$id], 0, 1) == "0") ? null : $from[$id],
        "endDate"  => (substr($end[$id], 0, 1) == "0") ? null : $end[$id],
        "enabled"  => (int) $enabled[$id],
        "maxHits"  => ($amount[$id] == "0") ? null : (int) $amount[$id],
    );
    $this->db->update("tf_camp_banners", $data, array("id" => $id));
    message("", "Banner adatai módosítva", "success");
}

if ($this->input->post("delete")) {
    $id = $this->input->post("delete");

    $data = $this->db->from("tf_camp_banners")->where("id", $id)->get()->row();

    unlink(FCPATH . "images/banners/news/" . $data->image);
    $this->db->delete("tf_camp_banners", array("id" => $id));
    message("", "Banner sikeresen törölve", "success");
}

$all = $this->db->from("tf_camp_banners")->order_by("enabled", "DESC")->order_by("title", "ASC")->get()->result();

?>
<div class="panel panel-default">
	<div class="panel-body">
        <h4>Táborfigyelő kiemelt táborok:</h4>
        <br>
		<form method="post" enctype="multipart/form-data">
			<table class="table table-bordered table-condensed">
				<thead>
					<tr>
						<th>Név</th>
						<th>Tól</th>
						<th>Ig.</th>
						<th>max megj.</th>
						<th>Megjelenés</th>
						<th>URL / Tábor ID</th>
						<th>Engedélyezeve</th>
						<th>&nbsp;</th>
					</tr>
					</head>
					<?php foreach ($all as $data): ?>
						<tr>
							<td>
								<input type="text" class="form-control" name="title[<?=$data->id;?>]" placeholder=" neve" value="<?=$data->title;?>">
							</td>
							<td class="w100">
								<input type="date" class="form-control" name="from[<?=$data->id;?>]" value="<?=$data->fromDate;?>">
							</td>
							<td class="w100">
								<input type="date" class="form-control" name="end[<?=$data->id;?>]" value="<?=$data->endDate;?>">
							</td>
							<td class="w100">
								<input type="number" class="form-control" name="amount[<?=$data->id;?>]" value="<?=$data->maxHits;?>">
							</td>
							<td class="w100">
								<?=$data->hits;?>
							</td>
							<td>
                                <?php if (is_null($data->campID)): ?>
                                    <div class="input-group">
                                        <div class="input-group-addon">URL</div>
                                        <input type="text" class="form-control" name="url[<?=$data->id;?>]" value="<?=$data->url;?>">
                                    </div>
                                <?php else: ?>
                                    <div class="input-group">
                                        <div class="input-group-addon">Tábor ID</div>
                                        <input type="text" class="form-control" name="campID[<?=$data->id;?>]" value="<?=$data->campID;?>">
                                    </div>
                                <?php endif;?>
							</td>
							<td>
                                <?=$this->tools->bs_checkbox("enabled[" . $data->id . "]", $data->enabled);?>
                                <?php if (is_null($data->campID)): ?>
                                    <a href='<?=base_url("images/banners/news/" . $data->image);?>' target='_blank'><i class="glyphicon glyphicon-picture"></i></a>
                                <?php endif;?>
							</td>
							<td>
								<button class="btn btn-primary" type="submit" name="update" value="<?=$data->id;?>" aria-label="">Módosít</button>
								<button class="btn btn-danger delete" type="submit" name="delete" value="<?=$data->id;?>" aria-label="">Töröl</button>
							</td>
						</tr>
					<?php endforeach;?>


					<tr class="success">
						<th>Név</th>
						<th>Tól</th>
						<th>Ig.</th>
						<th>max megjelenés</th>
						<th colspan="3">&nbsp;</th>
					</tr>
					<tr>
						<td>
							<input type="text" class="form-control" name="new_title" value="">
						</td>
						<td class="w100">
							<input type="date" class="form-control" name="new_from" value="">
						</td>
						<td class="w100">
							<input type="date" class="form-control" name="new_end" value="">
						</td>
						<td>
							<input type="number" class="form-control" name="new_maxhits" value="0">
						</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>
							<button class="btn btn-success" type="submit" name="addnew" value="1" aria-label="">Új hozzáadása</button>
						</td>
                    </tr>

					<tr>
                        <td class="info">
                            1.) Kép feltöltése (<?=$width . "*" . $height;?> képpont)
                        </td>
						<td colspan="2">
                            <input type="file" name="new_image" /><br>
						</td>
                        <td colspan="3">
                            <input type="text" class="form-control" name="new_url" value="" placeholder="hivatkozás címe">
                        </td>
                        <td> (a feltöltött kép fog megjelenni) </td>
					</tr>
					<tr>
                        <td class="info">
                        2.) Vagy tábor kiválasztása
                        </td>
						<td colspan="2">
                            <input type="text" class="form-control" name="new_campID" value="" placeholder="tábor sorszám">
						</td>
						<td colspan="3">&nbsp;</td>
						<td > (a tábor bejegyzés jelenik meg)
						</td>
					</tr>
			</table>
		</form>
	</div>
</div>
<script>

$(".delete").click( function(e) {
	if (confirm("Biztos törlöd?")) {
		return true;
	} else {
		return false;
	}
});

</script>