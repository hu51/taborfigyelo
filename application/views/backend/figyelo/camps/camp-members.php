<?php

$locID    = (int) filter_var($this->uri->segment(4));
$location = $this->db->from("tf_camps")->where("id", $locID)->get()->row();
$contract = $this->db->from("tf_camp_contracts")->where("id", $location->contractID)->get()->row();

$all = $this->db->from("tf_camp_members")->where("campID", $location->id)->order_by("id")->get()->result();

require FCPATH.'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

// Create new Spreadsheet object
$spreadsheet = new Spreadsheet();
$spreadsheet->setActiveSheetIndex(0);

$spreadsheet->getActiveSheet()->setTitle('data');
$fields = json_decode($form_data->data);


$spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(1, 1, "Tábor:");
$spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(2, 1, $location->name);

$rowID = 4;
$firstRow = true;
foreach ($all as $data) {
    $colID = 1;
    if ($firstRow) {
        foreach ($data as $key => $field) {
            if (!in_array($key, array("id", "turnID", "campID", "status"))) {
                $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow($colID, $rowID-1, $key);
                $colID++;
            }
        }
        $firstRow = false;
        $colID = 1;
    }
    
    foreach ($data as $key => $val) {
        if (!in_array($key, array("id", "turnID", "campID", "status"))) {
            $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow($colID, $rowID, $val);
            $colID++;
        }
    }
    $rowID++;
}


// Redirect output to a client’s web browser (Xls)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="data.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0

$writer = IOFactory::createWriter($spreadsheet, 'Xls');
$writer->save('php://output');