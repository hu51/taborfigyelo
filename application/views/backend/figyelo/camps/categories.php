<?php

if ($this->input->post("addnew")) {
    $data = array(
        "name"      => $this->input->post("new_name", true),
        "alias"     => $this->tools->toSlug($this->input->post("new_name", true)),
        "parent"    => (int) $this->input->post("new_parent", true),
        "published" => $this->input->post("new_published", true),
    );
    $this->db->insert("tf_camp_category_list", $data);
    message("", "Új tábor kategória felrögzítve", "success");
}

if ($this->input->post("update")) {
    $id = $this->input->post("update");

    $name      = $this->input->post("name", true);
    $parent    = $this->input->post("parent", true);
    $published = $this->input->post("published", true);
    $data      = array(
        "name"      => filter_var($name[$id], FILTER_SANITIZE_STRING),
//        "alias"     => $this->tools->toSlug(filter_var($name[$id], FILTER_SANITIZE_STRING)),
        "published" => (int) $published[$id],
        "parent"    => (int) $parent[$id],
    );
    $this->db->update("tf_camp_category_list", $data, array("id" => $id));
    message("", "Tábor kategória módosítva", "success");
}

$root     = $this->db->from("tf_camp_category_list")->where("parent", "0")->order_by("name", "ASC")->get()->result();
$rootList = array("0" => "* nincs");
foreach ($root as $tmp) {
    $rootList[$tmp->id] = $tmp->name;
}

$all = $this->db->from("tf_camp_category_list")->order_by("name", "ASC")->get()->result();

?>
<div class="panel panel-default">
	<div class="panel-body">
		<h4>Tábor kategóriák:</h4>
		<form method="post">
			<table class="table table-bordered table-condensed">
				<thead>
					<tr>
						<th>Név</th>
						<th>URL</th>
						<th>Szülő</th>
						<th>Engedélyezve</th>
						<th>&nbsp;</th>
					</tr>
					</head>
					<?php foreach ($all as $data): ?>
						<tr>
							<td>
								<input type="text" class="form-control" name="name[<?=$data->id;?>]" value="<?=$data->name;?>">
							</td>
							<td>
								<?=$data->alias;?>
							</td>
							<td class="w200">
								<?=$this->tools->bs_select("parent[" . $data->id . "]", $data->parent, $rootList, "form-control", $data->id);?>
							</td>
							<td>
								<?=$this->tools->bs_checkbox("published[" . $data->id . "]", $data->published);?>
							</td>
							<td>
								<button class="btn btn-primary" type="submit" name="update" value="<?=$data->id;?>" aria-label="">Módosít</button>
							</td>
						</tr>
					<?php endforeach;?>


					<tr class="success">
						<th>Név</th>
						<th>URL</th>
						<th>Szülő</th>
						<th>Engedélyezve</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
					</tr>
					<tr>
						<td>
							<input type="text" class="form-control" name="new_name" placeholder="Új neve" value="">
						</td>
						<td>&nbsp;</td>
						<td class="w200">
							<?=$this->tools->bs_select("new_parent", 0, $rootList, "form-control");?>
						</td>
						<td>
							<?=$this->tools->bs_checkbox("new_published", 0);?>
						</td>
						<td>
							<button class="btn btn-success" type="submit" name="addnew" value="1" aria-label="">Új hozzáadása</button>
						</td>
					</tr>
			</table>
		</form>
	</div>
</div>