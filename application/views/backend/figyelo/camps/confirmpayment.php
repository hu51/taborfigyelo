<?php

	$all = $this->db->from("tf_camp_contracts")->where("paymentEmailSent is not NULL")->where("contractPaid is NULL")->order_by("id", "DESC")->get()->result();
	
?>
<div class="panel panel-default">
	<div class="panel-body">
		<h4>Fizetési visszaigazolásra váró szerződések:</h4>
		<form method="post">
		<table class="table table-bordered table-condensed">
			<thead>
			<tr>
				<th class="w150">&nbsp;</th>
				<th>Partner</th>
				<th>Létrehozva</th>
				<th>Fizetési levél kiküldve</th>
				<th></th>
			</tr>	
			</head>
		<?php foreach ($all as $data): ?>
			<tr>
				<td>
					<?= anchor("figyelo/camps/editcontract/".$data->id, "Szerződés", " class='btn btn-sm btn-primary' target='_blank'"); ?>
				</td>
				<td><?= $data->partnerName; ?></td>
				<td>
					<?= (is_Null($data->createdTime))?"<i class='glyphicon glyphicon-remove text-warning'></i>":"<i class='glyphicon glyphicon-ok text-success'></i> ".date("m.d. H:i", strtotime($data->createdTime)); ?>
				</td>
				<td>
					<?= (is_Null($data->paymentEmailSent))?"<i class='glyphicon glyphicon-remove text-warning'></i>":"<i class='glyphicon glyphicon-ok text-success'></i> ".date("m.d. H:i", strtotime($data->paymentEmailSent)); ?>
				</td>
				<td>					
                    <button type="submit" class="btn btn-sm btn-success" name="confirmPayment" value="<?= $data->id; ?>"> Fizetés megerősítése</button>
                    <input type="checkbox" name="confSend" value="<?= $data->id; ?>"> megerősítés, táborhely szerkesztő levél kiküldése					
				</td>

			</tr>
		<?php endforeach; ?>
		</table>
		</form>
	</div>
</div>
<?php

	$all = $this->db->from("tf_camp_contracts")->where("contractPaid is not NULL")->where("campEditSent is NULL")->where("closed", 0)->order_by("id", "DESC")->get()->result();
	
?>
<div class="panel panel-default">
	<div class="panel-body">
		<h4>Fizetési visszaigazolás kiküldve de nem ment szerkesztői levél:</h4>
		<form method="post">
		<table class="table table-bordered table-condensed">
			<thead>
			<tr>
				<th class="w150">&nbsp;</th>
				<th>Partner</th>
				<th>Létrehozva</th>
				<th>Fizetési levél kiküldve</th>
				<th></th>
			</tr>	
			</head>
		<?php foreach ($all as $data): ?>
			<tr>
				<td>
					<?= anchor("figyelo/camps/editcontract/".$data->id, "Szerződés", " class='btn btn-sm btn-primary' target='_blank'"); ?>
				</td>
				<td><?= $data->partnerName; ?></td>
				<td>
					<?= (is_Null($data->createdTime))?"<i class='glyphicon glyphicon-remove text-warning'></i>":"<i class='glyphicon glyphicon-ok text-success'></i> ".date("m.d. H:i", strtotime($data->createdTime)); ?>
				</td>
				<td>
					<?= (is_Null($data->paymentEmailSent))?"<i class='glyphicon glyphicon-remove text-warning'></i>":"<i class='glyphicon glyphicon-ok text-success'></i> ".date("m.d. H:i", strtotime($data->paymentEmailSent)); ?>
				</td>
				<td>					
						<button type="submit" class="btn btn-sm btn-success" name="confirmPayment" value="<?= $data->id; ?>"> Fizetés megerősítése</button>
						<input type="checkbox" name="confSend" value="<?= $data->id; ?>"> megerősítés, táborhely szerkesztő levél kiküldése					
				</td>

			</tr>
		<?php endforeach; ?>
		</table>
		</form>
	</div>
</div>
<?php

	$all = $this->db->from("tf_camp_contracts")->where("contractPaid is not NULL")->where("campEditSent is not NULL")->where("campEditDone is NULL")->where("closed", 0)->order_by("id", "DESC")->get()->result();
	
?>
<div class="panel panel-default">
	<div class="panel-body">
		<h4>Fizetési visszaigazolás kiküldve és ment szerkesztői levél:</h4>
		<form method="post">
		<table class="table table-bordered table-condensed">
			<thead>
			<tr>
				<th class="w150">&nbsp;</th>
				<th>Partner</th>
				<th>Létrehozva</th>
				<th>Fizetési levél kiküldve</th>
				<th></th>
			</tr>	
			</head>
		<?php foreach ($all as $data): ?>
			<tr>
				<td>
					<?= anchor("figyelo/camps/editcontract/".$data->id, "Szerződés", " class='btn btn-sm btn-primary' target='_blank'"); ?>
				</td>
				<td><?= $data->partnerName; ?></td>
				<td>
					<?= (is_Null($data->createdTime))?"<i class='glyphicon glyphicon-remove text-warning'></i>":"<i class='glyphicon glyphicon-ok text-success'></i> ".date("m.d. H:i", strtotime($data->createdTime)); ?>
				</td>
				<td>
					<?= (is_Null($data->paymentEmailSent))?"<i class='glyphicon glyphicon-remove text-warning'></i>":"<i class='glyphicon glyphicon-ok text-success'></i> ".date("m.d. H:i", strtotime($data->paymentEmailSent)); ?>
				</td>
				<td>					

				</td>

			</tr>
		<?php endforeach; ?>
		</table>
		</form>
	</div>
</div>
