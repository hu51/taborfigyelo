<?php

$this->db->from("tf_camp_contracts");

if ($this->input->post("filter_name")) {
    $this->db->like("partnerName", $this->input->post("filter_name"), "BOTH");
}

if ($this->input->post("filter_paid") == 'i') {
    $this->db->where("contractPaid is NOT NULL");
} elseif ($this->input->post("filter_paid") == 'n') {
    $this->db->where("contractPaid is NULL");
}

if ($this->input->post("filter_contract") == 'i') {
    $this->db->where("contractSaved is NOT NULL");
} elseif ($this->input->post("filter_contract") == 'n') {
    $this->db->where("contractSaved is NULL");
}

if ($this->input->post("filter_pub") == 'i') {
    $this->db->where("publicationTime is NOT NULL");
} elseif ($this->input->post("filter_pub") == 'n') {
    $this->db->where("publicationTime is NULL");
}


if (is_null($this->input->post("filter_closed")) || $this->input->post("filter_closed") == 'i') {
    $this->db->where("closed", 0);
} elseif ($this->input->post("filter_closed") == "n") {
    $this->db->where("closed", 1);
}

$this->db->order_by("id", "DESC");
$all = $this->db->get()->result();

$arr = array(
    "0" => "* mindegy",
    "i" => "igen",
    "n" => "nem"
);
?>
<div class="panel panel-default">
	<div class="panel-body">
        <h4>Táborfigyelő szerződések:</h4>
        <form method="post">
		<table class="table table-bordered table-condensed">
			<thead>
				<tr>
					<th colspan="2"><input type="text" name="filter_name" class="form-control" value="<?= set_value("filter_name"); ?>" ></th>
                    <th colspan="3"><button type="submit" class="btn btn-primary">Keres</button></th>
                    <th><?= $this->tools->bs_select("filter_contract", set_value("filter_contract", "0"), $arr, 'form-control'); ?></th>  
                    <th></th>                  
                    <th><?= $this->tools->bs_select("filter_paid", set_value("filter_paid", "0"), $arr, 'form-control'); ?></th>  
                    <th></th>                  
					<th><?= $this->tools->bs_select("filter_closed", set_value("filter_closed", "n"), $arr, 'form-control'); ?></th>                    
					<th><?= $this->tools->bs_select("filter_pub", set_value("filter_pub", "0"), $arr, 'form-control'); ?></th>                    
				</tr>
				<tr>
					<th>ID</th>
					<th>Partner</th>
					<th>Létrehozva</th>
					<th>Reg.Email.</th>
					<th>Alapadatok</th>
					<th>Szerződés</th>
					<th>Fizetési levél</th>
					<th>Kifizetve</th>
					<th>Hely. sz. levél</th>
					<th>Hely. lezárva.</th>
					<th>Publikálva</th>
				</tr>
				</head>
				<?php foreach ($all as $data): ?>
					<tr class="<?= ($data->visible)?"success":""; ?>">
						<td class="ac"> <?= $data->id; ?> </td>                        
						<td>
							<?= anchor("figyelo/camps/editcontract/" .$data->id, (empty($data->partnerName))?$data->contactEmail:$data->partnerName); ?>
							<?= anchor("belepes/".$data->id."/".$data->uniqID, "<span class='glyphicon glyphicon-log-in m5'></span>", " class='fr' target='_blank'"); ?>
						</td>
						<td><?= (is_Null($data->createdTime)) ? "<i class='glyphicon glyphicon-remove text-warning'></i>" : "<i class='glyphicon glyphicon-ok text-success'></i> " . date("m.d", strtotime($data->createdTime)); ?></td>
						<td><?= (is_Null($data->regEmailSent)) ? "<i class='glyphicon glyphicon-remove text-warning'></i>" : "<i class='glyphicon glyphicon-ok text-success'></i> " . date("m.d", strtotime($data->regEmailSent)); ?></td>
						<td><?= (is_Null($data->contractSaved)) ? "<i class='glyphicon glyphicon-remove text-warning'></i>" : "<i class='glyphicon glyphicon-ok text-success'></i> " . date("m.d", strtotime($data->contractSaved)); ?></td>
						<td>
							<?= (is_Null($data->contractSent)) ? "<i class='glyphicon glyphicon-remove text-warning'></i>" : "<i class='glyphicon glyphicon-ok text-success'></i> " . date("m.d", strtotime($data->contractSent)); ?>

							<?php if (!is_Null($data->contractSaved)) : ?>
								<?= anchor("taborszerzodes/elonezet/?contractID=".$data->id, "<i class='glyphicon glyphicon-envelope'></i>", " target='_blank' class='fr' title='Szerződés PDF' "); ?>
							<?php endif; ?>
						</td>
						<td><?= (is_Null($data->paymentEmailSent)) ? "<i class='glyphicon glyphicon-remove text-warning'></i>" : "<i class='glyphicon glyphicon-ok text-success'></i> " . date("m.d", strtotime($data->paymentEmailSent)); ?></td>
						<td><?= (is_Null($data->contractPaid)) ? "<i class='glyphicon glyphicon-remove text-warning'></i>" : "<i class='glyphicon glyphicon-ok text-success'></i> " . date("m.d", strtotime($data->contractPaid)); ?></td>
						<td><?= (is_Null($data->campEditSent)) ? "<i class='glyphicon glyphicon-remove text-warning'></i>" : "<i class='glyphicon glyphicon-ok text-success'></i> " . date("m.d", strtotime($data->campEditSent)); ?></td>
						<td>
							<?= ($data->closed) ? "<i class='glyphicon glyphicon-lock text-warning'></i>" : ""; ?>

							<?php if (!is_Null($data->campEditDone)) : ?>
							    <?= "<i class='glyphicon glyphicon-ok text-success'></i> " . date("m.d", strtotime($data->campEditDone)); ?>
								<?= anchor("backend/main/locationtest/" . $data->id, "<i class='glyphicon glyphicon-eye-open'></i>", " target='_blank' class='fr' title='Helyszínek' "); ?>
							<?php endif; ?>
						</td>
						<td>
							<?= (is_Null($data->publicationTime)) ? "<i class='glyphicon glyphicon-remove text-warning'></i>" : "<i class='glyphicon glyphicon-ok text-success'></i> " . date("m.d", strtotime($data->publicationTime)); ?>
							<?= ($data->visible) ? "<i class='glyphicon glyphicon-eye-open text-success fr'></i>" : ""; ?>
						</td>
					</tr>
				<?php endforeach; ?>
        </table>
        </form>
        <a href="<?= site_url("figyelo/camps/contracts?export=xls"); ?>" target="_blank">Teljes kontakt lista exportálása Excel-be</a>
	</div>
</div>