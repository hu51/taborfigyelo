<?php

if ($this->input->post("addnew")) {
    $data = array(
        "name" => $this->input->post("new_name", true),
        "alias" => $this->tools->toSlug($this->input->post("new_name", true))
    );
    $this->db->insert("tf_camp_countries", $data);
    message("", "Új ország felrögzítve", "success");
}

if ($this->input->post("update")) {
    $id = $this->input->post("update");

    $name = $this->input->post("name", true);
    $data = array(
		"name" => filter_var($name[$id], FILTER_SANITIZE_STRING),
//		"alias" => $this->tools->toSlug(filter_var($name[$id], FILTER_SANITIZE_STRING)),
    );
    $this->db->update("tf_camp_countries", $data, array("id" => $id));
    message("", "Ország módosítva", "success");
}

$all = $this->db->from("tf_camp_countries")->order_by("name", "ASC")->get()->result();

?>
<div class="panel panel-default">
	<div class="panel-body">
		<h4>Tábor országok:</h4>
		<form method="post">
			<table class="table table-bordered table-condensed">
				<thead>
					<tr>
						<th>Név</th>
						<th>alias</th>
						<th>Engedélyezve</th>
						<th>&nbsp;</th>
					</tr>
					</head>
					<?php foreach ($all as $data): ?>
						<tr>
							<td>
								<input type="text" class="form-control" name="name[<?=$data->id;?>]" value="<?=$data->name;?>">
							</td>
							<td>
								<?=$data->alias;?>
							</td>
							<td>
								<button class="btn btn-primary" type="submit" name="update" value="<?=$data->id;?>" aria-label="">Módosít</button>
							</td>
						</tr>
					<?php endforeach;?>


					<tr class="success">
						<th>Név</th>
						<th>alias</th>
						<th>&nbsp;</th>
					</tr>
					<tr>
						<td>
							<input type="text" class="form-control" name="new_name" placeholder="Új neve" value="">
						</td>
						<td>&nbsp;</td>
						<td>
							<button class="btn btn-success" type="submit" name="addnew" value="1" aria-label="">Új hozzáadása</button>
						</td>
					</tr>
			</table>
		</form>
	</div>
</div>