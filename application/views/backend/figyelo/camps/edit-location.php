<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Táborhelyszín szerkesztése:</h3>
    </div>
    <div class="panel-body">
    	<form id="location_form" action="" method="post">
<?php

$locID    = (int) filter_var($this->uri->segment(4));
$location = $this->db->from("tf_camps")->where("id", $locID)->where("id", $locID)->get()->row();
$contract = $this->db->from("tf_camp_contracts")->where("id", $location->contractID)->get()->row();

if (!$location) {
    redirect("taborhelyek");
}

if (($this->input->post("deleteLocation")) && ($this->input->post("confDelete") == "on")) {
    message("", "A téborhelyszín adatokat!", "success");

    $this->db->delete("tf_camps", array("id" => $locID));
    return;
}

if ($location->closed && !$this->session->userdata("user_isAdmin")) {
    message("", "Ennek a helyszínnek adatai már nem módosíthatók!", "warning");
}

$all      = $this->db->from("tf_camp_types")->order_by("name", "ASC")->get()->result();
$typeList = [];
foreach ($all as $tmp) {
    $typeList[$tmp->id] = $tmp->name;
}

$all     = $this->db->from("tf_camp_category_list")->order_by("name", "ASC")->get()->result();
$catList = [];
foreach ($all as $tmp) {
    $catList[$tmp->id] = $tmp->name;
}

$all         = $this->db->from("tf_camp_countries")->order_by("name", "ASC")->get()->result();
$countryList = array("-1" => "* vagy válasszon a listából");
foreach ($all as $tmp) {
    $countryList[$tmp->id] = $tmp->name;
}

$all = $this->db->from("tf_camp_states")->order_by("name", "ASC")->get()->result();
foreach ($all as $tmp) {
    $stateList[$tmp->id] = $tmp->name;
}

$all = $this->db->from("tf_camp_locations")->order_by("name", "ASC")->get()->result();
foreach ($all as $tmp) {
    $locationList[$tmp->id] = $tmp->name;
}

?>
        <input type="hidden" name="locID" value="<?=$locID;?>">

    	<div class="row alert alert-info">
            <div class="col-lg-12">
                <label for="contactName" class="col-lg-4 ar">Szerződés azonosító :</label>
                <div class="col-lg-8">
                    <input type="text" class="form-control w200 fl" id="contractID" name="contractID" value="<?=$location->contractID;?>">
                    <div class="m10 fl">
                    Partner: <?=$contract->partnerName;?>
                    <a href="<?= site_url("figyelo/camps/editcontract/".$location->contractID); ?>"><button type="button" class="btn btn-primary">Ugrás</button></a>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <label for="closed" class="col-lg-4 ar">Adatok lezárva:</label>
                <div class="col-lg-8">
                    <?=$this->tools->bs_checkbox("closed", $location->closed, array("0" => "Nem", "1" => "Igen"));?>
                </div>
            </div>

            <div class="col-lg-12">
                <label for="closed" class="col-lg-4 ar">Elérhető:</label>
                <div class="col-lg-8">
                    <?=$this->tools->bs_checkbox("published", $location->published, array("0" => "Nem", "1" => "Igen"));?>
                </div>
            </div>

            <div class="col-lg-12">
                <label for="closed" class="col-lg-4 ar">Előresorolt (tól-ig):</label>
                <div class="input-group col-lg-8">
                    <?=$this->tools->bs_checkbox("featured", $location->featured, array("0" => "Nem", "1" => "Igen"));?>
                    <div class="input-group-addon">-</div>
                    <input type="date" class="form-control" id="featuredStart" name="featuredStart" value="<?=$location->featuredStart;?>">
                    <div class="input-group-addon">-</div>
                    <input type="date" class="form-control" id="featuredEnd" name="featuredEnd" value="<?=$location->featuredEnd;?>">
                </div>
            </div>
        </div>

    	<div>
            <div class="col-lg-12">
                <label for="" class="col-lg-4 ar">
                    <a href="<?=site_url("tabor/" . $location->id . "/" . $location->alias . "?key=" . substr($contract->uniqID, -5));?>" target="_blank" class='btn btn-sm btn-info'><i class="glyphicon glyphicon-link"></i> Előnézet</a>
                </label>
                <div class="col-lg-8">
                    <button type="submit" class="btn btn-success" value="1" id="saveLocation" name="saveLocation">Adatok mentése</button>
                    <a href="<?=site_url("figyelo/camps/editturns/" . $location->id);?>" class='btn btn-sm btn-info'><i class="glyphicon glyphicon-refresh"></i> Turnusok szerkesztése</a>
                </div>
            </div>

            <div class="col-lg-12">
                <label for="contactName" class="col-lg-4 ar">WEB SEO url:</label>
                <div class="col-lg-8">
                    <div class="input-group">
                        <div class="input-group-addon"><?= site_url("tabor/"); ?></div>
                        <input type="text" class="form-control" id="alias" name="alias" value="<?=$location->alias;?>" disabled>
                        <div class="input-group-addon">.html</div>
                        <div class="input-group-addon"><input type="checkbox" id="chgseo" name="chgseo"> URL módosítása</div>
                        <div class="input-group-addon"><a href="<?= site_url("tabor/".$location->alias.".html"); ?>" class="btn btn-primary" target="_blank"><span class="glyphicon glyphicon-link"></span></a></div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <label for="contactName" class="col-lg-4 ar">WEB url:</label>
                <div class="col-lg-8">
                    <div class="input-group">
                        <div class="input-group-addon"><?= site_url("tabor/".$location->id."/".url_title(convert_accented_characters($location->name))); ?></div>
                        <div class="input-group-addon"><a href="<?= site_url("tabor/".$location->id."/".url_title(convert_accented_characters($location->name))); ?>" class="btn btn-primary" target="_blank"><span class="glyphicon glyphicon-link"></span></a></div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <label for="contactName" class="col-lg-4 ar">Értékelő url:</label>
                <div class="col-lg-8">
                    <div class="input-group">
                        <div class="input-group-addon"><?= site_url("tabor/".$location->id."/".url_title(convert_accented_characters($location->name))."?ertekeles=".($location->id+$location->contractID)."#rating"); ?></div>
                        <div class="input-group-addon"><a href="<?= site_url("tabor/".$location->id."/".url_title(convert_accented_characters($location->name))."?ertekeles=".($location->id+$location->contractID)."#rating"); ?>" class="btn btn-primary" target="_blank"><span class="glyphicon glyphicon-link"></span></a></div>
                    </div>
                </div>
            </div>
        </div>

    	<div>
            <label>Kapcsolati adatok</label>
    		<div class="col-lg-12">
    			<label for="contactName" class="col-lg-4 ar">Táborszervező neve:</label>
    			<div class="col-lg-8">
    				<input type="text" class="form-control" id="contactName" name="contactName" value="<?=$location->contactName;?>">
    			</div>
            </div>

    		<div class="col-lg-12">
    			<label for="contactPhone" class="col-lg-4 ar">Táborszervező telefonszáma:</label>
    			<div class="col-lg-8">
    				<input type="text" class="form-control" id="contactPhone" name="contactPhone" value="<?=$location->contactPhone;?>">
    			</div>
            </div>

    		<div class="col-lg-12">
    			<label for="contactEmail" class="col-lg-4 ar">Kapcsolattartó e-mail:</label>
    			<div class="col-lg-8">
    				<input type="text" class="form-control" id="contactEmail" name="contactEmail" value="<?=$location->contactEmail;?>">
    			</div>
            </div>

    		<div class="col-lg-12">
    			<label for="name" class="col-lg-4 ar">Weboldal címe:</label>
    			<div class="col-lg-8">
    				<input type="text" class="form-control" id="contactWebsite" name="contactWebsite" value="<?=$location->contactWebsite;?>">
    			</div>
            </div>

    		<div class="col-lg-12">
    			<label for="name" class="col-lg-4 ar">Facebook oldal:</label>
    			<div class="col-lg-8">
    				<input type="text" class="form-control" id="contactFacebook" name="contactFacebook" value="<?=$location->contactFacebook;?>">
    			</div>
            </div>

        </div>
        <hr style="clear: both;" />

        <div>
            <label>Tábor neve és címe</label>
    		<div class="col-lg-12">
                <label for="name" class="col-lg-4 ar">Táborhely neve:</label>
    			<div class="col-lg-8">
                    <input type="text" class="form-control" id="name" name="name" value="<?=$location->name;?>">
    			</div>
    		</div>

            <div class="col-lg-12">
                <label for="address" class="col-lg-4 ar">Ország:</label>
                <div class="col-lg-8">
                    <input type="text" class="form-control" id="country" name="country" value="<?=$location->country_str;?>">
                    <?=$this->tools->bs_select("countryID", $location->countryID, $countryList, "form-control highlight");?>
                </div>
            </div>

            <div class="col-lg-12">
                <label for="address" class="col-lg-4 ar">Megye:</label>
                <div class="col-lg-8">
                    <input type="text" class="form-control" id="state" name="state" value="<?=$location->state_str;?>">
                    <?php if ($location->stateID): ?>
                        <?=$this->tools->bs_select("stateID", $location->stateID, $stateList, "form-control highlight");?>
                    <?php else: ?>
                        <select class="form-control highlight" name="stateID" id="stateID" style="display: none;"></select>
                    <?php endif;?>
                </div>
            </div>

            <div class="col-lg-12">
                <label for="address" class="col-lg-4 ar">Helyszín:</label>
                <div class="col-lg-8">
                    <input type="text" class="form-control" id="location" name="location" value="<?=$location->location_str;?>">
                    <?php if ($location->stateID): ?>
                        <?=$this->tools->bs_select("locationID", $location->locationID, $locationList, "form-control highlight");?>
                    <?php else: ?>
                        <select class="form-control highlight" name="locationID" id="locationID" style="display: none;"></select>
                    <?php endif;?>
                </div>
            </div>

            <div class="col-lg-12">
                <label for="address" class="col-lg-4 ar">Irányítószám:</label>
                <div class="col-lg-8">
                    <input type="text" class="form-control geocode w100" id="zip" name="zip" maxlength="4" value="<?=$location->zip;?>">
                </div>
            </div>


            <div class="col-lg-12">
                <label for="address" class="col-lg-4 ar">Település:</label>
                <div class="col-lg-8">
                    <input type="text" class="form-control geocode" id="city" name="city" value="<?=$location->city;?>">
                </div>
            </div>

            <div class="col-lg-12">
    			<label for="address" class="col-lg-4 ar">Utca, házszám:</label>
    			<div class="col-lg-8">
                    <input type="text" class="form-control geocode" id="address" name="address" value="<?=$location->address;?>">
    			</div>
            </div>

            <div class="col-lg-12">
    			<label for="address" class="col-lg-4 ar">Koordináta (LAT - 47.., LON - 19...):</label>
    			<div class="col-lg-3">
                    <input type="text" class="form-control" placeholder="lattitude" id="lat" name="lat" value="<?=$location->lat;?>">
    			</div>
    			<div class="col-lg-3">
                    <input type="text" class="form-control" placeholder="longitude" id="lng" name="lng" value="<?=$location->lng;?>">
    			</div>
				<div class="col-lg-2">
                    <button type="button" id="geocode" class="btn btn-primary"><i class="fa fa-globe"></i> Geocode lekérés</button>
    			</div>
            </div>

        </div>
        <hr style="clear: both;" />

        <div>
            <label>Tábor paraméterek</label>
    		<div class="col-lg-12">
                <label for="facebook" class="col-lg-4 ar">Táborminősítő.hu bejegyzés azonosítója:</label>
    			<div class="col-lg-8">
                    <input type="text" class="form-control w200" id="minositoID" name="minositoID" value="<?=$location->minositoID;?>">
    			</div>
            </div>

    		<div class="col-lg-12">
                <label for="facebook" class="col-lg-4 ar">Tábor típusa:</label>
    			<div class="col-lg-8">
                    <?=$this->tools->bs_select("typeID", $location->typeID, $typeList, "form-control");?>
    			</div>
    		</div>

    		<div class="col-lg-12">
                <label for="description" class="col-lg-4 ar">Rövid leírás:</label>
    			<div class="col-lg-8">
                    <textarea class="form-control" name="description" rows="6"><?=strip_tags($location->description);?></textarea>
    			</div>
            </div>

    		<div class="col-lg-12">
                <label for="text" class="col-lg-4 ar">Hosszú leírás:</label>
    			<div class="col-lg-8">
                    <textarea class="form-control htmlLightEditor" name="text" rows="7"><?=$location->text;?></textarea>
    			</div>
            </div>

    		<div class="col-lg-12">
                <label for="text" class="col-lg-4 ar">Napi programterv:</label>
    			<div class="col-lg-8">
                    <textarea class="form-control htmlLightEditor" name="program" rows="7"><?=$location->program;?></textarea>
    			</div>
            </div>

    		<div class="col-lg-12">
                <label for="text" class="col-lg-4 ar">Egy táborturnus ára:</label>
    			<div class="col-lg-8">
                    <input type="text" class="form-control w200" id="price" name="price" maxlength="8" value="<?=$location->price;?>">
    			</div>
            </div>

    		<div class="col-lg-12">
                <label class="col-lg-4 ar">Tábortémák száma:</label>
    			<div class="col-lg-8">
                    <input type="text" class="form-control w200" id="themes" name="themes" maxlength="8" value="<?=$location->themes;?>">
    			</div>
    		</div>

            <div class="col-lg-12">
                <?php foreach ($catList as $id => $name):
                    $tmp = $this->db->from("tf_camp_categories")->where("campID", $location->id)->where("categoryID", $id)->get()->row();
                    ?>
                        <div class="col-md-4">
                        <?php if ($tmp): ?>
                            <input type='checkbox' name='categories[]' class='categories' value='<?=$id;?>' checked> <?=$name;?><br>
                        <?php else: ?>
                            <input type='checkbox' name='categories[]' class='categories' value='<?=$id;?>'> <?=$name;?><br>
                        <?php endif;?>
                    </div>
                <?php endforeach;?>
            </div>

            <br />

    		<div class="col-lg-12">
    			<label for="facebook" class="col-lg-4 ar">Életkor (tól-ig):</label>
    			<div class="col-lg-4">
                    <input type="number" class="form-control" id="age_min" name="age_min" value="<?=$location->age_min;?>">
    			</div>
    			<div class="col-lg-4">
                    <input type="number" class="form-control" id="age_max" name="age_max" value="<?=$location->age_max;?>">
    			</div>
    		</div>

    		<div class="col-lg-12">
    			<label for="facebook" class="col-lg-4 ar">Szépkártya elfogadás:</label>
    			<div class="col-lg-8">
                    <div class="row">
                        <div class="col-md-4">
                            MKB
                        </div>
                        <div class="col-md-8">
                            <?=$this->tools->bs_checkbox("szep_MKB", $location->szep_MKB, array("0" => "Nem", "1" => "Igen"));?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            OTP
                        </div>
                        <div class="col-md-8">
                            <?=$this->tools->bs_checkbox("szep_OTP", $location->szep_OTP, array("0" => "Nem", "1" => "Igen"));?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            K&H
                        </div>
                        <div class="col-md-8">
                            <?=$this->tools->bs_checkbox("szep_KH", $location->szep_KH, array("0" => "Nem", "1" => "Igen"));?>
                        </div>
                    </div>
    			</div>
    		</div>

    	</div>

    	<div>
            <div class="col-lg-12">
                <label for="contactName" class="col-lg-4 ar"></label>
                <div class="col-lg-8">
                    <button type="submit" class="btn btn-success" value="1" id="saveLocation" name="saveLocation">Adatok mentése</button>
                </div>
            </div>

            <?php

$members = $this->db->from("tf_camp_members")->where("campID", $locID)->get()->num_rows();
$turns   = $this->db->from("tf_camp_turns")->where("campID", $locID)->get()->num_rows();
$photos  = $this->db->from("tf_camp_images")->where("campID", $locID)->get()->num_rows();

?>
            <div class="col-lg-12">
                <label for="contactName" class="col-lg-4 ar">
                    A táborra eddig ennyi ember jelentkezett: <?=$members;?><br>
                    Turnusok száma: <?=$turns;?><br>
                    Feltöltött képek száma: <?=$photos;?><br>
                </label>
                <div class="col-lg-8">
                    <?php if (($members + $turns + $photos) == 0): ?>
                    <button type="submit" class="btn btn-danger" value="1" id="deleteLocation" name="deleteLocation">Táborhely és adatok végleges törlése</button>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="confDelete">
                            Törlés megerősítése
                        </label>
                    </div>
                    <?php else: ?>
                        <span class="label label-danger">Ez a bejegyzés már nem törölhető!</span>
                    <?php endif;?>
                </div>
            </div>
        </div>

    </form>
    </div>
</div>

<br style="clear: both; "/>
<script>

    var limit = <?=$location->themes;?>

    $('.categories').on('change', function(evt) {
        if($('.categories:checked').length > limit) {
            this.checked = false;
            $.notify(
                "Nem jelölhető be több téma!",
                {
                    type: "warning",
                    placement: {
                        from: "bottom",
                        align: "center"
                    },
                    offset: 100,
                    delay: 1000
                }
            );
        }
    });

    $("#country").keyup(function (e) {
        $('#countryID option').eq(0).prop('selected', true);
    });

    $("#state").keyup(function (e) {
        $('#stateID option').eq(0).prop('selected', true);
    });

    $("#location").keyup(function (e) {
        $('#locationID option').eq(0).prop('selected', true);
    });

    $("#countryID").change(function() {
        id = $(this).val();

        $("#stateID").show();

        if ($("#countryID option:selected").val() != -1) {
            $("#country").val($("#countryID option:selected").text());
        }

        $("#stateID option").remove();
        $("#locationID option").remove();

        var $dropdown = $("#stateID");
        $dropdown.append($("<option />").val("-1").text("# vagy válasszon a listából" ));

        $.ajax({
            type: "get",
            url: "<?=site_url("ajax/statelist");?>",
            data: { q: id },
            dataType: "json",
            success: function (response) {
                if (response.status == "ok") {
                    var $dropdown = $("#stateID");
                    $.each(response.data, function() {
                        $dropdown.append($("<option />").val(this.id).text(this.name));
                    });

                    $("#stateID").onchange;
                }
            }
        });
    });

    $("#stateID").change(function() {
        id = $(this).val();
        $("#locationID option").remove();
        var $dropdown = $("#locationID");
        $dropdown.append($("<option />").val("-1").text("# vagy válasszon a listából" ));

        $("#locationID").show();

        if ($("#stateID option:selected").val() != -1) {
            $("#state").val($("#stateID option:selected").text());
        }

        $.ajax({
            type: "get",
            url: "<?=site_url("ajax/locationList");?>",
            data: { q: id },
            dataType: "json",
            success: function (response) {
                if (response.status == "ok") {
                    $.each(response.data, function() {
                        $dropdown.append($("<option />").val(this.id).text(this.name));
                    });
                }
            }
        });
    });

    $("#locationID").change(function() {
        if ($("#locationID option:selected").val() != -1) {
            $("#location").val($("#locationID option:selected").text());
        }
    });

    $("#zip").keyup(function() {
        id = $(this).val();

        $.ajax({
            type: "get",
            url: "<?=site_url("ajax/zip2city");?>",
            data: { q: id },
            dataType: "json",
            success: function (response) {
                if (response.status == "ok") {
                    $("#city").val(response.data.city);
                }
            }
        });
    });


    $("#chgseo").click(function(){
        chk = $("#chgseo").not(":checked");
        $("#alias").prop("disabled", $(this).is(":not(:checked)"));
    });


    $("#geocode").click(function (e) { 
        GeoCode();
    });

    function GeoCode()
    {
        street = $("#zip").val()+"+"+$("#city").val()+"+"+$("#address").val();

        $.ajax({
            type: "get",
            url: "https://maps.googleapis.com/maps/api/geocode/json?key=<?= $this->tools->GetSetting("googlemap-key"); ?>&address="+street,
            dataType: "json",
            success: function (response) {
                if (response.status !== "OK") {                   
                    $.notify(
                        'Google API: '+response.error_message,
                        {
                            type: "warning",
                            placement: {
                                from: "bottom",
                                align: "center"
                            },
                            offset: 100,
                            delay: 2000
                        }
                    );                    
                } else {
                    $("#lat").val(response.results[0].geometry.location.lat);
                    $("#lng").val(response.results[0].geometry.location.lng);

                    $.notify(
                        'Geocoding OK',
                        {
                            type: "success",
                            placement: {
                                from: "bottom",
                                align: "center"
                            },
                            offset: 100,
                            delay: 1000
                        }
                    );                      
                }             
            }
        });
    }

</script>
<script src="<?=base_url();?>js/tinymce/tinymce.min.js" type="text/javascript"></script>
<script>

    tinymce.init({
        mode: "specific_textareas",
        editor_selector: "htmlLightEditor",
        height: 400,
        menubar:false,
        theme: 'modern',
        plugins: ' directionality visualblocks link visualchars hr nonbreaking wordcount code paste',
        paste_auto_cleanup_on_paste : true,
        paste_convert_headers_to_strong : false,
        paste_strip_class_attributes : "all",
        paste_remove_spans : true,
        paste_remove_styles : true,
        remove_linebreaks : false,
        valid_elements: "p[style],strong/b,div[align],br,a[href]",
        toolbar1: ' bold italic strikethrough forecolor backcolor | alignleft aligncenter alignright alignjustify | link unlink | removeformat code',
        content_css: [
            '//www.tinymce.com/css/codepen.min.css'
        ],
        relative_urls: false
    });
</script>
