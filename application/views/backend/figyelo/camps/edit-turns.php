<?php

$locID    = (int) filter_var($this->uri->segment(4));
$location = $this->db->from("tf_camps")->where("id", $locID)->get()->row();
$contract = $this->db->from("tf_camp_contracts")->where("id", $location->contractID)->get()->row();

if (!$location) {
    message("", "Nincs ilyen táborhely!", "warning");
    return;
}

if ($this->input->post("newTurn") && $this->input->post("title_new")) {
    $data = array(
        "title"     => $this->input->post("title_new"),
        "validfrom" => $this->input->post("validfrom_new"),
        "validto"   => $this->input->post("validto_new"),
        "price"     => $this->input->post("price_new"),
        "info"      => $this->input->post("info_new"),
        "campID"    => $locID,
        "published" => 1,
    );

    if ($this->input->post("validfrom_new") > $this->input->post("validto_new")) {
        message("", "A záródátum kisebb mint a kezdő dátum!", "warning");
    } else {
        if ($this->db->insert("tf_camp_turns", $data)) {
            message("", "Új turnus hozzádva", "success");
        } else {
            print_r($this->db->error());
        }
    }
}

if ($this->input->post("update")) {
    $id   = $this->input->post("update");
    $data = array(
        "title"     => $this->input->post("title_" . $id),
        "validfrom" => $this->input->post("validfrom_" . $id),
        "validto"   => $this->input->post("validto_" . $id),
        "price"     => $this->input->post("price_" . $id),
        "info"      => $this->input->post("info_" . $id),
        "published" => $this->input->post("published_" . $id),
    );

    if ($this->input->post("validfrom_" . $id) > $this->input->post("validto_" . $id)) {
        message("", "A záródátum kisebb mint a kezdő dátum!", "warning");
    } else {    
        if ($this->db->update("tf_camp_turns", $data, array("id" => $id))) {
            message("", "Turnus módosítva", "success");
        } else {
            print_r($this->db->error());
        }
    }
}

if ($this->input->post("delete")) {
    $id = $this->input->post("delete");

    $members = $this->db->from("tf_camp_members")->where("turnID", $id)->where("status >", -1)->count_all_results();

    if ($members > 0) {
        message("Turnus törlése", "Nem lehet olyan turnust törölni amire van aktív jelentkezés!", "success");
    } else {
        if ($this->db->delete("tf_camp_turns", array("id" => $id))) {
            message("", "Turnus törölve");
        } else {
            print_r($this->db->error());
        }
    }
}

$turns = $this->db->from("tf_camp_turns")->where("campID", $locID)->order_by("validfrom")->get()->result();

?>
<form method="post">
<div class="panel panel-default">
      <div class="panel-heading">
            <h3 class="panel-title">Tábor turnusok szerkesztése</h3>
      </div>
      <div class="panel-body">

    <div>Tábor neve: <b><a href="<?= site_url("figyelo/camps/editlocation/".$location->id); ?>"><?=$location->name;?></a></b></div>
    <table class="table table-bordered table-condensed">
        <thead>
            <tr>
                <th>Név / Kezdő dátum</th>
                <th>Záró dátum</th>
                <th>Turnus ára</th>
                <th>Látható</th>
            </tr>
        </head>
        <?php foreach ($turns as $data):
    $turnMembers = $this->db->from("tf_camp_members")->where("turnID", $data->id)->get()->num_rows();
    ?>
			            <tr class="<?=($data->published) ? "" : "danger";?>">
			                <td colspan="3">
			                    <input type="text" class="form-control" name="title_<?=$data->id;?>" value="<?=htmlentities($data->title);?>">
			                </td>
			                <td>
			                    <?=$this->tools->bs_checkbox("published_" . $data->id, $data->published);?>
			                </td>
			            </tr>
			            <tr class="<?=($data->published) ? "" : "danger";?>">
			                    <td>
			                        <input type="date" class="form-control" name="validfrom_<?=$data->id;?>" value="<?=$data->validfrom;?>">
			                    </td>
			                <td>
			                    <input type="date" class="form-control" name="validto_<?=$data->id;?>" value="<?=$data->validto;?>">
			                </td>
			                <td>
			                    <input type="text" class="form-control ar" name="price_<?=$data->id;?>" value="<?=(int) $data->price;?>">
			                </td>
			                <td>
			                    <button class="btn btn-primary" name="update" value="<?=$data->id;?>">Módosítás</button>
			                    <button class="btn btn-danger" name="delete" value="<?=$data->id;?>">Törlés</button>
			                </td>
			            </tr>
			            <tr class="<?=($data->published) ? "" : "danger";?>">
			                <td colspan="4">
			                    <textarea class="form-control" name="info_<?=$data->id;?>" rows="2" placeholder="Rövid leírás, nem szükséges megadni..."><?=htmlentities($data->info);?></textarea>
			                </td>
			            </tr>
			            <?php if ($turnMembers > 0): ?>
			                <tr>
			                    <td colspan="4">
			                        <?=anchor(site_url("taborturnusok/" . $locID . "/resztvevok/" . $data->id), "Tábor turnusra jelentkezett emberek listája", " target='_blank'");?> (<?=$turnMembers;?> db)
			                    </td>
			                </tr>
			            <?php endif;?>
            <?php endforeach;?>

        </table>

        </div>
</div>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Új turnus hozzáadása</h3>
                </div>
                <div class="panel-body">
                <table class="table table-bordered table-condensed">
                    <thead>
                        <tr>
                            <th>Név / Kezdő dátum</th>
                            <th>Záró dátum</th>
                            <th>Turnus ára</th>
                            <th>Látható</th>
                        </tr>
                    </head>
                    <tr>
                        <td colspan="3">
                            <input type="text" class="form-control" name="title_new" placeholder="Új turnus neve, pl. a tábor megnevezése! Megadása kötelező!">
                        </td>
                        <td>
                            IGEN
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="date" class="form-control" name="validfrom_new" value="<?=date("Y-m-d");?>">
                        </td>
                        <td>
                            <input type="date" class="form-control" name="validto_new" value="<?=date("Y-m-d");?>">
                        </td>
                        <td>
                            <input type="text" class="form-control ar" name="price_new" value="<?=$location->price;?>">
                        </td>
                        <td>
                            <button class="btn btn-info" name="newTurn" value="1" >Új turnus rögzítése</button>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <textarea class="form-control" name="info_new" rows="2" placeholder="Rövid leírás, nem szükséges megadni..."></textarea>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
</form>
