<?php

	$all = $this->db->from("tf_camp_contracts")->where("campEditDone is not NULL")->where("publicationTime is NULL")->order_by("id", "DESC")->get()->result();
	
?>
<div class="panel panel-default">
	<div class="panel-body">
		<h4>Végleges ellenőrzésre váró szerződések:</h4>
		<form method="post">
		<table class="table table-bordered table-condensed">
			<thead>
			<tr>
				<th class="w150">&nbsp;</th>
				<th>Partner</th>
				<th>Létrehozva</th>
				<th>Tábor adatok mentve</th>
				<th></th>
			</tr>	
			</head>
		<?php foreach ($all as $data): ?>
			<tr>
				<td>
					<?= anchor("figyelo/camps/editcontract/".$data->id, "Szerződés", " class='btn btn-sm btn-primary' target='_blank'"); ?>
				</td>
				<td><?= $data->partnerName; ?></td>
				<td>
					<?= (is_Null($data->createdTime))?"<i class='glyphicon glyphicon-remove text-warning'></i>":"<i class='glyphicon glyphicon-ok text-success'></i> ".date("m.d. H:i", strtotime($data->createdTime)); ?>
				</td>
				<td>
					<?= (is_Null($data->campEditDone))?"<i class='glyphicon glyphicon-remove text-warning'></i>":"<i class='glyphicon glyphicon-ok text-success'></i> ".date("m.d. H:i", strtotime($data->campEditDone)); ?>
				</td>
				<td>					
                    <button type="submit" class="btn btn-sm btn-success" name="confirmFinalize" value="<?= $data->id; ?>"> Tábor publikálása </button>
                    <input type="checkbox" name="confFinalize" value="<?= $data->id; ?>"> megerősítés, levél kiküldése					
				</td>

			</tr>
		<?php endforeach; ?>
		</table>
		</form>
	</div>
</div>
