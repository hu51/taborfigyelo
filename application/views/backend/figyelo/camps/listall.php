<legend>Táborok listája</legend>
<?php

$all     = $this->db->from("tf_camp_category_list")->order_by("name", "ASC")->get()->result();
$catList = array("-1" => "* összes");
foreach ($all as $tmp) {
    $catList[$tmp->id] = $tmp->name;
}

$all         = $this->db->from("tf_camp_countries")->order_by("name", "ASC")->get()->result();
$countryList = array("-1" => "* összes");
foreach ($all as $tmp) {
    $countryList[$tmp->id] = $tmp->name;
}

$all       = $this->db->from("tf_camp_states")->order_by("name", "ASC")->get()->result();
$stateList = array("-1" => "* összes");
foreach ($all as $tmp) {
    $stateList[$tmp->id] = $tmp->name;
}

if (($this->input->post_get("filter_country") !== null) && ($this->input->post_get("filter_country") !== "-1")) {
    $all = $this->db->from("tf_camp_states")->where("countryID", (int) $this->input->post_get("filter_country"))->order_by("name", "ASC")->get()->result();
} else {
    $all = $this->db->from("tf_camp_states")->order_by("name", "ASC")->get()->result();
}
$stateFilterList = array("-1" => "* összes");
foreach ($all as $tmp) {
    $stateFilterList[$tmp->id] = $tmp->name;
}

if (!$this->input->get("oldal")) {
    $pageID = 1;
} else {
    $pageID = (int) $this->input->get("oldal");
}


if ($this->input->post_get("disableOld")=="1" && $this->input->post("confDisable") == "on") {
    $this->db->select("c.id, max( t.validfrom ) AS lastDate");
    $this->db->from("tf_camps as c");
    $this->db->join("tf_camp_turns as t", "t.campID = c.id");
    $this->db->where("c.published", 1);
    $this->db->group_by("c.id");
    $this->db->having("lastDate <", date("Y-m-d"));
    $oldCamps = $this->db->get()->result();
    foreach ($oldCamps as $tmp) {
        $this->db->update("tf_camps", array("published" => "0"), array("id" => $tmp->id));
    }
}

$this->db->select("c.id, max( t.validfrom ) AS lastDate");
$this->db->from("tf_camps as c");
$this->db->join("tf_camp_turns as t", "t.campID = c.id");
$this->db->where("c.published", 1);
$this->db->group_by("c.id");
$this->db->having("lastDate <", date("Y-m-d"));
$oldCamps = $this->db->count_all_results();

$this->db->from("tf_camps");
if ($this->input->post_get("filter_name")) {
	$this->db->like("name", $this->input->post_get("filter_name", true));
}
if (($this->input->post_get("filter_country") !== null) && ($this->input->post_get("filter_country") !== "-1")) {
	$this->db->where("countryID", (int) $this->input->post_get("filter_country"));
}
if (($this->input->post_get("filter_state") !== null) && ($this->input->post_get("filter_state") !== "-1")) {
	$this->db->where("stateID", (int) $this->input->post_get("filter_state"));
}
if (($this->input->post_get("filter_cat") !== null) && ($this->input->post_get("filter_cat") !== "-1")) {
	$this->db->where("categoryID", (int) $this->input->post_get("filter_cat"));
}
if (($this->input->post_get("filter_vis") !== null) && ($this->input->post_get("filter_vis") !== "-1")) {
	$this->db->where("published", (int) $this->input->post_get("filter_vis"));
}
$this->db->order_by("id", "DESC");
$allRows = $this->db->count_all_results();
$pageMaxRows  = 50;
$page         = $this->tools->pagination("figyelo/camps/", $allRows, $pageMaxRows, $pageID);
$pageID       = $page["current"];
$pageStartRow = ($pageID - 1) * $pageMaxRows;

$this->db->select("*, (SELECT uniqID FROM tf_camp_contracts WHERE id=contractID) as uniqID, "
    ."(SELECT count(1) FROM tf_camp_members WHERE campID=tf_camps.id) as members, "
    ."(SELECT partnerName FROM tf_camp_contracts WHERE id=contractID) as partnerName");
$this->db->from("tf_camps");
if ($this->input->post_get("filter_name")) {
    $this->db->like("name", $this->input->post_get("filter_name", true));
}
if ($this->input->post_get("filter_partner")) {
    $this->db->having("partnerName LIKE '%".$this->input->post_get("filter_partner", true)."%'");
}
if (($this->input->post_get("filter_country") !== null) && ($this->input->post_get("filter_country") !== "-1")) {
    $this->db->where("countryID", (int) $this->input->post_get("filter_country"));
}
if (($this->input->post_get("filter_state") !== null) && ($this->input->post_get("filter_state") !== "-1")) {
    $this->db->where("stateID", (int) $this->input->post_get("filter_state"));
}
if (($this->input->post_get("filter_cat") !== null) && ($this->input->post_get("filter_cat") !== "-1")) {
    $this->db->where("categoryID", (int) $this->input->post_get("filter_cat"));
}
if (($this->input->post_get("filter_vis") !== null) && ($this->input->post_get("filter_vis") !== "-1")) {
    $this->db->where("published", (int) $this->input->post_get("filter_vis"));
}
$this->db->order_by("id", "DESC");
$this->db->limit($pageMaxRows, $pageStartRow);
$camps = $this->db->get()->result();

?>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="col-lg-12">
			<p>Összes találat: <?= $allRows; ?></p>
			<?= $page["html"]; ?>
            
            <form action="" method="POST" role="form">            
                <div class="form-group">
                    <label>Összes publikus tábor inaktívvá tétele, aminek nincs a mai nap utáni turnusa (<?= date("Y-m-d"); ?>)</label>
                    <button type="submit" name="disableOld" value="1" class="btn btn-sm btn-warning"><?= $oldCamps; ?> db tétel inaktívvá tétele</button>
                    <input type="checkbox" name="confDisable" value="on"> Megerősítés                    
                </div>
            </form>
            
			<form method="get">
				<table class="table table-bordered table-condensed">
					<thead>
						<tr class="active">
							<th><i class="glyphicon glyphicon-search m5"></i></th>
							<th>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="filter_name" value="<?=$this->tools->set_value("filter_name");?>" placeholder="Keresés">
									<span class="input-group-btn">
                                        <button type="submit" name="setfilter" value="1" class="btn btn-primary">Keres</button>
									</span>
								</div>
							</th>
                            <th><input type="text" class="form-control" name="filter_partner" value="<?=$this->tools->set_value("filter_partner");?>" placeholder="..."></th>
							<th><?=$this->tools->bs_select("filter_cat", $this->tools->set_value("filter_cat", "-1"), $catList, "form-control");?></th>
							<th><?=$this->tools->bs_select("filter_country", $this->tools->set_value("filter_country", "-1"), $countryList, "form-control");?></th>
							<th><?=$this->tools->bs_select("filter_state", $this->tools->set_value("filter_state", "-1"), $stateFilterList, "form-control");?></th>
							<th><?=$this->tools->bs_select("filter_vis", $this->tools->set_value("filter_vis", "-1"), array("-1" => "# összes", "0" => "Nem", "1" => "Igen"), "form-control");?></th>
							<th>&nbsp;</th>
						</tr>
						<tr>
							<th>ID</th>
							<th>Név</th>
							<th>Partner</th>
							<th>Kategória</th>
							<th>Ország</th>
							<th>Megye/Régió</th>
							<th>Engedélyezve</th>
							<th>Jel.</th>
						</tr>
					</head>
					<?php foreach ($camps as $data): ?>
						<tr class="<?=($data->published) ? "" : "danger";?>">
							<td><?=$data->id;?></td>
							<td>
								<a href="<?=site_url("figyelo/camps/editlocation/" . $data->id);?>"><i class="glyphicon glyphicon-edit"></i>
								<?=$data->name;?></a>
							</td>
                            <td>
                                <a href="<?=site_url("figyelo/camps/editcontract/" . $data->contractID);?>"><i class="glyphicon glyphicon-edit"></i>
                                <?=$data->partnerName;?></a>
                                <?= anchor("belepes/".$data->contractID."/".$data->uniqID, "<span class='glyphicon glyphicon-log-in m5'></span>", " class='fr' target='_blank'"); ?>
                            </td>
							<td><?=$catList[$data->categoryID];?></td>
							<td><?=$countryList[$data->countryID];?></td>
							<td><?=$stateList[$data->stateID];?></td>
							<td><?=($data->published) ? "<span class='glyphicon glyphicon-eye-open'></span>" : "";?></td>
                            <td>
                                <?php if($data->members > 0): ?>
                                    <a href="<?=site_url("figyelo/camps/members/" . $data->id);?>" class="small" target="_blank"><span class="glyphicon glyphicon-list"></span> <?= $data->members; ?>
                                <?php else: ?>
                                    0
                                <?php endif; ?>
                            </td>
						</tr>
					<?php endforeach;?>
				</table>
			</form>
			<?=$page["html"];?>
		</div>
		<a href="<?= site_url("figyelo/camps/excel"); ?>" target="_blank">Aktuális táborlista exportálása Excel-be</a>
	</div>
</div>