<?php

$locID    = (int) filter_var($this->uri->segment(4));
$location = $this->db->from("tf_camp_locations")->where("id", $locID)->where("id", $locID)->get()->row();

$root = $this->db->from("tf_camp_states")->order_by("name", "ASC")->get()->result();
foreach ($root as $tmp) {
    $rootList[$tmp->id] = $tmp->name;
}

?>
<div class="panel panel-default">
	<div class="panel-body">
        <h4>Tábor helyszín szerkesztése:</h4>
			<form method="post" action="">
			<table class="table table-bordered table-condensed">
				<thead>
					<tr>
						<th>Név</th>
						<th>URL</th>
						<th>Megye</th>
						<th>Látható</th>
					</tr>
					</head>
					<tr class="success">
						<th>Név</th>
						<th>URL</th>
						<th>Megye</th>
						<th>Látható</th>
						<th>&nbsp;</th>
					</tr>
					<tr>
						<td>
							<input type="text" class="form-control" name="name" placeholder="Új neve" value="<?= $location->name; ?>">
						</td>
						<td>
							<input type="text" class="form-control" name="alias" placeholder="url" value="<?= $location->alias; ?>">
						</td>
						<td>
							<?=$this->tools->bs_select("stateID", $location->stateID, $rootList, "form-control");?>
						</td>
						<td>
							<?=$this->tools->bs_checkbox("published", $location->published);?>
						</td>
						<td>
							<button class="btn btn-success" type="submit" name="savelic" value="1" aria-label="">Mentés</button>
						</td>
					</tr>
			</table>
		</form>
	</div>
</div>