<?php

$root = $this->db->from("tf_camp_states")->order_by("name", "ASC")->get()->result();
foreach ($root as $tmp) {
    $rootList[$tmp->id] = $tmp->name;
}

$all = $this->db->from("tf_camp_locations")->order_by("published", "DESC")->order_by("name", "ASC")->get()->result();

?>
<div class="panel panel-default">
	<div class="panel-body">
        <h4>Tábor helyszínek:</h4>
			<form method="post" action="">
			<table class="table table-bordered table-condensed">
				<thead>
					<tr>
						<th>Név</th>
						<th>URL</th>
						<th>Megye</th>
						<th>Látható</th>
					</tr>
					</head>
					<?php foreach ($all as $data): ?>
						<tr>
							<td>
								<a href="<?=base_url("/figyelo/camps/locationedit/" . $data->id);?>" target="_blank"><i class="glyphicon glyphicon-edit"></i></a>
								<?=$data->name;?>
							</td>
							<td>
								<?=$data->alias;?>
							</td>
							<td>
								<?=$rootList[$data->stateID];?>
							</td>
							<td>
								<?=($data->published) ? "<i class='glyphicon glyphicon-ok'></i>" : "";?>
							</td>
						</tr>
					<?php endforeach;?>

					<tr class="success">
						<th>Név</th>
						<th>URL</th>
						<th>Megye</th>
						<th>Látható</th>
						<th>&nbsp;</th>
					</tr>
					<tr>
						<td>
							<input type="text" class="form-control" name="new_name" placeholder="Új neve" value="">
						</td>
						<td>&nbsp;</td>
						<td>
							<?=$this->tools->bs_select("new_stateID", 0, $rootList, "form-control");?>
						</td>
						<td>
							<?=$this->tools->bs_checkbox("new_published", 0);?>
						</td>
						<td>
							<button class="btn btn-success" type="submit" name="addnew" value="1" aria-label="">Új hozzáadása</button>
						</td>
					</tr>
			</table>
		</form>
	</div>
</div>