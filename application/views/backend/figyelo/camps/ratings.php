<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Tábor értékelések</h3>
    </div>

    <div class="panel-body">
        <?php
    
            if ($this->input->post("enableComment")) {  
                $id = $this->input->post("enableComment");
                $this->db->update("tf_camp_comments", array("status" => 1), array("id" => (int) $id));
                message("", "Értékelés jóváhagyva", "success");
            }

            if ($this->input->post("blockComment")) {  
                $id = $this->input->post("blockComment");
                $this->db->update("tf_camp_comments", array("status" => -1), array("id" => (int) $id));
                message("", "Értékelés letitlva", "success");
            }

        ?>
        <form action="" method="POST" role="form">
            <button type="submit" name="deleteSelected" value="1" class="btn btn-sm btn-warning fr">Bejelöltek törlése</button><br>
            <?php

                $this->db->delete("tf_camp_comments", array("emailConfirmed" => 0, "createdDatetime <" => date("Y-m-d H:i:s", strtotime("-30 DAY"))));

                $this->db->select('id');
                $this->db->from("tf_camp_comments");
                $allRows = $this->db->get()->num_rows();

                if (!$this->input->get("oldal")) {
                    $pageID = 1;
                } else {
                    $pageID = (int) $this->input->get("oldal");
                }
                $pageMaxRows = 20;

                $page         = $this->tools->pagination("figyelo/camp/ratings", $allRows, $pageMaxRows, $pageID);
                $pageID       = $page["current"];
                $pageStartRow = ($pageID - 1) * $pageMaxRows;

                $this->db->select("co.*, c.name as campTitle, t.validfrom, t.validto, t.title as turnTitle", false);      
                $this->db->from("tf_camp_comments as co");      
                $this->db->join("tf_camps as c", "c.id=co.campID", "LEFT");      
                $this->db->join("tf_camp_turns as t", "t.id=co.turnID", "LEFT");      
                //$this->db->where("emailConfirmed", "1");
                $this->db->order_by("co.id", "DESC");
                $this->db->limit($pageMaxRows, $pageStartRow);
                $query = $this->db->get();

                echo $page["html"];
                ?>     
            <p>Az email megerősítés nélküli bejegyzések 30 nap múlva törlődnek.</p>           
            <table class="table table-condended table-bordered">
                <thead>
                    <tr>
                        <th></th>
                        <th>E-mail</th>
                        <th>Cím</th>
                        <th>Leírás</th>
                        <th>Értékelés</th>
                        <th>IP/Dátum</th>
                        <th>Tábor</th>
                    </tr>
                </thead>
                <?php

                    foreach ($query->result() as $data): 
                        switch ($data->status) {
                            case '1':
                                $clr = "success";
                                break;
                            case '-1':
                                $clr = "danger";
                                break;
                            
                            default:
                                $clr = "";
                                break;
                        }
                    ?>
                        <tr class="<?=$clr; ?>">
                            <td><?=$data->id;?></td>
                            <td>
                                <?=$data->email;?>

                                <?php if($data->emailConfirmed): ?>
                                    <span class="glyphicon glyphicon-check text-success"></span>
                                <?php endif; ?>
                                <button type="submit" name="blockUser" value="<?= $data->id; ?>" class="btn btn-sm btn-danger fr"><span class="glyphicon glyphicon-user" title="Felhasználó tiltása"></span></button>
                            </td>
                            <td>
                                <?php if($data->status<>1): ?>
                                    <button type="submit" name="enableComment" value="<?= $data->id; ?>" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-check" title="Értékelés jóváhagyása"></span></button>
                                <?php endif; ?>
                                <?=$data->title;?>
                                <?php if($data->status<>-1): ?>
                                    <button type="submit" name="blockComment" value="<?= $data->id; ?>" class="btn btn-sm btn-danger fr"><span class="glyphicon glyphicon-remove" title="Értékelés tiltása"></span></button>
                                <?php endif; ?>
                            </td>
                            <td class="small"><?=$data->comment;?></td>
                            <td><?=$data->rating;?></td>
                            <td class="small">
                                <?=$data->ip;?><br>
                                <?=$data->createdDatetime;?>
                            </td>
                            <td class="small">
                                <?=anchor("tabor/".$data->campID, $data->campTitle, "target='_blank'");?>
                                <?php if(!is_null($data->turnTitle)): ?>
                                    <br><?=$data->turnTitle." (".$data->validfrom." - ".$data->validto.")";?>
                                <?php else: ?>
                                <br>* Nincs turnus megadva.
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach;?>
            </table>
        </form>
        <?php echo $page["html"]; ?>
    </div>
</div>
