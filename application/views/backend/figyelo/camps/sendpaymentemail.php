<?php

	$all = $this->db->from("tf_camp_contracts")->where("contractSent is not NULL")->where("paymentEmailSent is NULL")->order_by("id", "DESC")->get()->result();
	
?>
<div class="panel panel-default">
	<div class="panel-body">
		<h4>Fizetési email kiküldésére váró szerződések:</h4>
		<form method="post" enctype="multipart/form-data">
		<table class="table table-bordered table-condensed">
			<thead>
			<tr>
				<th class="w150">&nbsp;</th>
				<th>Partner</th>
				<th>Létrehozva</th>
				<th>Szerződés kiküldve</th>
				<th></th>
			</tr>	
			</head>
		<?php foreach ($all as $data): ?>
			<tr>
				<td>
					<?= anchor("figyelo/camps/editcontract/".$data->id, "Szerződés", " class='btn btn-sm btn-primary' target='_blank'"); ?>
				</td>
				<td><?= $data->partnerName; ?></td>
				<td>
					<?= (is_Null($data->createdTime))?"<i class='glyphicon glyphicon-remove text-warning'></i>":"<i class='glyphicon glyphicon-ok text-success'></i> ".date("m.d. H:i", strtotime($data->createdTime)); ?>
				</td>
				<td>
					<?= (is_Null($data->contractSent))?"<i class='glyphicon glyphicon-remove text-warning'></i>":"<i class='glyphicon glyphicon-ok text-success'></i> ".date("m.d. H:i", strtotime($data->contractSent)); ?>
				</td>
				<td>					
                    <button type="submit" class="btn btn-sm btn-success" name="sendPayment" value="<?= $data->id; ?>"> Fizetési levél kiküldése</button>
                    <input type="checkbox" name="confSend" value="<?= $data->id; ?>"> megerősítés, levél kiküldése					
				</td>

			</tr>
		<?php endforeach; ?>
		</table>
		
		<div class="alert alert-info">
			<p>Fájl csatolása a kiküldendő levélhez:</p>
			<input type="file" name="attachFile">
		</div>
		</form>
	</div>
</div>
