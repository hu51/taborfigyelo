<?php

if ($this->input->post("addnew")) {
    $data = array(
        "name"      => $this->input->post("new_name", true),
        "alias"     => $this->tools->toSlug($this->input->post("new_name", true)),
        "countryID" => (int) $this->input->post("new_countryID", true),
    );
    $this->db->insert("tf_camp_states", $data);
    message("", "Új megye felrögzítve", "success");
}

if ($this->input->post("update")) {
    $id = $this->input->post("update");

    $name      = $this->input->post("name", true);
    $countryID = $this->input->post("countryID", true);
    $data      = array(
        "name"      => filter_var($name[$id], FILTER_SANITIZE_STRING),
//        "alias"     => $this->tools->toSlug(filter_var($name[$id], FILTER_SANITIZE_STRING)),
        "countryID" => (int) $countryID[$id],
    );
    $this->db->update("tf_camp_states", $data, array("id" => $id));
    message("", "Megye módosítva", "success");
}

$root = $this->db->from("tf_camp_countries")->order_by("name", "ASC")->get()->result();
foreach ($root as $tmp) {
    $rootList[$tmp->id] = $tmp->name;
}

$all = $this->db->from("tf_camp_states")->order_by("name", "ASC")->get()->result();

?>
<div class="panel panel-default">
	<div class="panel-body">
		<h4>Tábor megyék:</h4>
		<form method="post">
			<table class="table table-bordered table-condensed">
				<thead>
					<tr>
						<th>Név</th>
						<th>URL</th>
						<th>Ország</th>
						<th>&nbsp;</th>
					</tr>
					</head>
					<?php foreach ($all as $data): ?>
						<tr>
							<td>
								<input type="text" class="form-control" name="name[<?=$data->id;?>]" value="<?=$data->name;?>">
							</td>
							<td>
								<?=$data->alias;?>
							</td>
							<td class="w200">
								<?=$this->tools->bs_select("countryID[" . $data->id . "]", $data->countryID, $rootList, "form-control", $data->id);?>
							</td>
							<td>
								<button class="btn btn-primary" type="submit" name="update" value="<?=$data->id;?>" aria-label="">Módosít</button>
							</td>
						</tr>
					<?php endforeach;?>


					<tr class="success">
						<th>Név</th>
						<th>URL</th>
						<th>Ország</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
					</tr>
					<tr>
						<td>
							<input type="text" class="form-control" name="new_name" placeholder="Új neve" value="">
						</td>
						<td>&nbsp;</td>
						<td class="w200">
							<?=$this->tools->bs_select("new_countryID", 0, $rootList, "form-control");?>
						</td>
						<td>
							<button class="btn btn-success" type="submit" name="addnew" value="1" aria-label="">Új hozzáadása</button>
						</td>
					</tr>
			</table>
		</form>
	</div>
</div>