<?php

if ($this->input->post("addnew")) {
	$data = array(
		"title"   => $this->input->post("new_title", true),
		"cupon"   => $this->input->post("new_cupon", true),
		"value"   => $this->input->post("new_value", true),
		"from"    => $this->input->post("new_from", true),
		"end"     => $this->input->post("new_end", true),
		"enabled" => $this->input->post("new_enabled", true),
		"amount"  => $this->input->post("new_amount", true),
	);
	$this->db->insert("tf_banner_cupons", $data);
	message("", "Az új kopon felrögzítve", "success");
}

if ($this->input->post("update")) {
	$id = $this->input->post("update");

	$title   = $this->input->post("title", true);
	$cupon   = $this->input->post("cupon", true);
	$value   = $this->input->post("value", true);
	$from     = $this->input->post("from", true);	
	$end     = $this->input->post("end", true);
	$enabled = $this->input->post("enabled", true);
	$amount  = $this->input->post("amount", true);
	$data    = array(
		"title"   => filter_var($title[$id], FILTER_SANITIZE_STRING),
		"cupon"   => filter_var($cupon[$id], FILTER_SANITIZE_STRIPPED),
		"value"   => filter_var($value[$id], FILTER_SANITIZE_STRIPPED),
		"from"    => $from[$id],
		"end"     => $end[$id],
		"enabled" => (int) $enabled[$id],
		"amount"  => (int) $amount[$id],
	);
	$this->db->update("tf_banner_cupons", $data, array("id" => $id));
	message("", "Kupon adatai mentve", "success");
}

$all = $this->db->from("tf_banner_cupons")->order_by("from", "DESC")->order_by("title", "ASC")->get()->result();

?>
<div class="panel panel-default">
	<div class="panel-body">
		<h4>Kuponok:</h4>
		<form method="post">
			<table class="table table-bordered table-condensed">
				<thead>
					<tr>
						<th>Név</th>
						<th>Kód</th>
						<th>Érték</th>
						<th>Tól</th>
						<th>Ig.</th>
						<th>Érvényes</th>
						<th>Hátralévő Alkalmak</th>
						<th>&nbsp;</th>
					</tr>
					</head>
					<?php foreach ($all as $data) : ?>
						<tr>
							<td>
								<input type="text" class="form-control" name="title[<?= $data->id; ?>]" placeholder="kupon neve" aria-label="" value="<?= $data->title; ?>">
							</td>
							<td class="w200">
								<input type="text" class="form-control" name="cupon[<?= $data->id; ?>]" placeholder="kupon neve" aria-label="" value="<?= $data->cupon; ?>">
							</td>
							<td class="w200">
								<input type="text" class="form-control" name="value[<?= $data->id; ?>]" placeholder="kupon neve" aria-label="" value="<?= $data->value; ?>">
							</td>
							<td class="w200">
								<input type="date" class="form-control" name="from[<?= $data->id; ?>]" placeholder="" aria-label="" value="<?= $data->from; ?>">
							</td>
							<td class="w200">
								<input type="date" class="form-control" name="end[<?= $data->id; ?>]" placeholder="" aria-label="" value="<?= $data->end; ?>">
							</td>
							<td>
								<?= $this->tools->bs_checkbox("enabled[" . $data->id . "]", $data->enabled); ?>
							</td>
							<td class="w200">
								<input type="number" class="form-control" name="amount[<?= $data->id; ?>]" placeholder="" aria-label="" value="<?= $data->amount; ?>">
							</td>
							<td>
								<button class="btn btn-primary" type="submit" name="update" value="<?= $data->id; ?>" aria-label="">Módosít</button>
							</td>
						</tr>
					<?php endforeach; ?>


					<tr class="success">
						<th>Név</th>
						<th>Kupon</th>
						<th>Érték</th>
						<th>Tól</th>
						<th>Ig.</th>
						<th>Érvényes</th>
						<th>Hátralévő Alkalmak</th>
						<th>&nbsp;</th>
					</tr>
					<tr>
						<td>
							<input type="text" class="form-control" name="new_title" placeholder="Új kupon neve" aria-label="" value="">
						</td>
						<td>
							<input type="text" class="form-control" name="new_cupon" placeholder="kódja" aria-label="" value="">
						</td>
						<td>
							<input type="text" class="form-control" name="new_value" placeholder="értéke" aria-label="" value="">
						</td>
						<td class="w200">
							<input type="date" class="form-control" name="new_from" placeholder="" aria-label="" value="<?= date("Y-m-d"); ?>">
						</td>
						<td class="w200">
							<input type="date" class="form-control" name="new_end" placeholder="" aria-label="" value="<?= date("Y-m-d", strtotime("+1 WEEK")); ?>">
						</td>
						<td>
							<?= $this->tools->bs_checkbox("new_enabled", 0); ?>
						</td>
						<td class="w200">
							<input type="number" class="form-control" name="new_amount" placeholder="" aria-label="" value="10">
						</td>
						<td>
							<button class="btn btn-success" type="submit" name="addnew" value="1" aria-label="">Új hozzáadása</button>
						</td>
					</tr>
			</table>
			<p>A kupon értéke vagy egész százalék vagy egész összeg lehet. (pl. <code>7%</code>, vagy <code>1500</code> Ft)</p>
		</form>
	</div>
</div>