<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Űrlapok</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-offset-1 col-sm-10 col-md-10">
                <div class="row">
                    <div class="col-sm-10">
                        <h4>Űrlap</h4>
                        <div>
                            <form id="form" method="post" class="form-horizontal">
                                <input type="hidden" name="formID" id="formID" value="<?= $this->uri->segment(4); ?>" />
                                <div>
                                    <label class="control-label col-sm-4" for="title">Űrlap neve</label>
                                    <div class="controls col-sm-8">
                                        <input type="text" name="title" id="title" class="form-control">
                                    </div>
                                </div>

                                <div>
                                    <label class="control-label col-sm-4" for="url">Kitöltési utáni URL</label>
                                    <div class="controls col-sm-8">
                                        <input type="text" name="url" id="url" class="form-control">
                                    </div>
                                </div>

                                <div>
                                    <label class="control-label col-sm-4" for="email">Adatok küldése e-mailben</label>
                                    <div class="controls col-sm-8">
                                        <input type="text" name="email" id="email" class="form-control">
                                    </div>
                                </div>

                                <div class="form-actions" data-type="submit">
                                    <button type="submit" class="btn btn-success">Módosítások mentése</button>
                                </div>

                                <fieldset id="content_form_name">
                                    <table id="fields" class="table"></table>
                                </fieldset>                                
                                
                                <div class="form-actions" data-type="submit">
                                    <button type="submit" class="btn btn-success">Módosítások mentése</button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div id="components-container" class="form-horizontal">
                            <h4>Mező típusok</h4>
                            <div class="component form-group" data-type="text">
                                <label class="control-label col-sm-4" for="text_input">Beviteli mező</label>
                                <div class="controls col-sm-8">
                                    <input type="text" name="" id="text_input" placeholder="placeholder" class="form-control">
                                </div>
                                <button type="button" class="btn btn-sm btn-primary addControl" data-type="text">Szöveg mező +</button>
                            </div>

                            <div class="component form-group" data-type="textarea">
                                <label class="control-label col-sm-4" for="textarea">Hosszú szöveg</label>
                                <div class="controls col-sm-8">
                                    <textarea name="" class="form-control" id="textarea" placeholder="placeholder"></textarea>
                                </div>
                                <button type="button" class="btn btn-sm btn-primary addControl" data-type="textarea">Hosszú szöveg +</button>
                            </div>

                            <div class="component form-group" data-type="select">
                                <label class="control-label col-sm-4" for="select">Legördülő lista</label>
                                <div class="controls col-sm-8">
                                    <select class="form-control" name="" id="select">
                                        <option value="1">Option 1</option>
                                        <option value="2">Option 2</option>
                                        <option value="3">Option 3</option>
                                    </select>
                                </div>
                                <button type="button" class="btn btn-sm btn-primary addControl" data-type="select">Legördülő lista +</button>
                            </div>

                            <div class="component form-group" data-type="checkbox">
                                <label class="control-label col-sm-4">Jelölő négyzetek</label>
                                <div class="controls col-sm-8">
                                    <div class="checkbox"><label class="" for="checkbox_1">
                                        <input type="checkbox" name="checkbox" id="checkbox_1">
                                        Option 1
                                    </label></div>
                                    <div class="checkbox"><label class="" for="checkbox_2">
                                        <input type="checkbox" name="checkbox" id="checkbox_2">
                                        Option 2
                                    </label></div>
                                    <div class="checkbox"><label class="" for="checkbox_3">
                                        <input type="checkbox" name="checkbox" id="checkbox_3">
                                        Option 3
                                    </label></div>

                                </div>
                                <button type="button" class="btn btn-sm btn-primary addControl" data-type="checkbox">Jelölő négyzetek +</button>
                            </div>

                            <div class="component form-group" data-type="radio">
                                <label class="control-label col-sm-4">Rádió gombok</label>
                                <div class="controls col-sm-8">
                                    <div class="radio"><label class="" for="radio_1">
                                        <input type="radio" name="radio" id="radio_1">
                                        Option 1
                                    </label></div>
                                    <div class="radio"><label class="" for="radio_2">
                                        <input type="radio" name="radio" id="radio_2">
                                        Option 2
                                    </label></div>
                                    <div class="radio"><label class="" for="radio_3">
                                        <input type="radio" name="radio" id="radio_3">
                                        Option 3
                                    </label></div>

                                </div>
                                <button type="button" class="btn btn-sm btn-primary addControl" data-type="radio">Rádió gombok +</button>
                            </div>

                            <div class="component form-group" data-type="info">
                                <label class="control-label col-sm-4" for="info">Leírás</label>
                                <div class="controls col-sm-8">
                                    Lorem ipsum....
                                </div>
                                <button type="button" class="btn btn-sm btn-primary addControl" data-type="info">Leírás +</button>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
