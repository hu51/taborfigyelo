<?php

$formID = $this->uri->segment(4);

$form_data = $this->db->from("tf_forms")->where("id", $formID)->get()->row();
$records = $this->db->from("tf_forms_records")->where("formID", $formID)->get()->result();
$tmp = $this->db->from("tf_forms_fields")->where("logging", 1)->where("formID", $formID)->order_by("ordering")->get()->result();
$fields = [];
$col = 4;
foreach ($tmp as $t) {
    $t->col = $col++;
    $fields[$t->id] = $t;
}

require FCPATH.'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

// Create new Spreadsheet object
$spreadsheet = new Spreadsheet();
$spreadsheet->setActiveSheetIndex(0);

$spreadsheet->getActiveSheet()->setTitle('data');

$rowID = 2;
foreach ($records as $row) {
    if ($rowID == 2) {
        $colID = 4;    
        $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(1, 1, "id");
        $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(2, 1, "time");
        $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(3, 1, "ip");

        if ($rowID == 2) {
            foreach ($fields as $id => $field) {   
                $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow($field->col, 1, trim($field->title));
            }
        }        
    }

    $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(1, $rowID, $row->id);
    $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(2, $rowID, $row->datetime);
    $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(3, $rowID, $row->ip);
     
    $datas = $this->db->from("tf_forms_records_data")->where("recordID", $row->id)->get()->result();
    foreach ($datas as $id => $data) {
        if (isset($fields[$data->fieldID])) {
            $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow($fields[$data->fieldID]->col, $rowID, $data->value);
        }
    }
    $rowID++;
}

// Redirect output to a client’s web browser (Xls)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="data.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0

$writer = IOFactory::createWriter($spreadsheet, 'Xls');
$writer->save('php://output');