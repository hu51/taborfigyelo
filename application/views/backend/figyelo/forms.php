<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Űrlapok</h3>
    </div>
    <div class="panel-body">
<?php

if ($this->input->post("doWipe") && $this->input->post("confWipe") == "on") {
    $formID = (int)$this->input->post("doWipe");
    $this->db->delete("tf_forms_records", array("formID" => $formID));
    $this->db->delete("tf_forms_records_data", array("formID" => $formID));
    message("", "Adatok törölve");
}

$this->db->from("tf_forms");
$this->db->order_by("title", "ASC");
$all = $this->db->get()->result();
?>
    <h4>Űrlapok:</h4>
    <form method="post">
        <div><a href="figyelo/main/forms/new" class='btn btn-success fr'>Új ürlap hozzáadása</a></div>
        <table class="table table-bordered table-condensed">
            <thead>
            <tr>
                <th>ID</th>
                <th>Név</th>
                <th>Kód</th>
                <th>Kitöltve</th>
                <th></th>
                <th></th>
            </tr>
            </head>
            <?php foreach ($all as $data): ?>
                <tr>
                    <td class="w50"><?=$data->id;?></td>
                    <td><a href="<?=site_url("figyelo/main/forms/" . $data->id);?>"><?=$data->title;?></a></td>
                    <td>[form id=<?= $data->id; ?>]</td>
                    <td><?= $this->db->select("id")->from("tf_forms_records")->where("formID", $data->id)->get()->num_rows(); ?> db </td>                        
                    <td><a href="<?=site_url("figyelo/main/forms/" . $data->id."?export=xls");?>"><i class='glyphicon glyphicon-download'></i> EXCEL</a></td>
                    <td>                        
                        <button type="submit" name="doWipe" value="<?= $data->id; ?>" class="btn btn-sm btn-warning">Ürlapadatok törlése</button>
                        <input type="checkbox" name="confWipe"> megerősítés
                    </td>
                </tr>
            <?php endforeach;?>
            </table>
        </form>
    </div>
</div>
