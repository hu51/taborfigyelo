<legend>HTML-ek szerkesztése (<?= $this->uri->segment(4); ?>)</legend>
<div class="panel panel-default">
	<div class="panel-body">
			<div class="col-lg-12">
				<table class="table">
				<thead>
				<tr>
					<th>Cím</th>
					<th>Kód</th>
					<th>Utoljára módosítva</th>
				</tr>
				</thead>
                    <?php
                    
                    $this->db->from("tf_htmls");
                    if ($this->uri->segment(4)) {
                        $this->db->where("cat", $this->uri->segment(4));
                    }
                    $this->db->order_by("title");
                    $htmls = $this->db->get()->result();

                    foreach ($htmls as $data): 
                    ?>
                    <tr>
                        <td>
                        <a href="<?= site_url("figyelo/main/htmledit/".$data->id); ?>"><i class="glyphicon glyphicon-edit"></i>
                        <?=$data->title;?></a>
                        </td>
                            <td><?=$data->slug;?></td>
                            <td><?=substr($data->lastModify, 0, 16);?></td>
                    </tr>
                    <?php endforeach; ?>
				</table>
			</div>
	</div>
</div>
<br style="clear: both;">