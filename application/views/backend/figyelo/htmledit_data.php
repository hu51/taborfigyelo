<?php

$htmlID = (int) $this->uri->segment(4);
$html   = $this->db->from("tf_htmls")->where("id", (int) $htmlID)->get()->row();
if (!$html) {
	message("", "Hibás HTML azonosító!", "warning");
    return;
}

if ($html && $this->input->post("saveHtml")) {
	
	$html = $this->db->from("tf_htmls")->where("id", (int) $htmlID)->get()->row();
    $data = array(
		"title"      => filter_input(INPUT_POST, "title", FILTER_SANITIZE_STRING),
        "cat"       => $this->input->post("cat"),
        "body"       => $this->input->post("body"),
        "lastModify" => date("Y-m-d H:i:s"),
    );
    $this->db->update("tf_htmls", $data, array("id" => $htmlID));
    message("", "Módosítások mentve.", "success");
	$html   = $this->db->from("tf_htmls")->where("id", (int) $htmlID)->get()->row();
}

?>

<div class="row">
	<a href="<?=site_url("figyelo/main/htmledit"); ?>" class="btn btn-primary fr">Vissza a leírásokhoz &gt;</a>
</div>

<div class="panel panel-default">
	<div class="panel-heading">Leírás adatai</div>
	<div class="panel-body">
		<form method="post" action="" class="">
			<div class="col-lg-12">
				<label class="col-lg-2">Tárgy (e-mail):</label>
				<div class="col-lg-6">
					<input type="text" class="form-control" name="title" value="<?=$html->title;?>">
				</div>
				<div class="col-lg-4">
					<button type="submit" name="saveHtml" class="btn btn-success" value="ok">Leírás mentése</button>
				</div>
            </div>
            
			<div class="col-lg-12">
				<label class="col-lg-2">Típus:</label>
				<div class="col-lg-6">
                    <?= $this->tools->bs_select("cat", $html->cat, array("html" => "Honlap leírások", "email" => "Levelek, szerződések"), "form-control"); ?>
				</div>
				<div class="col-lg-4">
				</div>
			</div>

			<div class="col-lg-12">
				<label class="col-lg-2">Szöveg:</label>
				<div class="col-lg-10">
					<textarea name="body" class="htmlEditor" style="max-width: 900px"><?=$html->body;?></textarea>
					<p>Utoljára módosítva: <?=$html->lastModify;?></p>
					<br>
				</div>
			</div>
		</form>
	</div>
</div>

<script src="<?=base_url();?>/js/tinymce/tinymce.min.js" type="text/javascript"></script>
<script>
	tinymce.init({
		mode: "specific_textareas",
		editor_selector: "htmlEditor",
		height: 500,
		theme: 'modern',
		plugins: ' searchreplace autolink directionality visualblocks visualchars fullscreen image code link table hr nonbreaking anchor toc lists textcolor wordcount',
		toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link image | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat code | fullscreen',
		image_advtab: true,
		templates: [{
				title: 'Test template 1',
				content: 'Test 1'
			},
			{
				title: 'Test template 2',
				content: 'Test 2'
			}
		],
		content_css: [
			'//www.tinymce.com/css/codepen.min.css'
		],
        relative_urls: false,
        remove_script_host : false,
        document_base_url : "<?= site_url(); ?>"
	});
</script>        