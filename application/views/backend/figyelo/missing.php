<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">404-es, nem létező oldalak</h3>
    </div>

    <div class="panel-body">
        <?php

            if ($this->input->post("deleteList")) {          
                message("", "Bejegyzések törölve", "success");
                $this->db->truncate("tf_404_pages");
            }
            
            if ($this->input->post("deleteSelected")) {  
                foreach($this->input->post("delSel") as $id) {
                    $this->db->delete("tf_404_pages", array("id" => (int) $id));
                }
                message("", "Megjelölt bejegyzések törölve", "success");
            }
        ?>
        <form action="" method="POST" role="form">
            <button type="submit" name="deleteList" value="1" class="btn btn-sm btn-danger fr">Teljes lista törlése</button>
            <button type="submit" name="deleteSelected" value="1" class="btn btn-sm btn-warning fr">Bejelöltek törlése</button><br>
            <?php

                $this->db->select('id');
                $this->db->from("tf_404_pages");
                if($this->input->post("search")) {
                    $this->db->like("url", $this->input->post("search", true), "BOTH");
                }
                $allRows = $this->db->get()->num_rows();

                if (!$this->input->get("oldal")) {
                    $pageID = 1;
                } else {
                    $pageID = (int) $this->input->get("oldal");
                }
                $pageMaxRows = 20;

                $page         = $this->tools->pagination("figyelo/main/missing", $allRows, $pageMaxRows, $pageID);
                $pageID       = $page["current"];
                $pageStartRow = ($pageID - 1) * $pageMaxRows;

                $this->db->from("tf_404_pages");
                if($this->input->post("search")) {
                    $this->db->like("url", $this->input->post("search", true), "BOTH");
                }                
                $this->db->order_by("hits", "DESC");
                $this->db->order_by("url", "ASC");
                $this->db->limit($pageMaxRows, $pageStartRow);
                $query = $this->db->get();

                echo $page["html"];

                ?>                
            <table class="table table-condended table-bordered">
                <tbody>
                    <tr>
                        <th></th>
                        <th><input type="text" class="form-control" name="search" value="<?= $this->input->post("search"); ?>"></th>
                        <th><button type="submit" class="btn btn-primary" >Szűrés</button></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </tbody>
                <thead>
                    <tr>
                        <th></th>
                        <th>URL</th>
                        <th>Találat</th>
                        <th>Utolsó találat</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <?php

                    foreach ($query->result() as $data): ?>
                        <tr>
                            <td><?=$data->id;?></td>
                            <td><code><?=$data->url;?></code></td>
                            <td><?=$data->hits;?></td>
                            <td><?=$data->lastTime;?></td>
                            <td>
                                <a href="<?=site_url("/figyelo/main/redirect/?import=" . $data->id);?>"><button type="button" class="btn btn-primary btn-sm">Átirányítás létrehozása</button></a>
                            </td>
                            <td>
                                <input type="checkbox" name="delSel[]" value="<?= $data->id; ?>"> bejelölés törlésre                     
                            </td>
                        </tr>
                    <?php endforeach;?>
            </table>
        </form>
        <?php echo $page["html"]; ?>
    </div>
</div>
