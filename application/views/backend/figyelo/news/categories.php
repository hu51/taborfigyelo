<?php

	$all = $this->db->from("tf_doc_categories")->order_by("title", "ASC")->get()->result();
	
?>
<div class="panel panel-default">
	<div class="panel-body">
		<h4>Cikk kategóriák:</h4>
		<table class="table table-bordered table-condensed">
			<thead>
			<tr>
				<th>&nbsp;</th>
				<th>Név</th>
				<th>db.</th>
			</tr>	
			</head>
		<?php foreach ($all as $data): 

		$db = $this->db->from('tf_documents')->where('categoryID', $data->id)->where("currentVersion", 1)->get()->num_rows();
			
			?>
			<tr>
				<td class="w50 ac"><?= $data->id; ?></td>
				<td><?= $data->title; ?></td>
				<td class="w100 ac"><?= $db; ?></td>
			</tr>
		<?php endforeach; ?>
		</table>
	</div>
</div>