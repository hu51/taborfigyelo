<?php

foreach ($this->input->post("correct") as $id => $tmp) {
    $data = $this->db->from("tf_documents")->where("id", $id)->get()->row();
    if ($data) {
        $this->db->update("tf_documents", array("slug" => $this->tools->toSlug($data->title)), array("id" => $id));
        echo "<li>".$data->slug." - ".$this->tools->toSlug($data->slug)."</li>";
    }
}

?>
<h3>Slug repair</h3>
<form action="" method="post">
<table class="table table-bordered">
<?php

$data = $this->db->from("tf_documents")->get()->result();
foreach ($data as $row) {

    ?>
    <tr class='<?= ($row->slug!==$this->tools->toSlug($row->title))?"danger":""; ?>'>
        <td><?= anchor(site_url("figyelo/news/edit/".$row->id), $row->id); ?></td>
        <td><?= $row->title; ?></td>
        <td><?= $row->slug; ?></td>
        <td><?= $this->tools->toSlug($row->title); ?></td>
        <td>
            <input type="checkbox" name="correct[<?= $row->id;?>]" value="1"> Repair
        </td>
    </tr>
    <?php  
}
?>
</table>

<button type="submit" class="btn btn-primary">RUN</button>

</form>