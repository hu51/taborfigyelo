<?php

$docID = $this->uri->segment(4);

if ($this->input->post("delPic")) {
    $targetPath = "images/news/pic_" . $docID . ".jpg";
    $thumbPath  = "images/news/pic_" . $docID . "_sm.jpg";

    unlink(FCPATH . $targetPath);
    unlink(FCPATH . $thumbPath);
    message("", "Kiemelt kép törölve: ", "success");
}

if ($this->input->post("saveHtml")) {
    if ($docID == "new") {
        $data = array(
            "title"          => filter_input(INPUT_POST, "title", FILTER_SANITIZE_STRING),
            "slug"           => $this->tools->toSlug(filter_input(INPUT_POST, "title", FILTER_SANITIZE_STRING)),
            "body"           => $this->input->post("body"),
            "excerpt"        => $this->input->post("excerpt"),
            "lastModify"     => date("Y-m-d H:i:s"),
            "createDatetime" => date("Y-m-d", strtotime($this->input->post("createDate"))) . " " . date("H:i", strtotime($this->input->post("createTime"))),
            "versionID"      => 1,
            "currentVersion" => 1,
            "categoryID"     => (int) $this->input->post("categoryID"),
            "visible"        => (int) $this->input->post("visible"),
        );
        $this->db->insert("tf_documents", $data);
        message("", "A bejegyzést mentettük.", "success");

        $docID = $this->db->insert_id();
        $this->db->update("tf_documents", array("originalID" => $docID), array("id" => $docID));
        return;
    } else {
        if ($this->input->post("newVersion")) {
            $data = array(
                "title"          => filter_input(INPUT_POST, "title", FILTER_SANITIZE_STRING),
                "body"           => $this->input->post("body"),
                "excerpt"        => $this->input->post("excerpt"),
                "lastModify"     => date("Y-m-d H:i:s"),
                "createDatetime" => date("Y-m-d", strtotime($this->input->post("createDate"))) . " " . date("H:i", strtotime($this->input->post("createTime"))),
                "categoryID"     => (int) $this->input->post("categoryID"),
                "visible"        => (int) $this->input->post("visible"),
            );
            if ($this->input->post("updateDatetime")) {
                $data["createDatetime"] = date("Y-m-d H:i:s");
            }
            if ($this->input->post("chgseo") == "on") {
                $data["slug"] = $this->tools->toSlug(filter_input(INPUT_POST, "title", FILTER_SANITIZE_STRING));
            }
            $html = $this->db->from("tf_documents")->where("currentVersion", 1)->where("originalID", (int) $docID)->get()->row();
            $this->db->update("tf_documents", array("currentVersion" => 0), array("originalID" => $docID));

            $data = $data + (array) $html;
            $data["versionID"]++;
            unset($data["id"]);
            $this->db->insert("tf_documents", $data);

            $docID = $this->db->insert_id();
            redirect("ackend/news/edit/" . $docID);
        } else {
            $data = array(
                "title"      => filter_input(INPUT_POST, "title", FILTER_SANITIZE_STRING),
                "body"       => $this->input->post("body"),
                "excerpt"    => $this->input->post("excerpt"),
                "lastModify" => date("Y-m-d H:i:s"),
                "createDatetime" => date("Y-m-d", strtotime($this->input->post("createDate"))) . " " . date("H:i", strtotime($this->input->post("createTime"))),                
                "categoryID" => (int) $this->input->post("categoryID"),
                "visible"    => (int) $this->input->post("visible"),
            );
            if ($this->input->post("updateDatetime")) {
                $data["createDatetime"] = date("Y-m-d H:i:s");
            }
            if ($this->input->post("chgseo") == "on") {
                $data["slug"] = filter_input(INPUT_POST, "slug", FILTER_SANITIZE_STRIPPED);
            }
            if ($this->input->post("resetCounter")) {
                $data["hits"] = 0;
            }
            $this->db->update("tf_documents", $data, array("currentVersion" => 1, "originalID" => $docID));

            $html = $this->db->from("tf_documents")->where("currentVersion", 1)->where("originalID", (int) $docID)->get()->row();
            //$this->tools->changeSlug("tf_documents", $data["title"], $html->id);
        }
        message("", "A bejegyzést módosítottuk.", "success");
    }

    if (!empty($_FILES["userImage"]["name"])) {
        if (in_array($_FILES['userImage']['type'], array('image/jpeg', 'image/png'))) {
            if (is_uploaded_file($_FILES['userImage']['tmp_name'])) {
                $sourcePath = $_FILES['userImage']['tmp_name'];
                $info       = getimagesize($sourcePath);
                $extension  = image_type_to_extension($info[2]);

                $targetPath = "images/news/pic_" . $docID . ".jpg";
                $thumbPath  = "images/news/pic_" . $docID . "_sm.jpg";

                if (move_uploaded_file($sourcePath, FCPATH . $targetPath)) {
                    $this->tools->ResizePic(FCPATH . $targetPath, FCPATH . $targetPath, 900, 600);
                    $this->tools->ResizePic(FCPATH . $targetPath, FCPATH . $thumbPath);
                    message("", "Kép feltöltve: " . $_FILES['userImage']['name'], "success");
                } else {
                    message("", "Hiba a kép mozgatásakor! " . $_FILES['userImage']['name'], "warning");
                }
            } else {
                message("", "Hiba a kép feltöltésekor! " . $_FILES['userImage']['name'], "warning");
            }
        } else {
            message("", "Nem megengedett képformátum!" . $_FILES['userImage']['type'], "warning");
        }
        unlink($_FILES['userImage']['tmp_name']);
    }
}

if ($docID == "new") {
    $html = (object) array(
        "id"             => "new",
        "title"          => "",
        "excerpt"        => "",
        "body"           => "",
        "categoryID"     => 1,
        "lastModify"     => "",
        "createDatetime" => date("Y-m-d H:i:s"),
        "visible"        => 0,
        "hits"           => 0,
        "hits"           => 0,
    );
    $url = "??";
} else {
    $html = $this->db->from("tf_documents")->where("originalID", (int) $docID)->order_by("currentVersion", "DESC")->limit(1)->get()->row();

    $title = url_title(convert_accented_characters($html->title));
    if ($html->categoryID == 2) {
        $url = site_url("leirasok/" . $html->originalID . "/" . $title);
    } else {
        $category = $this->db->from("tf_doc_categories")->where("id", $html->categoryID)->get()->row();
        $url      = site_url("hireink/" . $category->slug . "/" . $html->originalID . "/" . $title);
    }
}
$categories = $this->db->from("tf_doc_categories")->order_by("title")->get()->result();

?>
<div class="panel panel-default">
    <div class="panel-heading">Cikk adatai</div>
    <div class="panel-body">
        <form method="post" action="" class=""  enctype="multipart/form-data">
            <input type="hidden" name="htmlID" value="<?=$html->id;?>">

            <div class="row">
                <label class="col-lg-3">SEO URL:</label>
                <div class="col-lg-9 p5">
                    <div class="input-group">
                        <div class="input-group-addon"><?=site_url();?></div>
                        <input type="text" class="form-control" id="slug" name="slug" value="<?=$html->slug;?>" disabled>
                        <div class="input-group-addon">.html</div>
                        <div class="input-group-addon"><input type="checkbox" id="chgseo" name="chgseo"> URL módosítása</div>
                        <div class="input-group-addon"><a href="<?=site_url($html->slug . ".html");?>" class="btn btn-primary" target="_blank"><span class="glyphicon glyphicon-link"></span></a></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-lg-3">URL:</label>
                <div class="col-lg-9 p5">
                    <div class="input-group">
                        <?=$url;?>
                        <div class="input-group-addon"><a href="<?=$url;?>" class="btn btn-primary" target="_blank"><span class="glyphicon glyphicon-link"></span></a></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-lg-3">Kategória:</label>
                <div class="col-lg-9">
                    <select name="categoryID" class="form-control">
                        <option value="0"  <?=(0 == $html->categoryID) ? "selected" : "";?>>* Mindenhol megjelenik</option>
                        <?php foreach ($categories as $data): ?>
                        <option value="<?=$data->id;?>"  <?=($data->id == $html->categoryID) ? "selected" : "";?>><?=$data->title;?></option>
                        <?php endforeach;?>
                    </select>
                </div>
            </div>

            <div class="row">
                <label class="col-lg-3">Cím:</label>
                <div class="col-lg-9">
                    <input type="text" class="form-control" name="title" value="<?=$html->title;?>">
                </div>
            </div>

            <div class="row">
                <label class="col-lg-3">&nbsp;</label>
                <div class="col-lg-9">
                    <button type="submit" name="saveHtml" class="btn btn-success" value="ok">Cikk mentése</button>
                    <?php if ($docID !== "new"): ?>
                        <input type="checkbox" name="newVersion" value="ok" autocomplete="off"> Verzió léptetése
                    <?php endif;?>
                </div>
            </div>

            <div class="row">
                <label class="col-lg-3">Látható:</label>
                <div class="col-lg-9">
                    <?=$this->tools->bs_checkbox("visible", $html->visible);?>
                </div>
            </div>

            <div class="row">
                <label class="col-lg-3">Megtekintések:</label>
                <div class="col-lg-9 p10">
                    <?=$html->hits;?> <input type="checkbox" name="resetCounter" value="1"> Számláló nullázása
                </div>
            </div>


            <div class="row">
                <label class="col-lg-3">Létrehozási dátum:</label>
                <div class="col-lg-9 p10">
                    <input type="date" class="form-control w150 fl" name="createDate" value="<?=substr($html->createDatetime, 0, 10);?>">
                    <input type="time" class="form-control w150 fl" name="createTime" value="<?=substr($html->createDatetime, 11, 5);?>">
                </div>
            </div>
            <div class="row">
                <label class="col-lg-3">Utoljára módosítva:</label>
                <div class="col-lg-9 p10">
                    <?=$html->lastModify;?>
                </div>
            </div>

            <div class="row">
                <label class="col-lg-3">Szöveg:</label>
                <div class="col-lg-9">
                    <div  style="max-width: 900px;">
                        <textarea name="body" class="htmlEditor"><?=$html->body;?></textarea>
                    </div>
                </div>
            </div>

            <br>
            <div class="row">
                <label class="col-lg-3">Bevezető:</label>
                <div class="col-lg-9">
                    <div  style="max-width: 900px;">
                        <textarea name="excerpt" class="htmlLightEditor"><?=$html->excerpt;?></textarea>
                    </div>
                </div>
            </div>

            <br>
            <div class="row">
                <label class="col-lg-3">Cikk kép:</label>
                <div class="col-lg-4">
                    <input type="file" name="userImage">
                </div>
                <div class="col-lg-5">
                    <?php

if ($docID !== "new") {
    $pic = file_exists(FCPATH . "images/news/pic_" . $docID . ".jpg");
    if ($pic): ?>
                                <a href='<?=site_url("images/news/pic_" . $docID . ".jpg");?>' target='_blank'><img src='<?=site_url("images/news/pic_" . $docID . "_sm.jpg");?>' style='max-width:100px; max-height: 100px;' class='img-thumbnail'></a>
                                <button type="submit" name="delPic" class="btn btn-sm btn-warning" value="ok">Kép törlése</button>
                            <?php else: ?>
                                Nincs kiemelt kép.
                            <?php endif;
}
?>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="<?=base_url();?>js/tinymce/tinymce.min.js" type="text/javascript"></script>
<script>

    tinymce.init({
        mode: "specific_textareas",
        editor_selector: "htmlEditor",
        height: 500,
        theme: 'modern',
        plugins: ' searchreplace autolink directionality visualblocks visualchars fullscreen image media code link table hr nonbreaking anchor toc lists textcolor wordcount responsivefilemanager',
        toolbar1: 'responsivefilemanager | formatselect | bold italic strikethrough forecolor backcolor | link image media | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat code | fullscreen',
        image_advtab: true,
        external_filemanager_path:"<?=base_url();?>filemanager/",
        filemanager_title:"Responsive Filemanager" ,
        external_plugins: { "filemanager" : "<?=base_url();?>filemanager/plugin.min.js"},
        filemanager_access_key:"5EC4E40BB6748073F0ECEC3A2229371D4CEA1297D0A6B6742EBB351D490BA11A",
        content_css: [
            '//www.tinymce.com/css/codepen.min.css'
        ],
        relative_urls: false
    });

    tinymce.init({
        mode: "specific_textareas",
        editor_selector: "htmlLightEditor",
        height: 100,
        menubar:false,
        theme: 'modern',
        plugins: ' directionality visualblocks visualchars  hr nonbreaking wordcount ',
        toolbar1: ' bold italic strikethrough forecolor backcolor | alignleft aligncenter alignright alignjustify  | removeformat code ',
        content_css: [
            '//www.tinymce.com/css/codepen.min.css'
        ],
        relative_urls: false
    });

    $("#chgseo").click(function(){
        chk = $("#chgseo").not(":checked");
        console.log(chk);
        $("#slug").prop("disabled", $(this).is(":not(:checked)"));
    })

</script>