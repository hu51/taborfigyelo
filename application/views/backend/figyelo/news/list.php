<?php

if ($this->input->post("setfilter")) {
    $this->db->from("tf_documents");
	$this->db->where("currentVersion", 1);	
	if ($this->input->post("filter_name") != "-1") {
		$this->db->like("title", filter_input(INPUT_POST, "filter_name", FILTER_SANITIZE_STRING));	
	}
	if ($this->input->post("filter_cat") != "-1") {
		$this->db->where("categoryID", (int)$this->input->post("filter_cat"));	
	}
	if ($this->input->post("filter_vis") != "-1") {
		$this->db->where("visible", (int)$this->input->post("filter_vis"));	
	}
    $this->db->order_by("title", "ASC");
    $all = $this->db->get()->result();
} else {
    $all = $this->db->from("tf_documents")->order_by("id", "DESC")->order_by("title", "ASC")->where("currentVersion", 1)->get()->result();
}

$categories = ["-1" => "# összes", "0" => "* Mindenhol megjelenik"];
foreach ($this->db->from("tf_doc_categories")->order_by("title")->get()->result() as $data) {
    $categories[$data->id] = $data->title;
}

?>
<div class="panel panel-default">
	<div class="panel-body">
		<h4>Cikkek:</h4>
		<form method="post">
					<table class="table table-bordered table-condensed">
			<thead>
			<tr class="active">
				<th><i class="glyphicon glyphicon-search m5"></i></th>
				<th>
					<div class="input-group">
						<input type="text" class="form-control" name="filter_name" value="<?=set_value("filter_name");?>" placeholder="Keresés">
						<span class="input-group-btn">
							<button type="submit" name="setfilter" value="1" class="btn btn-primary">Keres</button>
						</span>
					</div>
				</th>
				<th><?=$this->tools->bs_select("filter_cat", set_value("filter_cat", "-1"), $categories, "form-control");?></th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th><?=$this->tools->bs_select("filter_vis", set_value("filter_vis", "-1"), array("-1" => "# összes", "0" => "Nem", "1" => "Igen"), "form-control");?></th>
				<th>&nbsp;</th>
			</tr>
			<tr>
				<th>ID</th>
				<th>Név</th>
				<th>Kategória</th>
				<th>Létrehozva</th>
				<th>Módosítva</th>
				<th>Látható</th>
				<th>Megnézve</th>
			</tr>
			</head>
		<?php foreach ($all as $data):
			$db = $this->db->from('tf_documents')->where('categoryID', $data->id)->where("currentVersion", 1)->get()->num_rows();
		?>
				<tr>
					<td class="w50 ac"><a href="<?=site_url("figyelo/news/edit/" . $data->originalID);?>"><?=$data->id;?></a></td>
					<td><a href="<?=site_url("figyelo/news/edit/" . $data->originalID);?>"><?=$data->title;?></a></td>
					<td><?=$categories[$data->categoryID];?></td>
					<td><?=substr($data->createDatetime, 0, 16);?></td>
					<td><?=substr($data->lastModify, 0, 16);?></td>
					<td><?=($data->visible) ? "<span class='glyphicon glyphicon-eye-open'></span>" : "";?></td>
					<td><?=$data->hits;?></td>
				</tr>
			<?php endforeach;?>
		</table>
		</form>
	</div>
</div>