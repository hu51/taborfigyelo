<div class="panel panel-danger">
      <div class="panel-heading">
            <h3 class="panel-title">Csoport törlése</h3>
      </div>
      <div class="panel-body">
<?php

$this->load->helper("form");

$listID = (int) $this->uri->segment(5);

$this->db->from("tf_newsletter_lists");
$this->db->where("id", $listID);
$list = $this->db->get()->row();

if (empty($list->uniqid)) {
    $this->db->update("tf_newsletter_lists", array("uniqid" => uniqid()), array("id" => $listID)); 

    $this->db->from("tf_newsletter_lists");
    $this->db->where("id", $listID);
    $list = $this->db->get()->row();    
}

$this->db->from("tf_newsletter_lists_users");
$this->db->where("listID", $list->id);
$list_users = $this->db->get()->result();

$this->db->from("tf_newsletter_lists");
$this->db->where("id <>", $list->id);
$this->db->order_by("name", "ASC");
$query = $this->db->get();
$lists = array("-1" => "* nincs csoport kiválasztva");
foreach ($query->result() as $tmp) {
    $lists[$tmp->id] = $tmp->name;
}


?>
<form method="post" class="form-horizontal">
    <div class="ß">
        <label class="col-lg-4">Csoport neve:</label>
        <div  class="col-lg-8">
            <?= $list->name; ?>
        </div>
    </div>

    <div class="row">
        <label class="col-lg-4">Leírás:</label>
        <div  class="col-lg-8">
            <?= $list->description; ?>
        </div>
    </div>

    <div class="row">
        <label class="col-lg-4">Megerősítő kód:</label>
        <div  class="col-lg-8">
            <?= $list->uniqid; ?>
        </div>
    </div>

    <p>A csoportnak <b><?= count($list_users); ?></b> tagja van</p>

    <h4 class='text-danger'>Mi történjen a lista tagokkal?</h4>

    <div class="row">
        <label class="col-lg-4"><?= form_radio("del_todo", "move", true); ?>&nbsp; tagok átmozgatása másik listába</label>
        <div  class="col-lg-8">
            <?= form_dropdown("target_list", $lists, false, " class='form-control' "); ?>
        </div> 
    </div>        

    <div class="row">
        <label class="col-lg-4"><?= form_radio("del_todo", "remove", false); ?>&nbsp; tagok eltávolítása a csoportból</label>
        <div  class="col-lg-8">
            A tagok csak a csoportból törlődnek.
        </div> 
    </div>     

    <div class="row">
        <label class="col-lg-4">Megerősítés: </label>
        <div  class="col-lg-2">
            <button type="submit" name="deleteListFinal" class="btn btn-danger btn-block" value="Törlés" >Lista törlése</button>
        </div>
        <div  class="col-lg-2">
            <input type="text" class="form-control" name="confirm" value="" placeholder="kód megadása" />
        </div>
    </div>

</form>
</div>
</div>