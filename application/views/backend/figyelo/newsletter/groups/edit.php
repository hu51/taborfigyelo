<?php

$this->load->helper("form");

if (is_numeric($this->uri->segment(5))) {
    $listID = (int) $this->uri->segment(5);
//$this->db->select("id");
    $this->db->from("tf_newsletter_lists");
    $this->db->where("id", $listID);
    $list = $this->db->get()->row();
} else {
    $listID = "create";
    $list   = (object) array();
}

?>
<div class="panel panel-default">
      <div class="panel-heading">
      <?php if ($listID  == "create"): ?>
            <h3 class="panel-title">Csoport létrehozása</h3>
      <?php else: ?>
            <h3 class="panel-title">Csoport adatai</h3>
      <?php endif; ?>
      </div>
      <div class="panel-body">

<form class="form-horizontal" method="post">
    <div class="row">
        <label class="col-lg-2">Csoport neve:</label>
        <div  class="col-lg-10">
            <input type="text" class="form-control" name="name" value="<?=$list->name;?>" />
        </div>
    </div>

    <div class="row">
        <label class="col-lg-2">Leírás:</label>
        <div  class="col-lg-10">
            <input type="text" class="form-control" name="description" value="<?=$list->description;?>" />
        </div>
    </div>

    <div class="row">
        <label class="col-lg-2">Aktív:</label>
        <div  class="col-lg-10">
            <?= $this->tools->bs_checkbox("active", $list->active);?>
        </div>
    </div>

    <div class="row">
        <label class="col-lg-2">Publikus:</label>
        <div  class="col-lg-10">
            <?= $this->tools->bs_checkbox("public", $list->public);?> a nyílvános oldalon látható, lehet jelentklezni rá.
        </div>
    </div>

    <div class="row">
        <label class="col-lg-2"></label>
        <div  class="col-lg-6">
            <button type="submit" name="saveList" class="btn btn-<?= ($listID=="create")?"success":"primary"; ?>" value="Mentés" >Módosítások mentése</button>
        </div>
        <div  class="col-lg-4">
            <button type="submit" name="deleteList" class="btn btn-danger" value="Törlés" >Lista törlése</button>
        </div>
    </div>

</form>
</div>
</div>