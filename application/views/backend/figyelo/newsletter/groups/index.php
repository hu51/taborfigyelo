<?php

$this->db->from("tf_newsletter_lists");
$this->db->order_by("name");
$query = $this->db->get();

?>
<div class="panel panel-default">
      <div class="panel-heading">
            <h3 class="panel-title">Csoportok</h3>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th width="50">&nbsp;</th>
                        <th width="50">ID</th>
                        <th>Csoport neve</th>
                        <th>Tagok száma</th>
                        <th>Leiratkozva</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($query->result() as $list):
                        $num = $this->db->from("tf_newsletter_lists_users")->where("listID", $list->id)->count_all_results();
                        $num2 = $this->db->from("tf_newsletter_lists_users")->where("unsubscribed is NOT NULL")->where("listID", $list->id)->count_all_results();
                        
                        ?>
                        <tr>
                            <td><?= anchor("backend/newsletter/groups/edit/" . $list->id, "<i class='glyphicon glyphicon-edit'></i>"); ?></td>
                            <td><?= $list->id; ?></td>
                            <td><?= $list->name; ?></td>
                            <td><?= anchor("backend/newsletter/groups/members/".$list->id, "<i class='glyphicon glyphicon-user'></i> ".$num); ?></td>
                            <td><i class='glyphicon glyphicon-remove'></i> <?= $num2; ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        </div>
</div>