<?php

$groupID = (int) $this->uri->segment(5);

$this->load->helper("form");

$this->db->from("tf_newsletter_lists");
$this->db->where("id", $groupID);
$list = $this->db->get()->row();


$this->db->select("u.*, l.unsubscribed");
$this->db->from("tf_newsletter_users as u");
$this->db->join("tf_newsletter_lists_users as l", "l.userID = u.id");
$this->db->where("l.listID", $groupID);
$this->db->order_by("email");
$query = $this->db->get();

$all_rows = $query->num_rows();

?>

<div class="panel panel-default">
      <div class="panel-heading">
            <h3 class="panel-title">Csoporttagok: <?= $list->name; ?> (<?= $all_rows;?> db)</h3>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th width="50">&nbsp;</th>
                        <th>E-mail</th>
                        <th>Létrehozva</th>
                        <th>Módosítva</th>
                        <th>Leiratkozva</th>
                    </tr>
                </thead>  
                <tfoot>
                    <tr>
                        <th>&nbsp;</th>                        
                        <th>E-mail</th>
                        <th>Létrehozva</th>
                        <th>Módosítva</th>
                        <th>Leiratkozva</th>
                    </tr>
                </tfoot>   
                <tbody>
                    <?php foreach ($query->result() as $user): ?>
                        <?php if ($user->unsubscribed): ?>
                            <tr class="table-danger">
                            <?php else: ?>
                            <tr>
                            <?php endif; ?>
                            <td><?= anchor("backend/newsletter/users/edit/" . $user->id, "<i class='glyphicon glyphicon-user'></i>"); ?></td>
                            <td><?= $user->email; ?></td>
                            <td><?= $user->created; ?></td>
                            <td <?php if ($user->created !== $user->modified): ?> class="table-success" <?php endif; ?>><?= $user->modified; ?></td>
                            <td><?= ($user->unsubscribed) ? "<i class='glyphicon glyphicon-cross text-danger'></i>" : ""; ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
