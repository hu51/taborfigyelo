<div class="panel panel-default">
      <div class="panel-heading">
            <h3 class="panel-title">Levél szerkesztése</h3>
      </div>
      <div class="panel-body">
<?php

$this->load->helper("form");

$this->db->from("tf_newsletter_templates");
$this->db->order_by("title", "ASC");
$query     = $this->db->get();
$templates = array();
foreach ($query->result() as $tmp) {
    $templates[$tmp->id] = $tmp->title;
}

$letterID = $this->uri->segment(5);
if (is_numeric($letterID)) {
    $this->db->from("tf_newsletter_letters");
    $this->db->where("id", $letterID);
    $letter = $this->db->get()->row();

    $title         = $letter->title;
    $body          = $letter->body;
    $from_address  = $letter->from_address;
    $from_name     = $letter->from_name;
    $reply_address = $letter->reply_address;
} else {
    $this->db->from("tf_newsletter_letters");
    $this->db->where("id", (int)$this->input->get("copy"));
    $letter = $this->db->get()->row();
    if ($letter) {
        $title         = $letter->title;
        $body          = $letter->body;
        $from_address  = $letter->from_address;
        $from_name     = $letter->from_name;
        $reply_address = $letter->reply_address;
        
    } else {
        $title         = "Levél";
        $body          = "Levéltörzs";
        $from_address  = $this->tools->GetSetting("email-from-address");
        $from_name     = $this->tools->GetSetting("email-from-title");
        $reply_address = $this->tools->GetSetting("email-reply-address");
    }
}

$all  = $this->db->from("tf_newsletter_letters_users")->where("letterID", $letterID)->count_all_results();
$sent = $this->db->from("tf_newsletter_letters_users")->where("letterID", $letterID)->where("sent", "1")->count_all_results();
$recv = $this->db->from("tf_newsletter_letters_users")->where("letterID", $letterID)->where("received", "1")->count_all_results();

?>
<div class="row text-muted">
    <div class="col-lg-3"><?=anchor("hirlevel/online/" . $letter->uniqid, "<i class='glyphicon glyphicon-share'></i> Előnézet", " target='_blank'");?></div>
    <div class="col-lg-3"><?=anchor("backend/newsletter/letters/members/" . $letter->id, "Címzettek száma: " . $all, "target='_blank'");?></div>
    <div class="col-lg-3">Elküldött üzenetek: <?=$sent;?></div>
    <div class="col-lg-3">Olvasott üzenetek: <?=$recv;?></div>
</div>
<form class="form" method="post">
    <div class="form-row">
        <label class="col-lg-2">Tárgy:</label>
        <div  class="col-lg-10">
            <input type="text" class="form-control" name="title" value="<?=set_value("title", $title);?>"/>
        </div>
    </div>

    <div class="form-row">
        <label class="col-lg-2">Sablon:</label>
        <div class="col-lg-10">
            <?=form_dropdown("template", $templates, set_value("template", 1), " class='form-control' ");?>
        </div>
    </div>

    <div class="form-row">
        <label class="col-lg-2">
            Szöveg:<br />
            <br />
            <small>

                Dinamikus mezők:<br />
                #id# - ID<br />
                #email# - E-mail<br />
                #name# - Name<br />
                #time# - <?=date("H:i");?> (frissül)<br />
                #date# - <?=date("Y. M. d.");?> (frissül)<br />
                #kiemelt#<br>
                #eloresorolt#<br>
            </small>
        </label>
        <div  class="col-lg-10">
            <textarea name="body" class="tinymce" ><?=set_value("body", $body);?></textarea>
        </div>
    </div>

    <div class="form-row">
        <label class="col-lg-2"></label>
        <div  class="col-lg-10">
            <button type="submit" name="saveLetter" class="btn btn-primary btn-block" value="Mentés" >Mentés</button>
        </div>
    </div>

    <hr />

    <div class="form-row">
        <label class="col-lg-2">Feladó:</label>
        <div  class="col-lg-4">
            <input type="text" class="form-control" name="from_address" placeholder="E-mail" readonly value="<?=set_value("from_address", $from_address);?>"/>
        </div>
        <label class="col-lg-2">Név:</label>
        <div  class="col-lg-4">
            <input type="text" class="form-control" name="from_name" placeholder="Név" value="<?=set_value("from_name", $from_name);?>"/>
        </div>
    </div>

    <div class="form-row">
        <label class="col-lg-2">Válasz cím:</label>
        <div  class="col-lg-10">
            <input type="text" class="form-control" name="reply_address" placeholder="Válaszcím" readonly value="<?=set_value("reply_address", $reply_address);?>"/>
        </div>
    </div>

    <hr />

    <div class="form-row">
        <label class="col-lg-2">Előnézet küldése <?=form_checkbox("preview_send", "on");?></label>
        <div class="col-lg-10">
            <input type="text" class="form-control" name="preview_to" value="<?=$this->session->userdata("user_email");?>" />
        </div>
    </div>

    <hr />

    <div class="form-row">
        <label class="col-lg-2">Címzettek hozzáadása</label>
        <div class="row col-lg-10">
            <div class="col-lg-6 col-sm-12">
                <ul class='list-group'>
                    <?php

$this->db->from("tf_newsletter_lists");
$this->db->order_by("name", "ASC");
$query = $this->db->get();

foreach ($query->result() as $list) {
    /*
    $this->db->from("lists_users as g");
    $this->db->join("letters_users as l", "g.userID = l.userID");
    $this->db->where("listID", $list->id);
    $found = $this->db->get()->num_rows();
     */
    echo "<li class='list-group-item p2'>"
    . form_checkbox("add_members[" . $list->id . "]", "on", false) . "&nbsp;"
    . $list->name . "</li>";
}

?>
                </ul>
            </div>

            <div class="col-lg-6 col-sm-12">
                <ul class='list-group'>
                    <li class="list-group-item list-group-item-primary">
                        <?=form_radio("clear", "", true);?>&nbsp;Nincs változás
                    </li>
                    <li class="list-group-item list-group-item-warning">
                        <?=form_radio("clear", "members");?>&nbsp;Frissítéskor az összes címzett törlése (ami még nem lett kiküldve)
                    </li>
                    <li class="list-group-item list-group-item-warning">
                        <?=form_radio("clear", "status");?>&nbsp;Frissítéskor a kiküldés és olvasás státusz törlése (újraküldéshez)
                    </li>
                    <li class="list-group-item list-group-item-danger">
                        <?=form_radio("clear", "all");?>&nbsp;Összes címzett törlése (mindenki!)
                    </li>
                </ul>
            </div>
        </div>
    </div>

</form>
</div>
</div>

<script src="<?=base_url();?>js/tinymce/tinymce.min.js" type="text/javascript"></script>
<script>
    tinymce.init({
        mode: "specific_textareas",
        editor_selector: "tinymce",
        paste_as_text: true,
        height: 500,
        theme: 'modern',
        plugins: ' save paste searchreplace autolink directionality visualblocks visualchars fullscreen image media code link table hr nonbreaking anchor toc lists textcolor wordcount responsivefilemanager',
        toolbar1: 'save | responsivefilemanager | formatselect | bold italic strikethrough forecolor backcolor | link image media | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat code | fullscreen',
        image_advtab: true,
        external_filemanager_path:"<?=base_url();?>filemanager/",
        filemanager_title:"Responsive Filemanager" ,
        external_plugins: { "filemanager" : "<?=base_url();?>filemanager/plugin.min.js"},
        filemanager_access_key:"5EC4E40BB6748073F0ECEC3A2229371D4CEA1297D0A6B6742EBB351D490BA11A",
        content_css: [
            '//www.tinymce.com/css/codepen.min.css'
        ],
        save_onsavecallback : "ajaxSave",
        relative_urls: false,
        remove_script_host : false,
        document_base_url : "<?= site_url(); ?>"
    });

    function ajaxSave() {
        var ed = tinymce.get('body');
        body = ed.getContent();

        $.ajax({
            type: "post",
            url: "<?=site_url("backend/newsletter/letters/ajaxSave");?>",
            data: { id: '<?=$letterID; ?>', body: body},
            dataType: "Json",
            success: function (response) {
                if (response.saved) {
                    $.notify(
                        "Módosítások mentve.",
                        {
                            type: "success",
                            placement: {
                                from: "top",
                                align: "left"
                            },
                            offset: 20,
                            delay: 1000
                        }
                    );
                }
            }
        });
    }

</script>
