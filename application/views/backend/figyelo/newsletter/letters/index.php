<div class="panel panel-default">
      <div class="panel-heading">
            <h3 class="panel-title">Hírlevelek</h3>
      </div>
      <div class="panel-body">
<?php

if ($this->input->get("oldal")) {
    $page_from = (int) $this->input->get("oldal");
} else {
    $page_from = 1;
}

$this->load->helper("form");

$this->db->select("id");
$this->db->from("tf_newsletter_letters");
if ($this->input->post("filter")) {
    $this->db->like("title", $this->input->post("filter", true));
}
$query = $this->db->get();

$all_rows  = $query->num_rows();
$page_rows = 20;
$page_max  = ceil($all_rows / $page_rows);

$pagi = $this->tools->pagination("backend/newsletter/letters/", $all_rows, 20, $page_from);

$this->db->from("tf_newsletter_letters");
if ($this->input->post("filter")) {
    $this->db->or_like("title", $this->input->post("filter", true));
}
$this->db->limit($pagi["rows"], $pagi["from"]);
$this->db->order_by("modified", "DESC");
$this->db->order_by("created", "DESC");
$this->db->order_by("title");
$query = $this->db->get();

?>
      <form method="post">
            <div class="input-group mb-3 col-lg-4">
                <div class="col-md-8">
                <input type="text" name="filter" class="form-control" value="<?=set_value("filter");?>" />
                </div>
                <div class="col-md-4">
                    <button class="btn btn-primary" type="submit">Keresés</button>
                </div>
            </div>
        </form>
        <?php echo $pagi["html"]; ?>
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th colspan="4">&nbsp;</th>
                        <th>Tárgy</th>
                        <th>Létrehozva</th>
                        <th>Módosítva</th>
                        <th>Kiküldve</th>
                        <th>Címzett</th>
                        <th>Elküldve</th>
                        <th>Hibás cím</th>
                        <th>Elolvasta</th>
                        <th width="50">Kiküldés</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($query->result() as $list):
                        $all      = $this->db->from("tf_newsletter_letters_users")->where("letterID", $list->id)->count_all_results();
                        $all_sent = $this->db->from("tf_newsletter_letters_users")->where("letterID", $list->id)->where("sent", "1")->count_all_results();
                        $all_recv = $this->db->from("tf_newsletter_letters_users")->where("letterID", $list->id)->where("received", "1")->count_all_results();
                        $all_bad  = $this->db->from("tf_newsletter_letters_users")->where("letterID", $list->id)->where("sendingTry >", "0")->count_all_results();

                        ?>
                        <tr>
                            <td><?=anchor("backend/newsletter/letters/edit/" . $list->id, "<i class='glyphicon glyphicon-edit'></i>");?></td>
                            <td><?=anchor("backend/newsletter/letters/members/" . $list->id, "<i class='glyphicon glyphicon-user'></i>");?></td>
                            <td><?=anchor("hirlevel/online/" . $list->uniqid, "<i class='glyphicon glyphicon-search'></i>", " target='_blank'");?></td>
                            <td><?=anchor("backend/newsletter/letters/edit/create/?copy=" . $list->id, "<i class='glyphicon glyphicon-duplicate'></i>", " target='_blank'");?></td>
                            <td><?=$list->title;?></td>
                            <td><?=substr($list->created,0,10);?></td>
                            <td><?=substr($list->modified, 5, 11);?></td>
                            <td><?=substr($list->sent, 5, 11);?></td>
                            <td><?=$all;?></td>
                            <td><?=$all_sent;?></td>
                            <td><?=$all_bad;?></td>
                            <?php if ($all_sent > 0): ?>
                                <td><a href="<?=base_url("backend/newsletter/letters/stat/" . $list->id);?>"><?=$all_recv;?> (<?=number_format($all_recv / $all_sent * 100, 2);?>%)</a></td>
                            <?php else: ?>
                                <td>&nbsp;</td>
                            <?php endif;?>
                            <td>
                                <?php if(!$list->autoSend): ?>
                                    <?=anchor("backend/newsletter/letters/send/" . $list->id, "<i class='glyphicon glyphicon-send'></i>");?>
                                <?php elseif ($all == ($all_sent+$all_bad)): ?>
                                    <span class="glyphicon glyphicon-stop"></span> KÉSZ
                                <?php else: ?>
                                    <span class="glyphicon glyphicon-refresh"></span> FUT
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?php echo $pagi["html"]; ?>
        </div>
    </div>
</div>
</div>