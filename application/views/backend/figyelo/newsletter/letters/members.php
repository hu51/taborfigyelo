<div class="panel panel-default">
      <div class="panel-heading">
            <h3 class="panel-title">Címzettek: <?=$letter->title;?></h3>
      </div>
      <div class="panel-body">
<?php

$letterID = (int) $this->uri->segment(5);
$this->load->helper("form");

$this->db->from("tf_newsletter_letters");
$this->db->where("id", $letterID);
$letter = $this->db->get()->row();

$this->db->select("l.*, u.email, u.id as userID, u.unsubscribed as u_unsubscribed, lu.unsubscribed as g_unsubscribed, g.name as groupTitle");
$this->db->from("tf_newsletter_users as u");
$this->db->join("tf_newsletter_letters_users as l", "l.userID = u.id");
$this->db->join("tf_newsletter_lists_users as lu", "lu.listID = l.listID AND lu.userID = u.id", "LEFT");
$this->db->join("tf_newsletter_lists as g", "g.id = l.listID", "LEFT");
$this->db->where("l.letterID", $letterID);
$this->db->order_by("sent", "DESC");
$this->db->order_by("sentDatetime");
$this->db->order_by("sendingTry", "DESC");
$query = $this->db->get();

$all_rows = $query->num_rows();

        ?>
        <form method="post">
            <div class="card mb-3">
                <div class="card-header">
                    Összesen: <?=$all_rows;?></div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>E-mail</th>
                                    <th>Csoport</th>
                                    <th>Küldési próbák száma</th>
                                    <th>Elküldve</th>
                                    <th>Elolvasva</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>E-mail</th>
                                    <th>Csoport</th>
                                    <th>Küldési próbák száma</th>
                                    <th>Elküldve</th>
                                    <th>Elolvasva</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php foreach ($query->result() as $user): ?>
                                    <tr>
                                        <td><?=$user->email;?></td>
                                        <td><?=$user->groupTitle;?></td>
                                        <td><?=$user->sendingTry;?></td>
                                        <?php if ($user->sent): ?>
                                            <td><?=$user->sentDatetime;?></td>
                                        <?php else: ?>
                                            <td>-</td>
                                        <?php endif;?>

                                        <?php if ($user->received): ?>
                                            <td><?=$user->receivedDatetime;?></td>
                                        <?php else: ?>
                                            <td>-</td>
                                        <?php endif;?>
                                    </tr>
                                <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>