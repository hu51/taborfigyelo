<?php

$letterID = (int) $this->uri->segment(5);

$this->db->from("tf_newsletter_letters");
$this->db->where("id", $letterID);
$letter = $this->db->get()->row();

$send_to = $this->db->from("tf_newsletter_letters_users")->where("letterID", $letterID)->where("sent", "0")->count_all_results();

?><div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Hírlevél kiküldése - <?=$letter->title;?></h3>
    </div>
    <div class="panel-body">
        <p>Még ki nem küldött címzettek száma: <?=$send_to;?></p>
        <br>
        <?php if(is_Null($letter->sent)): ?>
        <form method="post">
            <button name="confirm_send" type="submit" class="btn  btn-primary" value="send">Küldés megerősítése</button>
        </form>
        <?php else: ?>
            <div class='alert alert-warning'>A levelet újra kell szerkeszteni és csak utána lehet kiküldeni.</div>
        <?php endif; ?>
    </div>
</div>