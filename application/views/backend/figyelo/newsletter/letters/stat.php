<?php

$letterID = (int) $this->uri->segment(5);

$this->db->from("tf_newsletter_letters");
$this->db->where("id", $letterID);
$letter = $this->db->get()->row();

$this->db->select("id");
$this->db->from("tf_newsletter_letters_users");
$this->db->where("letterID", $letterID);
$query = $this->db->get();

$all_rows  = $query->num_rows();

if ($this->input->get("oldal")) {
    $page_from = (int) $this->input->get("oldal");
} else {
    $page_from = 1;
}
$page_rows = 50;
$page_max  = ceil($all_rows / $page_rows);

$pagi = $this->tools->pagination("backend/newsletter/letters/stat/".$letterID, $all_rows, 20, $page_from);

$this->db->select("l.*, u.email, u.id as userID");
$this->db->from("tf_newsletter_users as u");
$this->db->join("tf_newsletter_letters_users as l", "l.userID = u.id");
$this->db->where("l.letterID", $letterID);
$this->db->order_by("received", "DESC");
$this->db->order_by("receivedDatetime", "ASC");
$this->db->order_by("sent", "DESC");
$this->db->order_by("email");
$this->db->limit($pagi["rows"], $pagi["from"]);
$query = $this->db->get();


$all_rows = $query->num_rows();

?>
<div class="panel panel-default">
      <div class="panel-heading">
            <h3 class="panel-title">Statisztika: <?=$letter->title;?></h3>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
        <?php echo $pagi["html"]; ?>
            <table class="table table-bordered table-sm" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th width="50">&nbsp;</th>
                        <th>E-mail</th>
                        <th>Kiküldve</th>
                        <th>Elolvasva</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th width="50">&nbsp;</th>
                        <th>E-mail</th>
                        <th>Kiküldve</th>
                        <th>Elolvasva</th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php foreach ($query->result() as $user): ?>
                        <tr>
                            <td><?=anchor("backend/newsletter/users/edit/" . $user->userID, "<i class='glyphicon glyphicon-user'></i>");?></td>
                            <td><?=$user->email;?></td>
                            <td><?=$user->sentDatetime;?></td>
                            <td><?=$user->receivedDatetime;?></td>
                        </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
            <?php echo $pagi["html"]; ?>
        </div>
    </div>
</div>