<div class="card mb-3">
    <div class="card-header">
        <i class="fa fa-table"></i> Kép feltöltése</div>
    <div class="card-body">
        <form method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="form-group col-lg-6">
                    <label>Fájl:</label>
                    <input type="file" name="photo" accept="image/jpeg, image/png" />
                    <button type="submit" class="btn btn-primary">Feltöltés</button>
                </div>
                <div class="form-group col-lg-6">
                    <input type="input" name="photo_rename" class="form-control" placeholder="átnevezés" />
                    <input type="checkbox" name="photo_overwrite"  /> feltöltéskor felülírás
                </div>
            </div>
        </form>
    </div>
</div>

<div class="card mb-3">
    <div class="card-header">
        <i class="fa fa-table"></i> Képek</div>
    <div class="card-body">
        <?php
        
        GLOBAL $img_sizes;

        $this->load->helper("directory");

        $dir = FCPATH . "/upload/";
        $map = directory_map($dir);
        foreach ($map as $file):
            if (substr($file, 0, 3) !== "th_"):
                $info = getimagesize($dir . $file);
                $pi   = pathinfo($dir . $file);

                ?>
                <div class="card card-body col-lg-4 col-md-6 float-left">
                    <div class="row">
                        <div style="width: 160px; height: 160px;">
                            <img src="<?= base_url("upload/th_" . $pi["filename"] . "_200.jpg"); ?>" style="max-width: 150px; max-height: 150px;" class="img-thumbnail"><br>

                        </div>
                        <div style="float: left; left: 150px;">
                            <?= $pi["filename"]; ?><br>
                            <code><?= $info[0] . "x" . $info[1] . "px"; ?></code>
                            <small><?= date("Y-m-d H:i", filemtime($dir . $file)); ?></small><br>
                            <code><?= anchor("upload/" . $file, "Original", " target='_blank' "); ?></code>, 
                            <?php foreach ($img_sizes as $px): ?>
                                <?php if (file_exists(FCPATH . "upload/th_" . $pi["filename"] . "_{$px}.jpg")): ?>
                                    <code><?= anchor("upload/th_" . $pi["filename"] . "_{$px}.jpg", "{$px}px", " target='_blank' "); ?></code>, 
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>

                        <div style="position: absolute; bottom: 5px; right: 5px">
                            <form method="post">
                                <input type="hidden" name="del_photo" value="<?= $file; ?>">
                                <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                <input type="checkbox" name="del_photo_conf" value="<?= $file; ?>"  title="megerősítés" />
                            </form>                             
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
</div>