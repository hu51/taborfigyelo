<?php

$this->load->helper("form");

if (is_numeric($this->uri->segment(5))) {
    $userID = (int) $this->uri->segment(5);
    //$this->db->select("id");
    $this->db->from("tf_newsletter_users");
    $this->db->where("id", $userID);
    $user = $this->db->get()->row();
} else {
    $userID = "create";
    $user   = (object) array(
        "email"        => "",
        "confirmed"    => "0",
        "blacklisted"  => "0",
        "bouncecount"  => "0",
        "entered"      => "",
        "modified"     => "",
        "unsubscribed" => "0",
    );
}
?>
<div class="col-md-8 col-md-offset-2 col-sm-12">
<div class="panel panel-default">
      <div class="panel-heading">
            <h3 class="panel-title">Felhasználó adatai</h3>
      </div>
      <div class="panel-body">

        <form class="form form-horizontal" method="post">
            <input type="hidden" name="userID" value="<?=$userID;?>" />
            <div class="row">
                <label class="col-lg-4">E-mail:</label>
                <div  class="col-lg-8">
                    <input type="text" class="form-control" name="email" value="<?=$user->email;?>" />
                </div>
            </div>

            <div class="row">
                <label class="col-lg-4">Név:</label>
                <div  class="col-lg-8">
                    <input type="text" class="form-control" name="name" value="<?=$user->name;?>" />
                </div>
            </div>

            <div class="row">
                <label class="col-lg-4">Visszaigazolva:</label>
                <div  class="col-lg-8">
                    <?= ($user->confirmed)?"<span class='text-success'>Igen</span>":"<span class='text-warning'>Nem</span>";?>
                </div>
            </div>

            <div class="row">
                <label class="col-lg-4">Tiltólistán:</label>
                <div  class="col-lg-8">
                    <?= $this->tools->bs_checkbox("blacklisted", $user->blacklisted);?>
                </div>
            </div>

            <div class="row">
                <label class="col-lg-4">Visszapattant:</label>
                <div class="col-lg-8"><?=$user->bouncecount;?></div>
            </div>

            <div class="row">
                <label class="col-lg-4">Felrögzítve:</label>
                <div  class="col-lg-8"><?=$user->created;?></div>
            </div>

            <div class="row">
                <label class="col-lg-4">Utoljára módosítva:</label>
                <div  class="col-lg-8"><?=$user->modified;?></div>
            </div>

            <div class="row">
                <label class="col-lg-4">Leiratkozva:</label>
                <div  class="col-lg-8">
                    <?= $this->tools->bs_checkbox("unsubscribed", $user->unsubscribed, true);?>
                </div>
            </div>

            <div class="row">
                <label class="col-lg-4"></label>
                <div  class="col-lg-8">
                    <button type="submit" name="saveUser" class="btn btn-primary btn-block" value="Mentés" >Módosítások mentése</button>
                </div>
            </div>

            <div class="row p5">
                <label>Lista tagság:</label>
            </div>
            <div class="row">
                <?php if ($user->unsubscribed): ?>
                <div class="text-danger">A felhasználó le van iratkozva az oldalról!</div>
                <?php else: ?>
                    <?php

                        $this->db->from("tf_newsletter_lists");
                        $this->db->order_by("name", "ASC");
                        $query = $this->db->get();

                        foreach ($query->result() as $list) {
                            if (is_numeric($userID)) {
                                $found = $this->db->from("tf_newsletter_lists_users")->where("userID", $userID)->where("listID", $list->id)->get()->row();
                            } else {
                                $found = false;
                            }

                            echo "<div class='col-md-6 p5'>";
                            if ($found && (!is_null($found->unsubscribed))) {
                                echo "<i class='glyphicon glyphicon-remove text-danger'></i>";
                            } else {
                                echo form_checkbox("list_member[" . $list->id . "]", "on", $found);
                            }
                            echo $list->name . "</div>";
                        }

                    ?>
                <?php endif;?>
            </div>
        </form>
    </div>
</div>
</div>