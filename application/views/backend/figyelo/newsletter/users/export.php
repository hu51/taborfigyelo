<?php

   
require FCPATH.'vendor/autoload.php';
    
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

// Create new Spreadsheet object
$spreadsheet = new Spreadsheet();
$spreadsheet->setActiveSheetIndex(0);
$spreadsheet->getActiveSheet()->setTitle('data');


$this->db->select("l.name, u.userID");
$this->db->from("tf_newsletter_lists as l");
$this->db->join("tf_newsletter_lists_users as u", "u.listID=l.id");
$this->db->group_by("u.userID");
$tmp = $this->db->get()->result();
$lists = [];
foreach ($tmp as $data) {
    $lists[$data->userID] .= trim($data->name)."|";
}

$this->db->from("tf_newsletter_users as u");
$this->db->order_by("id", "ASC");
$all = $this->db->get()->result();

$rowID = 2;
$firstRow = true;

$hideFields = array("uniqid");
foreach ($all as $data) {
    $colID = 1;
    if ($firstRow) {
        foreach ($data as $key => $field) {
            if (!in_array($key, $hideFields)) {
                $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow($colID, 1, $key);
                $colID++;
            }
        }
        $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow($colID, 1, "Hirlevelek");

        $firstRow = false;
        $colID = 1;
    }
    
    foreach ($data as $key => $val) {
        if (!in_array($key, $hideFields)) {
            $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow($colID, $rowID, $val);
            $colID++;
        }

    }    

    if (isset($lists[$data->id])) {             
        foreach (explode("|", rtrim($lists[$data->id], "|")) as $tmp => $value) {                
            $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow($colID, $rowID, $value);
            $colID++;
        }
    }
    $rowID++;
}


// Redirect output to a client’s web browser (Xls)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="newsletter_members.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0

$writer = IOFactory::createWriter($spreadsheet, 'Xls');
$writer->save('php://output');  