<?php

$this->db->from("tf_newsletter_lists");
$this->db->order_by("name", "ASC");
$query = $this->db->get();
$lists = array("-1" => "* kihagy");
foreach ($query->result() as $tmp) {
    $lists[$tmp->id] = $tmp->name;
}

$this->load->helper("form");

?>

<div class="panel panel-default">
      <div class="panel-heading">
            <h3 class="panel-title">Felhasználók importálása</h3>
      </div>
      <div class="panel-body">
        <form class="form" method="post" enctype="multipart/form-data">
            <div class="form-row">
                <label class="col-lg-2">Oszlop tagolás</label>
                <div  class="col-lg-10">
                    <select name="selector" class="form-control">
                        <option value=";">Pontos-vessző (;)</option>
                        <option value=",">Vessző (,)</option>
                        <option value="\t">Tabulátor (tab)</option>
                    </select>
                </div>
            </div>

            <div class="form-row">
                <label class="col-lg-2">Listához adás:</label>
                <div class="col-lg-10">
                    <?= form_dropdown("add_list", $lists, false, " class='form-control' "); ?>
                </div>
            </div>

            <div class="form-row">
                <label class="col-lg-2">Szövegfájl:</label>
                <div  class="col-lg-10">
                    <input type="file" class="form-control" name="import_src" />
                </div>
            </div>

            <div class="form-row">
                <label class="col-lg-2">vagy szövegmező:</label>
                <div  class="col-lg-10">
                    <textarea class="form-control" name="import_text" rows="10"></textarea>
                </div>
            </div>

            <div class="form-row">
                <label class="col-lg-2"></label>
                <div  class="col-lg-10">
                    <button type="submit" name="import_step_2" class="btn btn-primary btn-block" value="Tovább" >Tovább</button>
                </div>
            </div>
        </form>
    </div>
</div>