<div class="panel panel-default">
      <div class="panel-heading">
            <h3 class="panel-title">Felhasználók importálása - mezők egyeztetése</h3>
            </div>
      <div class="panel-body">            
        <form class="form" method="post" enctype="multipart/form-data">
            <input type="hidden" name="add_list" value="<?= $this->input->post("add_list"); ?>" />

            <button type="submit" name="import_step_3" class="btn btn-primary btn-block" value="Tovább" >Tovább</button>
            <input type="checkbox" name="drop-first-row"> Első sor adatainak eldobása<br />
            <table class="table table-bordered table-sm">
                <?php

                if (!isset($upload_data)) {
                    $src = $this->input->post("import_text");
                    $rows = explode("\n", $src);
                } else {
                    $rows = explode(PHP_EOL, file_get_contents($upload_data["full_path"]));
                    unlink($upload_data["full_path"]);
                }               
                $fields = array("-" => "* mező eldobása", "email" => "E-mail", "name" => "Név");
                $this->load->helper("form");

                $row      = 0;
                $firstRow = true;
                foreach ($rows as $string) {
                    if (!empty($string)) {
                        $data = explode($this->input->post("selector"), $string);

                        $num = count($data);
                        if ($firstRow):

                            ?>
                            <thead>
                                <tr>
                                    <?php for ($col = 0; $col < $num; $col++): ?>
                                        <th>
                                            <?= form_dropdown("col_match[" . $col . "]", $fields, $id, " class='form-control input-sm' "); ?>
                                        </th>
                                    <?php endfor; ?>
                                </tr>
                            </thead>
                            <?php

                        endif;

                        $row++;
                        echo "<tr>\n";
                        for ($col = 0; $col < $num; $col++) {
                            echo "<td><input type='text' class='form-control input-sm' name='data[{$row}][{$col}]' value='{$data[$col]}' readonly /></td>";
                        }
                        echo "</tr>\n";
                    }

                    $firstRow = false;
                }

                ?>
            </table>
        </form>
    </div>
</div>