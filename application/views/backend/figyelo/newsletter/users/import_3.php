<h3>Felhasználók importálása - adatok feldolgozása</h3>
<div class="list-group list-group-flush small">
    <?php

    $drop_first_row   = ($this->input->post("drop-first-row") == "on");
    $add_list         = $this->input->post("add_list");
    $force_fill       = $this->input->post("force_fill");
    $force_fill_value = $this->input->post("force_fill_value");

    $email_field = false;
    $name_field = false;
    foreach ($this->input->post("col_match") as $col => $val) {
        if ($val !== "-") {
            if ($val == "email") {
                $email_field = $col;
            }
            if ($val == "name") {
                $name_field = $col;
            }
        }
    }

    if ($email_field === false) {
        echo "<div class='list-group-item alert alert-danger'>Nincs E-mail mező definiálva!</div>\n";
        return;
    }
    
    $rows = $this->input->post("data");
    foreach ($rows as $rowNum => $row) {
        if ($drop_first_row && $rowNum == 1) {
            echo "<div class='list-group-item alert alert-info'>Első sor kihagyva</div>\n";
        } else {
            if (!empty($row[$email_field])) {
                $email = filter_var(trim($row[$email_field]), FILTER_SANITIZE_EMAIL);
                if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $name = filter_var($row[$name_field], FILTER_SANITIZE_STRIPPED);
                    $found = $this->db->from("tf_newsletter_users")->like("email", $email)->get()->row();
                    if ($found) {
                        echo "<div class='list-group-item alert alert-warning'>Már létezik ez az e-mail cím: {$email}</div>\n";
                        $userID = $found->id;
                    } else {
                        $this->db->insert("tf_newsletter_users", array("email" => $email, "name" => $name, "created" => date("Y-m-d H:i:s"), "uniqID" => uniqid()));
                        $userID = $this->db->insert_id();
                        if ($userID) {
                            echo "<div class='list-group-item alert alert-success'>E-mail cím felrögzítve: {$email}</div>\n";
                        } else {
                            echo "<div class='list-group-item alert alert-danger'>Adatbázis hiba?</div>\n";
                        }
                    }

                    if ($userID) {
                        $this->db->from("tf_newsletter_lists_users");
                        $this->db->where("userID", $userID);
                        $this->db->where("listID", $add_list);
                        $this->db->where("unsubscribed is NOT NULL");
                        $list_unsubscribed = $this->db->get()->row();                                        
                        if (($add_list > 0) && (!$list_unsubscribed)) {
                            $data = array(
                                "userID" => $userID,
                                "listID" => $add_list,
                                "modified" => date("Y-m-d H:i:s")
                            );
                            $this->db->replace("tf_newsletter_lists_users", $data);
                        }
                    }
                } else {
                    echo "<div class = 'list-group-item alert alert-warning'>Hibás email: {$row[$email_field]}</div>\n";
                }
            } else {
                echo "<div class = 'list-group-item alert alert-warning'>Üres az email mező (oszlop: {$email_field})</div>\n  ";
            }
        }
    }

    ?>
</div>
