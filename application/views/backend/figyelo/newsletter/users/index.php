<?php

if ($this->input->get("oldal")) {
    $page_from = (int) $this->input->get("oldal");
} else {
    $page_from = 1;
}

$this->load->helper("form");

$this->db->select("id");
$this->db->from("tf_newsletter_users");
if ($this->input->post("filter")) {
    $this->db->like("email", $this->input->post("filter", true));
}
$this->db->order_by("email");
$query = $this->db->get();

$all_rows  = $query->num_rows();
$page_rows = 20;
$page_max  = ceil($all_rows / $page_rows);

$pagi = $this->tools->pagination("backend/newsletter/users/", $all_rows, 20, $page_from);

$this->db->from("tf_newsletter_users");
if ($this->input->post("filter")) {
    $this->db->like("email", $this->input->post("filter", true));
    $this->db->or_like("name", $this->input->post("filter", true));
}
$this->db->limit($pagi["rows"], $pagi["from"]);
$this->db->order_by("email");
$query = $this->db->get();

?>
<div class="panel mb-3">
    <div class="panel-heading">
        Felhasználók (össz: <?= $all_rows; ?>)
        </div>
    <div class="panel-body">
        <form method="post">
            <div class="input-group mb-3 col-lg-4">
                <div class="col-md-8">
                <input type="text" name="filter" class="form-control" value="<?= set_value("filter"); ?>" />
                </div>
                <div class="col-md-4">
                    <button class="btn btn-primary" type="submit">Keresés</button>
                </div>
            </div>           
        </form>
        <?php echo $pagi["html"]; ?>  
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th width="50">&nbsp;</th>
                        <th>E-mail</th>
                        <th>Név</th>
                        <th>Létrehozva</th>
                        <th>Módosítva</th>
                        <th>Leiratkozva</th>
                        <th>Blokkolva</th>
                    </tr>
                </thead>  
                <tfoot>
                    <tr>
                        <th>&nbsp;</th>                        
                        <th>E-mail</th>
                        <th>Név</th>
                        <th>Létrehozva</th>
                        <th>Módosítva</th>
                        <th>Leiratkozva</th>
                        <th>Blokkolva</th>
                    </tr>
                </tfoot>   
                <tbody>
                    <?php foreach ($query->result() as $user): ?>
                        <tr>
                            <td><?= anchor("backend/newsletter/users/edit/" . $user->id, "<i class='glyphicon glyphicon-user'></i>"); ?></td>
                            <td><?= $user->email; ?></td>
                            <td><?= $user->name; ?></td>
                            <td><?= substr($user->created,0,16); ?></td>
                            <td <?php if ($user->created !== $user->modified): ?> class="table-success" <?php endif; ?>><?= substr($user->modified,0,16); ?></td>
                            <td><?= ($user->unsubscribed) ? "<i class='glyphicon text-danger glyphicon-check'></i>" : ""; ?></td>
                            <td><?= ($user->blacklisted == "1") ? "<i class='glyphicon text-danger glyphicon-check'></i>" : ""; ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>

        <p><a href="<?= site_url("backend/newsletter/users?export=xls"); ?>">Összes felhasználó listázása excelbe</a></p>
    </div>
</div>