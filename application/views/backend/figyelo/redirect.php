<form method="post" action="figyelo/main/redirect">
<?php

if ($this->input->post("delete")) {
    $id = (int) $this->input->post("delete");
    message("", "Bejegyzés törölve");
    $this->db->delete("tf_redirect", array("id" => $id));
}

if ($this->input->post("modify")) {
    $id   = (int) $this->input->post("modify");
    $url  = $this->input->post("newURL");
    $data = array(
        "newUrl"  => str_ireplace(site_url(), "", $url[$id]),
        "enabled" => 1,
    );
    message("", "Bejegyzés módosítva", "success");
    $this->db->update("tf_redirect", $data, array("id" => $id));
}

if ($this->input->post("addNew") && $this->input->post("newUrl")) {
    $old_data = $this->db->from("tf_404_pages")->where("id", (int) $this->input->post("import"))->get()->row();
    $data     = array(
        "oldUrl"  => str_ireplace(site_url(), "", $this->input->post("oldUrl")),
        "newUrl"  => str_ireplace(site_url(), "", $this->input->post("newUrl")),
        "hits"    => 0,
        "enabled" => 1,
    );

    if ($data["oldUrl"] !== $data["newUrl"]) {
        if ($this->db->insert("tf_redirect", $data)) {
            $this->db->delete("tf_404_pages", array("id" => $old_data->id));
            redirect("figyelo/main/redirect");
        }
    } else {
        message("", "A két url nem lehet azonos!");
    }
}

?>
<div class="panel panel-success">
    <div class="panel-heading">
        <h3 class="panel-title">Új hozzáadása</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <label class="col-md-2">Régi URL</label>
            <div class="col-md-10">
            <?php
                if ($this->input->get("import")) {
                    $url = $this->db->from("tf_404_pages")->where("id", (int) $this->input->get("import"))->get()->row()->url;
                    echo form_hidden("import", $this->input->get("import"));
                } else {
                    $url = "";
                }
                ?>
                <code>Használható * karakter az URL végének levágására</code><br>
                <input type="text" name="oldUrl" value="<?=$url;?>" class="form-control">
            </div>
        </div>
        <div class="row">
            <label class="col-md-2">Új URL</label>
            <div class="col-md-10">
                <input type="text" name="newUrl" value="<?=set_value("newUrl");?>" class="form-control">
            </div>
        </div>
        <div class="row">
            <label class="col-md-2"></label>
            <div class="col-md-10">
                <button type="submit" name="addNew" value="1" class="btn btn-success">Felrögzítés</button>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Oldal átirányítások</h3>
    </div>
    <div class="panel-body">
        <?php

        $this->db->select('id');
        $this->db->from("tf_redirect");
        $allRows = $this->db->get()->num_rows();

        if (!$this->input->get("oldal")) {
            $pageID = 1;
        } else {
            $pageID = (int) $this->input->get("oldal");
        }
        $pageMaxRows = 20;

        $page         = $this->tools->pagination("figyelo/main/redirect", $allRows, $pageMaxRows, $pageID);
        $pageID       = $page["current"];
        $pageStartRow = ($pageID - 1) * $pageMaxRows;

        $this->db->from("tf_redirect");
        $this->db->order_by("hits", "DESC");
        $this->db->limit($pageMaxRows, $pageStartRow);
        $query = $this->db->get();

        echo $page["html"];
        ?>
        <table class="table table-condended table-bordered">
            <thead>
                <tr>
                    <th></th>
                    <th>keresett URL</th>
                    <th>Új URL</th>
                    <th>Találatok</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
                    <?php foreach ($query->result() as $data): ?>
                        <tr>
                            <td><?=$data->id;?></td>
                            <td><code><?=$data->oldURL;?></code></td>
                            <td><input type="text" class="form-control" name="newURL[<?=$data->id;?>]" value="<?=$data->newURL;?>"></td>
                            <td><?=$data->hits;?></td>
                            <td>
                                <?=$this->tools->bs_checkbox("enabled[" . $data->id . "]", $data->enabled);?>
                            </td>
                            <td>
                                <button name="modify" value="<?=$data->id;?>" class="btn btn-primary btn-sm">Mentés</button>
                                <button name="delete" value="<?=$data->id;?>" class="btn btn-danger btn-sm">Törlés</button>
                            </td>
                        </tr>
                        <?php endforeach;?>
                    </table>
            <?=$page["html"];?>
        </form>
    </div>
</div>