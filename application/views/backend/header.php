<html>

<head>
	<title>TMF Admin</title>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/style.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/theme.css">
    <?php if($css && is_array($css)) {
            foreach ($css as $c): ?>
        <link rel="stylesheet" type="text/css" href="<?= $c; ?>">
            <?php endforeach;
        }        
    ?>

	<script type="text/javascript" src="<?= base_url(); ?>js/jquery.min.js"></script>
	<script type="text/javascript" src="<?= base_url(); ?>js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?= base_url(); ?>js/sortable.min.js"></script>
	<script type="text/javascript" src="<?= base_url(); ?>js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?= site_url(); ?>js/bootstrap-notify.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>js/main.js"></script>
    <?php if($js && is_array($js)) {
            foreach ($js as $c): ?>
        <script type="text/javascript" src="<?= $c; ?>"></script>
            <?php endforeach;
        }        
    ?>

	<base href="<?= base_url(); ?>">
</head>

<body>
	<div class="navbar navbar-default">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">TMF</a>
		</div>

		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">

				<li class="navbar-text">Minősítő</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Szerződések <span class="caret"></span></a>
					<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
						<li><a href="<?= base_url("minosito/main/contractlist"); ?>">Folyamatban lévő szerződések</a></li>
						<li><a href="<?= base_url("minosito/main/contractlist/closed"); ?>">Lezárt szerződések</a></li>
						<li><a href="<?= base_url("minosito/main/exportcontracts"); ?>">Minősített táborok EXCEL-be</a></li>
						<li class="divider"></li>
						<li><a href="<?= base_url("minosito/main/sendpaymentemail"); ?>">Fizetési emailre váró szerződések</a></li>
						<li><a href="<?= base_url("minosito/main/checkpayment"); ?>">Beérkezett fizetések ellenőrzése</a></li>
						<li><a href="<?= base_url("minosito/main/finalize"); ?>">Publikálásra váró szerződések</a></li>
						<li class="divider"></li>
						<li><a href="<?= base_url("minosito/main/oldcontracts"); ?>">Régen nyitott szerződések</a></li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Egyéb <span class="caret"></span></a>
					<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
						<li><a href="<?= base_url("minosito/news/categories"); ?>">Cikkek kategóriák</a></li>
						<li><a href="<?= base_url("minosito/news/edit/new"); ?>">Új cikk rögzítése</a></li>
						<li><a href="<?= base_url("minosito/news/listall"); ?>">Bejegyzések</a></li>

						<li class="divider"></li>
						<li><a href="<?= base_url("minosito/main/htmledit"); ?>">Leírások szerkesztése</a></li>
						<li class="divider"></li>
						<li><a href="<?= base_url("minosito/main/questions"); ?>">Kérdések listája</a></li>
						<li class="divider"></li>
						<li><a href="<?= base_url("minosito/main/cuponlist"); ?>">Kuponok listája</a></li>
						<li class="divider"></li>
						<li><a href="<?= base_url("minosito/main/banners"); ?>">Bannerek</a></li>
					</ul>
				</li>

				<li class="navbar-text">Figyelő</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Táborok <span class="caret"></span></a>
					<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
						<li><a href="<?= base_url("figyelo/camps/"); ?>">Táborok listája</a></li>
						<li><a href="<?= base_url("figyelo/camps/contracts"); ?>">Tábor szerződések listája</a></li>
                        <li class="divider"></li>
						<li><a href="<?= base_url("figyelo/camps/ratings"); ?>">Tábor értékelések</a></li>
                        <li class="divider"></li>
						<li><a href="<?= base_url("figyelo/camps/todos"); ?>">Teendőkre váró táborok</a></li>
                        
						<li class="divider"></li>
						<li><a href="<?= base_url("figyelo/camps/types"); ?>">Típusok listája</a></li>
						<li><a href="<?= base_url("figyelo/camps/categories"); ?>">Kategóriák listája</a></li>
						<li><a href="<?= base_url("figyelo/camps/countries"); ?>">Országok listája</a></li>
						<li><a href="<?= base_url("figyelo/camps/states"); ?>">Megyék listája</a></li>
						<li><a href="<?= base_url("figyelo/camps/locations"); ?>">Helyszínek listája</a></li>
					</ul>
				</li>				
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Hírek, leírások <span class="caret"></span></a>
					<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
						<li><a href="<?= base_url("figyelo/news/categories"); ?>">Cikkek kategóriák</a></li>
						<li><a href="<?= base_url("figyelo/news/edit/new"); ?>">Új cikk rögzítése</a></li>
						<li><a href="<?= base_url("figyelo/news/listall"); ?>">Bejegyzések</a></li>
						<li class="divider"></li>
						<li><a href="<?= base_url("figyelo/main/htmledit/html"); ?>">Leírások/Honlapszövegek</a></li>
						<li><a href="<?= base_url("figyelo/main/htmledit/email"); ?>">Leírások/Levelek, szerződések</a></li>
						<li class="divider"></li>
						<li><a href="<?= base_url("figyelo/main/forms"); ?>">Űrlapok</a></li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Hírlevél <span class="caret"></span></a>
					<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
						<li><a href="<?= base_url("backend/newsletter/users"); ?>">Felhasználók</a></li>
						<li><a href="<?= base_url("backend/newsletter/users/import"); ?>">Felhasználók importálása</a></li>
						<li class="divider"></li>
						<li><a href="<?= base_url("backend/newsletter/letters/create"); ?>">Új hírlevél léterhozása</a></li>
						<li><a href="<?= base_url("backend/newsletter/groups/create"); ?>">Csoport létrehozása</a></li>
						<li class="divider"></li>
						<li><a href="<?= base_url("backend/newsletter/letters"); ?>">Levelek</a></li>
						<li><a href="<?= base_url("backend/newsletter/groups"); ?>">Csoportok</a></li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Egyéb <span class="caret"></span></a>
					<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
						<li><a href="<?= base_url("backend/main/banners"); ?>">Bannerek</a></li>
						<li><a href="<?= base_url("backend/main/campbanners"); ?>">Kiemelések</a></li>
						<li><a href="<?= base_url("figyelo/main/bannercupons"); ?>">Banner kuponok</a></li>
						<li class="divider"></li>
						<li><a href="<?= base_url("figyelo/main/missing"); ?>">404-es oldalak</a></li>
						<li><a href="<?= base_url("figyelo/main/redirect"); ?>">Oldal átirányítások</a></li>
					</ul>
				</li>
			</ul>

			<ul class="nav navbar-nav navbar-right">
                <li class="navbar-text">Email teszt: <?= ($this->tools->GetSEtting("email-testmode"))?"<span class='bg-success p5'>BE</span>":"<span class='bg-danger p5'>ki</span>"; ?></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Rendszer <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li class="dropdown-submenu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Beállítások</a>
							<ul class="dropdown-menu">
								<li><a href="<?= base_url("backend/main/settings/tm"); ?>">Táborminősítő</a></li>
								<li><a href="<?= base_url("backend/main/settings/tf"); ?>">Táborfigyelő</a></li>
							</ul>
						</li>
						<li class="dropdown-submenu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Felhasználók</a>
							<ul class="dropdown-menu">
								<li><a href="<?= base_url("backend/users/adduser"); ?>">Új felhasználó</a></li>
								<li><a href="<?= base_url("backend/users"); ?>">Lista</a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li><a href="<?= base_url("backend/logout"); ?>"><i class="glyphicon glyphicon-lock"></i> Kilépés</a></li>
			</ul>
		</div>
		<!-- /container -->
	</div>

	<div id="content">