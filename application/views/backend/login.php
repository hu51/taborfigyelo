<html>
    <head>
        <meta charset="UTF-8" />
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/style.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/theme.css">

        <script type="text/javascript" src="<?= base_url(); ?>js/jquery.min.js"></script>
		<script type="text/javascript" src="<?= base_url(); ?>js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/main.js"></script>
    </head>
	
    <body>	
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Belépés</h3>
                </div>
                <div class="panel-body">
                    <form method="post" action="<?= site_url("backend/login"); ?>" class="form-horizontal" role="form">
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">E-mail:</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" name="email" placeholder="E-mail" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pwd" class="col-sm-2 control-label">Jelszó:</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" name="password" placeholder="Jelszó megadása" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pwd" class="col-sm-2 control-label"></label>
                            <div class="col-sm-10">
                                <button type="submit" name="loginEmail" value="email" class="btn btn-primary">Belépés</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </body>
    </html>
