<?php

if ($this->input->post("addnew")) {
    if (empty($_FILES["new_image"])) {
        message("", "Nincs fájl kiválasztva!", "warning");
    } else {
        $typeID  = $this->input->post("new_typeID", true);
        $title   = $this->input->post("new_title", true);
        $url     = $this->input->post("new_url", true);
        $from    = $this->input->post("new_from", true);
        $end     = $this->input->post("new_end", true);
        $enabled = $this->input->post("new_enabled", true);
        $amount  = $this->input->post("new_amount", true);

        $data = array(
            "title"    => filter_var($title, FILTER_SANITIZE_STRING),
            "url"      => filter_var($url, FILTER_SANITIZE_URL),
            "fromDate" => (substr($from, 0, 1) == "0") ? null : $from,
            "endDate"  => (substr($end, 0, 1) == "0") ? null : $end,
            "typeID"   => (int) $typeID,
            "enabled"  => (int) $enabled,
            "maxHits"  => ($amount == "0") ? null : (int) $amount,
        );
        $this->db->insert("tm_banners", $data);
        $newID = $this->db->insert_id();

        $btype = $this->db->from("tm_banner_types")->where("id", $typeID)->get()->row();
        $img   = getimagesize($_FILES["new_image"]["tmp_name"]);

        if ($img["mime"] !== "image/png" && $img["mime"] !== "image/jpeg") {
            $this->db->delete("tm_banners", array("id" => $newID));
            message("", "JPG vagy PNG képet lehet feltölteni! " . $img["mime"], "warning");
            unlink($_FILES["new_image"]["tmp_name"]);
        } elseif (!is_null($btype->height) && (int) $btype->height !== $img[1]) {
            $this->db->delete("tm_banners", array("id" => $newID));
            message("", "A képnek pont {$btype->height} képpont magasnak kell lennie! " . $img[1], "warning");
            unlink($_FILES["new_image"]["tmp_name"]);
        } elseif (!is_null($btype->width) && (int) $btype->width !== $img[0]) {
            $this->db->delete("tm_banners", array("id" => $newID));
            message("", "A képnek pont {$btype->width} képpont szélesnek kell lennie! " . $img[0], "warning");
            unlink($_FILES["new_image"]["tmp_name"]);
        } else {
            $newFile = "bnr_" . sprintf("%04d", $newID) . "." . str_replace("image/", "", $img["mime"]);
            if (!move_uploaded_file($_FILES["new_image"]["tmp_name"], FCPATH . "images/banners/" . $newFile)) {
                $this->db->delete("tm_banners", array("id" => $newID));
                message("", "Nem iskerült a fájlt feltölteni.", "warning");
                unlink($_FILES["new_image"]["tmp_name"]);
            } else {
                $this->db->update("tm_banners", array("image" => $newFile), array("id" => $newID));
                message("", "A banner felrögzítve", "success");
            }
        }
    }
}

if ($this->input->post("update")) {
    $id = $this->input->post("update");

    $title   = $this->input->post("title", true);
    $url     = $this->input->post("url", true);
    $from    = $this->input->post("from", true);
    $end     = $this->input->post("end", true);
    $enabled = $this->input->post("enabled", true);
    $amount  = $this->input->post("amount", true);
    $data    = array(
        "title"    => filter_var($title[$id], FILTER_SANITIZE_STRING),
        "url"      => filter_var($url[$id], FILTER_SANITIZE_URL),
        "fromDate" => (substr($from[$id], 0, 1) == "0") ? null : $from[$id],
        "endDate"  => (substr($end[$id], 0, 1) == "0") ? null : $end[$id],
        "enabled"  => (int) $enabled[$id],
        "maxHits"  => ($amount[$id] == "0") ? null : (int) $amount[$id],
    );
    $this->db->update("tm_banners", $data, array("id" => $id));
    message("", "Banner adatai módosítva", "success");
}

if ($this->input->post("delete")) {
	$id = $this->input->post("delete");
	
	$data = $this->db->from("tm_banners")->where("id", $id)->get()->row();

	unlink(FCPATH . "images/banners/" . $data->image);
	$this->db->delete("tm_banners", array("id" => $id));
    message("", "Banner sikeresen törölve", "success");
}

$res   = $this->db->from("tm_banner_types")->order_by("title", "ASC")->get()->result();
$types = array();
foreach ($res as $data) {
    $types[$data->id] = $data->title." (".$data->width."x".$data->height.")";
}

$all = $this->db->from("tm_banners")->order_by("enabled", "DESC")->order_by("title", "ASC")->get()->result();

?>
<div class="panel panel-default">
	<div class="panel-body">
		<h4>Tárborminősítő Bannerek:</h4>
		<form method="post" enctype="multipart/form-data">
			<table class="table table-bordered table-condensed">
				<thead>
					<tr>
						<th>Típus</th>
						<th>Név</th>
						<th>Hivatkozás</th>
						<th>Tól</th>
						<th>Ig.</th>
						<th>max megj.</th>
						<th>Megj.</th>
						<th>Engedélyezeve</th>
						<th>&nbsp;</th>
					</tr>
					</head>
					<?php foreach ($all as $data): ?>
						<tr>
							<td>
								<?=$types[$data->typeID];?>
							</td>
							<td>
								<input type="text" class="form-control" name="title[<?=$data->id;?>]" placeholder=" neve" value="<?=$data->title;?>">
							</td>
							<td>
								<input type="text" class="form-control" name="url[<?=$data->id;?>]" placeholder="kupon neve" value="<?=$data->url;?>">
							</td>
							<td class="w100">
								<input type="date" class="form-control" name="from[<?=$data->id;?>]" value="<?=$data->fromDate;?>">
							</td>
							<td class="w100">
								<input type="date" class="form-control" name="end[<?=$data->id;?>]" value="<?=$data->endDate;?>">
							</td>
							<td class="w100">
								<input type="number" class="form-control" name="amount[<?=$data->id;?>]" value="<?=$data->maxHits;?>">
							</td>
							<td class="w100">
								<?=$data->hits;?>
							</td>
							<td>
								<?=$this->tools->bs_checkbox("enabled[" . $data->id . "]", $data->enabled);?>
								<a href='<?="https://taborminosito.hu/images/banners/" . $data->image;?>' target='_blank'><i class="glyphicon glyphicon-picture"></i></a>
							</td>
							<td>
								<button class="btn btn-primary" type="submit" name="update" value="<?=$data->id;?>" aria-label="">Módosít</button>
								<button class="btn btn-danger delete" type="submit" name="delete" value="<?=$data->id;?>" aria-label="">Töröl</button>
							</td>
						</tr>
					<?php endforeach;?>


					<tr class="success">
						<th>Típus</th>
						<th>Név</th>
						<th>Hivatkozás</th>
						<th>Tól</th>
						<th>Ig.</th>
						<th>max megjelenés</th>
						<th colspan="3">&nbsp;</th>
					</tr>
					<tr>
						<td>
							<?=$this->tools->bs_select("new_typeID", 0, $types);?>
						</td>
						<td>
							<input type="text" class="form-control" name="new_title" value="">
						</td>
						<td>
							<input type="text" class="form-control" name="new_url" value="">
						</td>
						<td class="w100">
							<input type="date" class="form-control" name="new_from" value="">
						</td>
						<td class="w100">
							<input type="date" class="form-control" name="new_end" value="">
						</td>
						<td>
							<input type="number" class="form-control" name="new_maxhits" value="0">
						</td>
						<td colspan="2">
							<input type="file" name="new_image" />
						</td>
						<td >
							<button class="btn btn-success" type="submit" name="addnew" value="1" aria-label="">Új hozzáadása</button>
						</td>
					</tr>
			</table>
		</form>
	</div>
</div>
<script>

$(".delete").click( function(e) {
	if (confirm("Biztos törlöd?")) {
		return true;
	} else {
		return false;
	}
});

</script>