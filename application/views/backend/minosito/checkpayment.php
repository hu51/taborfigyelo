<?php

	$all = $this->db->from("tm_contracts")->where("paymentEmailSent is not NULL")->where("contractPaid is NULL")->order_by("id", "DESC")->get()->result();
	
?>
<div class="panel panel-default">
	<div class="panel-body">
		<h4>Befizetés beérkezésére váró szerződések:</h4>
		<form method="post" enctype="multipart/form-data">
		<table class="table table-bordered table-condensed">
			<thead>
			<tr>
				<th class="w150">&nbsp;</th>
				<th>Partner</th>
				<th>Létrehozva</th>
				<th>Szerződés kiküldve</th>
				<th>Fizetési levél kiküldve</th>
				<th></th>
			</tr>	
			</head>
		<?php foreach ($all as $data): ?>
			<tr>
				<td>
					<?= anchor("szerkeszt/".md5($data->id."#".$data->uniqID)."/szerzodes", "Szerződés", " class='btn btn-sm btn-primary' target='_blank'"); ?>
				</td>
				<td><?= $data->partnerName; ?></td>
				<td>
					<?= (is_Null($data->createdTime))?"<i class='glyphicon glyphicon-remove text-warning'></i>":"<i class='glyphicon glyphicon-ok text-success'></i> ".date("m.d. H:i", strtotime($data->createdTime)); ?>
				</td>
				<td>
					<?= (is_Null($data->contractSent))?"<i class='glyphicon glyphicon-remove text-warning'></i>":"<i class='glyphicon glyphicon-ok text-success'></i> ".date("m.d. H:i", strtotime($data->contractSent)); ?>
				</td>
				<td>
					<?= (is_Null($data->paymentEmailSent))?"<i class='glyphicon glyphicon-remove text-warning'></i>":"<i class='glyphicon glyphicon-ok text-success'></i> ".date("m.d. H:i", strtotime($data->paymentEmailSent)); ?>
				</td>
				<td>					
						<button type="submit" class="btn btn-sm btn-success" name="checkPayment" value="<?= $data->id; ?>"> Beérkezett a befizetés</button>
						<input type="checkbox" name="confPayment" value="<?= $data->id; ?>"> megerősítés, levél kiküldése					
				</td>

			</tr>
		<?php endforeach; ?>
		</table>
	</div>
</div>
