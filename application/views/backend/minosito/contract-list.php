<?php

if ($this->uri->segment(4) == "closed") {
	$all = $this->db->from("tm_contracts")->where("closed", 1)->order_by("id", "DESC")->get()->result();
} else {
	$all = $this->db->from("tm_contracts")->where("closed", 0)->order_by("id", "DESC")->get()->result();
}

?>
<div class="panel panel-default">
	<div class="panel-body">
		<h4>Szerződések:</h4>
		<table class="table table-bordered table-condensed">
			<thead>
				<tr>
					<th>&nbsp;</th>
					<th>Partner</th>
					<th>Létrehozva</th>
					<th>Reg.Email.</th>
					<th>Alapdatok</th>
					<th>Szerződés</th>
					<th>Fizetési levél</th>
					<th>Kifizetve</th>
					<th>Hely. szerkesztő levél</th>
					<th>Helyszínek lezárva.</th>
					<th>Adatok publikálva</th>
				</tr>
				</head>
				<?php foreach ($all as $data): ?>
					<tr class="<?= ($data->visible)?"success":""; ?>">
						<td class="ac">
							<?= anchor("minosito/main/editcontract/" .$data->id , "<i class='glyphicon glyphicon-edit '></i>", " class='fr' target='_blank'"); ?>
						</td>
						<td>
							<?= (empty($data->partnerName))?"@".$data->representName:$data->partnerName; ?>
							<?= anchor("szerkeszt/" . md5($data->id . "#" . $data->uniqID), "<i class='glyphicon glyphicon-th-list '></i>", " class='fr' target='_blank'"); ?>
						</td>
						<td><?= (is_Null($data->createdTime)) ? "<i class='glyphicon glyphicon-remove text-warning'></i>" : "<i class='glyphicon glyphicon-ok text-success'></i> " . date("m.d. H:i", strtotime($data->createdTime)); ?></td>
						<td><?= (is_Null($data->regEmailSent)) ? "<i class='glyphicon glyphicon-remove text-warning'></i>" : "<i class='glyphicon glyphicon-ok text-success'></i> " . date("m.d. H:i", strtotime($data->regEmailSent)); ?></td>
						<td><?= (is_Null($data->contractSaved)) ? "<i class='glyphicon glyphicon-remove text-warning'></i>" : "<i class='glyphicon glyphicon-ok text-success'></i> " . date("m.d. H:i", strtotime($data->contractSaved)); ?></td>
						<td>
							<?= (is_Null($data->contractSent)) ? "<i class='glyphicon glyphicon-remove text-warning'></i>" : "<i class='glyphicon glyphicon-ok text-success'></i> " . date("m.d. H:i", strtotime($data->contractSent)); ?>

							<?php if (!is_Null($data->contractSaved)) : ?>
								<?= anchor("https://taborminosito.hu/szerkeszt/" . md5($data->id . "#" . $data->uniqID) . "/szerzodes", "<i class='glyphicon glyphicon-envelope'></i>", " target='_blank' class='fr' title='Szerződés PDF' "); ?>
							<?php endif; ?>
						</td>
						<td><?= (is_Null($data->paymentEmailSent)) ? "<i class='glyphicon glyphicon-remove text-warning'></i>" : "<i class='glyphicon glyphicon-ok text-success'></i> " . date("m.d. H:i", strtotime($data->paymentEmailSent)); ?></td>
						<td><?= (is_Null($data->contractPaid)) ? "<i class='glyphicon glyphicon-remove text-warning'></i>" : "<i class='glyphicon glyphicon-ok text-success'></i> " . date("m.d. H:i", strtotime($data->contractPaid)); ?></td>
						<td><?= (is_Null($data->campEditSent)) ? "<i class='glyphicon glyphicon-remove text-warning'></i>" : "<i class='glyphicon glyphicon-ok text-success'></i> " . date("m.d. H:i", strtotime($data->campEditSent)); ?></td>
						<td>
							<?= (is_Null($data->campEditDone)) ? "<i class='glyphicon glyphicon-remove text-warning'></i>" : "<i class='glyphicon glyphicon-ok text-success'></i> " . date("m.d. H:i", strtotime($data->campEditDone)); ?>

							<?php if (!is_Null($data->campEditDone)) : ?>
								<?= anchor("minosito/main/locationtest/" . $data->id, "<i class='glyphicon glyphicon-eye-open'></i>", " target='_blank' class='fr' title='Helyszínek' "); ?>
							<?php endif; ?>
						</td>
						<td>
							<?= (is_Null($data->publicationTime)) ? "<i class='glyphicon glyphicon-remove text-warning'></i>" : "<i class='glyphicon glyphicon-ok text-success'></i> " . date("m.d. H:i", strtotime($data->publicationTime)); ?>
							<?= ($data->visible) ? "<i class='glyphicon glyphicon-eye-open text-success fr'></i>" : ""; ?>
						</td>
					</tr>
				<?php endforeach; ?>
		</table>
	</div>
</div>