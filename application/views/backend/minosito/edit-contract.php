<?php

$contractID = $this->uri->segment(4);
$contract   = $this->db->from("tm_contracts")->where("id", $contractID)->get()->row();

if (!$contract) {
	echo message("Hiba", "Hibás azonosító!", "warning");
	return;
}


if ($this->input->post("deleteData")) {
    if ($this->input->post("confDel") == $contract->uniqID) {

        $this->db->delete("tm_con_loc_answers", array("contractID" => $contractID));
        $this->db->delete("tm_contracts_locations", array("contractID" => $contractID));        
        $this->db->delete("tm_contracts", array("id" => $contractID));

        message("", "A bejegyzést és a hozzá tartozó adatokat a rendszer törölte!", "info");
        return;
    } else {
        message("", "Hibás a törlésnél megadott azonsító!", "warning");
    }

}

if ($this->input->post("resetLocEdit") && $this->input->post("resetLocEditConf") == "1") {
	$data = array(
		"campEditDone"  => NULL,
		"publicationTime"  => NULL,
		"closed"  => 0
	);
	$this->db->update("tm_contracts", $data, array("id" => $contractID));

	$data = array(
		"closed"  => 0
	);
	$this->db->update("tm_contracts_locations", $data, array("contractID" => $contractID));
	message("", "Állapot visszaállítva!", "info");
	$contract = $this->db->from("tm_contracts")->where("id", $contractID)->get()->row();	
}

if ($this->input->post("updateData")) {
	if ($this->input->post("confModify") == "1") {
		$data = array(
			"partnerName"   => $this->input->post("partnerName", true),
			"zip"           => (!empty((int) $this->input->post("zip", true)))?(int) $this->input->post("zip", true):"",
			"city"          => $this->input->post("city", true),
			"address"       => $this->input->post("address", true),
			"taxID"         => $this->input->post("taxID", true),
			"compID"        => $this->input->post("compID", true),
			"representName" => $this->input->post("representName", true),
			"contactName"   => $this->input->post("contactName", true),
			"contactEmail"  => $this->input->post("contactEmail", true),
			"contactPhone"  => $this->input->post("contactPhone", true),
			"campPrice"     => (int)$this->input->post("campPrice", true),
			"lastModify"    => date("Y-m-d H:i:s"),
		);
		$this->db->update("tm_contracts", $data, array("id" => $contractID));
		message("", "Szerződés adatok módosítva!", "success");
        
        $campThemes = $this->input->post("campTheme");
        foreach ($campThemes as $id => $theme) {
            $this->db->update("tm_contracts_locations", array("themesReal" => $theme), array("id" => $id));
        }

    }

	$data = array(
		"visible" => (int) $this->input->post("visible"),
	);
	$this->db->update("tm_contracts", $data, array("id" => $contract->id));
	message("", "Szerződés láthatósága módosítva!", "success");
	$contract = $this->db->from("tm_contracts")->where("id", $contractID)->get()->row();
}

$locDB   = $this->db->from("tm_contracts_locations")->where("contractID", $contract->id)->get()->num_rows();
$themeDB = (int) $this->db->select("sum(themes) as db ")->from("tm_contracts_locations")->where("contractID", $contract->id)->get()->row()->db;
$camps = $this->db->from("tm_contracts_locations")->where("contractID", $contractID)->get()->result();

?>
<div class="row">
	<div class="panel panel-default">
		<div class="panel-body">
			<form method="post" action="" class="">
				<h2>Szerződés alapadatok:</h2>
				<br>
				<table class="table">
					<tbody>
						<tr>
							<td style="width: 200px;">Egyedi azonosító:</td>
							<td><pre class="w200"><?= $contract->uniqID; ?></pre></td>
							<td class="active"></td>							
							<td class="active"></td>							
						</tr>
						<tr>
							<td style="width: 200px;">Szerződő partner neve:</td>
							<td><input type="text" class="form-control w300 " name="partnerName" value="<?= $contract->partnerName; ?>"></td>
							<td class="active">Létrehozva</td>							
							<td class="active"><?= (is_Null($contract->createdTime)) ? "<i class='glyphicon glyphicon-remove text-warning'></i>" : "<i class='glyphicon glyphicon-ok text-success'></i> " . date("Y.m.d. H:i", strtotime($contract->createdTime)); ?></td>		
						</tr>
						<tr>
							<td style="width: 200px;">Székhely irányítószám:</td>
							<td><input type="text" class="form-control w300 " name="zip" value="<?= $contract->zip; ?>"></td>
							<td class="active">Regisztrációs email kiküldve</td>							
							<td class="active"><?= (is_Null($contract->regEmailSent)) ? "<i class='glyphicon glyphicon-remove text-warning'></i>" : "<i class='glyphicon glyphicon-ok text-success'></i> " . date("Y.m.d. H:i", strtotime($contract->regEmailSent)); ?></td>		
						</tr>
						<tr>
							<td style="width: 200px;">Székhely város:</td>
							<td><input type="text" class="form-control w300 " name="city" value="<?= $contract->city; ?>"></td>
							<td class="active">Szerződés adatok mentve</td>							
							<td class="active"><?= (is_Null($contract->contractSaved)) ? "<i class='glyphicon glyphicon-remove text-warning'></i>" : "<i class='glyphicon glyphicon-ok text-success'></i> " . date("Y.m.d. H:i", strtotime($contract->contractSaved)); ?></td>		
						</tr>
						<tr>
							<td style="width: 200px;">Székhely utca, házszám:</td>
							<td><input type="text" class="form-control w300 " name="address" value="<?= $contract->address; ?>"></td>
							<td class="active">Szerződés adatok elküldve</td>							
							<td class="active"><?= (is_Null($contract->contractSent)) ? "<i class='glyphicon glyphicon-remove text-warning'></i>" : "<i class='glyphicon glyphicon-ok text-success'></i> " . date("Y.m.d. H:i", strtotime($contract->contractSent)); ?></td>		
						</tr>
						<tr>
							<td style="width: 200px;">Adószám:</td>
							<td><input type="text" class="form-control w300 " name="taxID" value="<?= $contract->taxID; ?>"></td>
							<td class="active">Fizetési tájékozató kiküldve</td>							
							<td class="active"><?= (is_Null($contract->paymentEmailSent)) ? "<i class='glyphicon glyphicon-remove text-warning'></i>" : "<i class='glyphicon glyphicon-ok text-success'></i> " . date("Y.m.d. H:i", strtotime($contract->paymentEmailSent)); ?></td>		
						</tr>
						<tr>
							<td style="width: 200px;">Cégjegyzékszám:</td>
							<td><input type="text" class="form-control w300 " name="compID" value="<?= $contract->compID; ?>"></td>
							<td class="active">Minősítési díj befizetve</td>							
							<td class="active"><?= (is_Null($contract->contractPaid)) ? "<i class='glyphicon glyphicon-remove text-warning'></i>" : "<i class='glyphicon glyphicon-ok text-success'></i> " . date("Y.m.d. H:i", strtotime($contract->contractPaid)); ?></td>		
						</tr>
						<tr>
							<td style="width: 200px;">Képviselő neve:</td>
							<td><input type="text" class="form-control w300 " name="representName" value="<?= $contract->representName; ?>"></td>
							<td class="active">Táborhelyszín szerkesztő levél kiküldvve</td>							
							<td class="active"><?= (is_Null($contract->campEditSent)) ? "<i class='glyphicon glyphicon-remove text-warning'></i>" : "<i class='glyphicon glyphicon-ok text-success'></i> " . date("Y.m.d. H:i", strtotime($contract->campEditSent)); ?></td>		
						</tr>
						<tr>
							<td style="width: 200px;">Kapcsolattartó:</td>
							<td><input type="text" class="form-control w300" name="contactName" value="<?= $contract->contactName; ?>"></td>
							<td class="active">Táborhelyszínek mentve</td>							
							<td class="active"><?= (is_Null($contract->campEditDone)) ? "<i class='glyphicon glyphicon-remove text-warning'></i>" : "<i class='glyphicon glyphicon-ok text-success'></i> " . date("Y.m.d. H:i", strtotime($contract->campEditDone)); ?></td>								
						</tr>
						<tr>
							<td style="width: 200px;">Email:</td>
							<td><input type="text" class="form-control w300 " name="contactEmail" value="<?= $contract->contactEmail; ?>"></td>
							<td class="active">Publikálás az oldalon</td>							
							<td class="active">
								<?= (is_Null($contract->publicationTime)) ? "<i class='glyphicon glyphicon-remove text-warning'></i>" : "<i class='glyphicon glyphicon-ok text-success'></i> " . date("Y.m.d. H:i", strtotime($contract->publicationTime)); ?>
							</td>								
						</tr>
						<tr>
							<td style="width: 200px;">Telefon:</td>
							<td><input type="text" class="form-control w300 " name="contactPhone" value="<?= $contract->contactPhone; ?>"></td>
							<?php if(!is_Null($contract->publicationTime)): ?>
								<td class="info">Táborhelyszín szerkesztési állapot visszaállítása</td>							
								<td class="info">
									<button type="success" class="btn btn-success" name="resetLocEdit" value="1">Visszaállítás</button>
									<input type="checkbox" name="resetLocEditConf" value="1" /> megerősítés
								</td>							
							<?php else: ?>
								<td class="active">&nbsp;</td>							
								<td class="active">&nbsp;</td>													
							<?php endif; ?>
						</tr>

						<tr class="warning">
							<td style="width: 200px;">Minősítési költség:</td>
							<td><input type="text" class="form-control w200 " name="campPrice" value="<?= $contract->campPrice; ?>"></td>
							<td>Táborhelyek száma</td>							
							<td><code class="w100 ac"><?= $contract->campLocations; ?></code></td>
						</tr>
						<tr class="warning">
							<td style="width: 200px;">Kupon:</td>
							<td><code class="w200 ac"> <?= $contract->cupon; ?></code></td>
							<td>Tábortémák száma</td>							
							<td>
                                <?php
                                    foreach ($camps as $camp): ?>
                                    <div class="col-md-12">
                                        <div class="col-md-7">
                                            <a href="<?= site_url("backend/main/editlocation/".$camp->id); ?>"><button type="button" class="btn btn-sm btn-primary">Szerkeszt</button></a>
                                            <?= $camp->title; ?>
                                        </div>
                                        <div class="col-md-3">                                        
                                            <div class="input-group">
                                                <input type="text" class="form-control w50 " name="campTheme[<?= $camp->id; ?>]" value="<?= $camp->themesReal; ?>">
                                                <div class="input-group-addon">(eredeti: <?= $camp->themes; ?>)</div>
                                            </div>                                        
                                        </div>
                                        <div class="col-md-2"><pre>Kód: <?= $this->tools->ID2Text(array($contract->id, $camp->id)); ?></pre></div>
                                    </div>
                                    <?php endforeach; ?>
                            </td>
						</tr>

						<tr class="info">
							<td style="width: 200px;">Megerősítés:</td>
							<td colspan="3">
								<input type="checkbox" name="confModify" value="1"> Szerződés adatok módosításának megerősítése
							</td>
						</tr>
						<tr class="active">
							<td style="width: 200px;">Láthatóság:</td>
							<td colspan="3">
								<?= $this->tools->bs_checkbox("visible", $contract->visible); ?>
							</td>
						</tr>
						<tr class="active">
							<td style="width: 200px;">&nbsp;</td>
							<td >
								<button type="submit" name="updateData" value="ok" class="btn btn-primary">Adatok módosítása</button>

							</td>
							<td class="danger" style="width: 200px;">TÖRLÉS</td>
							<td class="danger">
                                <div class="col-md-12">
                                    <div class="col-md-6">                         
                                        Írd be a fenti <b>egyedi azonosítót</b> a törléshez
                                    </div>
                                    <div class="col-md-3">   
                                        <input type="text" class="form-control " name="confDel" placeholder="kód..">                      
                                    </div>
                                    <div class="col-md-3">
                                        <button type="submit" name="deleteData" value="ok" class="btn btn-danger">Bejegyzés törlése</button>
                                    </div>
                                </div>  
							</td>
                        </tr>
                        
                        <tr class="info">
                            <td colspan="4">Letöltések</td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <a href="<?= "https://taborminosito.hu/backend/main/attachement?type=contract&id=".$contractID; ?>" class="btn btn-sm" target="_blank">Tábor szerződés <i class="glyphicon glyphicon-download"></i></a><br>
                                <?php foreach ($camps as $camp): ?>
                                    <?= $camp->title; ?>:<br>
                                    <a href="<?= "https://taborminosito.hu/backend/main/attachement?type=logo&id=".$camp->id; ?>" class="btn btn-sm" target="_blank">Táborhely logo <i class="glyphicon glyphicon-download"></i></a>, 
                                    <a href="<?= "https://taborminosito.hu/backend/main/attachement?type=banner&id=".$camp->id; ?>" class="btn btn-sm" target="_blank">Táborhely banner <i class="glyphicon glyphicon-download"></i></a>, 
                                    <a href="<?= "https://taborminosito.hu/backend/main/attachement?type=diplome&id=".$camp->id; ?>" class="btn btn-sm" target="_blank">Táborhely diploma <i class="glyphicon glyphicon-download"></i></a>
                                    <br>
                                <?php endforeach; ?>
                            </td>
                        </tr>                        
					</tbody>
            </form>
		</div>
	</div>
</div>