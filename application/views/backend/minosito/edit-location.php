<?php

$id = $this->uri->segment(4);

if ($this->input->post("update")) {
    $data = array(
        "title"      => filter_input(INPUT_POST, "title", FILTER_SANITIZE_STRING),
        "zip"        => filter_input(INPUT_POST, "zip", FILTER_SANITIZE_NUMBER_INT),
        "county"     => filter_input(INPUT_POST, "county", FILTER_SANITIZE_STRING),
        "city"       => filter_input(INPUT_POST, "city", FILTER_SANITIZE_STRING),
        "address"    => filter_input(INPUT_POST, "address", FILTER_SANITIZE_STRING),
        "website"    => filter_input(INPUT_POST, "website", FILTER_SANITIZE_URL),
        "facebook"   => filter_input(INPUT_POST, "facebook", FILTER_SANITIZE_URL),
        "lastModify" => date("Y-m-d H:i:s"),
        "themesReal" => (int) $this->input->post("themesReal"),
        "point"      => (float) $this->input->post("point"),
        "level"      => (int) $this->input->post("level"),
    );
    $this->db->update("tm_contracts_locations", $data, array("id" => $id));
    message("", "Táborhelyszín adatok módosítva", "success");
}
$data = $this->db->from("tm_contracts_locations")->where("id", $id)->get()->row();

$counties = $this->db->select("county")->from("cities")->where("county is not null")->order_by("county")->group_by("county")->get()->result();


?>
<div class="panel panel-default">
	<div class="panel-body">
		<h4>Tábor helyszín módosítása:</h4>
		<form method="post">
			<table class="table table-bordered table-condensed">
				<thead>
					<tr>
						<th>Név</th>
						<td>
							<input type="text" class="form-control" name="title" value="<?=$data->title;?>">
						</td>
					</tr>
					<tr>
						<th>Irányítószám</th>
						<td>
							<input type="text" class="form-control w100" name="zip" maxlength="6" value="<?=$data->zip;?>">
						</td>
					</tr>
					<tr>
						<th>Megye</th>
						<td>
							<input type="text" class="form-control w300 fl" id="county" name="county" value="<?=$data->county;?>">
                            <select id='countyList' class="form-control w300 info">
                                <option disabled selected># választás listából</option>
                                <?php foreach($counties as $tmp): ?>
                                    <option><?= $tmp->county; ?></option>
                                <?php endforeach; ?>
                            </select>
						</td>
					</tr>
					<tr>
						<th>Város</th>
						<td>
							<input type="text" class="form-control" name="city" value="<?=$data->city;?>">
						</td>
					</tr>
					<tr>
						<th>Utca</th>
						<td>
							<input type="text" class="form-control" name="address" value="<?=$data->address;?>">
						</td>
					</tr>
					<tr>
						<th>Weboldal</th>
						<td>
							<input type="text" class="form-control" name="website" value="<?=$data->website;?>">
						</td>
					</tr>
					<tr>
						<th>Facebook</th>
						<td>
							<input type="text" class="form-control" name="facebook" value="<?=$data->facebook;?>">
						</td>
					</tr>

					<tr>
						<th>Témák száma</th>
						<td>
                            <input type="text" class="form-control w100" name="themesReal" value="<?=$data->themesReal;?>">
                            (megadott: <?=$data->themes;?>)
						</td>
					</tr>
					<tr class="warning">
						<th>Sátorszám </th>
						<td>
                            <input type="text" class="form-control w100" name="level" value="<?=$data->level;?>">
						</td>
					</tr>
					<tr class="warning">
						<th>Kapott pontok</th>
						<td>
                            <input type="text" class="form-control w100" name="point" value="<?=$data->point;?>">
						</td>
					</tr>
                    <tr>
						<th>&nbsp;</th>
						<td>
							<button class="btn btn-primary" type="submit" name="update" value="ok">Módosít</button>
						</td>
					</tr>
			</table>
		</form>
	</div>
</div>
<script>

    $("#countyList").click(function() {
        $("#county").val($("#countyList option:selected").text());
    });
</script>