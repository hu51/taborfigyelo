<?php

if ($this->input->post("saveData")) {
    $questionID = $this->uri->segment(4);

    $data = array(
        "title"    => $this->input->post("title", true),
        "reqOptID" => $this->input->post("reqOptID", true),
        "groupID"  => (int) $this->input->post("groupID", true),
        "multiple"  => (int) $this->input->post("multiple", true),
    );

    if ($this->uri->segment(4) == "new") {
        $this->db->insert("tm_question", $data);
        $questionID = $this->db->insert_id();
    } else {
        $this->db->update("tm_question", $data, array("id" => $questionID));
    }

    $opt_title = $this->input->post("opt_title");
    $opt_point = $this->input->post("opt_point");
    $opt_order = $this->input->post("opt_order");
    $opt_visible = $this->input->post("opt_visible");
    foreach ($opt_title as $id => $title) {
        $opt_data = array(
            "title"    => filter_var($title, FILTER_SANITIZE_STRING),
            "point"  => (int) $opt_point[$id],          
            "orderID"  => (int) $opt_order[$id],          
            "visible"  => (int) $opt_visible[$id]    
        );

        if (!empty ($title)) {     
            if ($id !== "new") {
                $this->db->update("tm_question_options", $opt_data, array("id" => $id));
            } else {
                $opt_data["questionID"] = $questionID;
                $this->db->insert("tm_question_options", $opt_data);
            }
        }
    }
    if ($this->uri->segment(4) == "new") {
        redirect("minosito/main/editquestion/".$questionID);
    }    
    message("", "Kérdés és a válaszok, módosítva.", "success");
}

$data = $this->db->from('tm_question')->where('id', $this->uri->segment(4))->get()->row();
?>

<div class="panel panel-default">
    <div class="panel-body">
        <?php if ($this->uri->segment(4) == "new"): ?>
            <legend>Kérdés hozzáadása</legend>
        <?php else: ?>
            <legend>Kérdés szerkesztése</legend>
        <?php endif; ?>
        <form method="post" class="form-horizontal">
            <div class="form-group">
                <label class="col-lg-2 col-sm-12">Csoport:</label>
                <div class="col-lg-10 col-sm-12">
                    <select name="groupID" class="form-control">
                        <?php
                        $groups = $this->db->from('tm_question_group')->order_by('orderID')->get()->result();
                        foreach ($groups as $tmp) : ?>
                            <option value="<?= $tmp->id; ?>" <?= ($tmp->id == $data->groupID) ? " selected" : ""; ?>><?= $tmp->title; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 col-sm-12">Kérdés:</label>
                <div class="col-lg-10 col-sm-12">
                    <textarea name="title" class="form-control" placeholder="Adja meg a kérdés címét"><?= $data->title; ?></textarea>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 col-sm-12">Válasz ID-k (#...):</label>
                <div class="col-lg-5 col-sm-12">
                    <input type="text" name="reqOptID" class="form-control w200 fl" placeholder="" value="<?= $data->reqOptID; ?>">
                    <div class="m10 fl">Melyik válasz(ok) kiválasztása után jelenjen meg a kérdés. Vesszővel elválasztva.</div>
                </div>
                <div class="col-lg-5 col-sm-12">
                    <input type="text" id="searchOption" class="form-control w200 fl" placeholder="Válasz keresés">
                    <br style="clear: both;" />
                    <div id="optionList">Minimum 3 karakter!</div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 col-sm-12">Több válasz bejelölhető:</label>
                <div class="col-lg-10 col-sm-12">
                    <input type="radio" name="multiple" value="1" <?= ($data->multiple) ? "checked" : ""; ?>> Igen,
                    <input type="radio" name="multiple" value="0" <?= (!$data->multiple) ? "checked" : ""; ?>> Nem
                </div>
            </div>
            <br style="clear: both;">

            <div class="form-group">
                <label class="col-lg-2 col-sm-12">Válaszok:</label>
                <div class="col-lg-10 col-sm-12">
                    <table class="table" style="max-width: 800px;">
                        <thead>
                            <tr>
                                <td>&nbsp;</td>
                                <td>Válasz</td>
                                <td>Pont</td>
                                <td>Sorrend</td>
                                <td>Látható</td>
                            </tr>
                        </thead>
                        <?php

                        $options = $this->db->from('tm_question_options')->where('questionID', $data->id)->order_by("orderID")->get()->result();
                        $maxOrder = 0;
                        foreach ($options as $opt) : ?>
                            <tr>
                                <td>
                                    #<?= $opt->id; ?>
                                </td>
                                <td>
                                    <textarea name="opt_title[<?= $opt->id; ?>]" class="form-control" placeholder=""><?= $opt->title; ?></textarea>
                                </td>
                                <td>
                                    <input type="text" name="opt_point[<?= $opt->id; ?>]" class="form-control w50 ac" placeholder="" value="<?= $opt->point; ?>">
                                </td>
                                <td>
                                    <input type="text" name="opt_order[<?= $opt->id; ?>]" class="form-control w50 ac" placeholder="" value="<?= $opt->orderID; ?>">
                                </td>
                                <td>
                                    <input type="radio" name="opt_visible[<?= $opt->id; ?>]" value="1" <?= ($opt->visible) ? "checked" : ""; ?>> Igen,
                                    <input type="radio" name="opt_visible[<?= $opt->id; ?>]" value="0" <?= (!$opt->visible) ? "checked" : ""; ?>> Nem
                                </td>
                            </tr>
                            <?php

                            $maxOrder = $opt->orderID +1;
                            $tmps = $this->db->from('tm_question')->like("CONCAT(',', reqOptID, ',')", "," . $opt->id . ",")->get()->result();
                            if ($tmps) : ?>
                                <tr class="active">
                                    <td colspan="5">
                                        <small>
                                            Az alábbi kérdések vannak a válaszhoz kapcsolva:<br>
                                            <?php
                                            foreach ($tmps as $tmp) {
                                                echo anchor("minosito/main/editquestion/" . $tmp->id, " &raquo; " . $tmp->title, " target='_blank'") . "<br>\n";
                                            }
                                            ?></small>
                                    </td>
                                </tr>
                            <?php endif; ?>
                        <?php endforeach; ?>

                        <tr>
                            <td colspan="4" class="ac">
                                <button type="submit" class="btn btn-sm btn-primary" name="saveData" value="ok">Módosítások mentése</button>
                            </td>
                        </tr>

                        <tr class="success">
                            <td colspan="4">Új válasz hozzáadása</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <textarea name="opt_title[new]" class="form-control" placeholder=""></textarea>
                            </td>
                            <td class="w50">
                                <input type="text" name="opt_point[new]" class="form-control w50 ac" placeholder="" value="0">
                            </td>
                            <td class="w50">
                                <input type="text" name="opt_order[new]" class="form-control w50 ac" placeholder="" value="<?= $maxOrder; ?>">
                            </td>
                            <td class="w150">
                                <input type="radio" name="opt_visible[new]" value="1" checked> Igen,
                                <input type="radio" name="opt_visible[new]" value="0" > Nem
                            </td>
                        </tr>
                    </table>
                </div>
        </form>
    </div>
</div>
<script>

	
    $("#searchOption").keyup( function () {
        if ($("#searchOption").val().length > 3) {
            $.ajax({
                url: "<?= base_url(); ?>ajax/searchOption",
                dataType: "json",
                type: "POST",
                data: { "string": $("#searchOption").val() },
                success: function (data, textStatus) {
                    if (data.found !== 0) {
                        let json = data.data;
							$("#optionList").html("");
                            for(var i = 0; i < json.length; i++) {
                                var obj = json[i];
                                $("#optionList").append("<li> "+obj.id+ " - " + obj.title + "</li>");
                            }
                    } else {
                        $("#optionList").html("Nincs találat.");
                    }
                }
            });
        }
    });

</script>