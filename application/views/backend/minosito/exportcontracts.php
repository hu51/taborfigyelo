<?php

$this->db->from("tm_contracts");
$this->db->where("closed", 1);
$this->db->where("publicationTime is NOT NULL");
$this->db->order_by("id", "DESC");

$all = $this->db->get()->result();

require FCPATH.'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

// Create new Spreadsheet object
$spreadsheet = new Spreadsheet();
$spreadsheet->setActiveSheetIndex(0);

$spreadsheet->getActiveSheet()->setTitle('data');

$rowID = 2;
foreach ($all as $data) {
    if ($rowID == 2) {
        $colID = 0;
        foreach ($data as $key => $val) {
            $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow($colID, 1, $key);
            $colID++;
        }
    }

    $colID = 0;
    foreach ($data as $key => $val) {
        $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow($colID, $rowID, $val);
        $colID++;
    }
    $rowID++;
}


// Redirect output to a client’s web browser (Xls)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="data.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0

$writer = IOFactory::createWriter($spreadsheet, 'Xls');
$writer->save('php://output');