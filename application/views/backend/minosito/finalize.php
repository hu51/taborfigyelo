<?php

	$all = $this->db->from("tm_contracts")->where("campEditDone is not NULL")->where("publicationTime is NULL")->order_by("id", "DESC")->get()->result();
	
?>
<div class="panel panel-default">
	<div class="panel-body">
		<h4>Minősítés lezárására váró szerződések:</h4>
		<form method="post" enctype="multipart/form-data">
		<table class="table table-bordered table-condensed">
			<thead>
			<tr>
				<th class="w150">&nbsp;</th>
				<th>Partner</th>
				<th>Létrehozva</th>
				<th>Szerződés kiküldve</th>
				<th>Táborhelyszínek mentve</th>
				<th></th>
			</tr>	
			</head>
		<?php foreach ($all as $data): ?>
			<tr>
				<td>
					<?= anchor("szerkeszt/".md5($data->id."#".$data->uniqID)."/szerzodes", "Szerződés", " class='btn btn-sm btn-primary' target='_blank'"); ?>
				</td>
				<td><?= $data->partnerName; ?></td>
				<td>
					<i class='glyphicon glyphicon-ok text-success'></i> <?= date("m.d. H:i", strtotime($data->createdTime)); ?>
				</td>
				<td>
					<i class='glyphicon glyphicon-ok text-success'></i> <?= date("m.d. H:i", strtotime($data->contractSent)); ?>
				</td>
				<td>
					<i class='glyphicon glyphicon-ok text-success'></i> <?= date("m.d. H:i", strtotime($data->campEditDone)); ?>

					<?= anchor("minosito/main/locationtest/" . $data->id, "<i class='glyphicon glyphicon-eye-open'></i>", " target='_blank' class='fr' title='Helyszínek' "); ?>
				</td>
				<td>					
						<button type="submit" class="btn btn-sm btn-success" name="finalizeContract" value="<?= $data->id; ?>"> Folyamat lezárása</button>
						<input type="checkbox" name="confFinalize" value="<?= $data->id; ?>"> megerősítés, levél kiküldése					
				</td>

			</tr>
		<?php endforeach; ?>
		</table>
	</div>
</div>
