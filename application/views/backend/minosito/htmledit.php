<?php

if ($this->input->post("htmlID") && $this->input->post("saveHtml")) {

	$html   = $this->db->from("tm_htmls")->where("id", (int) $this->input->post("htmlID"))->get()->row();
	$data = array(
		"title"      => filter_input(INPUT_POST, "title", FILTER_SANITIZE_STRING),
		"body"       => $this->input->post("body"),
		"lastModify" => date("Y-m-d H:i:s"),
	);
	$this->db->update("tm_htmls", $data, array("id" => $this->input->post("htmlID")));
	message("", "Módosítások mentve.", "success");
}

$htmls = $this->db->from("tm_htmls")->order_by("title")->get()->result();

?>
<legend>HTML-ek szerkesztése</legend>
<div class="panel panel-default">
	<div class="panel-body">
		<form method="post" action="" class="">
			<div class="col-lg-12">
				<div class="col-lg-8">
					<select name="htmlID" class="form-control">
						<?php foreach ($htmls as $data) : ?>
							<?php if ($this->input->post("htmlID") && ($data->id == $this->input->post("htmlID"))) : ?>
								<option value="<?= $data->id; ?>" selected="selected">
								<?php else : ?>
								<option value="<?= $data->id; ?>">
								<?php endif; ?>
								<?= $data->title; ?>
								&nbsp;(<?= $data->slug; ?>)
							</option>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="col-lg-4">
					<button type="submit" name="chgSource" class="btn btn-primary">Kiválasztott betöltése</button>
				</div>
			</div>
		</form>
	</div>
</div>
<br style="clear: both;">
<?php

if ($this->input->post("htmlID")) :

	$html = $this->db->from("tm_htmls")->where("id", (int) $this->input->post("htmlID"))->get()->row();
	?>

	<div class="panel panel-default">
		<div class="panel-heading">Leírás adatai</div>
		<div class="panel-body">
			<form method="post" action="" class="">
				<input type="hidden" name="htmlID" value="<?= $html->id; ?>">

				<div class="col-lg-12">
					<label class="col-lg-2">Tárgy (e-mail):</label>
					<div class="col-lg-6">
						<input type="text" class="form-control" name="title" value="<?= $html->title; ?>">
					</div>
					<div class="col-lg-4">
						<button type="submit" name="saveHtml" class="btn btn-success" value="ok">Leírás mentése</button>
					</div>
				</div>

				<div class="col-lg-12">
					<label class="col-lg-2">Szöveg:</label>
					<div class="col-lg-10">
						<textarea name="body" class="htmlEditor" style="max-width: 900px"><?= $html->body; ?></textarea>
						<p>Utoljára módosítva: <?= $html->lastModify; ?></p>
						<br>
					</div>
				</div>
			</form>
		</div>
	</div>

	<script src="<?= base_url(); ?>/js/tinymce/tinymce.min.js" type="text/javascript"></script>
	<script>
		tinymce.init({
			mode: "specific_textareas",
			editor_selector: "htmlEditor",
			height: 500,
			theme: 'modern',
			plugins: ' searchreplace autolink directionality visualblocks visualchars fullscreen image code link table hr nonbreaking anchor toc lists textcolor wordcount',
			toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link image | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat code | fullscreen',
			image_advtab: true,
			templates: [{
					title: 'Test template 1',
					content: 'Test 1'
				},
				{
					title: 'Test template 2',
					content: 'Test 2'
				}
			],
			content_css: [
				'//www.tinymce.com/css/codepen.min.css'
			],
			relative_urls: false
		});
	</script>

<?php endif;
