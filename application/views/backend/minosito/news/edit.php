<?php

$docID = $this->uri->segment(4);
if ($this->input->post("saveHtml")) {

    if ($docID == "new") {
        $data = array(
            "title"          => filter_input(INPUT_POST, "title", FILTER_SANITIZE_STRING),
            "body"           => $this->input->post("body"),
            "excerpt"        => $this->input->post("excerpt"),            
            "lastModify"     => date("Y-m-d H:i:s"),
            "createDatetime" => date("Y-m-d H:i:s"),
            "versionID"      => 1,
            "currentVersion" => 1,
            "categoryID"     => (int) $this->input->post("categoryID"),
            "visible"        => (int) $this->input->post("visible"),
        );

        $this->db->insert("tm_documents", $data);
        message("", "A bejegyzést mentettük.", "success");

        $docID = $this->db->insert_id();
        $this->db->update("tm_documents", array("originalID" => $docID), array("id" => $docID));
        return;
    } else {
        if ($this->input->post("newVersion")) {
            $data = array(
                "title"      => filter_input(INPUT_POST, "title", FILTER_SANITIZE_STRING),
                "body"       => $this->input->post("body"),
                "excerpt"    => $this->input->post("excerpt"),                            
                "lastModify" => date("Y-m-d H:i:s"),
                "categoryID" => (int) $this->input->post("categoryID"),
            );
            if ($this->input->post("updateDatetime")) {
                $data["createDatetime"] = date("Y-m-d H:i:s");
            }
            $html = $this->db->from("tm_documents")->where("currentVersion", 1)->where("originalID", (int) $docID)->get()->row();
            $this->db->update("tm_documents", array("currentVersion" => 0), array("originalID" => $docID));
            
            $data = $data + (array) $html;
            $data["versionID"]++;
            unset($data["id"]);
            $this->db->insert("tm_documents", $data);
            
            $docID = $this->db->insert_id();
            redirect("ackend/news/edit/" . $docID);
        } else {
            $data = array(
                "title"      => filter_input(INPUT_POST, "title", FILTER_SANITIZE_STRING),
                "body"       => $this->input->post("body"),
                "excerpt"    => $this->input->post("excerpt"),            
                "lastModify" => date("Y-m-d H:i:s"),
                "categoryID" => (int) $this->input->post("categoryID"),
            );
            if ($this->input->post("updateDatetime")) {
                $data["createDatetime"] = date("Y-m-d H:i:s");
            }
            $this->db->update("tm_documents", $data, array("currentVersion" => 1, "originalID" => $docID));
        }
        message("", "A bejegyzést módosítottuk.", "success");
    }
}

if ($docID == "new") {
    $html = (object) array(
        "id"         => "new",
        "title"      => "",
        "excerpt"    => "",        
        "body"       => "",
        "categoryID" => 1,
        "lastModify" => "",
        "visible" => 0,
    );
} else {
    $html = $this->db->from("tm_documents")->where("originalID", (int) $docID)->order_by("currentVersion", "DESC")->limit(1)->get()->row();
}

$categories = $this->db->from("tm_doc_categories")->order_by("title")->get()->result();
?>
<div class="panel panel-default">
    <div class="panel-heading">Cikk adatai</div>
    <div class="panel-body">
        <form method="post" action="" class="">
            <input type="hidden" name="htmlID" value="<?=$html->id;?>">

            <div class="col-lg-12">
                <label class="col-lg-2">Kategória:</label>
                <div class="col-lg-6">
                    <select name="categoryID" class="form-control">
                        <option value="0"  <?=(0 == $html->categoryID)?"selected":""; ?>>* Mindenhol megjelenik</option>
                        <?php foreach ($categories as $data): ?>
                        <option value="<?=$data->id;?>"  <?=($data->id == $html->categoryID)?"selected":""; ?>><?=$data->title;?></option>
                        <?php endforeach;?>
                    </select>
                </div>
            </div>

            <div class="col-lg-12">
                <label class="col-lg-2">Cím:</label>
                <div class="col-lg-6">
                    <input type="text" class="form-control" name="title" value="<?=$html->title;?>">
                </div>
                <div class="col-lg-4">
                    <button type="submit" name="saveHtml" class="btn btn-success" value="ok">Cikk mentése</button>
                    <?php if ($docID !== "new"): ?>
                        <input type="checkbox" name="newVersion" value="ok" autocomplete="off"> Verzió léptetése
                    <?php endif;?>
                </div>
            </div>

            <div class="col-lg-12">
                <label class="col-lg-2">Látható:</label>
                <div class="col-lg-6">
                    <?= $this->tools->bs_checkbox("visible", $html->visible); ?>
                </div>
            </div>

            <div class="col-lg-12">
                <label class="col-lg-2">Dátum:</label>
                <div class="col-lg-6 p10">
                    <input type="checkbox" name="updateDatetime" value="1"> Létrehozási dátum frissítése (sorrend miatt)
                </div>
            </div>

            <div class="col-lg-12">
                <label class="col-lg-2">Szöveg:</label>
                <div class="col-lg-10">
                    <div  style="max-width: 900px;">
                        <textarea name="body" class="htmlEditor"><?=$html->body;?></textarea>
                    </div>
                    <p>Utoljára módosítva: <?=$html->lastModify;?></p>
                    <br>
                    <?php if (!empty($html->reqFields)): ?>
                        <div class="col-lg-12 alert alert-warning">
                            A rendszer az alábbi szövegeket (%...%), automatikusan cseréli le, küldés/megjelenítéskor.<br>
                            Ezeknek a szövegeket a leírásnak tartalmaznia kell!<br>
                            <br>
                            <code><?=$html->reqFields;?></code>
                        </div>
                    <?php endif;?>
                </div>
            </div>
            
            <div class="col-lg-12">
                <label class="col-lg-2">Bevezető:</label>
                <div class="col-lg-10">
                    <div  style="max-width: 900px;">
                        <textarea name="excerpt" class="htmlLightEditor"><?=$html->excerpt;?></textarea>
                    </div>
                </div>
            </div>            
        </form>
    </div>
</div>

<script src="<?=base_url();?>js/tinymce/tinymce.min.js" type="text/javascript"></script>
<script>
    tinymce.init({
        mode: "specific_textareas",
        editor_selector: "htmlEditor",
        height: 500,
        theme: 'modern',
        plugins: ' searchreplace autolink directionality visualblocks visualchars fullscreen image media code link table hr nonbreaking anchor toc lists textcolor wordcount responsivefilemanager',
        toolbar1: 'responsivefilemanager | formatselect | bold italic strikethrough forecolor backcolor | link image media | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat code | fullscreen',
        image_advtab: true,
        external_filemanager_path:"<?=base_url();?>filemanager/",
        filemanager_title:"Responsive Filemanager" ,
        external_plugins: { "filemanager" : "<?=base_url();?>filemanager/plugin.min.js"},
        filemanager_access_key:"5EC4E40BB6748073F0ECEC3A2229371D4CEA1297D0A6B6742EBB351D490BA11A",
        content_css: [
            '//www.tinymce.com/css/codepen.min.css'
        ],
        relative_urls: false
    });
    
    tinymce.init({
        mode: "specific_textareas",
        editor_selector: "htmlLightEditor",
        height: 100,
        menubar:false,
        theme: 'modern',
        plugins: ' directionality visualblocks visualchars  hr nonbreaking wordcount ',
        toolbar1: ' bold italic strikethrough forecolor backcolor | alignleft aligncenter alignright alignjustify  | removeformat code ',
        content_css: [
            '//www.tinymce.com/css/codepen.min.css'
        ],
        relative_urls: false
    });    
</script>
