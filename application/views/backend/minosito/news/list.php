<?php

$all = $this->db->from("tm_documents")->order_by("createDatetime", "DESC")->order_by("title", "ASC")->where("currentVersion", 1)->get()->result();

$categories = ["0" => "* Mindenhol megjelenik"];
foreach ($this->db->from("tm_doc_categories")->order_by("title")->get()->result() as $data) {
	$categories[$data->id] = $data->title;
}
	
	
?>
<div class="panel panel-default">
	<div class="panel-body">
		<h4>Cikkek:</h4>
		<table class="table table-bordered table-condensed">
			<thead>
			<tr>
				<th>ID</th>
				<th>Név</th>
				<th>Kategória</th>
				<th>Létrehozva</th>
				<th>Módosítva</th>
				<th>Látható</th>
			</tr>	
			</head>
		<?php foreach ($all as $data): 

		$db = $this->db->from('tm_documents')->where('categoryID', $data->id)->where("currentVersion", 1)->get()->num_rows();
			
			?>
			<tr>
				<td class="w50 ac"><a href="<?= site_url("minosito/news/edit/".$data->originalID); ?>"><?= $data->id; ?></a></td>
				<td><a href="<?= site_url("minosito/news/edit/".$data->originalID); ?>"><?= $data->title; ?></a></td>
				<td><?= $categories[$data->categoryID]; ?></td>
				<td><?= $data->createDatetime; ?></td>
				<td><?= $data->lastModify; ?></td>
				<td><?= ($data->visible)?"<span class='glyphicon glyphicon-eye-open'></span>":""; ?></td>
			</tr>
		<?php endforeach; ?>
		</table>
	</div>
</div>