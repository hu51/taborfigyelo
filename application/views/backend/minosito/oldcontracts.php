<?php

	$exp = (int)$this->tools->GetSetting("contract-exp-days");
	
?>
<div class="panel panel-default">
	<div class="panel-heading">
		<h4>Valamelyik fázisnál elakadt szerződések (<?= $exp ?> nap):</h4>
	</div>
	<div class="panel-body">
		<table class="table table-bordered table-condensed">

		<tr class="active"><td colspan="4"><h4>Az alapadatok nem lettek lezárva</h4></td></tr>
		<?php

			$this->db->from("tm_contracts");
			$this->db->where("regEmailSent <", date("Y-m-d", strtotime("-{$exp} DAYS")));
			$this->db->where("(contractSent is NULL OR contractSaved is NULL)");
			$this->db->order_by("id", "DESC");
			$all = $this->db->get()->result();

		?>
			<tr>
				<th class="w50">&nbsp;</th>
				<th>Partner</th>
				<th>Regisztrációs levél kiküldve</th>
				<th></th>
			</tr>	
		<?php foreach ($all as $data): ?>
			<tr>
				<td>
				<?= anchor("minosito/main/editcontract/" .$data->id , "<i class='glyphicon glyphicon-edit '></i>", " class='fr' target='_blank'"); ?>
				</td>
				<td><?= $data->contactEmail; ?></td>
				<td>
					<i class='glyphicon glyphicon-ok text-success'></i> <?= date("Y. m. d. H:i", strtotime($data->regEmailSent)); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		
		<tr class="active"><td colspan="4"><h4>Befizetett, de a táborhelyszín kérdőivek nem lettek lezárva</h4></td></tr>
		<?php

			$this->db->from("tm_contracts");
			$this->db->where("contractPaid <", date("Y-m-d", strtotime("-{$exp} DAYS")));
			$this->db->where("campEditDone is NULL");
			$this->db->order_by("id", "DESC");
			$all = $this->db->get()->result();

		?>

			<tr>
				<th class="w50">&nbsp;</th>
				<th>Partner</th>
				<th>Regisztrációs levél kiküldve</th>
				<th>Fizetési levél kiküldve</th>
			</tr>	

		<?php foreach ($all as $data): ?>
			<tr>
				<td>
				<?= anchor("minosito/main/editcontract/" .$data->id , "<i class='glyphicon glyphicon-edit '></i>", " class='fr' target='_blank'"); ?>
				</td>
				<td><?= $data->partnerName; ?></td>
				<td>
					<i class='glyphicon glyphicon-ok text-success'></i> <?= date("Y. m. d. H:i", strtotime($data->regEmailSent)); ?>
				</td>
				<td>
					<i class='glyphicon glyphicon-ok text-success'></i> <?= date("Y. m. d. H:i", strtotime($data->contractSaved)); ?>
				</td>
			</tr>
		<?php endforeach; ?>

		<tr class="active"><td colspan="4"><h4>Táborhely szerkesztő levél kiküldve, de nincs kitöltve</h4></td></tr>
		<?php

			$this->db->from("tm_contracts");
			$this->db->where("campEditSent <", date("Y-m-d", strtotime("-{$exp} DAYS")));
			$this->db->where("campEditDone is NULL");
			$this->db->order_by("id", "DESC");
			$all = $this->db->get()->result();

		?>

			<tr>
				<th class="w50">&nbsp;</th>
				<th>Partner</th>
				<th>Regisztrációs levél kiküldve</th>
				<th>Szerkesztő levél kiküldve</th>
			</tr>
		<?php foreach ($all as $data): ?>
			<tr>
				<td>
					<?= anchor("minosito/main/editcontract/" .$data->id , "<i class='glyphicon glyphicon-edit '></i>", " class='fr' target='_blank'"); ?>
				</td>
				<td><?= $data->partnerName; ?></td>
				<td>
					<i class='glyphicon glyphicon-ok text-success'></i> <?= date("Y. m. d. H:i", strtotime($data->regEmailSent)); ?>
				</td>
				<td>
					<i class='glyphicon glyphicon-ok text-success'></i> <?= date("Y. m. d. H:i", strtotime($data->campEditSent)); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>
	</div>	
</div>