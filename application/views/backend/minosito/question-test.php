<div class="">
	<h2>Kérdések tesztelése:</h2>
	
	<form id="location_form" action="">
<?php

	$contractID = (int)filter_var($this->uri->segment(4));	
	$locID = (int)filter_var($this->uri->segment(5));	
    $contract = $this->db->from("tm_contracts")->where("id", $contractID)->get()->row();

if (!$locID) : 
    $locations = $this->db->from("tm_contracts_locations")->where("contractID", $contractID)->get()->result();

    ?>	
	<h2>Táborhelyszínek:</h2>
	<br>
    <div class="list-group">
        <?php
        foreach ($locations as $loc) {
            echo "<div class='list-group-item'>";
            echo anchor("minosito/main/locationtest/" . $contractID . "/" . $loc->id, "<i class='glyphicon glyphicon-eye-open'></i> " . $loc->title);
            echo "</div>\n";
        }
        ?>
    </div>
<?php else : 
	$location = $this->db->from("tm_contracts_locations")->where("id", $locID)->get()->row();	
	
	?>
    	<input type="hidden" name="readonlyData" value="1">
    	<input type="hidden" name="locID" value="<?= $locID; ?>">
    	<div>
    		<div class="col-lg-12">
    			<label for="name" class="col-lg-4">Táborhely neve:</label>
    			<div class="col-lg-8">
    				<?= $location->title; ?>
    			</div>
    		</div>
    		
    		<div class="col-lg-12">
    			<label for="address" class="col-lg-4">Irányítószám:</label>
    			<div class="col-lg-8">
    				<?= $location->zip; ?>
    			</div>
            </div>

    		<div class="col-lg-12">
    			<label for="address" class="col-lg-4">Település:</label>
    			<div class="col-lg-8">
    				<?= $location->city; ?>
    			</div>
            </div>	
            
    		<div class="col-lg-12">
    			<label for="address" class="col-lg-4">Utca, házszám:</label>
    			<div class="col-lg-8">
    				<?= $location->address; ?>
    			</div>
    		</div>	
    		
    		<div class="col-lg-12">
    			<label for="website" class="col-lg-4">Weboldal címe:</label>
    			<div class="col-lg-8">
    				<?= $location->website; ?>
    			</div>
    		</div>
    
    		<div class="col-lg-12">
    			<label for="facebook" class="col-lg-4">Facebook esemény címe:</label>
    			<div class="col-lg-8">
    				<?= $location->facebook; ?>
    			</div>
    		</div>
   		
    
    		<div class="col-lg-12">
    			<label for="facebook" class="col-lg-4">Tábortémák száma:</label>
    			<div class="col-lg-8">
    				<input type="text" class="form-control w50" disabled value="<?= $location->themes; ?>">
    			</div>
    		</div>
    		
    	</div>
    	
		<?php 
		
		
        $this->m_camp->buildQuestionList($location, true); 
        ?>
    </form>
</div>
<br>

<?php endif; ?>