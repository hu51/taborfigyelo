<div class="panel panel-default">
    <div class="panel-heading">
        <h3>Kérdések kezelése</h3>
    </div>    
    <div class="panel-body">
        <?php

        if ($this->input->post("saveOrder")) {
            $orders = $this->input->post("order");
            foreach ($orders as $id => $poz) {
                
                $this->db->update('tm_question', array("orderID" => $poz), array("id" => $id));
            }
            message("", "Sorrend mentve");
        }

        ?>
        <form method="post">
            <div class="col-lg-12">
                <div class="col-lg-6">
                    <button type="submit" class="btn btn-primary" name="saveOrder" value="1">Kérdések sorrendjének mentése</button></a><br>
                    Másik csoportba áthelyezni kérdés szerkesztésénél lehet!
                </div>
                <div class="col-lg-6 ar">
                    <a href="<?= site_url(); ?>minosito/main/editquestion/new" target="_blank"><button type="button" class="btn btn-success">Új kérdés hozzáadása</button></a><br>
                </div>
            </div>
            <table class="table table-condensed table-bordered">        
            <?php

            $groups = $this->db->from('tm_question_group')->order_by("orderID")->get()->result();
            foreach ($groups as $group): 
                $poz = 1;            
                ?>
                    <tr class="active">
                        <th class="w50">&nbsp;</th>
                        <th colspan="2"><?= $group->title; ?></th>
                        <th colspan="2" class="w100">Sorrend</th></th>
                        <th class="w50">Típus</th>
                        <th >max: <?= $group->maxPoint; ?></th>
                    </tr>
                    <?php

                    $questions = $this->db->from('tm_question')->where("groupID", $group->id)->order_by("orderID")->get()->result();
                    foreach ($questions as $data) :
                        $this->db->update('tm_question', array("orderID" => $poz++), array("id" => $data->id));
                        ?>
                        <tr class="sortable">
                            <th><?= anchor("minosito/main/editquestion/" . $data->id, "<span class='glyphicon glyphicon-edit'></span>", " target='_blank' "); ?></th>
                            <td><?= $data->id; ?>.)</td>
                            <td>
                                <?= $data->title; ?><br>                            
                            </td>
                            <td>
                                <input type="text" class="form-control w50 order" readonly name="order[<?= $data->id; ?>]" value="<?= $data->orderID; ?>">
                            </td>
                            <td>
                                <span class="glyphicon glyphicon-move handle"></span>
                            </td>
                            <td>
                                <?php if ($data->multiple) : ?>
                                    <i class="fa fa-check-square" aria-hidden="true"></i>
                                <?php else : ?>
                                    <i class="fa fa-circle-o" aria-hidden="true"></i>
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php 

                                    foreach (explode(",", $data->reqOptID) as $id) {
                                        if( ! empty($id)) {
                                            $opt = $this->db->from('tm_question_options')->where('id', $id)->get()->row();
                                            $que = $this->db->from('tm_question')->where('id', $opt->questionID)->get()->row();
                                            echo "<small>".anchor("minosito/main/editquestion/".$opt->questionID, " &raquo; ".$que->title." =&gt; ".$opt->title, " target='_blank'")."</small><br>\n";
                                        }
                                    }
                                ?>
                            </td>
                        </tr>
                    <?php
                    endforeach; ?>
                <?php endforeach; ?>
            </table>

        </div>
    </form>
    <script type="text/javascript">

    $('tbody').sortable({
        handle: ".handle",
        containment: 'parent',
        items: 'tr'            
    });

    $('tbody').on('sortupdate',function( event, ui) {
        tbl = ui.item.closest("table");
        poz = 1;
        tbl.find("input.order").each( function() {                
            obj = $(this);              
            obj.val(poz);
            poz = poz + 1;
        });
    });   
    </script>
</div>