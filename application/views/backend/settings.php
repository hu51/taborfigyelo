<?php
$prefix = $this->uri->segment(4);
$result = $this->db->from($prefix . "_settings")->order_by("title", "ASC")->get();

?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4>Beállítások</h4>
    </div>
    <div class="panel-body">
        <form method="post" action="" class="noenter">
            <table class="table table-condensed">
                <thead>
                    <th width="45%">Leírás</th>
                    <th width="100px">&nbsp;</th>
                    <th>Érték</th>
                </thead>
                <?php foreach ($result->result() as $data): ?>
                <tr>
                    <td class="w400"><?=$data->title;?>
                    <?php if (!empty($data->info)): ?>
                        <br><i class="small">(<?=$data->info;?>)</i>
                        <?php endif;?>
                    </td>
                    <td class="w50"><?=form_submit("update[" . $data->slug . "]", "Ment", "' class='btn btn-success'");?></td>
                    <td>
                        <?php 
                        
                        switch ($data->type) {
                            case "checkbox": {
                                echo $this->tools->bs_checkbox("value[" . $data->slug . "]", $data->value);
                            } break;

                            case "textarea": {
                                $tmp = array(
                                    'name'        => "value[" . $data->slug . "]",
                                    'value'       => $data->value,
                                    'rows'        => '5',
                                    'class'       => 'form-control',
                                  );
                                echo form_textarea($tmp);
                            } break;

                            case "date": {
                                echo "<input type='date' name='value[" . $data->slug . "]' value='".$data->value."' class='form-control' >\n";
                            } break;

                            case "email": {
                                echo "<input type='email' name='value[" . $data->slug . "]' value='".$data->value."' class='form-control' >\n";
                            } break;

                            case "number": {
                                echo "<input type='number' name='value[" . $data->slug . "]' value='".$data->value."' class='form-control' >\n";
                            } break;

                            default: {
                                echo form_input("value[" . $data->slug . "]", $data->value, " class='form-control'");
                            }
                        }
                        
                        ?>
                    </td>
                </tr>
                <?php endforeach;?>
            </table>
        </form>
    </div>
</div>