<?php

$this->load->helper("form");

if (isset($message)) {
    echo $message;
}

$pass = $this->tools->passgen(12, true);

?>
<div class="col-lg-6 col-md-12">
<form method="post" action="" class="form-horizontal" role="form"autocomplete="off">
    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title">Új Felhasználó</h3>
        </div>
        <div class="panel-body">

            <div class="form-group">
                <label class="col-lg-2 control-label" for="email" >E-mail:</label>
                <div class="col-lg-10">
                    <input type="email" class="form-control" name="new_email" id="email" placeholder="E-mail" value="<?= set_value("email"); ?>" maxlength="80"/>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-lg-2 control-label" for="password">Jelszó:</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" name="new_password" maxlength="20" autocomplete="off" value="<?= $pass; ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label" for="firstName">Keresztnév:</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" name="firstName" value="<?= set_value("firstName"); ?>" maxlength="40"/>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-lg-2 control-label" for="lastName">Vezetéknév:</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" name="lastName" value="<?= set_value("lastName"); ?>" maxlength="40"/>
                </div>
            </div>    

            <div class="form-group">
                <label class="col-lg-2 control-label"></label>
                <div class="col-lg-10">
                    <button type="submit" name="adduser" value="adduser" class="btn btn-primary">Felhasználó felvétele</button>
                </div>
            </div>

        </div>
    </div>
</form>  
</div>