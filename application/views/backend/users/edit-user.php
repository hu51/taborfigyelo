<?php

$this->load->helper("form");
$userID = $this->uri->segment(4);
if (isset($message)) {
    echo $message;
}

$user = $this->db->from('users')->where('id', $userID)->get()->row();
if (!$user) {
    $this->tools->message("Hiba", "Nincs ilyen felhasználó!<br />\n", "alert");
    return;
}

$pass = $this->tools->passgen(12, true);

?>
<div class="col-lg-6 col-md-12">
<form method="post" action="" class="form-horizontal" role="form" autocomplete="off">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Felhasználó adatai</h3>
        </div>
        <div class="panel-body">
            <input type="hidden" name="id" value="<?= $userID; ?>"/>

            <div class="form-group">
                <label class="col-lg-2 control-label" for="email" >E-mail:</label>
                <div class="col-lg-10">
                    <input type="email" class="form-control" name="email" id="email" placeholder="E-mail" value="<?= $user->email; ?>" maxlength="80"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label" for="firstName">Keresztnév:</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" name="firstName" value="<?= $user->firstName; ?>" maxlength="40"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label" for="lastName">Vezetéknév:</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" name="lastName" value="<?= $user->lastName; ?>" maxlength="40"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label" for="phone">Telefonszám:</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" name="phone" placeholder="Telefonszám" value="<?= $user->phone; ?>" maxlength="40"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label"></label>
                <div class="col-lg-10">
                    <button type="submit" name="modify" value="main" class="btn btn-success">Módosítások mentése</button>
                </div>
            </div>

            <?php if ($this->session->userdata("user_isAdmin")): ?>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Adminisztrátor</label>
                    <div class="col-lg-10">
                        <?= $this->tools->bs_checkbox("isAdmin", $user->isAdmin); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-2 control-label">Kitiltva</label>
                    <div class="col-lg-10">
                        <?= $this->tools->bs_checkbox("blocked", $user->blocked); ?>
                    </div>
                </div>
            <?php endif; ?>

            <div class="form-group">
                <label class="col-lg-2 control-label" for="password">Jelszó cseréje:</label>
                <div class="col-lg-10">
                    <input type="password" class="form-control" id="new_password" name="new_password" maxlength="20">
                    <br>
                    <label class="fl">Kattints a jelszóra beállításhoz:</label><pre class="w150 fillpass clickMe"><?= $pass; ?></pre>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label" for="new_password2">Jelszó megerősítés:</label>
                <div class="col-lg-10">
                    <input type="password" class="form-control" id="new_password2" name="new_password2" maxlength="20">
                </div>
            </div>
        </div>
    </div>
</form>
</div>
<script>

    $(document).ready(function () {

        $(".fillpass").click(function () {
            $("#new_password").val($(this).html());
            $("#new_password2").val($(this).html());
        });

    });

</script>