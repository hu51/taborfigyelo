<?php

$this->load->helper("form");

?>
<div class="panel panel-default">
	<div class="panel-body">
        <legend>Felhasználók</legend>
        <form method="get" action="" class="form-inline">
            <input type="text" class="form-control" name="filterstr" placeholder="szűrés" value="<?= $this->input->get("filterstr"); ?>"/>
            <button type="submit" value="filter" class="btn btn-success">Keresés</button>
        </form>
<?php

$this->db->select('id');
$this->db->from('users');
if ($this->input->get("filterstr")) {
    $this->db->like('firstName', $this->input->get("filterstr", true), 'BOTH');
    $this->db->or_like('lastName', $this->input->get("filterstr", true), 'BOTH');
}
$this->db->group_by('id');
$allRows = $this->db->get()->num_rows();

if (!$this->input->get("page")) {
    $pageID = 1;
} else {
    $pageID = (int) $this->input->get("page");
}
$pageMaxRows  = 20;
$page = $this->tools->pagination("users/", $allRows, $pageMaxRows, $pageID);
$pageID = $page["current"];
$pageStartRow = ($pageID - 1) * $pageMaxRows;

$this->db->from('users');
if ($this->input->get("filterstr")) {
    $this->db->like('firstName', $this->input->get("filterstr", true), 'BOTH');
    $this->db->or_like('lastName', $this->input->get("filterstr", true), 'BOTH');
}

$this->db->order_by('firstName', 'ASC');
$this->db->order_by('lastName', 'ASC');
$this->db->group_by('id');
$this->db->limit($pageMaxRows, $pageStartRow);
$query = $this->db->get();

echo $page["html"];

?>
        <table class="table table-striped sorted">
            <thead>
                <tr>
                    <th class="nosort">&nbsp;</th>
                    <th>Vezetéknév</th>
                    <th>Keresztnév</th>
                    <th>Email</th>
                    <th>Utolsó belépés</th>
                    <th>Kitiltva</th>
                </tr>
            </thead>
            <?php foreach ($query->result() as $data) : ?>
                <tr>
                    <td width='20px'><?= anchor("backend/users/edituser/" . $data->id, "<span class='glyphicon glyphicon-user' title='Fehlasználó szerkesztése'></span>"); ?></td>
                    <td><?= $data->lastName; ?></td>
                    <td><?= $data->firstName; ?></td>
                    <td><?= $data->email; ?></td>
                    <td><?= $data->lastLogin; ?></td>
                    <td><?= ($data->blocked)?"IGEN":""; ?></td>
                </tr>
            <?php endforeach; ?>
        </table>

        <?= $page["html"]; ?>
    </div>
</div>