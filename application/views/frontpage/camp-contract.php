<legend>Táborhely szerződés</legend>
<?php

$contractID = (int)$this->session->userdata("user_contractID");
$contract = $this->db->from("tf_camp_contracts")->where("id", (int) $contractID)->get()->row();

?>
<div class="col-lg-12">
    <label for="contactName" class="col-lg-4">Kapcsolattartó neve:</label>
    <div class="col-lg-8">
        <?=$contract->contactName;?>
    </div>
</div>

<div class="col-lg-12">
    <label for="contactEmail" class="col-lg-4">~ e-mail címe:</label>
    <div class="col-lg-8">
        <?=$contract->contactEmail;?>
    </div>
</div>

<div class="col-lg-12">
    <label for="contactPhone" class="col-lg-4">~ telefonszáma:</label>
    <div class="col-lg-8">
        <?=$contract->contactPhone;?>
    </div>
</div>

<div class="col-lg-12">
    <label for="representName" class="col-lg-4">Képviselő neve:</label>
    <div class="col-lg-8">
        <?=$contract->representName;?>
    </div>
</div>

<label>Szerződő partner</label>
<div class="col-lg-12">
    <label for="partnerName" class="col-lg-4">~ neve:</label>
    <div class="col-lg-8">
        <?=$contract->partnerName;?>
    </div>
</div>

<div class="col-lg-12">
    <label for="zip" class="col-lg-4">~ irányítószám, település:</label>
    <div class="col-lg-3">
        <?=$contract->zip;?>
    </div>
    <div class="col-lg-5">
        <?=$contract->city;?>
    </div>
</div>

<div class="col-lg-12">
    <label for="address" class="col-lg-4">~ utca, házszám:</label>
    <div class="col-lg-8">
        <?=$contract->address;?>
    </div>
</div>

<div class="col-lg-12">
    <label for="taxID" class="col-lg-4">Adószám:</label>
    <div class="col-lg-4">
        <?=$contract->taxID;?>
    </div>
</div>

<div class="col-lg-12">
    <label for="compID" class="col-lg-4">Cégjegyzékszám:</label>
    <div class="col-lg-4">
        <?=$contract->compID;?>
    </div>
</div>

<table class="table" id="datatable">
    <thead>
        <tr>
            <th width="30%">Tábor neve</th>
            <th class="w100">Tábortémák:</th>
        </tr>
    </thead>
    <tbody>
        <?php

        $locations = $this->db->from("tf_camps")->where("contractID", $contractID)->order_by("name")->get()->result();

        foreach ($locations as $loc): ?>
            <tr>
                <td><?= $loc->name; ?></td>
                <td><?= $loc->themes; ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

