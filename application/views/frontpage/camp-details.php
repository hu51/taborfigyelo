<?php

$this->db->select("c.*, con.visible as conVisible, con.uniqID, t.name as typeTitle, cl.name as categoryTitle, l.name as locationTitle, s.name as stateTitle, co.name as countryTitle");
$this->db->from("tf_camps as c");
$this->db->join("tf_camp_types as t", "t.id=c.typeID", "LEFT");
$this->db->join("tf_camp_category_list as cl", "cl.id=c.categoryID", "LEFT");
$this->db->join("tf_camp_locations as l", "l.id=c.locationID", "LEFT");
$this->db->join("tf_camp_states as s", "s.id=c.stateID", "LEFT");
$this->db->join("tf_camp_countries as co", "co.id=s.countryID", "LEFT");
$this->db->join("tf_camp_contracts as con", "con.id=c.contractID", "LEFT");
// $this->db->where("c.published", 1); // preview!

if (strstr($this->uri->segment(2), ".html")) {
    $slug = $this->uri->segment(2);
    $this->db->like("c.alias", str_ireplace(".HTML", "", $slug));
    $this->db->order_by("c.id", "DESC");
} else {
    $campID = (int) $this->uri->segment(2);
    $this->db->where("c.id", $campID);
}
$this->db->limit(1);
$campData = $this->db->get()->row();
$campID   = $campData->id;

if (!$campData) {
    redirect("/");
//} elseif (($campData->published !== "1" || $campData->conVisible !== 1) && ($this->input->get("key") !== substr($campData->uniqID, -5))) {
} elseif (($campData->published !== "1") && ($this->input->get("key") !== substr($campData->uniqID, -5))) {
    redirect("taborok");
}

$this->db->from("tf_camp_images");
$this->db->where("campID", $campID);
$this->db->where("published", 1);
$this->db->where("missing", 0);
$this->db->order_by("ordering, id");
$result = $this->db->get()->result();
$images = [];
foreach ($result as $data) {
    if (file_exists(FCPATH . $data->path)) {
        $images[] = $data;
    } else {
        $this->db->update("tf_camp_images", array("missing" => 1), array("id" => $data->id));
    }
}

if (count($images) > 0) {
    $img_header = array_shift($images);
} else {
    $img_header = false;
}
if (count($images) > 1) {
    $img_turns = array_shift($images);
} else {
    $img_turns = false;
}
if (count($images) > 1) {
    $img_details = array_shift($images);
} else {
    $img_details = false;
}

?>
<div class="col-md-12">
    <article class="camp-details">
        <a name="top"></a>
        <?php if (!$img_header): ?>
        <h3 class="title"><?=$campData->name;?></h3>
        <?php else: ?>
        <div class="row p10 header" style="background-image: url(<?=site_url($img_header->path);?>); ">
            <h3 class="title"><?=$campData->name;?></h3>
        </div>
        <?php endif;?>

        <div class="row page-nav">
            <div class="col-md-3">
                <a href="<?=current_url();?>#top" class="btn btn-sm btn-block btn-secondary">Oldal teteje</a>
            </div>
            <?php if (count($images) > 0): ?>
            <div class="col-md-3">
                <a href="<?=current_url();?>#photos" class="btn btn-sm btn-block btn-secondary">Képek</a>
            </div>
            <?php endif;?>
            <div class="col-md-3">
                <a href="<?=current_url();?>#details" class="btn btn-sm btn-block btn-secondary">Leírás</a>
            </div>
            <div class="col-md-3">
                <a href="<?=current_url();?>#turns" class="btn btn-sm btn-block btn-secondary">Turnusok</a>
            </div>
        </div>

        <div class="row desc p10"><?=strip_tags($campData->description, "b,i");?></div>

        <div class="row m10">
            <div class="p0">
                <div class="col-xs-7">
                    <?php

$this->db->select("*, (SELECT name FROM tf_camp_category_list WHERE id=categoryID) as catName");
$this->db->from("tf_camp_categories");
$this->db->where("campID", $campID);
$cats = $this->db->get()->result();

foreach ($cats as $cat): ?>
                    <i class="glyphicon glyphicon-tag" title="Tábor típusa"></i>
                    <a href="<?=site_url("taborok?category=" . $cat->categoryID);?>"><?=$cat->catName;?></a><br>
                    <?php endforeach;?>
                </div>
                <div class="col-xs-5">
                    <i class="glyphicon glyphicon-home"></i>
                    <?=$campData->typeTitle;?>
                </div>
                <div class="col-xs-12">
                    <i class="glyphicon glyphicon-map-marker" title="Helyszín"></i>
                    <?=($campData->locationID != 0) ? $campData->locationTitle : $campData->location_str;?> (<?=($campData->stateID != 0) ? $campData->stateTitle : $campData->state_str;?>),
                    <?=($campData->countryID != 0) ? $campData->countryTitle : $campData->country_str;?>
                </div>

                <div class="col-xs-7">
                    <i class="glyphicon glyphicon-credit-card" title="SZÉP kártya elfogadás"></i>
                    MKB <i
                        class="glyphicon glyphicon-<?=($campData->szep_MKB) ? "ok text-success" : "remove text-danger";?>"></i>
                    OTP <i
                        class="glyphicon glyphicon-<?=($campData->szep_OTP) ? "ok text-success" : "remove text-danger";?>"></i>
                    K&H <i
                        class="glyphicon glyphicon-<?=($campData->szep_KH) ? "ok text-success" : "remove text-danger";?>"></i>
                </div>

                <div class="col-xs-5">
                    <i class="glyphicon glyphicon-user" title="Korosztály"></i>
                    <?php if (is_null($campData->age_min)): ?>
                    -
                    <?php else: ?>
                    <?=$campData->age_min . "-" . $campData->age_max;?>
                    <?php endif;?>
                </div>
            </div>
        </div>

        <hr>
        <?php if (count($images) > 0): ?>
        <h3>Képek</h3>
        <?php if (count($images) > 4): ?>
        <p>Képek száma: <?=count($images);?>.</p>
        <?php endif;?>
        <div class="row" style="overflow: auto; height: 150px;">
            <a name="photos"></a>
            <?php

                for ($i = 0; $i < count($images); $i++):
                    $data = $images[$i];
                    ?>
		            <div style="float: left; height: 150px;">
		                <a href="<?=site_url($data->path);?>" data-toggle="lightbox" data-gallery="gallery"
		                    data-title="Kép <?=($i + 1);?>"
		                    data-footer="Kattintson a nyilakra a navigáláshoz, vagy használja a jobbra-balra gombokat">
		                    <img src="<?=site_url(str_replace("properties/images", "properties/thumbs", $data->path));?>"
		                        class="img-fluid thumbnail" style="width: auto; height: 140px;" />
		                </a>
		            </div>
		            <?php endfor;?>
        </div>
        <?php endif;?>

        <a name="details"></a>
        <h3>Részletek</h3>
        <?php if ($img_details): ?>
        <div class="row">
            <a href="<?=site_url($img_details->path);?>" data-toggle="lightbox">
                <img src="<?=site_url($img_details->path);?>" style="max-height: 400px; margin: 0px auto;">
            </a>
        </div>
        <?php endif;?>
        <div class="body p10"><?=$campData->text;?></div>


        <?php if (!empty($campData->minositoID)):
            list($contractID, $locID) = $this->tools->Text2ID($campData->minositoID);
            $contract                 = $this->db->from("tm_contracts")->where("id", $contractID)->where("visible", 1)->get()->row();
            $location                 = $this->db->from("tm_contracts_locations")->where("contractID", $contractID)->where("closed", 1)->where("id", $locID)->get()->row();

            if ($location):
            ?>
		        <div class="row desc p10">
		            <img src="http://www.taborminosito.hu/images/tm_logo.png" width="100" height="100" style="float: left;" />
		            <div class="m10 p10">A Táborminősítő által bevizsgált és ajánlott tábor!</div>
		        </div>
		        <?php endif;
            endif; ?>

        <?php if (!empty($campData->program)): ?>
            <h3>Napi programterv</h3>
            <div class="row desc p10"><?=$campData->program;?></div>
        <?php endif;?>

        <a name="turns"></a>
        <h3>Turnusok</h3>
        <?php if ($img_turns): ?>
        <div class="row">
            <a href="<?=site_url($img_turns->path);?>" data-toggle="lightbox">
                <img src="<?=site_url($img_turns->path);?>" style="max-height: 400px; margin: 0px auto;">
            </a>
        </div>
        <?php endif;?>
        <div class="turns p10"><?=$this->m_fcamp->getTurns($campData->id, true);?></div>

        <?php if (!empty($campData->contactEmail)): ?>
        <div class="row m5">
            <h3>Kapcsolati adatok</h3>          
            <form method="post" id="contactForm" class="form-horizontal">
                <p>Kérdését a táborról ezen az űrlapon keresztül tegye fel. 
                <div class="col-md-10 col-md-offset-1">
                    <div class="row">
                        <label class="col-md-3">Név:</label>
                        <div class="col-md-9">
                            <input type="text" name="contact-name" class="form-control required">
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-md-3">Email:</label>
                        <div class="col-md-9">
                            <input type="text" name="contact-email" class="form-control required">
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-md-3">Telefon:</label>
                        <div class="col-md-9">
                            <input type="text" name="contact-phone" class="form-control required">
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-md-3">Gyermek neve:</label>
                        <div class="col-md-9">
                            <input type="text" name="contact-child" class="form-control required">
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-md-3">Kérdés:</label>
                        <div class="col-md-9">
                            <textarea name="contact-comment" class="form-control required"></textarea>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-md-3">Hírlevél:</label>
                        <div class="col-md-9">
                            <input type="checkbox" name="addNewsletter"> Iratkozzon fel a Táborfigyelő havi hírlevelére,
                            hogy a táborokkal kapcsolatos kedvezményekről, hírekről értesíteni tudjuk!
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-md-3"></label>
                        <div class="col-md-9">
                            <button type="submit" name="sendContact" class="btn btn-primary" value="1">Kérdés
                                küldése</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <?php endif;?>

        <?php if($this->input->get("ertekeles") == ((int)$campID+(int)$campData->contractID)): ?>
            <a name="rating"></a>
            <div class="row m5">
            <?php
                $data = $this->db->select("COUNT(id) as  amount, AVG(rating) as val")->from("tf_camp_comments")->where("campID", $campID)->where("status", "1")->get()->row();
                ?>
                <h3>Tábor értékelése</h3>
                <?php if ($data->amount > 0): ?>
                    <p>Tábor értékelése: <?= number_format($data->val,1); ?> (<?= $data->amount; ?>db értékelés alapján)</p>
                <?php endif; ?>
                
                <form method="post" id="ratingForm" class="form-horizontal">            
                    <p>Minősítse a tábort és turnusát az alábbi űrlapon keresztül. 
                    <input type="hidden" name="rating-camp" class="form-control" value="<?= $campID; ?>">
                
                    <div class="col-md-10 col-md-offset-1">
                        <div class="row">
                            <label class="col-md-3">Megjelenő név:</label>
                            <div class="col-md-9">
                                <input type="text" name="rating-name" class="form-control">
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-md-3">Email:</label>
                            <div class="col-md-9">
                                <input type="text" name="rating-email" class="form-control required">
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-md-3">Rövid leírás:</label>
                            <div class="col-md-9">
                                <input type="text" name="rating-title" class="form-control required">
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-md-3">Részletes leírás:</label>
                            <div class="col-md-9">
                                <textarea name="rating-text" rows="6" class="form-control required"></textarea>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-md-3">Minősítés:</label>
                            <div class="col-md-9">     
                            <?php for ($i=1; $i <6 ; $i++) { 
                                echo "<input type='radio' name='rating-value' value='".$i."'>".$i."&nbsp;\n";
                            }   ?>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-md-3">Turnus kijelölése:</label>
                            <div class="col-md-9">
                            
                                <select name="rating-turn" id="input" class="form-control">
                                    <option value="" selected="selected">* Nem kívánom megadni a turnust</option>
                                <?php

                                    $turns = $this->db->from("tf_camp_turns")->where("campID", $campID)->order_by("validfrom", "ASC")->get()->result();
                                    foreach ($turns as $key => $turn) {
                                        echo "<option value=\"".$turn->id."\">".$turn->title. " (".$turn->validfrom." - ".$turn->validto.")</option>\n";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-md-3"></label>
                            <div class="col-md-9">
                                <button type="submit" name="sendRating" class="btn btn-primary" value="1">Minősítés beküldése</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        <?php endif; ?>

        <?php
            $ratings = $this->db->from("tf_camp_comments")->where("campID", $campID)->where("status", "1")->order_by("id", "DESC")->get()->result();

            if(count($ratings)> 0): ?>
            <div class="row m5">
                <h3>Tábor értékelések</h3>
                <table class="table table-condended">
                <?php foreach ($ratings as $data): ?>
                    <tr>
                        <td><i class="fa fa-user" aria-hidden="true"></i>&nbsp;<?= (!empty($data->name))?$data->name:"névtelen hozzászólás";?></td>
                        <td><i class="fa fa-star" aria-hidden="true"></i>&nbsp;<?=$data->rating;?></td>
                        <td><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;<?=substr($data->createdDatetime,0,16);?></td>
                        <td><i class="fa fa-list" aria-hidden="true"></i>
                            <?php if(!is_null($data->turnTitle)): ?>
                                <?=$data->turnTitle." (".$data->validfrom." - ".$data->validto.")";?>
                            <?php else: ?>
                                nincs turnus megadva.
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4"><?=$data->comment;?></td>
                    </tr>
                <?php endforeach;?>
            </table>                
            </div>
        <?php endif; ?>
    </article>

</div>
<script>
    $("#contactForm").submit(function(e) {
        //e.preventDefault();

        var empty = 0;
        $("#contactForm").find('.required').each(function() {
            if ($(this).val().length < 2) {
                empty++;
            }
        });

        if (empty > 0) {
            $.notify(
                "Kérem töltse ki az összes kötelező mezőt!", {
                    type: "warning",
                    placement: {
                        from: "bottom",
                        align: "center"
                    },
                    offset: 100,
                    delay: 1000
                }
            );
            return false;
        }
    })


    $("#ratingForm").submit(function(e) {
        //e.preventDefault();

        var empty = 0;
        $("#ratingForm").find('.required').each(function() {
            if ($(this).val().length < 2) {
                empty++;
            }
        });

        if (empty > 0) {
            $.notify(
                "Kérem töltse ki az összes kötelező mezőt!", {
                    type: "warning",
                    placement: {
                        from: "bottom",
                        align: "center"
                    },
                    offset: 100,
                    delay: 1000
                }
            );
            return false;
        }
    })

    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox({
            showArrows: true,
            alwaysShowClose: true
        });
    });


</script>