<?php

$campID = (int) $this->uri->segment(2);
$turnID = (int) $this->uri->segment(3);

$camp = $this->m_fcamp->getCampData($campID, true);
if (!$camp) {
    message("", "Nincs ilyen tábor!", "danger");
    return;
} else {
    $contract = $this->db->from("tf_camp_contracts")->where("id", $camp->contractID)->get()->row();
    $turn = $this->db->from("tf_camp_turns")->where("id", $turnID)->get()->row();

    //if (($this->input->get("key") !== substr($contract->uniqID, -5)) && (!$turn->published || !$camp->published || !$contract->visible)) {
    if (($this->input->get("key") !== substr($contract->uniqID, -5)) && (!$turn->published || !$camp->published)) {
        message("", "Nincs ilyen nyílvános tábor, vagy a turnus nem elérhető!", "danger");
        return;       
    } 
}

?>
<article class="camp-details">
    <div class="row p10 header" style="background-image: url(<?=site_url($camp->imagePath);?>); ">
        <h3 class="title"><?=$camp->name;?></h3>
    </div>
    <div class="row desc p10"><?=character_limiter(strip_tags($camp->description, "b,i"), 200);?></div>
    <div class="row p10">
        <a href="<?=site_url("tabor/" . $campID . "/" . $this->tools->toslug($camp->name));?>"><button type="button" class="btn btn-sm btn-primary">Teljes tábor leírás &raquo;</button></a>
    </div>
</article>

<div class='row m10'>
<form method="post"id ="joinForm">
    <div class="row m10">
        <h3>Jelentkezés</h3>
    </div>
    <?php
        $turn = $this->db->from("tf_camp_turns")->where("id", $turnID)->where("published", "1")->where("validfrom >=", date("Y-m-d"))->get()->row();
        if (!$turn) {
            message("", "Nincs ilyen aktív turnus!", "danger");
            return;
        }
    ?>
    <div class="row m5">
        <label class="col-md-3 col-sm-12">Turnus:</label>
        <div class="col-md-9 col-sm-12">
            <?=$turn->title;?>
        </div>
    </div>

    <div class="row m5">
        <label class="col-md-3 col-sm-12r">Időpont:</label>
        <div class="col-md-9 col-sm-12">
            <?=$this->tools->huDate($turn->validfrom, false, false) . " - " . $this->tools->huDate($turn->validto, false, true);?>
        </div>
    </div>

    <div class="row m5">
        <label class="col-md-3 col-sm-12">Ár:</label>
        <div class="col-md-9 col-sm-12">
            <?=number_format($turn->price, 0, ",", ".");?> Ft/turnus
        </div>
    </div>


    <div class="row m5">
        <label class="col-md-12"><b>Kapcsolattartói adatok:</b></label>

        <div class="col-md-6 col-sm-12">
            <label>Szülő 1.</label>

            <div class="row">
                <label class="col-md-3 col-sm-12">Név *:</label>
                <div class="col-md-9 col-sm-12">
                    <input type="text" required="required" class="required form-control" id="name" name="name">
                </div>
            </div>

            <div class="row">
                <label class="col-md-3 col-sm-12">Telefon *:</label>
                <div class="col-md-9 col-sm-12">
                    <input type="text" required="required" class="required form-control" id="phone" name="phone">
                </div>
            </div>

            <div class="row">
                <label class="col-md-3 col-sm-12">E-mail *:</label>
                <div class="col-md-9 col-sm-12">
                    <input type="text" required="required" class="required form-control" id="email" name="email">
                </div>
            </div>

            <div class="row">
                <label class="col-md-3 col-sm-12"></label>
                <div class="col-md-9 col-sm-12">
                    <p class="small"><input type="checkbox" name="newsletter"> Iratkozzon fel a Táborfigyelő havi hírlevelére, hogy a táborokkal kapcsolatos kedvezményekről, hírekről értesíteni tudjuk!</p>
                </div>
            </div>
        </div>
        
        <div class="col-md-6 col-sm-12">
            <label>Szülő 2.</label>
            <div class="row">
                <label class="col-md-3 col-sm-12">Név:</label>
                <div class="col-md-9 col-sm-12">
                    <input type="text" class="form-control" id="name2" name="name2">
                </div>
            </div>
            
            <div class="row">
                <label class="col-md-3 col-sm-12">Telefon:</label>
                <div class="col-md-9 col-sm-12">
                    <input type="text" class="form-control" id="phone2" name="phone2">
                </div>
            </div>
            
            <div class="row">
                <label class="col-md-3 col-sm-12">E-mail:</label>
                <div class="col-md-9 col-sm-12">
                    <input type="text" class="form-control" id="email2" name="email2">
                </div>
            </div>

            <div class="row">
                <label class="col-md-3 col-sm-12"></label>
                <div class="col-md-9 col-sm-12">
                    <p class="small"><input type="checkbox" name="newsletter2"> Iratkozzon fel a Táborfigyelő havi hírlevelére, hogy a táborokkal kapcsolatos kedvezményekről, hírekről értesíteni tudjuk!</p>
                </div>
            </div>
        </div>

        <div class="row m5">
            <label class="col-md-12">Pontos levelezési cím *:</label>
            <div class="col-md-12">
                <input type="text" required="required" class="required form-control" id="address" name="address">
            </div>
        </div>

        <label class="col-md-12"><b>Jelentkezők adatai:</b></label>


        <div class="col-md-6 col-sm-12">
            <label>Résztvevő 1.</label>
            <div class="row">
                <label class="col-md-4 col-sm-12">Név *:</label>
                <div class="col-md-8 col-sm-12">
                    <input type="text" required="required" class="required form-control" id="child_1" data-id="<?$i;?>" name="child[1]" maxlength="50">
                </div>
            </div>

            <div class="row">
                <label class="col-md-4 col-sm-12">Születési dátum *:</label>
                <div class="col-md-8 col-sm-12">
                    <input type="date" required="required" class="required form-control" id="birth_1" name="birth[1]" maxlength="10">
                </div>
            </div>

            <div class="row">
                <label class="col-md-4 col-sm-12">TAJ szám:</label>
                <div class="col-md-8 col-sm-12">
                    <input type="text" class="form-control" id="insurance_1" name="insurance[1]" maxlength="15">
                </div>
            </div>

            <?php if ($camp->countryID != 1): ?>
                <div class="row">
                    <label class="col-md-4 col-sm-12">Útlevélszám:</label>
                    <div class="col-md-8 col-sm-12">
                        <input type="text" class="form-control" name="passport[1]" maxlength="10">
                    </div>
                </div>

                <div class="row">
                    <label class="col-md-4 col-sm-12">Útl. érv.:</label>
                    <div class="col-md-8 col-sm-12">
                        <input type="date" class="form-control" name="passport_valid[1]">
                    </div>
                </div>
            <?php endif;?>
            <hr>
        </div>

        <div class="col-md-6 col-sm-12">
            <label>Résztvevő 2.</label>
            <div class="row">
                <label class="col-md-4 col-sm-12">Név:</label>
                <div class="col-md-8 col-sm-12">
                    <input type="text" class="form-control" id="child_2" data-id="<?$i;?>" name="child[2]" maxlength="50">
                </div>
            </div>

            <div class="row">
                <label class="col-md-4 col-sm-12">Születésnap:</label>
                <div class="col-md-8 col-sm-12">
                    <input type="date" class="form-control" id="birth_2" name="birth[2]" maxlength="10">
                </div>
            </div>

            <div class="row">
                <label class="col-md-4 col-sm-12">TAJ szám:</label>
                <div class="col-md-8 col-sm-12">
                    <input type="text" class="form-control" id="insurance_2" name="insurance[2]" maxlength="15">
                </div>
            </div>

            <?php if ($camp->countryID != 1): ?>
                <div class="row">
                    <label class="col-md-4 col-sm-12">Útlevélszám:</label>
                    <div class="col-md-8 col-sm-12">
                        <input type="text" class="form-control" name="passport[2]" maxlength="10">
                    </div>
                </div>

                <div class="row">
                    <label class="col-md-4 col-sm-12">Útl. érv.:</label>
                    <div class="col-md-8 col-sm-12">
                        <input type="date" class="form-control" name="passport_valid[2]">
                    </div>
                </div>
            <?php endif;?>
            <hr>
        </div>

        <p>A *-al megjelölt mezők kitöltése kötelező!</p>

        <div class="row m5">
            <label class="col-md-12">Megjegyzés:</label>
            <div class="col-md-12">
                <textarea name="comment" class="form-control" rows="5"></textarea>
            </div>
        </div>

        <div class="row m5">
            <div class="col-md-12">
                <button type="submit" id="sendData" name="sendData" value="ok" class="btn btn-success">Adatok elküldése</button>
            </div>
        </div>
    </div>
</form>
</article>

<script>

    $("#sendData").click( function() {
        formValid = true;
        if ($("#name").val().length < 8) {
            $.notify({ message: 'Kérem töltse ki a `szülő 1` nevét (min 8 betű)!' },{ type: 'danger' });
            formValid = false;
        } else {
            if ($("#email").val().length < 10) {
                $.notify({ message: 'Kérem ellenőrizze le, töltse ki a `szülő 1` e-mail címét!' },{ type: 'info' });
            }

            if ($("#phone").val().length < 7) {
                $.notify({ message: 'Kérem ellenőrizze le, töltse ki a `szülő 1` telefonszámát!' },{ type: 'danger' });
                formValid = false;
            }
        }

        if ($("#name2").val().length > 5) {
            if ($("#email2").val().length < 10) {
                $.notify({ message: 'Kérem ellenőrizze le, töltse ki a `szülő 2` e-mail címét!' },{ type: 'info' });
            }

            if ($("#phone2").val().length < 7) {
                $.notify({ message: 'Kérem ellenőrizze le, töltse ki a `szülő 2` telefonszámát!' },{ type: 'danger' });
                formValid = false;
            }
        }

        if ($("#address").val().length < 10) {
            $.notify({ message: 'Kérem töltse ki a `levelezési cím` mezőt!' },{ type: 'danger' });
            formValid = false;
        }

        if ($("#child_1").val().length < 8) {
            $.notify({ message: 'Kérem ellenőrizze le, töltse ki a `résztvevő 1` nevét (min 8 betű)!' },{ type: 'danger' });
                formValid = false;
        }

        if ($("#birth_1").val().length < 8) {
            $.notify({ message: 'Kérem ellenőrizze le, töltse ki a `résztvevő 1` születési dátumát!' },{ type: 'danger' });
            formValid = false;
        }
/*
        if ($("#insurance_1").val().length < 8) {
            $.notify({ message: 'Kérem ellenőrizze le, töltse ki a `résztvevő 1` TAJ számát!' },{ type: 'danger' });
            formValid = false;
        }
*/
        if ($("#child_2").val().length > 0) {
            if ($("#child_2").val().length < 8) {
                $.notify({ message: 'Kérem ellenőrizze le, töltse ki a `résztvevő 2` nevét (min 8 betű)!!' },{ type: 'danger' });
                formValid = false;
            }

            if ($("#birth_2").val().length < 8) {
                $.notify({ message: 'Kérem ellenőrizze le, töltse ki a `résztvevő 2` születési dátumát!' },{ type: 'danger' });
                formValid = false;
            }
/*
            if ($("#insurance_2").val().length < 8) {
                $.notify({ message: 'Kérem ellenőrizze le, töltse ki a `résztvevő 2` TAJ számát!' },{ type: 'danger' });
                formValid = false;
            }
*/         
        }


        if (formValid) {
            $("#joinForm").submit();
        } else {
            return false;
        }
    });

</script>