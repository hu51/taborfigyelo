<div class="">
	<h2>Táborhelyszín szerkesztése:</h2>
	
	<form id="location_form" action="" method="post">
<?php

    $contractID = (int) $this->session->userdata("user_contractID");

    $locID = (int)filter_var($this->uri->segment(2));
    $location = $this->db->from("tf_camps")->where("id", $locID)->where("contractID", $contractID)->where("id", $locID)->get()->row();	
    $contract = $this->db->from("tf_camp_contracts")->where("id", $location->contractID)->get()->row();

    if (!$location) {
        redirect("taborhelyek");
    }
    
    if ($location->closed) {
        message("", "Ennek a helyszínnek az adatai már nem módosíthatók!", "warning");
    }

    $all     = $this->db->from("tf_camp_types")->order_by("name", "ASC")->get()->result();
    $typeList = [];
    foreach ($all as $tmp) {
        $typeList[$tmp->id] = $tmp->name;
    }
    
    $all     = $this->db->from("tf_camp_category_list")->order_by("name", "ASC")->get()->result();
    $catList = [];
    foreach ($all as $tmp) {
        $catList[$tmp->id] = $tmp->name;
    }
    
    $all         = $this->db->from("tf_camp_countries")->order_by("name", "ASC")->get()->result();
    $countryList = array("-1" => "* vagy válasszon a listából");
    foreach ($all as $tmp) {
        $countryList[$tmp->id] = $tmp->name;
    }
    
    $all       = $this->db->from("tf_camp_states")->order_by("name", "ASC")->get()->result();
    foreach ($all as $tmp) {
        $stateList[$tmp->id] = $tmp->name;
    }    

    $all       = $this->db->from("tf_camp_locations")->order_by("name", "ASC")->get()->result();
    foreach ($all as $tmp) {
        $locationList[$tmp->id] = $tmp->name;
    }    
    
?>
        <input type="hidden" name="locID" value="<?= $locID; ?>">
        <br />

        <?php if (!$location->closed) : ?>
    	<div>    
            <div class="col-lg-12">
                <label for="contactName" class="col-lg-4 ar"></label>
                <div class="col-lg-8">
                    <button type="submit" class="btn btn-success" value="1" id="saveLocation" name="saveLocation">Adatok mentése</button>
                </div>
            </div>
        </div>
        <?php endif; ?>  
        
    	<div>
            <h3>Kapcsolati adatok</h3>
            <p class="text-warning">Ezen adatokat a táborjelentkezők levélben kapják meg!</p>
    		<div class="col-lg-12">
    			<label for="contactName" class="col-lg-4 ar">Táborszervező neve:</label>
    			<div class="col-lg-8">
    				<input type="text" class="form-control required"id="contactName" name="contactName" value="<?= $location->contactName; ?>">
    			</div>
            </div>

    		<div class="col-lg-12">
    			<label for="contactPhone" class="col-lg-4 ar">Táborszervező telefonszáma:</label>
    			<div class="col-lg-8">
    				<input type="text" class="form-control required"id="contactPhone" name="contactPhone" value="<?= $location->contactPhone; ?>">
    			</div>
            </div>

    		<div class="col-lg-12">
    			<label for="contactEmail" class="col-lg-4 ar">Kapcsolattartó e-mail:</label>
    			<div class="col-lg-8">
    				<input type="text" class="form-control required"id="contactEmail" name="contactEmail" value="<?= $location->contactEmail; ?>">
    			</div>
            </div>

    		<div class="col-lg-12">
    			<label for="name" class="col-lg-4 ar">Weboldal címe:</label>
    			<div class="col-lg-8">
    				<input type="text" class="form-control required"id="contactWebsite" name="contactWebsite" value="<?= $location->contactWebsite; ?>">
    			</div>
            </div>

    		<div class="col-lg-12">
    			<label for="name" class="col-lg-4 ar">Facebook oldal:</label>
    			<div class="col-lg-8">
    				<input type="text" class="form-control required"id="contactFacebook" name="contactFacebook" value="<?= $location->contactFacebook; ?>">
    			</div>
            </div>

        </div>
        <hr style="clear: both;" />

        <div>
            <h3>Tábor neve és címe</h3>    
            
            <p class="text-info">Rövid, de frappáns nevet érdemes megadni. Nem elég, hogy Sporttábor, hanem Sporttábor Érden, vagy Izgalmas sporttábor, vagy Sporttábor kézműves foglalkozásokkal.</p>      
    		<div class="col-lg-12">
                <label for="name" class="col-lg-4 ar">Táborhely megnevezése:</label>
    			<div class="col-lg-8">
                    <input type="text" class="form-control required"id="name" name="name" value="<?= $location->name; ?>">
    			</div>
    		</div>
    		
            <p class="text-warning">Nem a táborszervező címét kérjük, hanem a táborhelyszín pontos címét, ahol a gyerekek üdülnek!</p>
            <br>
            <div class="col-lg-12">
                <label for="address" class="col-lg-4 ar">Ország:</label>
                <div class="col-lg-8">
                    <input type="text" class="form-control required"id="country" name="country" value="<?= $location->country_str; ?>">
                    <?=$this->tools->bs_select("countryID", $location->countryID, $countryList, "form-control highlight");?>
                </div>
            </div>
            
            <div class="col-lg-12">
                <label for="address" class="col-lg-4 ar">Megye:</label>
                <div class="col-lg-8">
                    <input type="text" class="form-control required"id="state" name="state" value="<?= $location->state_str; ?>">
                    <?php if ($location->stateID): ?>
                        <?=$this->tools->bs_select("stateID", $location->stateID, $stateList, "form-control highlight");?>
                        <?php else: ?>
                            <select class="form-control highlight" name="stateID" id="stateID" style="display: none;"></select>
                            <?php endif; ?>
                        </div>
                    </div>
                    
                    <div class="col-lg-12">
                        <label for="address" class="col-lg-4 ar">Helyszín:</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control required"id="location" name="location" value="<?= $location->location_str; ?>">
                            <?php if ($location->stateID): ?>
                                <?=$this->tools->bs_select("locationID", $location->locationID, $locationList, "form-control highlight");?>
                                <?php else: ?>                    
                                    <select class="form-control highlight" name="locationID" id="locationID" style="display: none;"></select>
                                    <?php endif; ?>
                                </div>
                            </div>
                            
                            
                            <div class="col-lg-12">
                                <label for="address" class="col-lg-4 ar">Irányítószám:</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control required w100" id="zip" name="zip" maxlength="10" value="<?= $location->zip; ?>">
                                </div>
                            </div>	
                            
                            
                            <div class="col-lg-12">
                                <label for="address" class="col-lg-4 ar">Település:</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control required"id="city" name="city" value="<?= $location->city; ?>">
                                </div>
                            </div>	
                            
                            <div class="col-lg-12">
    			<label for="address" class="col-lg-4 ar">Utca, házszám:</label>
    			<div class="col-lg-8">
                    <input type="text" class="form-control required"id="address" name="address" value="<?= $location->address; ?>">
    			</div>
            </div>
            
        </div>
        <hr style="clear: both;" />

        <div>   
            <h3>Tábor paraméterek</h3> 
            <p class="text-info">Az űrlap kitöltésekor ne adjanak meg linket, konkrét kontaktadatokat, elérhetőséget, ezeket a jelentkezők megkapják e-mailben a Táborfigyelőtől! Kérjük, az űrlap kitöltésekor figyeljenek a helyesírásra!</p>      
            <br>
    		<div class="col-lg-12">
                <label for="facebook" class="col-lg-4 ar">Táborminősítő.hu bejegyzés azonosítója:</label>
    			<div class="col-lg-8">
                    <input type="text" class="form-control w200" id="minositoID" name="minositoID" value="<?= $location->minositoID; ?>">
    			</div>
            </div>
            
    		<div class="col-lg-12">
                <label for="facebook" class="col-lg-4 ar">Tábor típusa:</label>
    			<div class="col-lg-8">
                    <?=$this->tools->bs_select("typeID", $location->typeID, $typeList, "form-control");?>
    			</div>
    		</div>
            
            <p class="text-info">Maximum 4-5 mondat, kifejezetten marketing célra, azaz figyelemfelhívó jelleggel tartalmazza tömören a tábor lényegét!</p>      
    		<div class="col-lg-12">
                <label for="description" class="col-lg-4 ar">Rövid leírás:</label>
    			<div class="col-lg-8">
                    <textarea class="form-control required" name="description" rows="6"><?= strip_tags($location->description); ?></textarea>
                    <p>Minden hivatkozás automatikusan eltávolításra kerül a mentéskor!</p>
    			</div>
            </div>
            
            <p class="text-info">Itt kérjük a tábor összes olyan információját, amit külön pontban nem kellett megadni.</p>      
    		<div class="col-lg-12">
                <label for="text" class="col-lg-4 ar">Hosszú leírás:</label>
    			<div class="col-lg-8">
                    <textarea class="form-control htmlLightEditor" name="text" rows="7"><?= $location->text; ?></textarea>
                    <p>Minden hivatkozás automatikusan eltávolításra kerül a mentéskor!</p>
    			</div>
            </div>

            <div class="col-lg-12">
                <label for="text" class="col-lg-4 ar">Napi programterv:</label>
    			<div class="col-lg-8">
                    <textarea class="form-control htmlLightEditor" name="program" rows="7"><?= $location->program; ?></textarea>
    			</div>
            </div>            
            
    		<div class="col-lg-12">
                <label for="text" class="col-lg-4 ar">Egy táborturnus ára:</label>
    			<div class="col-lg-8">
                    <input type="text" class="form-control required w200" id="price" name="price" maxlength="8" value="<?= $location->price; ?>">
    			</div>
            </div>
            
    		<div class="col-lg-12">
                <label class="col-lg-4 ar">Tábortémák száma:</label>
    			<div class="col-lg-8">
                    <div class="badge"><?= $location->themes; ?></div>
    			</div>
    		</div>

            <div class="col-lg-12">   
                <label class="col-lg-12 ac text-danger">A tábortémák közül az lesz a főkategória, amit először pipálnak be (zölden megjelölve)! Ez a téma legyen a leginkább jellemző a táborra. Ha választottak altémákat, úgy azok sorrendjének megadása nem releváns.</label>             
                <?php

                foreach ($catList as $id => $name): 
                    $tmp = $this->db->from("tf_camp_categories")->where("campID", $location->id)->where("categoryID", $id)->get()->row();
                    $cls = ((int)$location->categoryID == (int)$id)?"text-success":"";
                    ?>
                <div class="col-md-4 <?= $cls; ?>">
                    <?php if($tmp): ?>
                        <input type='checkbox' name='categories[]' class='categories' value='<?= $id; ?>' checked> <?= $name; ?><br>
                    <?php else: ?>
                        <input type='checkbox' name='categories[]' class='categories' value='<?= $id; ?>'> <?= $name; ?><br>
                    <?php endif; ?>
                    </div>
                <?php endforeach;?>
                <input type="hidden" name="categoryID" id="categoryID" value="<?= ($location->categoryID=="0")?"":$location->categoryID; ?>">           
            </div>
            <br />

    		<div class="col-lg-12">
    			<label for="facebook" class="col-lg-4 ar">Életkor (tól-ig):</label>
    			<div class="col-lg-4">
                    <input type="number" class="form-control required" id="age_min" name="age_min" value="<?= $location->age_min; ?>">
    			</div>
    			<div class="col-lg-4">
                    <input type="number" class="form-control required" id="age_max" name="age_max" value="<?= $location->age_max; ?>">
    			</div>
    		</div>            

    		<div class="col-lg-12">
    			<label for="facebook" class="col-lg-4 ar">Szépkártya elfogadás:</label>
    			<div class="col-lg-8">
                    <div class="row">
                        <div class="col-md-4">
                            MKB
                        </div>
                        <div class="col-md-8">
                            <?=$this->tools->bs_checkbox("szep_MKB", $location->szep_MKB, array("0" => "Nem", "1" => "Igen"));?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            OTP
                        </div>
                        <div class="col-md-8">
                            <?=$this->tools->bs_checkbox("szep_OTP", $location->szep_OTP, array("0" => "Nem", "1" => "Igen"));?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            K&H
                        </div>
                        <div class="col-md-8">
                            <?=$this->tools->bs_checkbox("szep_KH", $location->szep_KH, array("0" => "Nem", "1" => "Igen"));?>
                        </div>
                    </div>
    			</div>
    		</div>            
    		
    	</div>

        <?php if (!$location->closed): ?>
    	<div>
            <div class="col-lg-12">
                <p class="text-info">A Táborfigyelőn csak olyan minőségi táborokat jelenítünk meg, amelyek a táborszervezési szakmai alapok szerint rendezik táboraikat, és elfogadják a <a href="<?= site_Url("taboros-szakmai-minimumprogram.html"); ?>" target="_blank">minimumprogramot</a>.</p>      
                <input type="checkbox" id="progrCheck"  name="progrCheck" class="required fl m10"> Táborainkat a <a href="<?= site_Url("taboros-szakmai-minimumprogram.html"); ?>" target="_blank">Táboros szakmai minimumprogram</a> szellemében szervezzük!
            </div>                
    		<div class="col-lg-12">
                <button type="submit" class="btn btn-success" value="1" id="saveLocation" name="saveLocation">Adatok mentése</button>
            </div>
        </div>
        <?php endif; ?>        

    </form>
<br style="clear: both; "/>
<script>

    var limit = <?= $location->themes; ?>
    
    $('.categories').on('change', function(evt) {        
        $(this).parent().removeClass("text-success");
        if($('.categories:checked').length > limit) {
            this.checked = false;
            $.notify(
                "Nem jelölhető be több téma! (max: "+limit+")",
                {
                    type: "warning",
                    placement: {
                        from: "bottom",
                        align: "center"
                    },
                    offset: 100,
                    delay: 1000
                }
            );
        }

        if($('.categories:checked').length == 0) {
            $("#categoryID").val("");
        }

        if ($(this).is(":checked") && $("#categoryID").val()=="") {
            $(this).parent().addClass("text-success");
            $("#categoryID").val($(this).val());
        }
    });

    $("#country").keyup(function (e) {       
        $('#countryID option').eq(0).prop('selected', true);
    });

    $("#state").keyup(function (e) {       
        $('#stateID option').eq(0).prop('selected', true);
    });

    $("#location").keyup(function (e) {       
        $('#locationID option').eq(0).prop('selected', true);
    });

    $("#countryID").change(function() {
        id = $(this).val();

        $("#stateID").show();

        if ($("#countryID option:selected").val() != -1) {
            $("#country").val($("#countryID option:selected").text());
        }

        $("#stateID option").remove();
        $("#locationID option").remove();

        var $dropdown = $("#stateID");
        $dropdown.append($("<option />").val("-1").text("# vagy válasszon a listából" ));        
        
        $.ajax({
            type: "get",
            url: "<?= site_url("ajax/statelist"); ?>",
            data: { q: id },
            dataType: "json",
            success: function (response) {          
                if (response.status == "ok") {
                    var $dropdown = $("#stateID");
                    $.each(response.data, function() {
                        $dropdown.append($("<option />").val(this.id).text(this.name));
                    });

                    $("#stateID").onchange;
                }
            }
        });
    });

    $("#stateID").change(function() {
        id = $(this).val();
        $("#locationID option").remove();
        var $dropdown = $("#locationID");
        $dropdown.append($("<option />").val("-1").text("# vagy válasszon a listából" ));

        $("#locationID").show();

        if ($("#stateID option:selected").val() != -1) {
            $("#state").val($("#stateID option:selected").text());
        }

        $.ajax({
            type: "get",
            url: "<?= site_url("ajax/locationList"); ?>",
            data: { q: id },
            dataType: "json",
            success: function (response) {          
                if (response.status == "ok") {
                    $.each(response.data, function() {
                        $dropdown.append($("<option />").val(this.id).text(this.name));
                    });
                }
            }
        });
    });
    
    $("#locationID").change(function() {
        if ($("#locationID option:selected").val() != -1) {
            $("#location").val($("#locationID option:selected").text());
        }
    });
    
    $("#zip").keyup(function() {
        id = $(this).val();

        $.ajax({
            type: "get",
            url: "<?= site_url("ajax/zip2city"); ?>",
            data: { q: id },
            dataType: "json",
            success: function (response) {          
                if (response.status == "ok") {
                    $("#city").val(response.data.city);
                }
            }
        });
    });


    $("#location_form").submit( function(e) {
        //e.preventDefault();

        var empty = 0;
        $("#location_form").find('.required').each(function(){         
            if($(this).val().length < 1) {
                $(this).css("border", "10px solid orange");
                empty++;
            }
        });   

        if(!$("#progrCheck").is(":checked")) {
            $.notify(
                "Nincs a Táboros szakmai minimumprogram elfogadva!",
                {
                    type: "info",
                    placement: {
                        from: "bottom",
                        align: "center"
                    },
                    offset: 100,
                    delay: 2000
                }
            );
            empty++;
        }

        if($('.categories:checked').length == 0) {
            $.notify(
                "Nincs egyetlen egy kategória sem megjelölve!",
                {
                    type: "info",
                    placement: {
                        from: "bottom",
                        align: "center"
                    },
                    offset: 100,
                    delay: 2000
                }
            );
            empty++;        
        }

        if (empty > 0) {
            $.notify(
                "Kérem töltse ki az összes kötelező mezőt (pirossal jelölve)!",
                {
                    type: "warning",
                    placement: {
                        from: "bottom",
                        align: "center"
                    },
                    offset: 100,
                    delay: 2000
                }
            );
            return false;
        }
    })    

</script>
<script src="<?=base_url();?>js/tinymce/tinymce.min.js" type="text/javascript"></script>
<script>

    tinymce.init({
        mode: "specific_textareas",
        editor_selector: "htmlLightEditor",
        height: 400,
        menubar:false,
        theme: 'modern',
        plugins: ' directionality visualblocks visualchars hr nonbreaking wordcount code paste',
        paste_auto_cleanup_on_paste : true,
        paste_convert_headers_to_strong : false,
        paste_strip_class_attributes : "all",
        paste_remove_spans : true,
        paste_remove_styles : true,        
        valid_elements: "p[style],strong/b,div[align],br",
        toolbar1: ' bold italic strikethrough forecolor backcolor | alignleft aligncenter alignright alignjustify | removeformat code',        
        content_css: [
            '//www.tinymce.com/css/codepen.min.css'
        ],
        relative_urls: false
    });
</script>
