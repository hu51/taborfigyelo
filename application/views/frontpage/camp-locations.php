<?php

$contractID = (int) $this->session->userdata("user_contractID");
$contract   = $this->db->from("tf_camp_contracts")->where("id", $contractID)->get()->row();
$locations  = $this->db->from("tf_camps")->where("contractID", $contractID)->order_by("name")->get()->result();

if (!$contract->closed || ($this->session->userdata("user_isAdmin"))): ?>
	<h2>Táborhelyszínek:</h2>

    <div class="row p10">    
        <?= $this->tools->GetHtml("camp-locations-message"); ?>
    </div>
    <br>
    <?php if (is_Null($contract->campEditDone) || ($this->session->userdata("user_isAdmin"))): ?>
        <table class="table table-bordered">
            <?php foreach ($locations as $loc):
                $pics  = $this->db->from("tf_camp_images")->where("campID", $loc->id)->get()->result();
                $turns = $this->db->from("tf_camp_turns")->where("campID", $loc->id)->count_all_results();

                $smallPic = false;
                foreach ($pics as $id => $photo) {
                    $dimension = @getimagesize(FCPATH.$photo->path);                
                    if (($dimension[0] < 800) || ($dimension[1] < 600)) {
                        $smallPic = true;
                    }                   
                }                

                if ($loc->closed && !$this->session->userdata("user_isAdmin")): ?>
                    <tr>
                        <td><?=anchor("taborhely/" . $loc->id, "<i class='glyphicon glyphicon-edit'></i> " . $loc->name);?></td>
                        <td class="w150"><i class="glyphicon glyphicon-picture"></i> Képek (<?=count($pics);?> db)</td>
                        <td class="w200"><i class="glyphicon glyphicon-refresh"></i> Turnusok (<?=$turns;?> db)</td>
                        <?php if ($loc->published): ?>
                            <td class="w150"><a href="<?=site_url("tabor/" . $loc->id . "/" . $loc->alias);?>" target="_blank"><button class='btn btn-sm btn-success'><i class="glyphicon glyphicon-link"></i> Publikus</button></a></td>
                        <?php else: ?>
                            <td class="w150"><a href="<?=site_url("tabor/" . $loc->id . "/" . $loc->alias . "?key=" . substr($contract->uniqID, -5));?>" target="_blank"><button class='btn btn-sm btn-info'><i class="glyphicon glyphicon-link"></i> Előnézet</button></a></td>
                        <?php endif;?>
                        <td class="w150"><a href="<?=site_url("tabor/" . $loc->id . "/" . $loc->alias . "?ertekeles=" . ($loc->id+$loc->contractID));?>#rating" target="_blank"><button class='btn btn-sm btn-info'><i class="glyphicon glyphicon-link"></i> Értékelő oldal</button></a></td>
                    </tr>
                    <?php else: ?>
                    <tr>
                        <td><?=anchor("taborhely/" . $loc->id, "<i class='glyphicon glyphicon-edit'></i> " . $loc->name);?></td>
                        <td class="w150">
                            <a href="<?=site_url("taborkepek/" . $loc->id);?>"><button class='btn btn-sm btn-<?=(count($pics) > 0) ? "default" : "warning";?>'><i class="glyphicon glyphicon-picture"></i> Képek (<?=count($pics);?> db)</button></a>
                            <?php if($smallPic): ?>
                                <br style="clear: both;">
                                <span class="text-warning"  title="Kérjük, a képeket cseréljék le nagyobb méretű/felbontású képekre!"><i class="glyphicon glyphicon-alert"></i> Kis képek!</span>
                            <?php endif; ?>
                        </td>
                        <td class="w200"><a href="<?=site_url("taborturnusok/" . $loc->id);?>"><button class='btn btn-sm btn-<?=($turns > 0) ? "default" : "warning";?>'><i class="glyphicon glyphicon-refresh"></i> Turnusok (<?=$turns;?> db)</button></a></td>
                        <td class="w150">
                            <a href="<?=site_url("tabor/" . $loc->id . "/" . $loc->alias . "?key=" . substr($contract->uniqID, -5));?>" target="_blank"><button class='btn btn-sm btn-info'><i class="glyphicon glyphicon-link"></i> Előnézet</button></a>
                            <?php if ($loc->published): ?>
                                <a href="<?=site_url("tabor/" . $loc->id . "/" . $loc->alias);?>" target="_blank"><button class='btn btn-sm btn-success'><i class="glyphicon glyphicon-link"></i> Publikus</button></a>
                            <?php endif;?>
                        </td>
                        <td class="w150"><a href="<?=site_url("tabor/" . $loc->id . "/" . $loc->alias . "?ertekeles=" . ($loc->id+$loc->contractID));?>#rating" target="_blank"><button class='btn btn-sm btn-info'><i class="glyphicon glyphicon-link"></i> Értékelő oldal</button></a></td>
                    </tr>
                <?php endif;?>
            <?php endforeach;?>
		</table>

        <?php if(!is_null($contract->contractPaid)): ?>
            <form action="" id="saveForm" method="post">
                <input type="hidden" name="saveLocations" value="ok" />
                <button type="button" id="openModal" class="btn btn-primary ac" data-toggle="modal" data-target="#saveModal">A táborhelyszín adatokat LEZÁROM</button>
            </form>
        <?php endif; ?>

		<script>

		$('#openModal').click( function(){
			$("#saveModal").show();
		});

		$(document).on('click', '.modalClose', function() {
			$("#saveModal").hide();
		});

		$(document).on('click', '.modalSave', function() {
			$("#saveModal").hide();

			chk = $("#confSave").prop("checked");
			if (chk) {
				$("#saveForm").submit();
			} else {
				$.notify(
					"Nincs a megerősítő pipa bejelölve.",
					{
						type: "warning",
						placement: {
							from: "top",
							align: "center"
						},
						offset: 50,
						delay: 2000
					});
			}
		});

		</script>

		<div class="modal" tabindex="-1" id="saveModal" role="dialog">
			<div class="modal-dialog modal-info" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Helyszínek lezárása</h5>
						<button type="button" class="close modalClose" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<p><input type="checkbox" id="confSave" name="confSave" value="ok"> Lezárás megerősítése.</p>
						<p>&nbsp;</p>
						<p>A lezárással a helyszínek adatai már nem szerkeszthetők! Kérjük ellenőrizze le mielőtt biztosan lezárja a helyszíneket.</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-success modalSave" name="saveLocations" value="ok">LEZÁROM</button>
						<button type="button" class="btn btn-secondary modalClose" data-dismiss="modal">Vissza</button>
					</div>
				</div>
			</div>
		</div>
	<?php else: ?>
		<?=message("", "A táborhelyszín adatok zárolva vannak.", "info");?>
	<?php endif;?>

<?php endif;?>