<div class="">
    <h2>Táborhelyszín képeinek szerkesztése:</h2>
<?php

$contractID = (int) $this->session->userdata("user_contractID");

$locID    = (int) filter_var($this->uri->segment(2));
$location = $this->db->from("tf_camps")->where("id", $locID)->where("contractID", $contractID)->where("id", $locID)->get()->row();
$contract = $this->db->from("tf_camp_contracts")->where("id", $location->contractID)->get()->row();

if (!$location) {
    redirect("taborhelyek");
}

if ($location->closed && !$this->session->userdata("user_isAdmin")) {
    message("", "Ennek a helyszínnek adatai már nem módosíthatók!", "warning");
}

?>
    <div class="row">
        <div>Tábor neve: <b><?= $location->name; ?></b></div>
    </div>
    <br>
    <?php if(!$location->closed || $this->session->userdata("user_isAdmin")): ?>
        <div class="row">
            <p>A képek át lesznek automatikusan méretezve 1200x900 képpont nagyságúra, ezért kérjük minél nagyobb méretű képeket töltsenek fel! Csak JPEG és PNG kép tölthető fel.</p>
            <div id="drop-area" style="padding: 10px; border: 2px dotted gray; cursor: pointer;">
                <h3 class="drop-text ac p10">Húzzon ide képeket a feltöltésükhöz!</h3>
            </div>
        </div>
        <form id="manualForm" class="form-inline" enctype="multipart/form-data">
            <input type="file" id="userImage" style="float: left;" />
            <input type="button" id="manualUpload" class="btn btn-sm btn-primary" value="Kézi feltöltés">
        </form>
    <?php endif; ?>

    <h4>Képek:</h4>

    <form id="photos">
        <input type="hidden" name="contractID" value="<?=$contractID;?>" />
        <input type="hidden" name="campID" value="<?=$locID;?>" />
        <div id="photolist" class="p5" style="border: 1px dotted gray;"></div>
    </form>
<script>

    $(document).ready(function() {
        $("#drop-area").on('dragenter', function (e){
            e.preventDefault();
            $(this).css('background', '#BBD5B8');
        });

        $("#drop-area").on('dragover', function (e){
            e.preventDefault();
        });

        $("#drop-area").on('drop', function (e){
            savePhotos();

            $(this).css('background', '#D8F9D3');
            e.preventDefault();
            var image = e.originalEvent.dataTransfer.files;
            createFormData(image);
        });
    });

    function createFormData(image)
    {
        for(i=0; i<image.length; i++) {
            var formData = new FormData();
            formData.append("contractID", <?=$contractID;?>);
            formData.append("campID", <?=$locID;?>);
            formData.append('userImage', image[i]);
            uploadFormData(formData);
        }
    }

    $("#manualUpload").click( function(){
        var image = document.getElementById("userImage").files[0];
        var formData = new FormData();
        formData.append("contractID", <?=$contractID;?>);
        formData.append("campID", <?=$locID;?>);
        formData.append('userImage', image);
        uploadFormData(formData);
        document.getElementById("manualForm").reset(); 
    });

    function uploadFormData(formData)
    {
        $.ajax({
            url: "<?=site_url("ajax/camp_upload_image");?>",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            cache: false,
            success: function(data) {
                data = JSON.parse(data);
                if (data.status == "ok") {
                    $.notify(
                        data.message,
                        {
                            type: "success",
                            placement: {
                                from: "bottom",
                                align: "right"
                            },
                            offset: 20,
                            delay: 1000
                        }
                    );
                } else {                    
                    $.notify(
                        data.message,
                        {
                            type: "danger",
                            placement: {
                                from: "top",
                                align: "left"
                            },
                            offset: 20,
                            delay: 1000
                        }
                    );
                }
                $("#drop-area").css('background', 'transparent');
                reloadImageList();
            }
        });
    }

    function reloadImageList()
    {
        $("#photolist").html('loading...');
        $.ajax({
            url: "<?=site_url("ajax/camp_imagelist");?>",
            type: "POST",
            data: { contractID: '<?=$contractID;?>', campID: '<?=$locID;?>' },
            dataType: "JSON",
            success: function(result) {
                $("#photolist").html('');

                if (result.data.length > 0) {
                    $('<button type="button" class="savePhotos btn btn-success">Mentés</button>').appendTo("#photolist");

                    $.each(result.data, function(i, data) {
                        if (data.published == 1) {
                            chk = " checked='checked' ";
                        } else {
                            chk = "";
                        }
                        content = "<div class='row' id='picrow_"+data.id+"'>"
                            +"<input type='hidden' name='id["+data.id+"]' value='"+data.id+"' >"
                            +"<div class='col-md-4'>"
                            +"Név:<input type='text' class='form-control' name='name["+data.id+"]' value='"+data.name+"' >"
                            +"</div>"
                            +"<div class='col-md-2'>"
                            +"Látható: <input type='checkbox' name='published["+data.id+"]' "+chk+" ><br>"
                            +"<button type='button' class='btn btn-danger deletePic' value='"+data.id+"'>Kép törlése</button><br>"
                            +"<button type='button' class='btn btn-primary rotatePic' data-dir='left' data-id='"+data.id+"'><i class='fa fa-rotate-left'></i></button>"
                            +"<button type='button' class='btn btn-primary rotatePic' data-dir='right' data-id='"+data.id+"'><i class='fa fa-rotate-right'></i></button>"
                            +"</div>"
                            +"<div class='col-md-2'>"
                            +"Sorszám:<input type='text' class='form-control' name='ordering["+data.id+"]' value='"+data.ordering+"' >"
                            +"</div>"
                            +"<div class='col-md-4'>"
                            +"<a target='_blank' href='<?=site_url();?>"+data.path+"'>"
                            +"<img src='<?=site_url();?>"+data.path.replace("ties/images/", "ties/thumbs/")+"' class='img-thumbnail' >"
                            +"</a>"
                            +"</div>"
                            +"</div>";

                        $(content).appendTo("#photolist");

                        if ((data.dimension[0] < 800) || (data.dimension[1] < 600)) {
                            content = "<p class='alert alert-warning'>Kérjük, a képet cseréljék le nagyobb méretű/felbontású képre!</p>";
                            $(content).appendTo("#photolist");
                        }
                    });
                    $('<button type="button" class="savePhotos btn btn-success">Mentés</button>').appendTo("#photolist");
                } else {
                    $('<div class="alert alert-info">Nincs még kép feltöltve.</div>').appendTo("#photolist");
                }
            }
        });
    }

    $(document).on("click", ".deletePic", function() {
        if (confirm("Biztosan törlöd a képet?")) {
            picID = $(this).val();
            $.ajax({
                url: "<?=site_url("ajax/camp_image_delete");?>",
                type: "POST",
                data: { contractID: '<?=$contractID;?>', campID: '<?=$locID;?>', picID: picID },
                dataType: "JSON",
                success: function(result) {
                    if (result.status == "ok") {
                        $.notify( 
                            "A kép sikeresen törölve",
                            {
                                type: "success",
                                placement: {
                                    from: "bottom",
                                    align: "right"
                                },
                                offset: 20,
                                delay: 1000
                            }          
                        );
                        $("#picrow_"+picID).remove();
                    } else {                        
                        $.notify(
                            result.message,
                            {
                                type: "danger",
                                placement: {
                                    from: "bottom",
                                    align: "right"
                                },
                                offset: 20,
                                delay: 1000
                            }
                        );   
                    }
                }
            });
        }
    });

    $(document).on("click", ".rotatePic", function() {
        picID = $(this).data("id");
        $.ajax({
            url: "<?=site_url("ajax/camp_image_rotate");?>",
            type: "POST",
            data: { contractID: '<?=$contractID;?>', campID: '<?=$locID;?>', picID: picID, dir: $(this).data("dir") },
            dataType: "JSON",
            success: function(result) {
                if (result.status == "ok") {
                    $.notify( 
                        "Érdemes az oldalt újratölteni! (F5)",
                        {
                            type: "success",
                            placement: {
                                from: "bottom",
                                align: "right"
                            },
                            offset: 20,
                            delay: 1000
                        }          
                    );
                    reloadImageList();
                } else {                        
                    $.notify(
                        result.message,
                        {
                            type: "danger",
                            placement: {
                                from: "bottom",
                                align: "right"
                            },
                            offset: 20,
                            delay: 1000
                        }
                    );   
                }
            }
        });
    });


    function savePhotos()
    {
        $.ajax({
            url: "<?=site_url("ajax/camp_imagelist_modify");?>",
            type: "POST",
            data: $("#photos").serialize(),
            success: function(result) {
                result = JSON.parse(result);
                if (result.status == "ok") {
                    $.notify( 
                        "A képek adatai frissítve.",
                        {
                            type: "success",
                            placement: {
                                from: "bottom",
                                align: "right"
                            },
                            offset: 20,
                            delay: 1000
                        }          
                    );

                    setTimeout(function(){ window.location.href = '<?= site_url("taborhelyek"); ?>'; }, 1000);
                } else {
                    $.notify(
                        result.message,
                        {
                            type: "danger",
                            placement: {
                                from: "bottom",
                                align: "right"
                            },
                            offset: 20,
                            delay: 1000
                        }
                    );   
                }            
                reloadImageList();
            }
        });
    }

    $(document).on("click", ".savePhotos", function() {
        savePhotos();
    });

    $(document).ready( function()
    {
        reloadImageList();
    });

</script>