<?php
$title = url_title(convert_accented_characters($data->name));
?>
<div class="col-md-6 col-sm-12 p5">
    <div class="campdata-box row">
        <div class='featured-label'>Kiemelt tábor</div>
        <div class="col-sm-12 p0">
            <div class="left-col col-sm-12 col-md-4 p0 m0">
                <a href="<?=base_url("tabor/" . $data->id . "/" . $title);?>" target="_blank">
                    <div class="thumb" style="width: 100%; background-image: url('<?=base_url(str_replace("properties/images/", "properties/thumbs/", $data->imagePath));?>'); "></div>
                </a>
            </div>
            <div class="right-col col-sm-12 col-md-8">
                <div class="title"><a href="<?=base_url("tabor/" . $data->id . "/" . $title);?>" target="_blank"><?=$data->name;?></a></div>
                <div class="desc">
                    <?=character_limiter(strip_tags($data->description, "b,i"), 200);?>
                </div>

                <div class="col-sm-12 footer">
                    <div class="col-md-12 p0">
                        <div class="col-xs-8">
                            <i class="glyphicon glyphicon-asterisk" title="Tábor típusa"></i>
                            <?=$data->categoryTitle;?>
                        </div>
                        <div class="col-xs-4">
                            <i class="glyphicon glyphicon-home"></i>
                            <?=$data->typeTitle;?>
                        </div>
                        <div class="col-xs-12">
                            <i class="glyphicon glyphicon-map-marker" title="Helyszín"></i>
                            <?php if ($data->countryID == 1): ?>
                                <?=$data->locationTitle;?> (<?=$data->stateTitle;?>)
                            <?php else: ?>
                                <?=$data->countryTitle;?>
                                <?php endif;?>
                        </div>

                        <div class="col-xs-8">
                            <i class="glyphicon glyphicon-credit-card" title="SZÉP kártya elfogadás"></i>
                            MKB <i class="glyphicon glyphicon-<?=($data->szep_MKB) ? "ok text-success" : "remove text-danger";?>"></i>
                            OTP <i class="glyphicon glyphicon-<?=($data->szep_OTP) ? "ok text-success" : "remove text-danger";?>"></i>
                            K&H <i class="glyphicon glyphicon-<?=($data->szep_KH) ? "ok text-success" : "remove text-danger";?>"></i>
                        </div>

                        <div class="col-xs-4">
                            <i class="glyphicon glyphicon-user" title="Korosztály"></i>
                            <?php if (is_null($data->age_min)): ?>
                                -
                            <?php else: ?>
                                <?=$data->age_min . "-" . $data->age_max;?>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>