<?php
$title = url_title(convert_accented_characters($data->name));
?>
<div style="clear: both; margin: 5px; padding: 5px;">
    <img src="<?=base_url(str_replace("properties/images/", "properties/thumbs/", $data->imagePath));?>" style="clear: both; float:left; width: 180px; margin: 5px;">
    <a href="<?=base_url("tabor/" . $data->id . "/" . $title);?>" target="_blank">
    </a>
    <div style="font-weight: bold;"><a href="<?=base_url("tabor/" . $data->id . "/" . $title);?>" target="_blank"><?=$data->name;?></a></div>
    <div style="font-align: justify;"><?=character_limiter(strip_tags($data->description, "b,i"), 200);?></div>           
</div>