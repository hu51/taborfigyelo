<legend>Térképes táborhelykereső</legend>
<div id="map" style="width: 100%; height: 80vh;"></div>
<script>

    function initMap() {
        var bp = {lat: 47.481192, lng: 19.0600924};
        var map = new google.maps.Map(
            document.getElementById('map'), {zoom: 4, center: bp}
        );
        var latlngbounds = new google.maps.LatLngBounds();
        var infowindow = new google.maps.InfoWindow();      

        $.ajax({
            url: "<?=site_url("ajax/getCampMap");?>",
            type: "POST",
            dataType: "JSON",
            success: function(results) {
                for (var i = 0; i < results.data.length; i++) {                
                    var latLng = new google.maps.LatLng(results.data[i].lat,results.data[i].lng);
                    latlngbounds.extend(latLng);
                    var marker = new google.maps.Marker({
                        position: latLng,
                        title: "<h4>"+results.data[i].name+"</h4><img src='"+results.data[i].imagePath.replace("ties/images/", "ties/thumbs/")+"' class='img-thumbnail' style='float: left; margin: 5px;'><br>"+results.data[i].typeTitle+" - "+results.data[i].locationTitle+"<br><br>"+results.data[i].description+"<br><br><a target='_blank' class='btn btn-sm btn-primary' href='<?= site_url("tabor/"); ?>"+results.data[i].id+"/"+results.data[i].alias+"'>Tábor adatlap</a>",
                        map: map,
                    });

                    google.maps.event.addListener(marker, 'click', (function(marker, i) {
                        return function() {
                            console.log(marker);
                            infowindow.setContent(marker.title);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));                    
                }
                map.fitBounds(latlngbounds);        
            }
        });
    };

</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=<?= $this->tools->GetSetting("googlemap-key"); ?>&callback=initMap"></script>
