<?php

$all      = $this->db->from("tf_camp_types as t")->where("published", 1)->order_by("name", "ASC")->get()->result();
$typeList = array("-1" => "* összes");
foreach ($all as $tmp) {
    $typeList[$tmp->id] = $tmp->name;
}

$all     = $this->db->select("t.*")->from("tf_camp_category_list as t")->join("tf_camps as c", "t.id=c.categoryID AND c.published=1")->order_by("t.name", "ASC")->get()->result();
$catList = array("-1" => "* összes");
foreach ($all as $tmp) {
    $catList[$tmp->id] = $tmp->name;
}

$all         = $this->db->select("t.*")->from("tf_camp_countries as t")->join("tf_camps as c", "t.id=c.countryID AND c.published=1")->order_by("t.name", "ASC")->get()->result();
$countryList = array("-1" => "* összes");
foreach ($all as $tmp) {
    $countryList[$tmp->id] = $tmp->name;
}

$all       = $this->db->select("t.*")->from("tf_camp_states as t")->join("tf_camps as c", "t.id=c.stateID AND c.published=1")->order_by("t.name", "ASC")->get()->result();
$stateList = array("-1" => "* összes");
foreach ($all as $tmp) {
    $stateList[$tmp->id] = $tmp->name;
}
if (($this->input->post_get("country") !== null) && ($this->input->post_get("country") !== "-1")) {
    $all = $this->db->select("t.*")->from("tf_camp_states as t")->join("tf_camps as c", "t.id=c.stateID AND c.published=1")->where("t.countryID", (int) $this->input->post_get("country"))->order_by("t.name", "ASC")->get()->result();
} else {
    $all = $this->db->select("t.*")->from("tf_camp_states as t")->join("tf_camps as c", "t.id=c.stateID AND c.published=1")->order_by("t.name", "ASC")->get()->result();
}
$stateFilterList = array("-1" => "* összes");
foreach ($all as $tmp) {
    $stateFilterList[$tmp->id] = $tmp->name;
}

$all       = $this->db->select("t.*")->from("tf_camp_states as t")->join("tf_camps as c", "t.id=c.stateID AND c.published=1")->order_by("t.name", "ASC")->get()->result();
$locationList = array("-1" => "* összes");
foreach ($all as $tmp) {
    $locationList[$tmp->id] = $tmp->name;
}
if (($this->input->post_get("state") !== null) && ($this->input->post_get("state") !== "-1")) {
    $all = $this->db->select("t.*")->from("tf_camp_locations as t")->join("tf_camps as c", "t.id=c.locationID AND c.published=1")->where("t.stateID", (int) $this->input->post_get("state"))->order_by("t.name", "ASC")->get()->result();
} else {
    $all = $this->db->select("t.*")->from("tf_camp_locations as t")->join("tf_camps as c", "t.id=c.locationID AND c.published=1")->order_by("t.name", "ASC")->get()->result();
}
$locationFilterList = array("-1" => "* összes");
foreach ($all as $tmp) {
    $locationFilterList[$tmp->id] = $tmp->name;
}

if (!isset($title)) {
    $title = "Táborok listája";
}
if (!isset($filters)) {
    $filters = [];
}

?>
<div class="col-md-3 col-md-push-9 col-sm-12 p0">
    <div class="panel panel-default m2">
        <div class="panel-heading">
            <h3 class="panel-title">Táborok szűrése</h3>
        </div>
        <div class="panel-body">
            <form id="camps" method="get" class="m5" action="<?= site_url("taborok"); ?>">
                <div class="row">
                    <label>Típus szerint:</label>
                    <?=$this->tools->bs_select("type", $this->tools->set_value("type", "-1"), $typeList, "form-control");?>
                </div>

                <div class="row">
                    <label>Kategória szerint:</label>
                    <?=$this->tools->bs_select("category", $this->tools->set_value("category", "-1"), $catList, "form-control");?>
                </div>

                <div class="row">
                    <label>Ország szerint:</label>
                    <?=$this->tools->bs_select("country", $this->tools->set_value("country", "-1"), $countryList, "form-control chgSubmit");?>
                </div>

                <?php if(($this->input->post_get("country") && $this->input->post_get("country") !== "-1")): ?>
                    <div class="row">
                        <label>Megye/régió szerint:</label>
                        <?=$this->tools->bs_select("state", $this->tools->set_value("state", "-1"), $stateFilterList, "form-control chgSubmit");?>
                    </div>

                    <?php if(($this->input->post_get("state") && $this->input->post_get("state") !== "-1")): ?>
                        <div class="row">
                            <label>Település szerint:</label>
                            <?=$this->tools->bs_select("location", $this->tools->set_value("location", "-1"), $locationFilterList, "form-control chgSubmit");?>
                        </div>
                    <?php endif; ?>
                <?php else: ?>
                    <div class="row" style="font-size: 80%; text-align: justify;">Kérem, válasszon országot, majd megyét a többi szűrés megjelenéséhez!</div>
                <?php endif; ?>

                <div class="row">
                    <label>SZÉP-kártya-elfogadás:</label>
                    <div class="row">
                        <div class="col-md-4">
                            MKB
                        </div>
                        <div class="col-md-8">
                            <?=$this->tools->bs_checkbox("szep1", $this->tools->set_value("szep1", "-1"), array("-1" => "Mindegy", "1" => "Igen"));?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            OTP
                        </div>
                        <div class="col-md-8">
                            <?=$this->tools->bs_checkbox("szep2", $this->tools->set_value("szep2", "-1"), array("-1" => "Mindegy", "1" => "Igen"));?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            K&H
                        </div>
                        <div class="col-md-8">
                            <?=$this->tools->bs_checkbox("szep3", $this->tools->set_value("szep3", "-1"), array("-1" => "Mindegy", "1" => "Igen"));?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <label class="col-md-5 p5">Életkor:</label>
                    <div class="col-md-7">
                        <input type="number" name="age" min="1" max="99" class="form-control" value="<?=$this->tools->set_value("age", "");?>">
                    </div>
                </div>


                <div class="row">
                    <label>Ártartomány:</label>
                    <div class="input-group">
                        <input type="number" name="price_min" min="1" class="form-control" value="">
                        <span class="input-group-addon">-</span>
                        <input type="number" name="price_max" min="10" class="form-control" value="">
                    </div>
                </div>

                <div class="row">
                    <label>Dátumtartomány szűrése: </label>
                    <?=$this->tools->bs_checkbox("date", $this->tools->set_value("date", 0), array("0" => "Nincs", "1" => "Bekapcsolva"));?>

                    <div class="col-md-12">
                        <div class="col-md-3 p5">Mettől:</div>
                        <div class="col-md-9">
                            <input type="date" name="date_from" class="form-control" value="<?=$this->tools->set_value("date_from", date("Y") . "-01-01");?>">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-3 p5">Meddig:</div>
                        <div class="col-md-9">
                            <input type="date" name="date_to" class="form-control" value="<?=$this->tools->set_value("date_to", date("Y") . "-12-31");?>">
                        </div>
                    </div>
                    <p><small>A táborturnusok a megadott időszakon belül indulnak</small></p>
                </div>

                <div class="row">
                    <button type="submit" name="setfilter" value="1" class="col-md-12 btn btn-primary">Szűrés</button>
                    <small><input type="checkbox" name="showLink"> szűrés utáni URL mutatása</small>
                </div>

                <div class="row">
                    <br>
                    <a href="<?=site_url("taborok");?>">
                        <button type="button" name="setfilter" value="1" class="col-md-12 btn btn-sm btn-default">Szűrési feltételek törlése</button>
                    </a>
                </div>
            </form>


        </div>
    </div>

    <div class="banner_area side_banners">
        <?=$this->tools->showBanners(1, 4); ?>
    </div>
</div>

<div class="col-md-9 col-md-pull-3 col-sm-12">
    <legend><?= $title; ?></legend>
    <section>
        <div class="banner_area banners4x4">
            <?= $this->tools->showBanners(2, (int)$this->tools->GetSetting("camp-banner-small")); ?>
        </div>  
    <?php

        $count = (int)$this->tools->GetSetting("camp-banner-big");
        $this->db->from("tf_camp_banners");
        $this->db->where("enabled", "1");
        $this->db->where("(maxHits is NULL or maxHits =0 or maxHits >= hits)");
        $this->db->where("(fromDate is NULL or fromDate <= NOW())");
        $this->db->where("(endDate is NULL or endDate >= NOW())");

        $this->db->order_by("rand()");
        $this->db->limit($count);
        $banners = $this->db->get()->result();

        foreach ($banners as $data): 
            $this->db->where('id', $data->id);
            $this->db->set('hits', 'hits+1', FALSE);            
            $this->db->update("tf_camp_banners");

            if (is_null($data->campID)): ?>
                <a href="<?=$data->url;?>" target="_blank"><img src="<?=site_url("images/banners/news/" . $data->image);?>" style="width: 100%; height: auto;"></a>
            <?php else:
                $camp = $this->m_fcamp->getCampData((int) $data->campID);
            ?>
            <div class='row'>
                <?php

                    $box = $this->load->view("frontpage/campdata2", array("data" => $camp), true);
                    $box = str_replace("col-md-6", "col-md-12", $box);
                    $box = str_replace('campdata-box', 'campdata-box campdata-box2x', $box);
                    $box = str_replace('campdata-box', 'campdata-box campdata-box2x', $box);
                    echo str_replace("properties/thumbs/", "properties/images/", $box);
                ?>
            </div>
            <?php endif;?>
        <?php endforeach;?>
    </section>

    <section class="camplist">
        <?php

        if (!isset($ids)) {
            $filters = array();

            if (($this->input->post_get("type")) && ((int) $this->input->post_get("type") > 0)) {
                $filters["type"] = (int) $this->input->post_get("type");
            }

            if (($this->input->post_get("country")) && ((int) $this->input->post_get("country") > 0)) {
                $filters["country"] = (int) $this->input->post_get("country");
            }

            if (($this->input->post_get("state")) && ((int) $this->input->post_get("state") > 0)) {
                $filters["state"] = (int) $this->input->post_get("state");
            }

            if (($this->input->post_get("location")) && ((int) $this->input->post_get("location") > 0)) {
                $filters["location"] = (int) $this->input->post_get("location");
            }

            if (($this->input->post_get("category")) && ((int) $this->input->post_get("category") > 0)) {
                $filters["category"] = (int) $this->input->post_get("category");
            }

            if (($this->input->post_get("age")) && ((int) $this->input->post_get("age") > 0)) {
                $filters["age_min"] = (int) $this->input->post_get("age");
            }

            if (($this->input->post_get("age")) && ((int) $this->input->post_get("age") > 0)) {
                $filters["age_max"] = (int) $this->input->post_get("age");
            }

            if ($this->input->post_get("szep1") == "1") {
                $filters["szep1"] = 1;
            }
            if ($this->input->post_get("szep2") == "1") {
                $filters["szep2"] = 1;
            }
            if ($this->input->post_get("szep3") == "1") {
                $filters["szep3"] = 1;
            }

            if ($this->input->post_get("date")) {
                $filters["date_from"] = $this->input->post_get("date_from");
                $filters["date_to"]   = $this->input->post_get("date_to");
            }

            if (($this->input->post_get("price_min")) && ((int) $this->input->post_get("price_min") > 0)) {
                $filters["price_min"] = (int) $this->input->post_get("price_min");
            }

            if (($this->input->post_get("price_max")) && ((int) $this->input->post_get("price_max") > 0)) {
                $filters["price_max"] = (int) $this->input->post_get("price_max");
            }

            $ids = $this->m_fcamp->getCampIDS($filters,"c.featured DESC, turnYear DESC, rand()");
        }

        if (!$this->input->get("oldal")) {
            $pageID = 1;
        } else {
            $pageID = (int) $this->input->get("oldal");
        }

        $allRows     = count($ids);
        $pageMaxRows = (int) $this->tools->GetSetting("camps-per-page");

        $page         = $this->tools->pagination($this->uri->segment(1) . $slug . "/", $allRows, $pageMaxRows, $pageID);
        $pageID       = $page["current"];
        $pageStartRow = ($pageID - 1) * $pageMaxRows;
        $pageStartRow = max(0, min($pageStartRow - 1, $allRows - $pageMaxRows));

        $subIDs = array_slice($ids, $pageStartRow, $pageMaxRows);

        ?>
        <br style="clear: both;" />
        <p>Összesen <?=$allRows;?> tábor felel meg a keresési feltételeknek.</p>

        <?php if($this->input->post("showLink")): 
            $url = site_url("taborok?");
            foreach ($filters as $key => $val) {
                $url .= "&".$key."=".$val;
            }
            $url = str_replace("?&", "?", $url);
            ?>
            <div class="input-group">
                <div class="input-group-addon">URL:</div>
                <input type="text" class="form-control" value="<?=$url;?>">
            </div>
        <?php endif; ?>

        <div class='row'>
            <?php

        echo $page["html"];

        $odd = true;
        $camps = $this->m_fcamp->getCampsList($subIDs);
        if (is_array($camps)) {
            foreach ($camps as $data) {
                $this->load->view("frontpage/campdata", array("data" => $data));
                if (!$odd) {
                    echo "</div>\n";
                    echo "<div class='row'>\n";
                }
                $odd = !$odd;
            }
        }
        echo $page["html"];

        ?>
        </div>
    </section>
</div>
<script>

    $(".chgSubmit").change(function() {
        $("#camps").submit();
    });

</script>