            <div class="share">
                <?php $url = urlencode(current_url()); ?>
                <div class="col-md-3 ar small p10">
                    <b>Ossza meg ismerőseivel!</b>
                </div>
                <ul class="col-md-9">
                    <li class="m10"><a href="http://www.facebook.com/sharer.php?u=<?php echo $url; ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</a></li>
                    <li class="m10"><a href="http://twitter.com/share?url=<?php echo $url; ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i> Twitter</a></li>
                </div>      
            </div>
		</div><!-- content -->

		<footer>
			<ul>
				<li><a href="<?= base_url("#contact");?>">Kapcsolat</a></li>
				<li><a href="<?= base_url("impresszum");?>">Impresszum</a></li>
				<li><a href="<?= base_url("adatkezeles");?>">Adatkezelési tájékoztató</a></li>
				<li><a href="<?= base_url("hirlevel/feliratkozas");?>">Hírlevél-feliratkozás</a></li>
            </ul>
			<br style="clear: both;">
			<p class='small'>
			2019 &copy; Activa T. B. Gazdasági Tanácsadó Betéti Társaság | by Hu51
			</p>
		</footer>			
    </div><!-- body -->
    

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-154645723-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-154645723-1');
</script>

</body>
</html>
