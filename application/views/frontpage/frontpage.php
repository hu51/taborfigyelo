<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <title>Táborfigyelő</title>
  <link href="<?= site_url("img/favicon.ico"); ?>" rel="shortcut icon" type="image/x-icon" />
  <meta name="keywords" content="<?=$this->tools->GetSetting("meta-keywords");?>" />
  <meta name="description" content="<?=$this->tools->GetSetting("meta-description");?>" />

  <link rel="stylesheet" type="text/css" href="<?=site_url();?>css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="<?=site_url();?>css/style.css">
  <link rel="stylesheet" type="text/css" href="<?=site_url();?>css/frontpage-welcome.css">
  <link rel="stylesheet" type="text/css" href="<?=site_url();?>css/fontawesome.min.css">
  <link rel="stylesheet" type="text/css" href="<?=site_url();?>css/solid.min.css">

  <script type="text/javascript" src="<?=site_url();?>js/jquery.min.js"></script>
  <script type="text/javascript" src="<?=site_url();?>js/jquery.easing.min.js"></script>
  <script type="text/javascript" src="<?=site_url();?>js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?=site_url();?>js/bootstrap-notify.min.js"></script>
  <script type="text/javascript" src="<?=site_url();?>js/fontawesome.min.js"></script>
  <script type="text/javascript" src="<?=site_url();?>js/frontpage.js"></script>
</head>

<body id="page-top">
  <!-- Navigation -->
  <div class="navbar navbar-default navbar-inverse navbar-fixed-top" id="mainNav">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar" style="background-color: white;"></span>
        <span class="icon-bar" style="background-color: white;"></span>
        <span class="icon-bar" style="background-color: white;"></span>
      </button>
      <a class="navbar-brand" href="<?=base_url("/");?>">Táborfigyelő</a>
    </div>

    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="<?=base_url("hireink");?>">Híreink</a>
        </li>
      </ul>

      <ul class="nav navbar-nav navbar-right">
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#services">Szolgáltatásaink</a>
        </li>
        <li class="dropdown">
            <a href="<?= base_url("taborok"); ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Táborok keresése <span class="caret"></span></a>
            <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
                <li><a class="nav-link js-scroll-trigger" href="<?=site_url("taborok");?>">Táborok keresése</a></li>
                <li><a href="<?= site_url("terkepes-taborkereses"); ?>">Térképes táborkereső</a></li>
            </ul>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#portfolio">Gyakran keresett táborok</a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="rolunk">Rólunk</a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="<?= site_url("taborszervezoknek"); ?>">Táborszervezőknek </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url("/hireink/sajtosarok"); ?>">Sajtósarok</a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#contact">Kapcsolat</a>
        </li>
      </ul>
    </div>
  </div>
<?php

$this->db->select("t.id");
$this->db->from("tf_camp_turns as t");
$this->db->join("tf_camps as c", "c.id=t.campID");
$this->db->join("tf_camp_contracts as co", "co.id=c.contractID");
$this->db->where("t.validfrom >=", date("Y") . "-01-01");
$this->db->where("c.published", 1);
// $this->db->where("co.visible", 1);
$this->db->where("t.published", 1);

$db = $this->db->get()->num_rows();

?>
    <!-- Header -->
    <header class="masthead">
        <div class="container">
            <div class="intro-text">
                <div class="turn-box alert alert-info"><a href="<?=site_url("taborok");?>" style="font-weight: normal;">Már <?=$db;?> táborturnus!</a></div>
                <div class="intro-lead-in">Üdvözöljük oldalunkon!</div>
                <div class="intro-heading text-uppercase">Táborozzon velünk!</div>
                <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="#services">További részletek</a>
                <div class="m10">
                    <a class="btn btn-primary btn-xl text-uppercase m10" href="<?=site_url("taborok");?>">Táborok keresése</a>
                    <a class="btn btn-primary btn-xl text-uppercasem10" href="<?=site_url("hireink");?>">Friss táboros hírek</a>
                </div>
            </div>
        </div>
    </header>

  <!-- Services -->
<section class="page-section" id="services">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading text-uppercase">Szolgáltatásaink</h2>
                <!-- <h3 class="section-subheading text-muted">Vagy valami</h3> -->
            </div>
        </div>
        <div class="row text-center">
            <?php

for ($i = 1; $i < 4; $i++):
    $html = $this->tools->GetHtml("frontpage-services-" . $i, true);
    $icon = $this->tools->GetSetting("frontpage-services-" . $i . "-icon");
    $url  = $this->tools->GetSetting("frontpage-services-" . $i . "-url");
    ?>
	            <div class="col-md-4">
	                <span class="fa-stack fa-4x">
	                    <i class="fas fa-circle fa-stack-2x text-primary"></i>
	                    <?php if ($url): ?>
	                        <a href='<?=$url;?>'><i class="fas fa-<?=$icon;?> fa-stack-1x fa-inverse"></i></a>
	                    <?php else: ?>
                        <i class="fas fa-<?=$icon;?> fa-stack-1x fa-inverse"></i>
                    <?php endif;?>
                </span>
                <h4 class="service-heading"><?=$html->title;?></h4>
                <div class="text-muted body">
                    <?=$html->body;?>
                    <?php if ($url): ?>
                        <a href='<?=$url;?>' class='more'>Tovább &raquo;</a>
                    <?php endif;?>
                </div>
            </div>
            <?php endfor;?>
        </div>
    </div>
</section>

  <!-- Portfolio Grid -->
    <section class="bg-light page-section" id="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading text-uppercase">Gyakran keresett táborok</h2>
                    <!-- <h3 class="section-subheading text-muted">Vagy valami</h3> -->
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a class="portfolio-link" href="<?=site_url("taborok/budapesti-napkozis-taborok");?>">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                            Élménydús szünidő a biztonságos felügyelet mellett!
                            </div>
                        </div>
                        <img class="img-responsive" src="img/portfolio/01-thumbnail.jpg" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>Budapesti napközis táborok</h4>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a class="portfolio-link" href="<?=site_url("taborok/balatoni-taborok");?>">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                            Rengeteg pancsolás a magyar tengerben más szuper programok mellett
                            </div>
                        </div>
                        <img class="img-responsive" src="img/portfolio/02-thumbnail.jpg" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>Balatoni táborok</h4>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a class="portfolio-link" href="<?=site_url("taborok/taborok-kisebbeknek");?>">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                            Nyári táborok egészen piciktől a 12 éves korosztályig
                            </div>
                        </div>
                        <img class="img-responsive" src="img/portfolio/03-thumbnail.jpg" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>Táborok kisebbeknek</h4>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a class="portfolio-link" href="<?=site_url("taborok/taborok-nagyobbaknak");?>">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                            Nyári táborok 13 éves kor felett
                            </div>
                        </div>
                        <img class="img-responsive" src="img/portfolio/04-thumbnail.jpg" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>Táborok nagyobbaknak</h4>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a class="portfolio-link" href="<?=site_url("taborok/izgalmas-kalandtaborok");?>">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                            Kalandos vakáció kicsiknek és nagyoknak egyaránt
                            </div>
                        </div>
                        <img class="img-responsive" src="img/portfolio/05-thumbnail.jpg" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>Izgalmas kalandtáborok</h4>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a class="portfolio-link" href="<?=site_url("taborok/mozgalmas-es-vidam-sporttaborok");?>">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                            Örökmozgók nyári szünete rengeteg sportágban
                            </div>
                        </div>
                        <img class="img-responsive" src="img/portfolio/06-thumbnail.jpg" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>Mozgalmas és vidám sporttáborok</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Timeline -->
    <section class="page-section" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading text-uppercase">Rólunk</h2>
                    <!-- <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3> -->
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <ul class="timeline">
                    <?php

                        $about = $this->db->from("tf_htmls")->like("slug", "frontpage-about")->order_by("slug")->get();
                        $odd   = true;
                        foreach ($about->result() as $data):
                            $odd = (!$odd);
                            ?>
                            <li <?=($odd) ? " class='timeline-inverted'" : "";?>>
                                <div class="timeline-image">
                                    <img class="img-circle img-fluid" src="img/about/<?=substr($data->slug, -1);?>.jpg" alt="">
                                </div>
                                <div class="timeline-panel">
                                    <div class="timeline-heading">
                                        <h4><?=$data->title;?></h4>
                                    </div>
                                    <div class="timeline-body">
                                        <p class="text-muted"><?=$data->body;?></p>
                                    </div>
                                </div>
                            </li>
                        <?php endforeach;?>
                        <li class="timeline-inverted">
                            <div class="timeline-image lastbox">
                                <a href="<?=site_url("taborok");?>">
                                    <h4>Legyenek <br>Önök is <br>részesei &raquo;</h4>
                                    <div class="text">Legyenek Önök is részesei</div>
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>


    <!-- Contact -->
    <section class="page-section" id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h3 class="section-heading text-uppercase">Vegye fel a kapcsolatot velünk!</h3>
                    <h5 class="section-subheading text-muted">Bármi kérdése, észrevétele van, kérjük, forduljon hozzánk bizalommal!</h5>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-lg-12">
                    <form id="contactForm" name="sentMessage" novalidate="novalidate" style="color: white !important;">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input class="form-control required" id="name" name="name" type="text" placeholder="Az Ön neve *">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input class="form-control required" id="email" name="email" type="email" placeholder="Az Ön e-mail címe *">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input class="form-control required" id="phone" name="phone" type="text" placeholder="Telefonos elérhetősége *" >
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" name="campname" type="text" placeholder="Ha konkrét táborhoz lenne kérdése, a tábor neve" >
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea class="form-control required" id="message" name="message" placeholder="Üzenet *" required="required"></textarea>
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 text-center">
                                <div id="success"></div>
                                <button id="sendMessageButton" class="btn btn-primary btn-xl text-uppercase" type="button">Üzenet küldése</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!-- Footer -->
    <footer class="footer">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-4">
                    <span class="copyright">Copyright &copy; Táborfigyelő 2019</span>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline social-buttons">
                        <li class="list-inline-item">
                            <a href="https://www.facebook.com/taborfigyelo/" title="Facebook oldalunk" target="_blank">FB</a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://taborminosito.hu/" title="Táborminősítő" target="_blank">TM</a>
                        </li>
                        <li class="list-inline-item">
                            <a href="<?= base_url("hirlevel/feliratkozas");?>" title="Hírlevél feliratkozás"><i class="fas fa-envelope-open-text"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline quicklinks">
                        <li class="list-inline-item">
                            <a href="<?= base_url("impresszum");?>">Impresszum</a>
                        </li>
                        <li class="list-inline-item">
                            <a href="<?= base_url("adatkezeles");?>">Adatkezelési tájékoztató</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer -->

<script>

    $("#sendMessageButton").click( function() {
        $.ajax({
            type: "post",
            url: "ajax/sendMessage",
            data: $("#contactForm").serialize(),
            dataType: "json",
            success: function (response) {
                if (response && response.status !== "ok") {
                    $.notify(
                        response.message,
                        {
                            type: "warning",
                            placement: {
                                from: "bottom",
                                align: "center"
                            },
                            offset: 100,
                            delay: 1000
                        }
                    );
                }

                if (response && response.status == "ok") {
                    $.notify(
                        "Köszönjük levelét, hamarosan válaszolunk rá!",
                        {
                            type: "success",
                            placement: {
                                from: "bottom",
                                align: "center"
                            },
                            offset: 300,
                            delay: 2000
                        }
                    );
                    $('#contactForm').trigger("reset");

                }
            }
        });
    });

</script>

</body>
</html>