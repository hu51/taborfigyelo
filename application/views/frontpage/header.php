<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta charset="utf-8">
    <title>Táborfigyelő</title>
    <link href="<?= site_url("img/favicon.ico"); ?>" rel="shortcut icon" type="image/x-icon" />
	<meta name="keywords" content="<?= $this->tools->GetSetting("meta-keywords"); ?>" />
    <meta name="description" content="<?= $this->tools->GetSetting("meta-description"); ?>" />
    <base href="<?= site_url(); ?>">
    

    <?php if ($opengraph): ?>
        <meta property="og:description" content="<?= $this->tools->GetSetting("meta-description"); ?>" />
        <?php foreach($opengraph as $key => $val): ?>
            <meta property="og:<?= $key; ?>" content="<?= $val; ?>" />
        <?php endforeach; ?>
    <?php endif; ?>

	<link rel="stylesheet" type="text/css" href="<?= site_url(); ?>css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?= site_url(); ?>css/ekko-lightbox.css">
	<link rel="stylesheet" type="text/css" href="<?= site_url(); ?>css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?= site_url(); ?>css/style.css">
	<link rel="stylesheet" type="text/css" href="<?= site_url(); ?>css/frontpage.css?v=1">

	<script type="text/javascript" src="<?= site_url(); ?>js/jquery.min.js"></script>
	<script type="text/javascript" src="<?= site_url(); ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= site_url(); ?>js/ekko-lightbox.min.js"></script>
	<script type="text/javascript" src="<?= site_url(); ?>js/bootstrap-notify.min.js"></script>
	<script type="text/javascript" src="<?= site_url(); ?>js/main.js"></script>

</head>

<body>
	<div id="body">
		<div class="navbar navbar-inverse">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?= base_url("/"); ?>">Táborfigyelő</a>
			</div>

			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li class="nav-item">
						<a class="nav-link" href="<?= base_url("hireink"); ?>">Híreink</a>
					</li>
				</ul>

                <?php if($this->session->userdata("user_contractID")): ?>
                    <ul class="nav navbar-nav bg-primary">
                        <li><a href="<?= site_url("taborszerzodes"); ?>">Szerződés adatok</a></li>
                        <li><a href="<?= site_url("taborhelyek"); ?>">Táborhelyszínek</a></li>
                        <li><a href="<?= site_url("kilepes"); ?>" class="alert-danger font-warning">Kilépés</a></li>
                    </ul>
                <?php endif; ?>

				<ul class="nav navbar-nav navbar-right">
					<li class="nav-item">
                        <ul class="nav navbar-nav m0">
                            <li class="dropdown">
                                <a href="<?= base_url("taborok"); ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Szolgáltatásaink <span class="caret"></span></a>
                                <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
                                    <li><a href="<?= site_url("taborszervezoknek"); ?>">Táborszervezőknek </a></li>
                                    <li><a href="<?= base_url("hirlevel/feliratkozas");?>">Hírlevél feliratkozás</a></li>
                                </ul>
                            </li>
                        </ul> 
					</li>

					<li class="nav-item">
						<!-- <a class="nav-link" href="<?= base_url("taborok"); ?>">Táborok</a> -->
                        <ul class="nav navbar-nav m0">
                            <li class="dropdown">
                                <a href="<?= base_url("taborok"); ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Táborok <span class="caret"></span></a>
                                <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
                                    <li><a href="<?= site_url("taborok"); ?>">Táborok listája</a></li>
                                    <li><a href="<?= site_url("terkepes-taborkereses"); ?>">Térképes táborkereső</a></li>
                                </ul>
                            </li>
                        </ul>                        
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?= base_url("rolunk"); ?>">Rólunk</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?= base_url("/hireink/sajtosarok"); ?>">Sajtósarok</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?= base_url("#contact"); ?>">Kapcsolat</a>
					</li>
				</ul>
			</div>
		</div>
		<div id="content">