<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Belépés</h3>
            </div>
            <div class="panel-body">
                <form method="post" action="<?= site_url("belepes"); ?>" class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="pwd" class="col-sm-3 control-label">Kódszó:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="hash" placeholder="Kód megadása" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pwd" class="col-sm-2 control-label"></label>
                        <div class="col-sm-10">
                            <button type="submit" name="login" value="1" class="btn btn-primary">Belépés</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>