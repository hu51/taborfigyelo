<legend>Táborhely szerződés</legend>
<?php

$html = $this->tools->GetHtml("camp-create-calc");

$contractID = (int) $this->session->userdata("user_contractID");
$contract   = $this->db->from("tf_camp_contracts")->where("id", (int) $contractID)->get()->row();

?>
<form id="form" method="post" action="" class="form-horizontal">
    <input type="hidden" name="contractID" value="<?=$contract->id;?>" />
    <input type="hidden" name="hash" value="<?=$contract->hash;?>" />
    <?=$html;?>
	<br>
	<br>
	<div class="col-lg-12">
		<label for="contactName" class="col-lg-4">Kapcsolattartó neve:</label>
		<div class="col-lg-8">
			<input type="text" class="form-control" name="contactName" value="<?=$contract->contactName;?>" >
		</div>
	</div>

	<div class="col-lg-12">
		<label for="contactEmail" class="col-lg-4">~ e-mail címe:</label>
		<div class="col-lg-8">
			<input type="text" class="form-control" name="contactEmail" value="<?=$contract->contactEmail;?>" >
		</div>
    </div>

	<div class="col-lg-12">
		<label for="contactPhone" class="col-lg-4">~ telefonszáma:</label>
		<div class="col-lg-8">
			<input type="text" class="form-control" name="contactPhone" value="<?=$contract->contactPhone;?>" >
		</div>
    </div>

	<div class="col-lg-12">
		<label for="representName" class="col-lg-4">Képviselő neve:</label>
		<div class="col-lg-8">
			<input type="text" class="form-control" name="representName" value="<?=$contract->representName;?>" >
		</div>
    </div>

    <label>Szerződő partner</label>
	<div class="col-lg-12">
		<label for="partnerName" class="col-lg-4">~ neve (cégnév):</label>
		<div class="col-lg-8">
			<input type="text" class="form-control" name="partnerName" value="<?=$contract->partnerName;?>" >
		</div>
    </div>

	<div class="col-lg-12">
		<label for="zip" class="col-lg-4">~ irányítószám, település:</label>
		<div class="col-lg-3">
			<input type="text" class="form-control" name="zip" placeholder="Irsz." value="<?=$contract->zip;?>" >
		</div>
		<div class="col-lg-5">
			<input type="text" class="form-control" name="city" placeholder="Település" value="<?=$contract->city;?>" >
		</div>
    </div>

	<div class="col-lg-12">
		<label for="address" class="col-lg-4">~ utca, házszám:</label>
		<div class="col-lg-8">
			<input type="text" class="form-control" name="address" value="<?=$contract->address;?>" >
		</div>
    </div>

	<div class="col-lg-12">
		<label for="taxID" class="col-lg-4">Adószám:</label>
		<div class="col-lg-4">
			<input type="text" class="form-control checkMatch" id="taxID" name="taxID" value="<?=$contract->taxID;?>">
		</div>
    </div>

	<div class="col-lg-12">
		<label for="compID" class="col-lg-4">Cégjegyzékszám:</label>
		<div class="col-lg-4">
			<input type="text" class="form-control checkMatch" id="compID" name="compID" value="<?=$contract->compID;?>" >
		</div>
    </div>

    <table class="table" id="datatable">
        <thead>
            <tr>
                <th>Új / régi</th>
                <th class="w150">&nbsp;</th>
                <th class="w100">Tábortémák:</th>
                <th class="w100">&nbsp;</th>
                <th class="w100">Összesen:</th>
                <th class="w50">&nbsp;</th>
            </tr>
        </thead>
        <tbody>

        </tbody>

        <tfoot>
            <tr>
                <td colspan="3">
                A táborhelyszínek neve később megváltoztatható.
                <span id='matchText'></span>
                </td>
                <td colspan="2">
                    <button type="button" class="btn btn-success" id="addLocation" value="1">Új táborhelyszín hozzáadása</button>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="ar">Összesen:</td>
                <td>
                    <input type="text" id="total" class="form-control ac" disabled value="">
                </td>
            </tr>
        </tfoot>
    </table>


    <div class="col-lg-12 ac">
        <button type="button" class="btn btn-info" id="viewContract" value="go">Szerződési feltételek megtekintése</button><br>
        <button type="submit" class="btn btn-primary" name="startReg" value="go">Elfogadom a szerződéses feltételeket!</button><br>
    </div>
    <br style="clear: both; " />
    <br>
</form>

<script>

    var locPrice = <?=$this->tools->GetSetting("camp-baseprice");?>;
    var themePrice = <?=$this->tools->GetSetting("camp-themeprice");?>;

    function SetStatus(txt) {
        $.notify(
            txt,
            {
                type: "info",
                placement: {
                    from: "top",
                    align: "center"
                },
                offset: 20
            }
        );
    }

    $("#viewContract").click( function() {
        var myWindow = window.open("<?=site_url("taborszerzodes/elonezet");?>?"+$("#form").serialize(), "", "width=600,height=400");
    });

    var locID = 1;

    $("#addLocation").click( function() {
        addLocation();
    });

    $(document).on("change", ".recalc", function() {
        calc();
    });

    $(document).on("click", ".deleteRow", function() {
        $(this).closest("tr").remove();
        calc();
    });

    var options = '';

    function addLocation()
    {
        $("#datatable").find('tbody').append( "<tr>"
            + "<td><select name='loc_type["+ locID + "]' class='form-control typeSelect' id='type_'"+locID+"' data-id='" + locID +"'><option value='new'># Új táborhely " + locID +"</option>"+options+"</select></td>"
            + "<td><input type='text' class='form-control ac' disabled id='loc_price_"+ locID + "' value=''></td>"
            + "<td><select class='form-control recalc ac' name='loc_themes["+ locID + "]' id='loc_themes_"+ locID + "'><?php for ($i = 1; $i <= 9; $i++): ?><option><?=$i;?></option><?php endfor;?></select></td>"
            + "<td><input type='text' class='form-control ac' disabled id='themeTotal_"+ locID + "' value=''></td>"
            + "<td><input type='text' class='form-control ac' disabled id='rowTotal_"+ locID + "' value=''></td>"
            + "<td><button type='button' class='btn btn-danger deleteRow' id='deleteRow_"+ locID + "'><i class='glyphicon glyphicon-trash'></i></button></td>"
            + "</tr>" );

        locID++;
        calc();
    }

    $(document).ready( function() {
        addLocation();
    });


    $(".checkMatch").keyup( function() {
       $.ajax({
           type: "post",
           url: "ajax/checkMatch",
           data: { taxID: $("#taxID").val(), compID: $("#compID").val() },
           dataType: "json",
           success: function (response) {
                if (response.status == "OK") {
                    $("#matchText").html("<div class='alert alert-info'>Korábbi táborokat találtam, kérjük válassza ki az 'Új / régi' résznél ha ezeket szeretné módosítani.</div>")
                    $(".typeSelect option").each(function() {
                        $(this).remove();
                    });

                    $(document).find(".typeSelect").each(function() {
                        id = $(this).data("id");
                        $(this).append('<option value="new" selected="selected"># Új táborhely '+id+'</option>');
                    });                    
                    
                    options = '';
                    for (let index = 0; index < response.data.length; index++) {
                        const element = response.data[index];
                        $('.typeSelect').append('<option value="'+element.id+'">Régi: '+element.name+'</option>');
                        options = options + '<option value="'+element.id+'">Régi: '+element.name+'</option>';
                    }
                }
           }
       });
    });


    function calc()
    {
        total = 0;
        $(".typeSelect").each( function() {
            id = $(this).data("id");
            themes = parseInt($("#loc_themes_" + id ).val());

            $("#loc_price_"+id).val(locPrice + " Ft");
            rowtotal = locPrice;

            themesPrice = parseInt((themes-1) * themePrice);
            rowtotal = eval(rowtotal + themesPrice);
            total = eval(total + rowtotal);

            $("#themeTotal_"+id).val(themesPrice + " Ft");
            $("#rowTotal_"+id).val(rowtotal + " Ft");
        });

        $("#total").val(total + " Ft");
    };

    calc();


    $('form').submit(function () {
        empty = 0;
        $(document).find(".form-control").each(function() {
            if ($(this).val() == '') {
                empty++
            }
        });
        // Check if empty of not
        if (empty > 0) {
            SetStatus('Kérem ellenőrizze az adatokat és töltsön ki minden mezőt!');
            return false;
        }

        loc = 0;
        $(document).find(".typeSelect").each(function() {
            loc++
        });

        if (loc < 1) {
            SetStatus('Legalább egy táborhelyszínnek kell lennie!');
            return false;
        }
    });


</script>