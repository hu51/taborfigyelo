<legend>Tisztelt Táborszervezők!</legend>

<?= $this->tools->GetHtml("new-camp-welcome"); ?>

<div class="col-md-8 col-md-offset-2">
<br>
<form method="post" class="form-horizontal">
    <div class="row">
        <label>Név:</label>
        <input type="text" name="reg_name" class="form-control" value="">
    </div>

    <div class="row">
        <label>E-mail:</label>
        <input type="email" name="reg_email" class="form-control" value="">
    </div>

    <div class="row">
        <label>Telefon:</label>
        <input type="text" name="reg_phone" class="form-control" value="">
    </div>
    
    <div class="row">
        <br>
        <button type="submit" name="sendReg" value="1" class="col-md-12 btn btn-success">Adatok elküldése</button>
    </div>
</form>
</div>
<p>&nbsp;</p>