<?php

if (!$newsID) {
    if ($this->uri->segment(1) == "leirasok") {
        $newsID = (int) $this->uri->segment(2);
        $news   = $this->db->from('tf_documents')->where('originalID', $newsID)->where("currentVersion", 1)->get()->row();
    } else {
        $slug = filter_var($this->uri->segment(2), FILTER_SANITIZE_STRING);
        $cat  = $this->db->from('tf_doc_categories')->where("id <>", 2)->where('slug', $slug)->get()->row();
        if ($cat) {
            if ($this->uri->segment(3)) {
                $newsID = (int) $this->uri->segment(3);
                $news   = $this->db->from('tf_documents')->where('originalID', $newsID)->where("currentVersion", 1)->get()->row();
            }
        }
    }
} else {
    $news = $this->db->from('tf_documents')->where('originalID', $newsID)->where("currentVersion", 1)->get()->row();
}

$showCategory = $this->tools->GetSetting("news-show-category");
$showDatetime = $this->tools->GetSetting("news-show-create");

if ($news) {
    $this->db->select("d.*, dc.title as categoryTitle, dc.slug as categorySlug");
    $this->db->from("tf_documents as d");
    $this->db->join("tf_doc_categories as dc", "dc.id = d.categoryID");
    $this->db->where("visible", 1);
    $this->db->where('originalID', $newsID);
    $this->db->where("currentVersion", 1);
    $news = $this->db->get()->row();

    ?>
    <article>
        <h3 class="title"><?=$news->title;?></h3>
        <?php
            $pic = file_exists(FCPATH . "images/news/pic_" . $newsID . ".jpg");
            if ($pic) {
                echo "<a href='" . site_url("images/news/pic_" . $newsID . ".jpg") . "' target='_blank'><img src='" . site_url("images/news/pic_" . $newsID . ".jpg") . "' style='width: auto; max-height: 50vh; margin: 5px auto;' ></a>";
            }
        ?>
        <br>
        <div class="info">
            <?php if ($this->session->userdata("user_isAdmin")): ?>
                <a href="<?=site_url("figyelo/news/edit/" . $news->id);?>" target="_blank" class="btn btn-sm btn-primary fr">Cikk szerkesztése</a>
            <?php endif;?>

            <p class='small'>
                <?php if ($showCategory): ?>
                    Kategória: <a href="<?=base_url("hireink/" . $news->categorySlug . "/");?>"><?=$news->categoryTitle;?> &raquo;</a>
                <?php endif;?>

                <?php if ($showDatetime): ?>
                    <?php if ($showCategory): ?>&nbsp;|&nbsp;<?php endif;?>
                    Létrehozva: <?=substr($news->createDatetime, 0, 16);?></a>
                <?php endif;?>
            </p>
        </div>
        <div class="body">
            <?php
            if (empty($news->body)) {
                echo $news->excerpt;
            } else {
                $body = $this->tools->buildInlineForm($news->body);

                //echo preg_replace("#([src|href]=['\"])/#mi", "$1" . site_url(), $body);
                echo $body;
            }
            ?>
        </div>
    </article>
    <button type="button" class="btn btn-sm btn-default" style="clear: both; display: block;" onclick="history.back()">&laquo; Vissza</button>

<script>
/*
    $(document).ready( function(){
        $(document).find("article a img").each(function() {
            str = $(this).parent().attr("href");
            n = str.search(/\.(jpg|png)/i);

            if (n > 0) {
                $(this).parent().attr("data-toggle", "lightbox");
            }
        })
    });
*/
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox({
            showArrows: true,
            alwaysShowClose: true
        });

//        $(".ekko-lightbox .modal-header").html("<a href='"+$(this).attr("href")+"' target='_blank'><b>Kép megnyitása nagy méretben</b></a>");
    });
</script>

    <?php
} else {
    if (!$cat) {
        echo "<h4>Legfrissebb híreink:</h4>\n";
    } else {
        echo "<h4>Kategória: " . $cat->title . "</h4>\n";
    }

    if ($cat->slug == "sajtosarok") {
        $html = $this->tools->GetHtml("sajtosarok-info", true);
        ?>
        <br style='clear: both;' />
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                        <h3 class="panel-title"><?= $html->title; ?></h3>
                </div>
                <div class="panel-body">
                        <?= $html->body; ?>
                </div>
            </div>
        </div>
        <br style='clear: both;' />
        <?php
    }

    $this->db->select('id');
    $this->db->from("tf_documents");

    if (isset($cat)) {
        $this->db->where("(categoryID =" . $cat->id . " OR categoryID=0)");
    } else {
        $this->db->where("categoryID <>", 2);
    }
    $this->db->where("visible", 1);
    $this->db->where("currentVersion", 1);
    $allRows = $this->db->get()->num_rows();

    if (!$this->input->get("oldal")) {
        $pageID = 1;
    } else {
        $pageID = (int) $this->input->get("oldal");
    }

    $pageMaxRows = max(5, (int) $this->tools->GetSetting("news-perpage"));

    $page         = str_replace("hireink//", "hireink/", $this->tools->pagination("hireink/" . $slug . "/", $allRows, $pageMaxRows, $pageID));
    $pageID       = $page["current"];
    $pageStartRow = ($pageID - 1) * $pageMaxRows;

    $this->db->select("d.*, dc.title as categoryTitle, dc.slug as categorySlug");
    $this->db->from("tf_documents as d");
    $this->db->join("tf_doc_categories as dc", "dc.id = d.categoryID");
    if (isset($cat)) {
        $this->db->where("(categoryID=" . $cat->id . " OR categoryID=0)");
    } else {
        $this->db->where("categoryID <>", 2);
    }
    $this->db->where("visible", 1);
    $this->db->where("currentVersion", 1);
    $this->db->order_by("createDatetime", "DESC");
    $this->db->limit($pageMaxRows, $pageStartRow);
    $query = $this->db->get();

    echo $page["html"];

    foreach ($query->result() as $news) {
        $pic   = file_exists(FCPATH . "images/news/pic_" . $news->id . ".jpg");
        $title = url_title(convert_accented_characters($news->title));
        $url   = site_url("hireink/" . $news->categorySlug . "/" . $news->originalID . "/" . $title);
        ?>
        <article class='news-list'>
            <?php if ($pic): ?>
                <a href='<?=$url;?>'><img src='<?=site_url("images/news/pic_" . $news->id . "_sm.jpg");?>' class='photo'></a>
            <?php endif;?>
            <h4 class="title"><a href="<?=$url;?>"><?=$news->title;?></a></h4>
            <div class="category">
                <?php if ($showCategory): ?>
                    Kategória: <a href="<?=base_url("hireink/" . $news->categorySlug . "/");?>"><?=$news->categoryTitle;?> &raquo;</a>
                <?php endif;?>

                <?php if ($showDatetime): ?>
                    <?php if ($showCategory): ?>&nbsp;|&nbsp;<?php endif;?>
                    Létrehozva: <?=substr($news->createDatetime, 0, 16);?></a>
                <?php endif;?>
            </div>
            <div class="body">
                <?=(empty($news->excerpt)) ? character_limiter(strip_tags($news->body), 200) : strip_tags($news->excerpt, "br,p,i,b");?>
            </div>
            <div class="more"><a href="<?=$url;?>" class="more">tovább &raquo;</a></div>
        </article>

    <?php }
    echo $page["html"];
}
