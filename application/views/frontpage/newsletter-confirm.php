<div class="col-md-8 col-md-offset-2 col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">
                <h3 class="panel-title">Hírlevél feliratkozás megerősítés</h3>
        </div>
        <div class="panel-body">
            <?php

            $userID  = preg_replace("/[^\w\.]/i", "", $this->input->get("u"));
            $user = $this->db->from("tf_newsletter_users")->where("MD5(CONCAT(id,uniqid))", $userID)->get()->row();
            if ($user):
                if (!$user->confirmed) {
                    $this->db->update("tf_newsletter_users", array("confirmed" => 1), array("id" => $user->id));              
                    echo "<div class='alert alert-success'>Köszönjük, Ön sikeresen megerősítette hílrevél feliratkozását.</div>\n";
                } else {              
                    echo "<div class='alert alert-info'>Ez a felhasználó már megerősítette a fiókját.</div>\n";
                }
            else: ?>
                <div class='alert alert-warning'>Hibás adatok!</div>
            <?php endif; ?>
            <div class="text-center">

            </div>
        </div>
    </div>
</div>