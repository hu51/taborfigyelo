<div class="col-md-8 col-md-offset-2 col-sm-12">
    <div class="panel panel-success">
        <div class="panel-heading">
                <h3 class="panel-title">Hírlevél-feliratkozás</h3>
        </div>
        <div class="panel-body">
            <?= $this->tools->GetHtml("newsletter-subscribe"); ?>
            <?php

            $this->load->helper("form");

            if ($this->input->post("sendSubs") == "on"):
                if (filter_input(INPUT_POST, "email", FILTER_VALIDATE_EMAIL)) {
                    $email = filter_input(INPUT_POST, "email", FILTER_SANITIZE_EMAIL);
                    $name = filter_input(INPUT_POST, "name", FILTER_SANITIZE_STRIPPED);

                    $user = $this->db->from("tf_newsletter_users")->where("email", $email)->get()->row();
                    $needConfirm = false;
                    if (!$user) {
                        $this->db->insert("tf_newsletter_users", array("email" => $email, "name" => $name, "lastIP" => $this->tools->getIP(), "source" => "user", "created" => date("Y-m-d H:i:s"), "modified" => date("Y-m-d H:i:s"), "uniqid" => uniqid()));
                        $userID = $this->db->insert_id();
                        $this->output->append_output( "<div class='alert alert-success'>A megadott e-mail címet berögzítettük az adatbázisba: `".$$email."`.</div>\n");
                        $needConfirm = true;
                    } else {
                        $userID = $user->id;
                        $this->output->append_output("<div class='alert alert-info'>Ez az e-mail cím már szerepel az adatbázisban: `".$email."`.</div>\n");
                        
                        if (!$user->confirmed) {
                            $needConfirm = true;
                        }
                    }

                    if ($needConfirm) {
                        $this->load->model("m_letters");
                        $this->m_letters->sendConfirmLetter($data->id);                             
                    }

                    $lists = $this->input->post("lists");
/*
                    foreach ($lists as $tmp => $id) {
                        $this->db->where("userID", $userID);
                        $this->db->where("listID", $group->id);
                        $this->db->update("tf_newsletter_lists_users", array("unsubscribed" => date("Y-m-d H:i:s")));              
                    }
*/
                } else {
                    echo "<div class='alert alert-warning'>Hibás a megadott e-mail cím.</div>\n";
                }
            endif; ?>
            <form method="post">
                <div class="form-group">
                    <label >E-mail: </label>
                    <input type="text" name="email" class="form-control" placeholder="E-mail cím" value="<?= set_value("email"); ?>">
                </div>

                <div class="form-group">
                    <label >Név: </label>
                    <input type="text" name="name" class="form-control" placeholder="Név" value="<?= set_value("name"); ?>">
                </div>  

                <div class="form-group">
                    <label >Hírlevél listák: </label>
                <?php
                
                    $result = $this->db->from("tf_newsletter_lists")->where("public", 1)->order_by("name")->get();
                    foreach ($result->result() as $data): ?>
                    <div class="p5">                            
                        <?= form_checkbox("lists[]", $data->id). " ".$data->name; ?>
                    </div>
                    <?php endforeach; ?>
                </div>  

                <div class="form-group bg-warning p10">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input name="sendSubs" class="form-check-input" type="checkbox"> Kérjük, erősítse meg feliratkozási szándékát!
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-check">
                        <label class="form-check-label">
                            <button type="submit" class="btn btn-primary btn-block">Feliratkozás</button>
                        </label>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>