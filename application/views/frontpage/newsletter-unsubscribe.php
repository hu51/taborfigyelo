<div class="col-md-8 col-md-offset-2 col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Hírlevél leiratkozás</h3>
        </div>
        <div class="panel-body">
            <?php

            $userID  = preg_replace("/[^\w\.]/i", "", $this->input->get("user"));
            $groupID = preg_replace("/[^\w\.]/i", "", $this->input->get("group"));

            $user = $this->db->query("SELECT * FROM tf_newsletter_users WHERE (MD5(CONCAT(id,uniqid))='".$userID."') OR (MD5(id)='".$userID."' AND `uniqid` is NULL)")->row();      
            if (!empty($groupID)) {
                $group = $this->db->query("SELECT * FROM tf_newsletter_lists WHERE (MD5(CONCAT(id,uniqid))='".$groupID."') OR (MD5(id)='".$groupID."' AND `uniqid` is NULL)")->row();      
            } else {
                $group = NULL;
            }

            if ($this->input->post("sendUnsubs") == "on"):
                if (!empty($groupID)) {
                    $this->db->where("userID", $user->id);
                    $this->db->where("listID", $group->id);
                    $this->db->update("tf_newsletter_lists_users", array("unsubscribed" => date("Y-m-d H:i:s")));              
                    
                    echo "<div class='alert alert-success'>Sikeresen leiratkoztál a(z) `".$group->name."` csoport leveleíről.</div>\n";
                } else {
                    $this->db->where("userID", $user->id);
                    $this->db->update("tf_newsletter_lists_users", array("unsubscribed" => date("Y-m-d H:i:s")));
                    
                    $this->db->where("id", $user->id);
                    $this->db->update("tf_newsletter_users", array("unsubscribed" => date("Y-m-d H:i:s")));                 
                    echo "<div class='alert alert-success'>Sikeresen leiratkoztál az oldal leveleíről.</div>\n";
                }
            else:

                ?>
                <?php if (is_null($user)): ?>
                    <div class='alert alert-danger'>Hibás adatokat adtál meg.</div>
                <?php else: ?>
                    <form method="post">
                        <div class="form-group">
                            <label >E-mail: </label>
                            <span class="text-primary"><?= $user->email; ?></span>
                        </div>

                        <?php if (!is_null($group)): ?>
                            <div class="form-group">
                                <label >Csoport: </label>
                                <span class="text-primary"><?= $group->name; ?></span>
                            </div>
                        <?php endif; ?>

                        <div class="form-group">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input name="sendUnsubs" class="form-check-input" type="checkbox"> Kérjük erősítse meg leiratkozási szándékát
                                </label>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary btn-block">Leiratkozás</button>

                    </form>
                <?php endif; ?>
            <?php endif; ?>
            <div class="text-center">

            </div>
        </div>
    </div>
</div>