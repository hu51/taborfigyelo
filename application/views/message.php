<?php

    if (! isset($type)) {
        $type = "info";
    }
?>
    <div class="alert alert-<?php echo $type; ?>" role="alert">
        <?php if(! empty ($title)): ?>
            <h4><?php echo $title; ?></h4>
        <?php endif; ?>
        <p><?php echo $text; ?></p>
    </div>